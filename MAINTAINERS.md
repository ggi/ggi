
# Maintainers

- Boris Baldassari (Eclipse Foundation)
- Catherine Nuel (OW2)
- Florent Zara (Eclipse Foundation)
- Fréderic Aatz (Microsoft France)
- Nicolas Toussaint (Orange Business)
- Sébastien Lejeune (Thales)
- Silvério Santos (Orange Business)

