# A Roadmap for 2023/2024

## The initiative

Goal: Managing the entire Good Governance Initiative \
Timeline: Continuous

* Operations:
  - Weekly sync calls
  - Create Taskforces / WGs to work on specific topics (see below)
  - Rebrand Monday meetings as OSPO Alliance instead of GGI (so as to include Handbook, Comm, etc.). 
* Onboarding of new members.
* Partner Hub


## Communication and events

Goal: Disseminate. Make the initiative known and adopted. \
Timeline: S1 (first semester)  \
Taskforce: Communication team

* Foster ecosystem
  - Connect with similar initiatives (OSPO++, cf. Christian's email)
  - Partnership program (Silona) 
    - What could a partnership look like? 
    - Recruiting more contributors. 
    - Define & decide.
* Foster community
  - Promote national/linguistic groups
  - Use and promote the forum
  - Social Networks
  - News (e.g. on the website)
* Maintain a calendar of (GGI+OSPO OnRamp+?) events from the website's front page
* Maintain a public Timeline of (proposed) events 
  * Lighting talks (FOSDEM?, FOSSBackstage?, etc)
  * Full Presentations
* Find relevant use cases and how to communicate about it.


## Handbook

Goal: Improve our main outcome: the handbook. \
Timeline: S2 (second semester) \
Taskforce: Handbook team

* Update Weblate to v1.2: 
  * Require v1.2 to be committed to the "translations" branch
  * Notify translators to update and review
* Maintain / Update
  - Check/Update references
  - Integrate real life feedback
* Translations:
  - Update translations to v1.2.
  - Foster translations (Silvério): more languages! 

* Metrics on activities (Boris, Silvério, Daniel Izquierdo)
  * Work with Daniel from Bitergia (still interested SL)
  * Link to CHAOSS WG

* Variants (Florent, Sébastien, Silvério) 
  * InnerSource: update individual activities (interested SL)
  * Academics (Ask for AM Scott participation), public administrations? 
  * Would include an early chapter + possible changes to activities (to be decided by the group in charge of the variants).


## Tooling

Goal: Support all other initiatives and tasks. \
Timeline: S2 (second semester) \

### My GGI Board

Taskforce: GGI Deploy team

> Should that be put under handbook instead of tooling?

* Contents updated with activities from GGI v1.2. [DONE]
* Translate content in deployment.
* Discuss new features (how to upgrade for future versions, translations, GitHub deployement?)
* Promote its use, begginers and practitioners workshops? (online and remote?)

### Infrastructure

Taskforce: Infra team

* Website
  * Publish GGI Handbook in web form in various available languages
  * Update the Dockerfile to deploy latest theme updates
  * [low priority] Translate complete website (not just th GGI Handbook pages)
* Publishing and Translation tools
  * timeline for new features and bug fixing?
* Mailing lists
* Forum (leave it with the mailing list?).
* Discuss new features:
  - Chat ? Don't create an instance but only a room (IRC or Matrix?)
  - Etherpad, GitLab, NextCloud?
