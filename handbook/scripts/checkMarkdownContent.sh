#!/bin/bash

# Echoes a program header to standard output
function echoHeader {
    echo "checkMarkdownContent.sh"
    echo "Lints the Markdown files in the content subfolder."
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
    echo "----------------------------------------------------------"
}

# Checks POSIX compatible if the required commands are available.
function checkConditions {
    if ! command -v mdl &> /dev/null; then
        echo "Error: Command mdl not found, please install it, e.g. by gem install mdl."
        exit 2
    fi
}

echoHeader
checkConditions

# Exclusions: 
# - MD002: First header should be a top level header
# - MD013: Line length
mdl -r ~MD002,~MD013,~MD029,~MD033,~MD027 ../content