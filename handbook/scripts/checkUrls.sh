#!/bin/bash

# Check URLs in content.
# Example of usage: ./checkUrls.sh > /tmp/urlstatus.txt
# Then, checking status codes in output can be done as follows:
# cat /tmp/urlstatus3.txt | sort 
# Note that 000 means timeout, 200-299 OK, 300-399 redirect (generally fine)
# Only 400+ ere definitely errors! (and 100-199 should be investigated).

rm -f /tmp/url1.txt /tmp/url2.txt
for file in `find content/ -name "*.md" -print`; do grep "\((http\S*)\)" $file >> /tmp/url1.txt ; done

sed -r 's/.*(\((.*)\)).*/\2/' /tmp/url1.txt | grep http |sed -r 's/\)//g' |sed -r 's/\"//g' |sed -r 's/\/$//g' | sort | uniq > /tmp/url2.txt

status=0

user_agent="'-A 'Mozilla/5.0 (X11; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0"
while read url
do
    urlstatus=$(curl -o /dev/null --connect-timeout 45 -H "Accept-Encoding: zstd, br, gzip, deflate" -A '${user_agent}' --silent --head --write-out '%{http_code}' "$url" )
    echo "$urlstatus	$url"
    if [ $urlstatus -ge 400 ] || [ $urlstatus -eq 0 ]
    then
      status=1
    fi
done < /tmp/url2.txt

exit $status
