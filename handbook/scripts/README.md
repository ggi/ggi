# Scripts subfolder

This `scripts/` directory contains the scripts needed to build the handbook and manage translations.

## Building the handbook

* `convert_handbook.sh` builds the handbook PDF for onscreen viewing, optionally producing HTML files.
* `paperback_convert_handbook.sh` builds the handbook PDF for printing (i.e. with links made visible).

## Translations

These scripts provide common services and features for the handbook updates and maintenance.

* `setupDebian.sh` installs required software on a Debian system, configures it and gives hints for manual improvements, Also run inside the Debian-based Docker image (see `po2all-Docker.sh` below) to set it up properly.
* `po2all.sh` Calls all scripts to create translated content files (= after the translation) in the language specific subfolder(s) in handbook/translations.
* `convert_translation.sh` executes all steps required to update the translation from the po files, set defaults, convert images, and creates the pdf for the provided language.
* `convert_translation_docker.sh` Sets up a Docker container, and calls `convert_translation.sh` from inside there, making the PDF creation less depending on the system it is run. Of course, this will only work when Docker is installed and Debian packages can be installed from a network connection.
* `checkUrls.sh` looks for all URLs in the handbook, checks them and reports the 4xx return codes.
* `get_activities.py` retrieves the activities (issues) from a GitLab instance and writes them on disk as markdown files.

## Utilities

The content files (e.g. markdown) need to be transformed into or update existing po/pot files, sent through the git repository to Weblate where they are translated by the community, and then retrieved back in the git repository.


* `latexPo4a.cfg` po4a configuration file, used in `latexUpdate2po.sh`
* `latexUpdate2po.sh` (works bi-directional)
* `markdown2po.sh`
* `scorecard2po.sh`
(* `latex2pot.sh` Don't use: to create raw latex pot files from scratch.)

Once translation is done, they are converted back to content files and can be safely compiled to produce the PDF.

* `latexPo4a.cfg`po4a configuration file, used in `latexUpdate2po.sh`
* `latexUpdate2po.sh` (works bi-directional)
* `po2markdown.sh`
* `po2scorecard.sh`
* `po2CvrImg.sh`
* `po2MaslowImg.sh`
