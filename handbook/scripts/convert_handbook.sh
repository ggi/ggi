#!/bin/bash

######################################################################
# Copyright (c) 2021 Castalia Solutions and others
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
######################################################################

# Execute using "bash scripts/convert_handbook.sh"

# Default values
UPDATE=NO
DEST_WEB=""
MOVE_PDF=NO # if YES it will move the generated PDF in the same directory than the main one
PDF_FILENAME="ggi_handbook"
BASE_DIR=$(dirname "$0")/..
BASE_DIR=$(realpath "$BASE_DIR")
BUILD_DIR="$BASE_DIR"

CMD_PANDOC=pandoc
# Export environment variable to use non-default binary for "pandoc"
# Example:
# export CMD_PANDOC=/opt/pandoc/pandoc-2.16/bin/pandoc

while [ $# -gt 0 ]; do
    key="$1"

    case $key in
	-u|--update)
	    UPDATE=YES
	    shift # shift argument
	    ;;
	-l|--language)
	    GGI_LANG="$2"
        BUILD_DIR="$BASE_DIR/translations/$GGI_LANG"
	    shift # shift argument
	    shift # shift value
	    ;;
	-w|--write-web)
	    DEST_WEB="$2"
	    shift # shift argument
	    shift # shift value
	    ;;
	-m|--move-pdf)
	    MOVE_PDF="$2"
	    shift # shift argument
	    shift # shift value
	    ;;
	*) # unknown option
	    echo "Unknown parameter $1. Exit."
	    exit 4
	    ;;
    esac
done

echo "Change Dir to '$BUILD_DIR'"
cd "$BUILD_DIR"


echo "# Setup python3 env."
if [ -d ./env ]; then
    echo "  - Reusing ./env"
else
    echo "  - Creating env in ./env"
    python3 -m venv env/
    pip install -r "$BASE_DIR/requirements.txt"
fi
. env/bin/activate
    
echo "# Start build of the body of knowledge."
if [ "$UPDATE" = "YES" ]; then
    echo "  - Update activities."
else
    echo "  - No update of activities."
fi

if [ "$GGI_LANG" != "" ]; then
    echo "  - Building for language $GGI_LANG."
    GGI_LANG_OPT="-V lang=$GGI_LANG" 
    PDF_FILENAME="$PDF_FILENAME"_"$GGI_LANG"
else
    MOVE_PDF=NO
    echo "  - Building main english version."
fi

if [ "$DEST_WEB" != "" ]; then
    echo "  - Write web files to $DEST_WEB."
else
    echo "  - No web export."
fi

if [ "$UPDATE" = "YES" ]; then
    echo "# Clean up activities."
    rm content/5*_activity_*.md

    echo "# Retrieving activities from GitLab."
    python3 get_activities.py
fi

# Initialise EXIT_CODE to provide information about the status while still
# trying to build the pdf.
EXIT_CODE=0

if [ "$GGI_LANG" != "" ]; then
    echo '# Regenerate md files from translation.'
    echo '# Check that we have everything we need to build the PDF.'
    if [ -d 'content' ] && [ -d 'resources' ]; then
	echo '  - Found content/ and resources/ dirs.'
    else
	echo 'ERROR: Cannot find content/ or resources/ dirs. Passing build.'
	exit 0
    fi
    for file in resources/latex/customisations.tex resources/latex/customised_activity_scorecard_template.pdf; do
	echo "  - Checking $file" 
	if [ -f $file ] ; then
	    echo "  - Found $file."
	else
	    echo "ERROR: Cannot find $file. Copying from default."
	    cp ../../$file $file
	    EXIT_CODE=1
	fi
    done
fi


echo "# Generating tex file."
$CMD_PANDOC content/*.md \
       --number-sections \
       --from=gfm \
       --include-before-body resources/latex/title.tex \
       --include-in-header resources/latex/customisations.tex \
       -V documentclass:article $GGI_LANG_OPT \
       -V linkcolor:blue \
       -V geometry:a4paper \
       -V geometry:margin=2cm \
       -V mainfont="DejaVu Serif" \
       -V monofont="DejaVu Sans Mono" \
       -M "The GGI Good Book" \
       -o ggi_handbook.tex \
       --pdf-engine=xelatex \
       --toc --toc-depth=2 
#       -o ggi_handbook.tex \

head -n-1 ggi_handbook.tex > ggi_handbook_with_appendices.tex
echo "
\includepdf[pages=-]{resources/latex/customised_activity_scorecard_template.pdf}

\end{document}" >> ggi_handbook_with_appendices.tex

echo "# Generating pdf file."
for i in 1 2 3
do
	echo "  - Run $i."
	xelatex -interaction=nonstopmode ggi_handbook_with_appendices.tex | grep -E '^l.[0-9]+ '
done
echo "  - Done."

if [ "$MOVE_PDF" = "YES" ]; then
	mv -v ggi_handbook_with_appendices.pdf "$BASE_DIR/$PDF_FILENAME.pdf"
else
	mv -v ggi_handbook_with_appendices.pdf "$PDF_FILENAME.pdf"
fi


if [ "$DEST_WEB" != "" ]; then
    echo "# Generate Markdown export for website."
    rm -rf export_web/
    mkdir -p export_web/

    # BoK early chapters
    for md in introduction organisation methodology conclusion; do
	cp resources/html/$md* export_web/
	file=$(ls export_web/$md*)
	dir=${file%.md}
	mkdir -p "$dir"
	mv "$file" "$dir"/index.md
	cat content/*$md* >> "$dir"/index.md
	sed -i "s!resources/images/!/images/ggi/!" "$dir"/index.md
	sed -i 's/^###/##/' "$dir"/index.md
    done

    mkdir -p export_web/activities/
    for activity in `ls content/*activity*`; do
	base_name=$(basename "$activity")
	file_name=${base_name#*_}
	cp resources/html/activity.md export_web/activities/"${file_name}"
	cat "${activity}" >> export_web/activities/"${file_name}"
	file=$(ls export_web/activities/"${file_name}"*)
	title=$(grep -E "^## " "$activity" | cut -d\  -f2-)
	file_out=$(echo "$title" | tr " " "_" | tr "[:upper:]" "[:lower:]")
	dir="export_web/activities/${file_out}"
	echo "- export to [$dir]."
	mkdir -p "$dir"
	mv "$file" "$dir"/index.md
	sed -i "s/GGI_ACTIVITY_TITLE/${title}/" "$dir"/index.md
	sed -i 's/^###/##/' "$dir"/index.md
    done

    rm -rf "$DEST_WEB/[1-9]{2}_*"
    if [ ! -d "$DEST_WEB" ]; then
	echo "Creating directory $DEST_WEB"
	mkdir -p "$DEST_WEB"/
    fi
    cp -r export_web/* "$DEST_WEB"/
fi

# Cleaning up temporary files.
rm ggi_handbook_with_appendices.aux \
   ggi_handbook_with_appendices.log \
   ggi_handbook_with_appendices.tex \
   ggi_handbook_with_appendices.toc \
   ggi_handbook_with_appendices.out \
   ggi_handbook.tex

exit $EXIT_CODE
