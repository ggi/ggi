#! /bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires: 
#   Package bash
#   Package mdpo (po2json), 

# Predefined values #
# Path to the handbook folder 
pathHandbook='..'

# Find languages configured as subfolders of translations
function findLanguages {
    if (( ${#langs[@]} == 0 )); then
        while IFS= read -r -d '' translatedLang
        do
            langs+=($(basename "$translatedLang"))
        done <   <(find "$pathHandbook"/translations -mindepth 1 -maxdepth 1 -type d -print0)
    else
        # Checking existence of the passed language's translated files folder
        if [ ! -d "$pathHandbook"/translations/"${langs[0]}" ]; then
            echo "Error: the translated files folder for the passed language does not exist."
            exit 1
        fi
    fi
}

# Checks POSIX compatible if the required commands are available.
function checkConditions {
    if ! command -v po2json &> /dev/null; then
        echo "Error: Command po2json not found, please install the mdpo package."
        exit 2
    fi
}

# Creates a translated MD file based on the language specific (translated) PO file and the original MD file
# Avoid overwriting except told to do so.
function poToJsonFile {
    for lang in "${langs[@]}";  do
        targetFile="$pathHandbook"/translations/"$lang"/content/"$1"_"$lang".json
        if [ -f "$targetFile" ] && [ -z "$overwrite" ]; then
            # Skip this language if the file exists, but overwrite is not set  
            echo "The target file $targetFile exists, but the overwrite option is not set."
        else 
            po2json -i "$pathHandbook"/translations/"$lang"/po_files/content/"$1".json.po -o "$targetFile" -t "$pathHandbook"/content/"$1".json
        fi
    done
}

# Translates PO files as in subfolder translate into MD files
function translateContent {
    echo "Files:"
    while IFS= read -r -d '' path
    do
        echo "- Processing file $(basename "${path}" .json.pot)"
        poToJsonFile "$(basename "${path}" .json.pot)"
    done <   <(find  "$pathHandbook"/resources/pot_files/content/ -mindepth 1 -maxdepth 1 -name '*.json.pot' -type f -print0)
}

# End
function endPrg {
    echo "Done."
    exit 0
}

# Echoes help
function echoHelp {
    echo "usage: po2json.sh [-h|--help] | [ [-l|--language LANGCODE] [-o|--overwrite]] ]"
    echo
    echo "Transforms all configured language's translations from .po files"
    echo "based on their original .json files into translated .json files."
    echo "Default behavior:"
    echo " - does not overwrite exisitng target files"
    echo " - iterates through all found languages resp. subfolders of the"
    echo "translations folder"
    echo
    echo "Cd into the scripts folder to run this command."
    echo
    echo "options:"
    echo "  -h, --help       Echoes this help message, then exits"
    echo "  -l, --language LANGCODE"
    echo "                   Limit processing to the language LANGCODE."
    echo "  -o, --overwrite  Overwrite target file."
    echo 
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
}

# Global var: Languages configured as subfolders of translations
declare -a langs=()
# Global var: overwrite target file(s) 
overwrite="" 

# Script parameter handling
while [ $# -gt 0 ]; do
    case $1 in
        -h|--help)
            echoHelp
            exit 0
            ;;
        -l|--language)
            langs=("$2")
            shift # shift argument
            shift # shift value
            ;;
        -o|--overwrite)
            overwrite=1
            shift # shift argument
            ;;
        *)
        	echo "Error: unknown parameter $1!"
        	echoHelp
        	echo "Exiting."
        	exit 1
    esac
done

findLanguages
checkConditions
echo "Language(s) to process: " "${langs[@]}"
translateContent
endPrg
