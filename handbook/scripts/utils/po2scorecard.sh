#! /bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires:
#   Package translation toolkit (po2xliff, xliff2po),
#   Package LibreOffice (soffice),

# Predefined values #
# Path to the handbook folder 
pathHandbook='..'

# Find directory for temporary files
function findTmpDir {
    if [ -n "$TMPDIR" ] && [ -d "$TMPDIR" ]; then
        # POSIX compatible
        tmpdir="$TMPDIR"
    elif [ -d /tmp ]; then
        # fixed
        tmpdir="/tmp"
    else
        echo "Error: no folder for temporary files found."
        exit 1
    fi
}

# Find languages configured as subfolders of translations
function findLanguages {
    if (( ${#langs[@]} == 0 )); then
        while IFS= read -r -d '' translatedLang
        do
            langs+=($(basename "$translatedLang"))
        done <   <(find "$pathHandbook"/translations -mindepth 1 -maxdepth 1 -type d -print0)
    else
        # Checking existence of the passed language's translated files folder
        if [ ! -d "$pathHandbook"/translations/"${langs[0]}" ]; then
            echo "Error: the translated files folder for the passed language does not exist."
            exit 1
        fi
    fi
}

# Checks POSIX compatible if the required commands are available.
function checkConditions {
    if ! command -v po2xliff &> /dev/null; then
        echo "Error: Command po2xliff not found, please install the translate-toolkit package."
        exit 2
    fi
    if ! command -v xliff2odf &> /dev/null; then
        echo "Error: Command xliff2odf not found, please install the translate-toolkit package."
        exit 2
    fi
    if ! command -v soffice &> /dev/null; then
        echo "Error: Command soffice not found, please install the LibreOffice package."
        exit 2
    fi
    # Check for POSIX compatible environment variable TMPDIR pointing to the tempory files folder.
    if [ -z "$TMPDIR" ]; then 
        echo "Info: Environment variable TMPDIR does not exist, using /tmp."
	export TMPDIR="/tmp/"
    fi;
}

# Converts po to xliff files (temporarily) and combines that with the
# original ODT files to translated (permanent) ODT and PDF files.
# Avoid overwriting except told to do so.
function po2odtFile {
    echo "File: $1, final: $2"
    for lang in "${langs[@]}";  do
        echo "Processing language: $lang"        
        # If overwrite is not set and any of the target files exists
	    if [ -f "$pathHandbook"/translations/"$lang"/resources/latex/"$1".odt ] && [ -z "$overwrite" ]; then
	    	echo "One of the target files exists, but the overwrite option is not set: $1.odt."
	    elif [ -f "$pathHandbook"/translations/"$lang"/resources/latex/"$1".pdf ] && [ -z "$overwrite" ]; then
	    	echo "One of the target files exists, but the overwrite option is not set: $1.pdf."
	    elif [ -f "$pathHandbook"/translations/"$lang"/resources/latex/"$2" ] && [ -z "$overwrite" ]; then
	    	echo "One of the target files exists, but the overwrite option is not set: $2."	    
		else         
	        # Create a (temporary) XLIFF from the translated PO
	        po2xliff --progress=none -i "$pathHandbook"/translations/"$lang"/po_files/resources/"$1".po -o "$tmpdir"/"$1".xlf
	        # Create a (permanent) ODT from the translated XLIFF
	        xliff2odf --progress=none -i "$tmpdir"/"$1".xlf  -t "$pathHandbook"/../resources/scorecards/"$1".odt  -o "$pathHandbook"/translations/"$lang"/resources/latex/"$1".odt
	        # Create a PDF from the translated ODT
	        soffice --headless --convert-to pdf --outdir "$pathHandbook"/translations/"$lang"/resources/latex/ "$pathHandbook"/translations/"$lang"/resources/latex/"$1".odt
	        # Copy pdf to final filename
	        cp  -f "$pathHandbook"/translations/"$lang"/resources/latex/"$1".pdf  "$pathHandbook"/translations/"$lang"/resources/latex/"$2"
        fi
    done
}

# End
function endPrg {
    echo "Done."
    exit 0
}

# Echoes help
function echoHelp {
    echo "usage: po2scorecard.sh [-h|--help] | [ [-l|--language LANGCODE] [-o|--overwrite]] ]"
    echo
    echo "Converts po to xliff files (temporarily) and combines that with the"
    echo "original ODT files to translated (permanent) Scorecard-Template-v0.odt, "
    echo "Scorecard-Template-v0.pdf and customised_activity_scorecard_template.pdf "
    echo "in LANGCODEresources/latex/."
    echo "Default behavior:"
    echo " - does not overwrite exisitng target files"
    echo " - iterates through all found languages resp. subfolders of the"
    echo "translations folder"
    echo
    echo "Cd into the scripts folder to run this command."
    echo
    echo "options:"
    echo "  -h, --help       Echoes this help message, then exits"
    echo "  -l, --language LANGCODE"
    echo "                   Limit processing to the language LANGCODE."
    echo "  -o, --overwrite  Overwrite target file."
    echo ""
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
}

# Global var: directory for temporary files
tmpdir=""
# Global var: Languages configured as subfolders of translations
declare -a langs=()
# Global var: overwrite target file(s) 
overwrite="" 

# Script parameter handling
while [ $# -gt 0 ]; do
    case $1 in
        -h|--help)
            echoHelp
            exit 0
            ;;
        -l|--language)
            langs=("$2")
            shift # shift argument
            shift # shift value
            ;;
        -o|--overwrite)
            overwrite=1
            shift # shift argument
            ;;
        *)
        	echo "Error: unknown parameter $1!"
        	echoHelp
        	echo "Exiting."
        	exit 1
    esac
done

findTmpDir
findLanguages
checkConditions
echo "Language(s) to process: " "${langs[@]}"
po2odtFile "Scorecard-Template-v0" "customised_activity_scorecard_template.pdf"
endPrg
