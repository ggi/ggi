#! /bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires:
#  Package bash
#  Package gettext (msgfmt)
#  Package imagemagick (convert)

# Predefined values #
# Path to the handbook folder 
pathHandbook='..'
# Path to the empty Maslow image
emptyImg="$pathHandbook"/resources/images/ggi_maslow-empty.png
# Width of the image in pixel
imgWidth=1932
# Name of the font to draw the text with
fontname="DejaVu-Serif"
# Right-To-Left language codes
declare -a rtls=('ar' 'arc' 'dv' 'fa' 'ha' 'he' 'khw' 'ks' 'ku' 'ps' 'ur' 'yi')

#####

# Find directory for temporary files
function findTmpDir {
    if [ -n "$TMPDIR" ] && [ -d "$TMPDIR" ]; then
        # POSIX compatible
        tmpdir="$TMPDIR"
    elif [ -d /tmp ]; then
        # fixed
        tmpdir="/tmp"
    else
        echo "Error: no folder for temporary files found."
        exit 1
    fi
}

# Find languages configured as subfolders of translations
function findLanguages {
    if (( ${#langs[@]} == 0 )); then
        while IFS= read -r -d '' translatedLang
        do
            langs+=($(basename "$translatedLang"))
        done <   <(find "$pathHandbook"/translations -mindepth 1 -maxdepth 1 -type d -print0)
    else
        # Checking existence of the passed language's translated files folder
        if [ ! -d "$pathHandbook"/translations/"${langs[0]}" ]; then
            echo "Error: the translated files folder for the passed language does not exist."
            exit 1
        fi
    fi
}

# Checks POSIX compatible if the required commands are available.
# Checks if the configured font is available to imagemagick's convert
function checkConditions {
    if ! command -v msgfmt &> /dev/null; then
        echo "Error: Command msgfmt not found, please install the gettext package."
        exit 2
    fi
    if ! command -v sed &> /dev/null; then
        echo "Error: Command sed not found, please install the sed package."
        exit 2
    fi
    if ! command -v convert &> /dev/null; then
        echo "Error: Command convert not found, please install the imagemagick package."
        exit 2
    fi
    if [ -z "$(convert -list font | grep -i $fontname)" ]; then 
        echo "Error: The configured font is not installed: $fontname"
        exit 2
    fi
}

# Convert .po to .mo
function convertPoToMo {
    echo "- Converting .po to .mo"
    mkdir -p "$tmpdir"/"$1"/LC_MESSAGES
    msgfmt -o "$tmpdir"/"$1"/LC_MESSAGES/maslow.mo "$pathHandbook"/translations/"$1"/po_files/resources/maslow.po
}

# Configure gettext
function configGettext {
    #export LC_ALL=$lang
    export LANGUAGE=$1 
    export TEXTDOMAINDIR=$tmpdir
    export TEXTDOMAIN='maslow'
}

# Draw text into left triangle
function drawTriangle {
    echo "- Drawing triangle"

    # Move offset to center
    cntrOffset=550
    # Vertical distance from the top corner
    yStart=175
    # Vertical distance of  texts to each other
    yDiff=100
    
    yl1=$yStart
    let yl2=$(($yStart + $yDiff))
    let yl3=$(($yStart + (2 * $yDiff)))
    let yl4=$(($yStart + (3 * $yDiff)))
    let yl5=$(($yStart + (4 * $yDiff)))

    convert -font "$fontname" -pointsize 24 -fill black -gravity north -draw " \
text -$cntrOffset,$yl1 '$(gettext -s "Self-Actualization" | sed "s/'/\\\'/g")' \
text -$cntrOffset,$yl2 '$(gettext -s "Esteem" | sed "s/'/\\\'/g")' \
text -$cntrOffset,$yl3 '$(gettext -s "Love & Belonging" | sed "s/'/\\\'/g")' \
text -$cntrOffset,$yl4 '$(gettext -s "Safety" | sed "s/'/\\\'/g")' \
text -$cntrOffset,$yl5 '$(gettext -s "Physiological Needs" | sed "s/'/\\\'/g")' \
" "$1" "$2"
}

# Draw goals
function drawGoals {
    echo "- Drawing goals"

    # Move offset to center
    center=980
    let cntrOffset=$((($center-($imgWidth)/2)*2))
    #cntrOffset=50
    # Vertical distance from the top
    yStart=115
    # Vertical distance of  texts to each other
    yDiff=114
    
    y1=$yStart
    let y2=$(($yStart + $yDiff))
    let y3=$(($yStart + (2 * $yDiff)))
    let y4=$(($yStart + (3 * $yDiff)))
    let y5=$(($yStart + (4 * $yDiff)))

    convert -font "$fontname" -pointsize 34 -fill black -gravity north -draw " \
text $cntrOffset,$y1 '$(gettext -s "Strategy" | sed "s/'/\\\'/g")' \
text $cntrOffset,$y2 '$(gettext -s "Engagement" | sed "s/'/\\\'/g")' \
text $cntrOffset,$y3 '$(gettext -s "Culture" | sed "s/'/\\\'/g")' \
text $cntrOffset,$y4 '$(gettext -s "Trust" | sed "s/'/\\\'/g")' \
text $cntrOffset,$y5 '$(gettext -s "Usage" | sed "s/'/\\\'/g")' \
" "$1" "$2"
}

# Draw goal boxes
function drawGoalbox {
    echo "- Drawing goal boxes"

    # Vertical distance from the top of the text in the first box
    yStart=77

    # Box: Horizontal distance from the left border
    boxOffsetX=1130
    # Box: Vertical distance of each box to another
    boxDiffY=23
    # Box: size
    boxSizeX=800
    boxSizeY=94
    
    # Get the translation strings
    # Note: keep the newlines below intact to match the newline in the source string!
    boxSt="$(gettext -s "Embracing the full potential of OSS. Proactively 
using OSS for innovation and competetiveness.")"
    boxEn="$(gettext -s "Engaging with the OSS ecosystem. Contributing 
back. Developing visibility, event participation.")"
    boxCu="$(gettext -s "Implementing best practices. Developing OSS 
culture. Sharing experience.")"
    boxTr="$(gettext -s "Securely and responsibly using OSS. Compliance 
and dependency management policies.")"
    boxUs="$(gettext -s "Technically using OSS. Technical ability and 
experience with OSS. Some OSS awareness.")"

    # Escape single quotes in the strings, so they do not interfere with the command's quoting
    boxSt="$(echo "$boxSt" | sed "s/'/\\\'/g")"
    boxEn="$(echo "$boxEn" | sed "s/'/\\\'/g")"
    boxCu="$(echo "$boxCu" | sed "s/'/\\\'/g")"
    boxTr="$(echo "$boxTr" | sed "s/'/\\\'/g")"
    boxUs="$(echo "$boxUs" | sed "s/'/\\\'/g")"

    # Calculate the positions on the image
    ySt=110
    yEn=$(($ySt+$boxDiffY+$boxSizeY))
    yCu=$(($ySt+($boxDiffY+$boxSizeY)*2))
    yTr=$(($ySt+($boxDiffY+$boxSizeY)*3))
    yUs=$(($ySt+($boxDiffY+$boxSizeY)*4))
    
    convert -font "$fontname" -pointsize 22 -size ${boxSizeX}x${boxSizeY} -fill white -draw " \
text $boxOffsetX,$ySt '$boxSt' \
text $boxOffsetX,$yEn '$boxEn' \
text $boxOffsetX,$yCu '$boxCu' \
text $boxOffsetX,$yTr '$boxTr' \
text $boxOffsetX,$yUs '$boxUs' \
" "$1" "$2"
}

function drawFooter {
    echo "- Drawing footer"
        
    footerY=680

    subTri="$(gettext -s "Abraham Maslow's Hierarchy of Behavioral Motives" | sed "s/'/\\\'/g")"
    subGoal="$(gettext -s "OW2 Goals to OSS Good Governance" | sed "s/'/\\\'/g")"
    
    convert -font "$fontname" -pointsize 30 -fill black  -gravity north -draw " \
text -550,$footerY '$subTri' \
text 550,$footerY '$subGoal' \
" "$1" "$2"
}

# Cleanup
function cleanup {
    echo "Cleaning up."
    # Language folder
    rm -r "${tmpdir:?}"/"${1:?}"
    # Intermediary image file
    rm "$tmpdir"/maslow-tmp.png
}

# Process a language and target file
function po2MaslowLang {
	lang="$1"
	targetFile="$2"
	# Echo a warning if the current language is right-to-left
    for elem in "${rtls[@]}"; do
        if [[ $elem == "$lang" ]]; then
            echo "Warning: this is an RTL language, so this script might not create expected results!"
        fi
    done
    # Compose the Maslow image
    if [ -f "$pathHandbook"/translations/"$lang"/po_files/resources/maslow.po ]; then
	mkdir -p "$pathHandbook"/translations/"$lang"/resources/images "$pathHandbook"/translations/"$lang"/resources/latex/
        convertPoToMo "$lang"
        configGettext "$lang"
        drawTriangle "$emptyImg" "$tmpdir/maslow-tmp.png"
        drawGoals "$tmpdir/maslow-tmp.png" "$tmpdir/maslow-tmp.png"
        drawGoalbox "$tmpdir/maslow-tmp.png" "$tmpdir/maslow-tmp.png"
        drawFooter "$tmpdir/maslow-tmp.png" "$targetFile"
        cleanup "$lang"
    else
        echo "maslow.po not found for this language, skipping..."
    fi
}

# Process all configured languages
# Avoid overwriting except told to do so.
function po2MaslowLanguages {
    for lang in "${langs[@]}";  do
	echo "Processing language: $lang"
	targetFile=""$pathHandbook"/translations/$lang/resources/images/ggi_maslow.png"		 
	if [ -f "$targetFile" ] && [ -z "$overwrite" ]; then
	    # Skip this language if the file exists, but overwrite is not set  
	    echo "The target file $targetFile exists, but the overwrite option is not set."
	else
	    po2MaslowLang "$lang" "$targetFile"
	fi
    done
}

# End
function endPrg {
    echo "Done."
    exit 0
}

# Echoes help
function echoHelp {
	echo "usage: po2MaslowImg.sh [-h|--help] | [ [-l|--language LANGCODE] [-o|--overwrite]] ]"
	echo
	echo "Draws translated strings into a predefined empty PNG image and saves it into"
	echo "LANGCODE/resources/images/ggi_maslow.png."
	echo "Default behavior:"
	echo " - does not overwrite exisitng target files"
	echo " - iterates through all found languages resp. subfolders of the"
	echo "translations folder"
	echo
	echo "Cd into the scripts folder to run this command."
	echo
	echo "options:"
	echo "  -h, --help       Echoes this help message, then exits"
	echo "  -l, --language LANGCODE"
	echo "                   Limit processing to the language LANGCODE."
	echo "  -o, --overwrite  Overwrite all target files."
	echo
        echo "By Silvério Santos for the OW2 Good Governance Initiative."
}

# Global var: directory for temporary files
tmpdir=""
# Global var: Languages configured as subfolders of translations
declare -a langs=()
# Global var: overwrite target file(s) 
overwrite="" 

# Script parameter handling
while [ $# -gt 0 ]; do
    case $1 in
        -h|--help)
            echoHelp
            exit 0
            ;;
        -l|--language)
            langs=($2)
            shift # shift argument
            shift # shift value
            ;;
        -o|--overwrite)
            overwrite=1
            shift # shift argument
            ;;
        *)
        	echo "Error: unknown parameter $1!"
        	echoHelp
        	echo "Exiting."
        	exit 1
    esac
done

findTmpDir
findLanguages
checkConditions
echo "Language(s) to process: " "${langs[@]}"
po2MaslowLanguages
endPrg
