#!/bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires:
#  Package bash
#  Package gettext (msgfmt)
#  Package imagemagick (convert)

# Predefined values #
# Path to the handbook folder 
pathHandbook='..'
# Path to the empty cover image
coverimg="$pathHandbook"/resources/latex/cover-empty.png
# Box X-coords [px] in the empty cover image
boxStart=64
boxEnd=714
imgEnd=824
# Title coords [px] in the empty cover image
titleYStart=502
# Vertical title line distance 
titleYDiff=70
# Footer coords [px] in the empty cover image
footerXStart=88
footerYStart=832
# Vertical footer line distance 
footerYDiff=32
# Name of the font to draw the text with
fontname="DejaVu-Serif"

#####

# Find directory for temporary files
function findTmpDir {
    if [ -n "$TMPDIR" ] && [ -d "$TMPDIR" ]; then
        # POSIX compatible
        tmpdir="$TMPDIR"
    elif [ -d /tmp ]; then
        # fixed
        tmpdir="/tmp"
    else
        echo "Error: no folder for temporary files found."
        exit 1
    fi
}

# Find languages configured as subfolders of translations
function findLanguages {
    if (( ${#langs[@]} == 0 )); then
        while IFS= read -r -d '' translatedLang
        do
            langs+=($(basename "$translatedLang"))
        done <   <(find "$pathHandbook"/translations -mindepth 1 -maxdepth 1 -type d -print0)
    else
        # Checking existence of the passed language's translated files folder
        if [ ! -d "$pathHandbook"/translations/"${langs[0]}" ]; then
            echo "Error: the translated files folder for the passed language does not exist."
            exit 1
        fi
    fi
}

# Checks POSIX compatible if the required commands are available.
# Checks if the configured font is available to imagemagick's convert
function checkConditions {
    if ! command -v msgfmt &> /dev/null; then
        echo "Command msgfmt not found, please install the gettext package."
        exit 2
    fi
    if ! command -v convert &> /dev/null; then
        echo "Command convert not found, please install the imagemagick package."
        exit 2
    fi
    if [ -z "$(convert -list font | grep -i $fontname)" ]; then 
        echo "The configured font is not installed: $fontname"
        exit 2
    fi
    # Imagemagick convert image to PDF
    if ! command -v gs &> /dev/null; then
        echo "Command gs not found, please install the ghostscript package."
        exit 2
    fi
    if [ -f /etc/ImageMagick-6/policy.xml ]; then
        if grep -q '<policy domain="coder" rights="none" pattern="PDF" />' /etc/ImageMagick-6/policy.xml; then
            echo "ImageMagick is forbidden to convert images to PDFs."
            echo 'E.g. in /etc/ImageMagick-6/policy.xml on the line <policy domain="coder" rights="none" pattern="PDF" /> make sure to replace rights="none" with rights="read | write".'
            exit 2
        fi
    fi
}

# Convert .po to .mo
function convertPoToMo {
    echo "- Converting .po to .mo."
    mkdir -p "$tmpdir"/"$1"/LC_MESSAGES
    msgfmt -o "$tmpdir"/"$1"/LC_MESSAGES/cover.mo "$pathHandbook"/translations/"$1"/po_files/resources/cover.po
}

# Configure gettext
function configGettext {
    export LANGUAGE="$1"
    export TEXTDOMAINDIR="$tmpdir"
    export TEXTDOMAIN='cover'
}

# Draw title into image
function drawTitle {
    echo "- Drawing title"
    
    # Translated title (multiline)
	tttl=$(gettext -s "Open Source
Good Governance
Handbook" | sed "s/'/\\\'/g")

	# Split multiline into an array
	IFS=$'\n' header=($tttl)
	readarray -t titleTriplet <<<"$tttl"
	
	# Assign each line to a variable 
    os=${titleTriplet[0]}
    gg=${titleTriplet[1]}
    hb=${titleTriplet[2]}

    let cntrOffset=($imgEnd-$boxEnd-$boxStart)/2

    yos=$titleYStart
    let ygg=$titleYStart+$titleYDiff
    let yhb=$titleYStart+$titleYDiff+$titleYDiff

    convert -font "$fontname" -pointsize 40 -fill white -gravity north -draw " \
text -$cntrOffset,$yos '$os' \
text -$cntrOffset,$ygg '$gg' \
text -$cntrOffset,$yhb '$hb' \
" "$coverimg" "$tmpdir"/cover-tmp.png
}

# Draw footer into image
function drawFooter {
    echo "- Drawing footer"
    
    imgtargetdir="$pathHandbook"/translations/"$1"/resources/latex/
    
    yau=$footerYStart
    yvr=$footerYStart+$footerYDiff
    ydt=$footerYStart+$footerYDiff+$footerYDiff

    au=$(gettext -s "Authors: OSPO Alliance & The GGI participants" | sed "s/'/\\\'/g")
    vr=$(gettext -s "Version: v1.2" | sed "s/'/\\\'/g")
    dt=$(gettext -s "Date: 2023-10-16" | sed "s/'/\\\'/g")
    
    convert -font "$fontname" -pointsize 20 -fill white -draw " \
text $footerXStart,832 '$au' \
text $footerXStart,864 '$vr' \
text $footerXStart,896 '$dt' \
" "$tmpdir"/cover-tmp.png "$imgtargetdir"/cover.png
}

# Convert cover.png to cover.pdf, needed to create the handbook PDF
function convertImageToPdf {
    echo "- Converting to PDF"
    
    convert "$pathHandbook"/translations/"$1"/resources/latex/cover.png "$pathHandbook"/translations/"$1"/resources/latex/cover.pdf
    
    if [ ! -f "$pathHandbook"/translations/"$1"/resources/latex/cover.pdf ]; then 
        echo "Error: the expected file cover.pdf has not been created. Check previous command's error messages"
    fi
}

# Cleanup
function cleanup {
    echo "Cleaning up."
    rm -r "${tmpdir:?}"/"$1"
    rm "$tmpdir"/cover-tmp.png
}

# Process all configured languages
# Avoid overwriting except told to do so.
function po2CvrLanguages {
	for lang in "${langs[@]}";  do
	    echo "Processing language: $lang"
	    if [ -f "$pathHandbook"/translations/"$lang"/resources/latex/cover.png ] && [ -z "$overwrite" ]; then
	    	echo "One of the target files exists, but the overwrite option is not set: cover.png."
	    elif [ -f "$pathHandbook"/translations/"$lang"/resources/latex/cover.pdf ] && [ -z "$overwrite" ]; then
	    	echo "One of the target files exists, but the overwrite option is not set:  cover.pdf."
	    else	    
		    if [ -f "$pathHandbook"/translations/"$lang"/po_files/resources/cover.po ]; then
                mkdir -p "$pathHandbook"/translations/"$lang"/resources/images "$pathHandbook"/translations/"$lang"/resources/latex/
		        convertPoToMo "$lang"
		        configGettext "$lang"
		        drawTitle
		        drawFooter "$lang"
		        convertImageToPdf "$lang"
		        cleanup "$lang"
		    else
		        echo "cover.po not found for this language, skipping..."
		    fi
	    fi
	done
}

# End
function endPrg {
    echo "Done."
    exit 0
}

# Echoes help
function echoHelp {
    echo "usage: po2CvrImg.sh [-h|--help] | [ [-l|--language LANGCODE] [-o|--overwrite]] ]"
    echo
    echo "Creates the files cover.png and cover.pdf in LANGCODE/resources/latex with "
    echo "text from translations in .po files."
    echo "Default behavior:"
    echo " - does not overwrite exisitng target files"
    echo " - iterates through all found languages resp. subfolders of the"
    echo "translations folder"
    echo
    echo "Cd into the scripts folder to run this command."
    echo
    echo "options:"
    echo "  -h, --help       Echoes this help message, then exits"
    echo "  -l, --language LANGCODE"
    echo "                   Limit processing to the language LANGCODE."
    echo "  -o, --overwrite  Overwrite all target files."
    echo ""
    echo "By Silvério Santos for the OW2 Good Governance Initiative."	
}

# Global var: directory for temporary files
tmpdir=""
# Global var: Languages configured as subfolders of translations
declare -a langs=()
# Global var: overwrite target file(s) 
overwrite="" 

# Script parameter handling
while [ $# -gt 0 ]; do
    case $1 in
        -h|--help)
            echoHelp
            exit 0
            ;;
        -l|--language)
            langs=($2)
            shift # shift argument
            shift # shift value
            ;;
        -o|--overwrite)
            overwrite=1
            shift # shift argument
            ;;
        *)
        	echo "Error: unknown parameter $1!"
        	echoHelp
        	echo "Exiting."
        	exit 1
    esac
done

findTmpDir
findLanguages
checkConditions
echo "Language(s) to process: ${langs[@]}"
po2CvrLanguages
endPrg
