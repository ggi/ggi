#!/bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires:
#  Package bash
#  Package git
#  Package wl-clipboard (optional)
#  Package libreoffice (optional)
#  Package pandoc (optional)
#  Script utils/po2markdown.sh

# Previous release's tag prefix for tag format: v<major>.<minor>_<langcode>, e.g. v1.1_pt
prevRelTag="v1.1"

echo "usage: diffTranslatedMds.sh LANGCODE"

# Parameter: language code (required)
if [ -z $1 ]; then
    echo "Parameter language code is required."
    exit 1
fi
lang=$1
if [ ! -d "../translations/${lang}/content/" ]; then
    echo "Error: the translated files folder for the passed language does not exist."
    exit 1
fi

# Echoes help
function echoHelp {
    echo
    echo "Pulls current files translated into a given language from the remote repo, transforms them into translated content files and presents the difference against the previous release's translated content files."
    echo
    echo "Cd into the scripts folder to run this command."
    echo "For better convenience, install LibreOffice and wl-clipboard."
    echo "For best convenience, install LibreOffice, wl-clipboard, and pandoc."
    echo
    echo "options:"
    echo "  LANGCODE   Processing the md files for the language LANGCODE."
    echo ""
    echo "By Silvério Santos for the OW2 Good Governance Initiative."	
}

function gitSparse {
    lang=$1
    if [ ! -d /tmp/ggi_handbook-${prevRelTag} ]; then
        mkdir /tmp/ggi_handbook-${prevRelTag}
    fi
    cwd=$(pwd)
    cd /tmp/ggi_handbook-${prevRelTag}
    git init
    git remote add -f origin "https://gitlab.ow2.org/ggi/ggi.git"
    git sparse-checkout init
    git sparse-checkout set "handbook/translations/${lang}"
    git pull origin translations
    cd $cwd
}

function mdToOdt {
    pandoc $1 -V documentclass:article -f gfm -o "ggi_handbook-$2$3.odt" -t odt
}

# End
function endPrg {
    echo "Done."
    exit 0
}

git fetch && git switch translations && git pull
utils/po2markdown.sh -l ${lang} -o

# Different options depending on the presence of respectively required tools
if [ ! command -v wl-copy &> /dev/null ]; then
    # Tool to copy to Wayland's clipboard NOT available: plain diff per markdown file
    git difftool ${prevRelTag}_${lang} HEAD "../translations/${lang}/content/";
elif [ ! command -v pandoc &> /dev/null ]; then
    # Pandoc NOT available: compare translated release'es markdown file one by one in LibreOffice
    echo "---------------------"
    echo "Your action required: The git URL of the previous release's file is copied into the clipboard before opening the current version in LibreOffice. Please paste it into menu item: Edit/Track changes/compare document. Don't overwrite the source file by simply saving."
    echo "To go to the next file, close LibreOffice."
    echo "---------------------"
    for mdfile in $(find ../content -type f -name '*.md'); do
        wl-copy "https://gitlab.ow2.org/ggi/ggi/-/raw/${prevRelTag}_${lang}/handbook/translations/${lang}/content/${mdfile}"
        libreoffice "../translations/${lang}/content/${mdfile}"
    done
else
    # Pandoc available to convert markdown to odt: compare whole document's translated releases in LibreOffice
    echo "---------------------"    
    echo "Compiling the current release's odt file..."
    cd ..
    mdToOdt "translations/$1/content/*.md" $1
    echo "---------------------"    
    echo "Sparse-checkout'ing the previous release's files to a temporary folder..."
    gitSparse $1
    echo "---------------------"    
    echo "Compiling the previous release's odt file..."
    cwd=$(pwd)
    cd "/tmp/ggi_handbook-${prevRelTag}/handbook/translations/$1"
    mdToOdt "/tmp/ggi_handbook-${prevRelTag}/handbook/translations/$1/content/*.md" $1 ${prevRelTag}
    cd $cwd
    wl-copy "/tmp/ggi_handbook-${prevRelTag}/handbook/translations/$1/ggi_handbook-$1${prevRelTag}.odt"
    echo "---------------------"
    echo "Your acion required: The path to the previous release's file is copied into the clipboard before opening the current version in LibreOffice. Please paste it into menu item: Edit/Track changes/compare document."
    echo "---------------------"
    libreoffice "ggi_handbook-$1.odt"&
fi
endPrg
