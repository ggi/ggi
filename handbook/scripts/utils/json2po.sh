#! /bin/bash
# Author: Silvério Santos <ssantos@web.de>
# Requires: 
#   Package mdpo (md2po), 
#   Package gettext (msgmerge), 
#   Folder content on the same level as tis script

#####

# Echoes a program header to standard output
function echoHeader() {
    echo "json2po.sh"
    echo ""
    echo "Extracts translateable strings from JSON files."
    echo "By Silvério Santos for the OW2 Good Governance Initiative."
    echo ""
}

# Creates POT files
function procFiles {
    json2po -P -o "../resources/pot_files/content/ggi_activities_metadata.json.pot" -i "../content/ggi_activities_metadata.json" --filter=name,roles,goal --duplicates merge
}

# End
function endPrg() {
    echo "Done."
    exit 0
}

echoHeader

echo "Files:"
procFiles

endPrg
