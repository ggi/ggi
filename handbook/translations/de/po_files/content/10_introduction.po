# Silvério Santos <SSantos@web.de>, 2022.
# ssantos <ssantos@web.de>, 2022, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2024-01-08 14:11+0000\n"
"Last-Translator: ssantos <ssantos@web.de>\n"
"Language-Team: German <https://hosted.weblate.org/projects/ospo-zone-ggi/10-introduction/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.4-dev\n"

#. Top level headline
#: ../content/10_introduction.md:block 1 (header)
msgid "Introduction"
msgstr "Einleitung"

#. Introduction
#: ../content/10_introduction.md:block 2 (paragraph)
msgid "This document introduces a methodology to implement professional management of open source software in an organisation. It addresses the need to use open source software properly and fairly, safeguard the company from technical, legal and IP threats, and maximise the advantages of open source. Wherever an organisation stands on these topics this document proposes guidance and ideas to move forward and make your journey a success."
msgstr "In diesem Dokument wird eine Methodik zur Vorstellung einer professionellen Verwaltung von Open-Source-Software in einer Organisation vorgestellt. Es befasst sich mit der Notwendigkeit, Open-Source-Software richtig und fair zu nutzen, das Unternehmen vor technischen und rechtlichen Bedrohungen und der des geistigen Eigentums zu schützen und die Vorteile von Open Source zu maximieren. Unabhängig davon, wo eine Organisation bei diesen Themen steht, bietet dieses Dokument Anleitungen und Ideen, um voranzukommen und Ihre Reise zu einem Erfolg zu machen."

#: ../content/10_introduction.md:block 3 (header)
msgid "Context"
msgstr "Kontext"

#. Context
#: ../content/10_introduction.md:block 4 (paragraph)
msgid "Most large end-users and systems integrators already use Free and Open-Source Software (FOSS) either in their information systems or product and service divisions. Open source compliance has become an ever-growing concern, and many large companies have established compliance officers. However, while sanitising a company's open-source production chain – which is what compliance is about – is fundamental, users *must* give back to communities and contribute to the sustainability of the open-source ecosystem. We see open source governance encompassing the whole ecosystem, engaging with local communities, nurturing a healthy relationship with open source software vendors and service specialists. This takes compliance to the next level, and this is what open source *good* governance is about."
msgstr "Die meisten großen Endnutzer und Systemintegratoren verwenden bereits Freie und Open-Source-Software (FOSS) entweder in ihren Informationssystemen oder in ihren Produkt- und Dienstleistungsbereichen. Die Einhaltung von Open-Source-Richtlinien ist zu einem immer wichtigeren Thema geworden und viele große Unternehmen haben Compliance-Beauftragte eingesetzt. Während jedoch die Klärung der Open-Source-Produktionskette eines Unternehmens - worum es bei der Einhaltung der Compliance geht - von grundlegender Bedeutung ist, *müssen* die Nutzer der Gemeinschaft etwas zurückgeben und zur Nachhaltigkeit des Open-Source-Ökosystems beitragen. Unserer Ansicht nach umfasst die Open-Source-Verwaltung das gesamte Ökosystem, die Zusammenarbeit mit lokalen Gemeinschaften, die Pflege einer gesunden Beziehung zu Open-Source-Anbietern von Open-Source-Software und Dienstleistungsspezialisten. Dies hebt die Einhaltung auf die nächste Stufe und ist, worum es bei einer *guten* Open-Source-Verwaltung geht."

#. Context
#: ../content/10_introduction.md:block 5 (paragraph)
msgid "This initiative goes beyond compliance and liability. It is about building awareness in communities of end-users (often software developers themselves) and systems integrators, and developing mutually beneficial relationships within the European FOSS ecosystem."
msgstr "Diese Initiative geht über Compliance und Haftung hinaus. Es geht um den Aufbau von Wissen in Gemeinschaften von Endnutzern (oft selbst Softwareentwickler) und Systemintegratoren, sowie um die Entwicklung von für beide Seiten vorteilhaften Beziehungen innerhalb des europäischen FOSS-Ökosystems."

#. Context
#: ../content/10_introduction.md:block 6 (paragraph)
msgid "OSS Good Governance enables organisations of all types -- companies, small and large, city councils, universities, associations, etc. -- maximise the benefits derived from open source by helping them align people, processes, technology and strategy. And in this area, that of maximising the advantages of open source, especially in Europe, everyone is still learning and innovating, with nobody knowing where they actually stand regarding state of the art in the domain."
msgstr "OSS Good Governance ermöglicht es Organisationen aller Art -- kleinen und großen Unternehmen, Stadtverwaltungen, Universitäten, Verbänden, usw. -- die Vorteile von Open Source zu maximieren, indem sie hilft, Menschen, Prozesse, Technologie und Strategie aufeinander abzustimmen. In diesem Bereich, der Maximierung der Vorteile von Open Source, sind insbesondere in Europa alle noch dabei, zu lernen und Neuerungen einzuführen, ohne dass jemand weiß, wo er eigentlich bezüglich des Stands der Technik in diesem Bereich steht."

#. Context
#: ../content/10_introduction.md:block 7 (paragraph)
msgid "This initiative aims to help organisations achieve these goals with:"
msgstr "Diese Initiative hat zum Ziel Organisationen zu helfen, diese Ziele zu erreichen über:"

#. Context: What helps an organisation to achieve this initiative's goals
#: ../content/10_introduction.md:block 8 (unordered list)
msgid "A structured catalog of **activities**, a roadmap for the implementation of professional management of open source software."
msgstr "Einen strukturierten Katalog von **Aktivitäten**, einen Fahrplan für die Einrichtung einer professionellen Verwaltung von Open-Source-Software."

#. Context: What helps an organisation to achieve this initiative's goals
#: ../content/10_introduction.md:block 8 (unordered list)
msgid "A **management tool** to define, monitor, report and communicate about progress."
msgstr "Ein **Verwaltungswerkzeug** zum Definieren, Überwachen, Berichten und Kommunizieren über dem Fortschritt,"

#. Context: What helps an organisation to achieve this initiative's goals
#: ../content/10_introduction.md:block 8 (unordered list)
msgid "A **clear and practical path for improvement**, with small, affordable steps to mitigate risks, educate people, adapt processes, communicate inwards and outwards the organisation's realm."
msgstr "Ein **klarer und umsetzbarer Verbesserungsweg** mit kleinen, erschwinglichen Schritten zur Risikominderung, zur Schulung von Mitarbeitern, zur Anpassung von Prozessen und zur Kommunikation nach innerhalb und außerhalb der Organisation."

#. Context: What helps an organisation to achieve this initiative's goals
#: ../content/10_introduction.md:block 8 (unordered list)
msgid "**Guidance** and a range of **curated references** about open-source licensing, best practices, training, and ecosystem engagement to leverage open-source awareness and culture, consolidate internal knowledge and extend leadership."
msgstr "**Leitfaden** und eine Reihe von **ausgewählten Referenzen** über Open-Source-Lizenzierung, bewährte Verfahren, Schulungen und das Engagement im Ökosystem, um das Open-Source-Bewusstsein und die -Kultur zu fördern, internes Wissen zu konsolidieren und Führung zu erweitern."

#. Context
#: ../content/10_introduction.md:block 9 (paragraph)
msgid "This guide has been developed with the following requirements in mind:"
msgstr "Dieser Leitfaden wurde unter Berücksichtigung der folgenden Anforderungen entwickelt:"

#. Context: requirements
#: ../content/10_introduction.md:block 10 (unordered list)
msgid "Any type of organisation is covered: from SMEs to large companies and not-for-profit organisations, from local authorities (e.g. town councils) to large institutions (e.g. European or governmental institutions). The framework provides building blocks for a strategy and hints for its realisation, but *how* the activities are executed depends entirely on the program's context and is up to the program manager. It may prove helpful to look for consulting services and to exchange with peers."
msgstr "Alle Arten von Organisationen werden abgedeckt: von KMUs bis zu großen Unternehmen und gemeinnützigen Organisationen, von lokalen Behörden (z.B. Stadtverwaltungen) bis zu großen Institutionen (z.B. europäische oder staatliche Einrichtungen). Der Rahmen bietet Bausteine für eine Strategie und Hinweise für deren Umsetzung, aber *wie* die Aktivitäten durchgeführt werden, hängt ganz vom Programmkontext ab und liegt in der Verantwortung des Programmmanagers. Es kann sich als hilfreich erweisen, Beratungsdienste in Anspruch zu nehmen und sich mit Gleichgesinnten auszutauschen."

#. Context: requirements
#: ../content/10_introduction.md:block 10 (unordered list)
msgid "No assumption is made about the level of technical knowledge within the organisation or the domain of activity. For example, some organisations will need to set up a complete training curriculum, while others might simply propose ad-hoc material to the teams."
msgstr "Es werden keine Annahmen über das Niveau der technischen Kenntnisse innerhalb der Organisation oder des Tätigkeitsbereichs gemacht. Einige Organisationen müssen beispielsweise einen kompletten Lehrplan aufstellen, während andere den Teams lediglich Ad-hoc-Materialien vorschlagen."

#. Context
#: ../content/10_introduction.md:block 11 (paragraph)
msgid "Some activities will not be relevant to all situations, but the whole framework still provides a comprehensive roadmap and paves the way for tailored strategies."
msgstr "Einige Aktivitäten werden nicht für alle Situationen relevant sein, aber der gesamte Rahmen bietet dennoch einen umfassenden Fahrplan und ebnet den Weg für maßgeschneiderte Strategien."

#: ../content/10_introduction.md:block 12 (header)
msgid "About the Good Governance Initiative"
msgstr "Über die Good Governance Initiative"

#. About the Good Governance Initiative
#: ../content/10_introduction.md:block 13 (paragraph)
msgid "At OW2, an initiative is a joint effort to address a market need. The [Good Governance Initiative](https://www.ow2.org/view/OSS_Governance) proposes a methodological framework to implement professional management of open source software within organisations."
msgstr "Bei OW2 ist eine Initiative eine gemeinsame Anstrengung, um Bedürfnisse des Marktes zu decken. Die [Good Governance Initiative](https://www.ow2.org/view/OSS_Governance) schlägt einen methodischen Rahmen vor, um eine professionelle Verwaltung von Open-Source-Software in Organisationen einzuführen."

#. About the Good Governance Initiative
#: ../content/10_introduction.md:block 14 (paragraph)
msgid "The Good Governance initiative is based on a comprehensive model inspired by the popular Abraham Maslow's hierarchy of human needs and motivations, as illustrated by the picture below."
msgstr "Die Good Governance Initiative basiert auf einem umfassenden Modell, das sich an der bekannten Hierarchie der menschlichen Bedürfnisse und Motivationen von Abraham Maslow orientiert, wie die folgende Abbildung zeigt."

#. About the Good Governance Initiative,Maslow image, keep the filename as it is
#: ../content/10_introduction.md:block 15 (paragraph)
msgid "![Maslow and the GGI](resources/images/ggi_maslow.png)"
msgstr "![Maslow und die GGI](resources/images/ggi_maslow.png)"

#. About the Good Governance Initiative
#: ../content/10_introduction.md:block 16 (paragraph)
msgid "Through ideas, guidelines and activities the Good Governance initiative provides a blueprint for the implementation of organisational entities tasked with professional management of open source software, what is also called OSPO (for Open Source Program Offices). The methodology is also a management system to define priorities, and monitor and share progress."
msgstr "Die Good Governance Initiative liefert durch Ideen, Leitlinien und Aktivitäten eine Vorlage für die Einführung von Organisationseinheiten, die mit der professionellen Verwaltung von Open-Source-Software betraut sind, was auch als OSPO (Open Source Program Offices) bezeichnet wird. Die Methodik ist auch ein Verwaltungssystem für die Festlegung von Prioritäten, die Überwachung und den Austausch von Fortschritten."

#. About the Good Governance Initiative
#: ../content/10_introduction.md:block 17 (paragraph)
msgid "As they implement the OSS Good Governance methodology, organisations will enhance their skills in a number of directions, including:"
msgstr "Bei der Umsetzung der OSS Good Governance-Methodik werden die Organisationen ihre Fähigkeiten in verschiedenen Bereichen verbessern, u. a:"

#. About the Good Governance Initiative: skills organizations will enhance
#: ../content/10_introduction.md:block 18 (unordered list)
msgid "**using** open source software properly and safely within the company to improve software reuse and maintainability and software development velocity;"
msgstr "**einsetzen** von Open-Source-Software, richtig und sicher im Unternehmen, um die Wiederverwendung und Wartbarkeit von Software sowie die Geschwindigkeit der Softwareentwicklung zu verbessern;"

#. About the Good Governance Initiative: skills organizations will enhance
#: ../content/10_introduction.md:block 18 (unordered list)
msgid "**mitigating** the legal and technical risks associated with external code and collaboration;"
msgstr "**mindern** der rechtlichen und technischen Risiken im Zusammenhang mit externem Code und externer Zusammenarbeit;"

#. About the Good Governance Initiative: skills organizations will enhance
#: ../content/10_introduction.md:block 18 (unordered list)
msgid "**identifying** required training for teams, from developers to team leaders and managers, so everybody shares the same vision;"
msgstr "**ermitteln** der erforderlichen Schulungen für die Teams, von den Entwicklern bis zu den Teamleitern und Managern, damit alle die gleiche Vorstellung haben;"

#. About the Good Governance Initiative: skills organizations will enhance
#: ../content/10_introduction.md:block 18 (unordered list)
msgid "**prioritizing** goals and activities, to develop an efficient open source strategy;"
msgstr "**priorisieren** von Zielen und Aktivitäten, um eine effiziente Open-Source-Strategie zu entwickeln;"

#. About the Good Governance Initiative: skills organizations will enhance
#: ../content/10_introduction.md:block 18 (unordered list)
msgid "**communicating** efficiently within the company and to the external world to make the most off the open source strategy;"
msgstr "**kommunizieren** auf effiziente Weise, innerhalb des Unternehmens und nach außen, um die Open-Source-Strategie optimal zu nutzen;"

#. About the Good Governance Initiative: skills organizations will enhance
#: ../content/10_introduction.md:block 18 (unordered list)
msgid "**improving** the organisation's competitiveness and attractiveness for top open source talents."
msgstr "**verbessern** der Wettbewerbsfähigkeit und Attraktivität des Unternehmens für Top-Talente im Bereich Open Source."

#: ../content/10_introduction.md:block 19 (header)
msgid "About the OSPO Alliance"
msgstr "Über die OSPO Alliance"

#. About the OSPO Alliance
#: ../content/10_introduction.md:block 20 (paragraph)
msgid "The **OSPO Alliance** was launched by a coalition of leading European open source non-profit organizations, including OW2, Eclipse Foundation, OpenForum Europe, and Foundation for Public Code, with a mission to grow awareness for open source in Europe and globally and to promote the structured and professional management of open source by companies and administrations."
msgstr "Die **OSPO Alliance** wurde von einer Koalition führender europäischer Open Source Non-Profit-Organisationen ins Leben gerufen, darunter OW2, Eclipse Foundation, OpenForum Europe und Foundation for Public Code, mit dem Ziel, das Bewusstsein für Open Source in Europa und weltweit zu stärken und die strukturierte und professionelle Verwaltung von Open Source durch Unternehmen und Verwaltungen zu fördern."

#. About the OSPO Alliance
#: ../content/10_introduction.md:block 21 (paragraph)
msgid "While the Good Governance initiative is focused on developing a management methodology, the OSPO Alliance has the broader goal to help companies, particularly in non-technology sectors, and public institutions discover and understand open source, start benefiting from it across their activities and grow to host their own OSPOs."
msgstr "Während sich die Good Governance-Initiative auf die Entwicklung einer Verwaltungsmethodik konzentriert, verfolgt die OSPO Alliance das weiter gefasste Ziel, Unternehmen, insbesondere in nicht-technologischen Sektoren, und öffentlichen Einrichtungen dabei zu helfen, Open Source zu entdecken und zu verstehen, in ihren Aktivitäten davon zu profitieren und ihre eigenen OSPOs zu beherbergen."

#. About the OSPO Alliance
#: ../content/10_introduction.md:block 22 (paragraph)
msgid "The OSPO Alliance has established the **OSPO Alliance** website hosted at <https://ospo-alliance.org>. The OSPO Alliance serves the community with a safe place to discuss and exchange on the topics of OSPOs, and provides a repository for a comprehensive set of resources for corporations, public institutions, and research and academic organizations. The OSPO Alliance connects with OSPOs across Europe and the world as well as with supportive community organizations. It encourages best practices and fosters contribution to the sustainability of the open source ecosystem. Check out the [OSPO Alliance](https://ospo-alliance.org) website for a quick overview of complementary frameworks of IT management best practices."
msgstr "Die OSPO Alliance hat die Website **OSPO Alliance** eingerichtet, gehostet auf <https://ospo-alliance.org>. Die OSPO Alliance bietet der Community einen sicheren Ort, um über die Themen von OSPOs zu diskutieren und sich auszutauschen, und bietet ein Repository für umfassende Ressourcen für Unternehmen, öffentliche Einrichtungen, Forschungs- und akademische Organisationen. Die OSPO Alliance verbindet sich mit OSPOs in ganz Europa und der Welt sowie mit unterstützenden Community-Organisationen. Die OSPO.Zone fördert bewährte Verfahren und leistet einen Beitrag zur Nachhaltigkeit des Open-Source-Ökosystems. Auf der Website [OSPO Alliance](https://ospo-alliance.org) finden Sie einen schnellen Überblick über ergänzende Rahmenwerke für bewährte IT-Managementverfahren."

#. About the OSPO Alliance
#: ../content/10_introduction.md:block 23 (paragraph)
msgid "The [OSPO Alliance](https://ospo-alliance.org) website is also the place where we collect feedback about the initiative and its content (e.g. activities, body of knowledge) from the community at large."
msgstr "Die Website [OSPO Alliance](https://ospo-alliance.org) ist auch der Ort, an dem wir Rückmeldungen über die Initiative und ihren Inhalt (z.B. Aktivitäten, Wissensbestand) von der gesamten Gemeinschaft sammeln."

#: ../content/10_introduction.md:block 24 (header)
msgid "Translations"
msgstr "Übersetzungen"

#. Translations
#: ../../content/10_introduction.md:block 25 (paragraph)
msgid "This book was originally written in English. It is also available in French, German, Portuguese, Dutch, Italian and Spanish, thanks to an ongoing community work to translate the GGI Handbook. As progress evolves rapidly, we recommend to check out our official website for a complete list of available translations."
msgstr "Dieses Buch wurde ursprünglich auf Englisch geschrieben. Das GGI-Handbuch ist aufgrund der dauerhaften gemeinsamen Übersetzungsarbeit auch auf Französisch, Deutsch, Portugiesisch, Holländisch, Italienisch und Spanisch verfügbar. Da es sich schnell weiterentwickelt, empfehlen wir, für eine vollständige Liste der verfügbaren Übersetzungen unsere offizielle Website zu besuchen."

#. Translations
#: ../../content/10_introduction.md:block 26 (quote)
msgid "See <https://ospo-alliance.org/ggi/>"
msgstr "Siehe <https://ospo-alliance.org/ggi/>"

#. Translations
#: ../content/10_introduction.md:block 27 (paragraph)
msgid "The GGI handbook is translated using [Weblate](https://hosted.weblate.org/), an open source project and platform that offers free hosting for open source projects. We want to thank them deeply, as well as all our translation contributors. You are amazing."
msgstr "Das GGI-Handbuch wird mit [Weblate](https://hosted.weblate.org/) übersetzt, einem Open-Source-Projekt und einer Plattform, die kostenloses Hosting für Open-Source-Projekte anbietet. Wir möchten ihnen sowie allen unseren Übersetzern herzlich danken. Ihr seid unglaublich."

#. Translations
#: ../content/10_introduction.md:block 27 (quote)
msgid "See <https://hosted.weblate.org/projects/ospo-zone-ggi/#languages>"
msgstr "Siehe <https://hosted.weblate.org/projects/ospo-zone-ggi/#languages>"

#: ../../content/10_introduction.md:block 30 (paragraph)
msgid "The following great people have contributed to the Good Governance Initiative handbook:"
msgstr "Die folgenden tollen Leute haben zum Handbuch der Good Governance Initiative beigetragen:"

msgid "Contributors"
msgstr "Beitragende"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Frédéric Aatz (Microsoft France)"
msgstr "Frédéric Aatz (Microsoft France)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Boris Baldassari (Castalia Solutions, Eclipse Foundation)"
msgstr "Boris Baldassari (Castalia Solutions, Eclipse Foundation)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Philippe Bareille (Ville de Paris)"
msgstr "Philippe Bareille (Ville de Paris)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Gaël Blondelle (Eclipse Foundation)"
msgstr "Gaël Blondelle (Eclipse Foundation)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Vicky Brasseur (Wipro)"
msgstr "Vicky Brasseur (Wipro)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Philippe Carré (Nokia)"
msgstr "Philippe Carré (Nokia)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Pierre-Yves Gibello (OW2)"
msgstr "Pierre-Yves Gibello (OW2)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Michael Jaeger (Siemens)"
msgstr "Michael Jaeger (Siemens)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 30 (unordered list)
msgid "Sébastien Lejeune (Thales)"
msgstr "Sébastien Lejeune (Thales)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Max Mehl (Free Software Foundation Europe)"
msgstr "Max Mehl (Free Software Foundation Europe)"

#. Has contributed to the Good Governance Initiative
#: ../../content/10_introduction.md:block 31 (unordered list)
msgid "Catherine Nuel (OW2)"
msgstr "Catherine Nuel (OW2)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Hervé Pacault (Orange)"
msgstr "Hervé Pacault (Orange)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Stefano Pampaloni (RIOS)"
msgstr "Stefano Pampaloni (RIOS)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Christian Paterson (OpenUp)"
msgstr "Christian Paterson (OpenUp)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 26 (unordered list)
msgid "Simon Phipps (Meshed Insights)"
msgstr "Simon Phipps (Meshed Insights)"

#. Has contributed to the Good Governance Initiative
#: ../../content/10_introduction.md:block 31 (unordered list)
msgid "Silvério Santos (Orange Business)"
msgstr "Silvério Santos (Orange Business)"

#. Has contributed to the Good Governance Initiative
#: ../content/10_introduction.md:block 30 (unordered list)
msgid "Cédric Thomas (OW2)"
msgstr "Cédric Thomas (OW2)"

#. Has contributed to the Good Governance Initiative
#: ../../content/10_introduction.md:block 31 (unordered list)
msgid "Nicolas Toussaint (Orange Business)"
msgstr "Nicolas Toussaint (Orange Business)"

#. Has contributed to the Good Governance Initiative
#: ../../content/10_introduction.md:block 31 (unordered list)
msgid "Florent Zara (Eclipse Foundation)"
msgstr "Florent Zara (Eclipse Foundation)"

#. Has contributed to the Good Governance Initiative
#: ../../content/10_introduction.md:block 31 (unordered list)
msgid "Igor Zubiaurre (Bitergia)"
msgstr "Igor Zubiaurre (Bitergia)"

#: ../content/10_introduction.md:block 27 (header)
msgid "Licence"
msgstr "Lizenz"

#. When copied text from creativecommons.org, because the licence was translated there into your language, please add a citation to the URL by adding a translated "[Source: URL]" to the end of this string.
#: ../content/10_introduction.md:block 32 (paragraph)
msgid "This work is licenced under a [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) licence (CC-BY 4.0). From the Creative Commons website:"
msgstr "Dieses Werk ist lizenziert unter der [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) Lizenz (CC-BY 4.0). Aus der Creative Commons-Webseite:"

#. When copied text from creativecommons.org, because the licence was translated there into your language, please add a citation to the URL by adding a translated "[Source: URL]" to the end of this string.
#: ../content/10_introduction.md:block 29 (quote)
msgid "You are free to:"
msgstr "Sie dürfen:"

#. When copied text from creativecommons.org, because the licence was translated there into your language, please add a citation to the URL by adding a translated "[Source: URL]" to the end of this string.
#: ../content/10_introduction.md:block 29 (quote)
msgid "Share it — copy and redistribute the material in any medium or format"
msgstr "Teilen — das Material in jedwedem Format oder Medium vervielfältigen und weiterverbreiten"

#. When copied text from creativecommons.org, because the licence was translated there into your language, please add a citation to the URL by adding a translated "[Source: URL]" to the end of this string.
#: ../content/10_introduction.md:block 29 (quote)
msgid "Adapt it — remix, transform, and build upon the material"
msgstr "Bearbeiten — das Material remixen, verändern und darauf aufbauen"

#. When copied text from creativecommons.org, because the licence was translated there into your language, please add a citation to the URL by adding a translated "[Source: URL]" to the end of this string.
#: ../content/10_introduction.md:block 29 (quote)
msgid "for any purpose, even commercially."
msgstr "und zwar für beliebige Zwecke, sogar kommerziell"

#. When copied text from creativecommons.org, because the licence was translated there into your language, please add a citation to the URL by adding a translated "[Source: URL]" to the end of this string.
#: ../content/10_introduction.md:block 29 (quote)
msgid "As long as you give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use."
msgstr "Sie müssen angemessene Urheber- und Rechteangaben machen, einen Link zur Lizenz beifügen und angeben, ob Änderungen vorgenommen wurden. Diese Angaben dürfen in jeder angemessenen Art und Weise gemacht werden, allerdings nicht so, dass der Eindruck entsteht, der Lizenzgeber unterstütze gerade Sie oder Ihre Nutzung besonders. (Quelle: https://creativecommons.org/licenses/by/4.0/deed.de)"

#: ../../content/10_introduction.md:block 35 (paragraph)
msgid "All content is Copyright OSPO Alliance and others."
msgstr "Alle Inhalte sind Copyright von der OSPO Alliance und weiteren."

#~ msgid "All content is Copyright: 2020-2022 OW2 & The Good Governance Initiative participants."
#~ msgstr "Jeder Inhalt ist unter Copyright: 2020-2022 Teilnehmer des OW2 & der Good Governance Initiative."

#~ msgid "At OW2, an initiative is a joint effort to address a market need. The [OW2 OSS Good Governance Initiative](https://www.ow2.org/view/OSS_Governance) proposes a methodological framework to implement professional management of open source software within organisations."
#~ msgstr "Bei OW2 ist eine Initiative eine gemeinsame Anstrengung, um Bedürfnisse des Marktes zu decken. Die [OW2 OSS Good Governance Initiative](https://www.ow2.org/view/OSS_Governance) schlägt einen methodischen Rahmen vor, um eine professionelle Verwaltung von Open-Source-Software in Organisationen einzuführen."

#~ msgid "While the OW2 OSS Good Governance initiative is focused on developing a management methodology, the OSPO Alliance has the broader goal to help companies, particularly in non-technology sectors, and public institutions discover and understand open source, start benefiting from it across their activities and grow to host their own OSPOs."
#~ msgstr "Während sich die OW2 OSS Good Governance-Initiative auf die Entwicklung einer Verwaltungsmethodik konzentriert, verfolgt die OSPO Alliance das weiter gefasste Ziel, Unternehmen, insbesondere in nicht-technologischen Sektoren, und öffentlichen Einrichtungen dabei zu helfen, Open Source zu entdecken und zu verstehen, in ihren Aktivitäten davon zu profitieren und ihre eigenen OSPOs zu beherbergen."

#~ msgid "The OSPO Alliance has established the **OSPO.Zone** website hosted at https://ospo.zone. Based on the OW2 Good Governance Initiative, OSPO.Zone is a repository for a comprehensive set of resources for corporations, public institutions, and research and academic organizations. OSPO.Zone enables the Alliance to connect with OSPOs across Europe and the world as well as with supportive community organizations. It encourages best practices and fosters contribution to the sustainability of the open source ecosystem. Check out the [OSPO Zone](https://ospo.zone) website for a quick overview of complementary frameworks of IT management best practices."
#~ msgstr "Die OSPO Alliance hat die Website **OSPO.Zone** eingerichtet, die unter https://ospo.zone gehostet wird. Basierend auf der OW2 Good Governance Initiative ist OSPO.Zone ein Repository für umfassende Ressourcen für Unternehmen, öffentliche Einrichtungen, Forschungs- und akademische Organisationen. OSPO.Zone ermöglicht es der Allianz, mit OSPOs in ganz Europa und der Welt sowie mit unterstützenden Community-Organisationen in Kontakt zu treten. Die OSPO.Zone fördert bewährte Verfahren und leistet einen Beitrag zur Nachhaltigkeit des Open-Source-Ökosystems. Auf der Website [OSPO.Zone](https://ospo.zone) finden Sie einen schnellen Überblick über ergänzende Rahmenwerke für bewährte IT-Managementverfahren."

#~ msgid "The [OSPO Zone](https://ospo.zone) website is also the place where we collect feedback about the initiative and its content (e.g. activities, body of knowledge) from the community at large."
#~ msgstr "Die Website [OSPO Zone](https://ospo.zone) ist auch der Ort, an dem wir Rückmeldungen über die Initiative und ihren Inhalt (z.B. Aktivitäten, Wissensbestand) von der gesamten Gemeinschaft sammeln."

#~ msgid "Cédric Thomas, our master of ceremony (OW2)"
#~ msgstr "Cédric Thomas, unser Zeremonienmeister (OW2)"

#~ msgid "This work is licenced under a [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) license (CC-BY 4.0). From the Creative Commons website:"
#~ msgstr "Dieses Werk ist lizenziert unter der [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) Lizenz (CC-BY 4.0). Aus der Creative Commons-Webseite:"

#~ msgid "All content is Copyright: 2020-2021 OW2 & The Good Governance Initiative participants."
#~ msgstr "Jeder Inhalt ist unter Copyright: 2020-2021 OW2 & Teilnehmer der Good Governance Initiative."

#~ msgid "content/10_introduction.md"
#~ msgstr "content/10_introduction.md"

#, fuzzy
#~| msgid "Silvério Santos (Orange Business Services)"
#~ msgid "Silverio Santos (Orange Business Services)"
#~ msgstr "Silvério Santos (Orange Business Services)"

#, fuzzy
#~| msgid "Cédric Thomas, our master of ceremony (OW2)"
#~ msgid "Cedric Thomas, our master of ceremony (OW2)"
#~ msgstr "Cédric Thomas, unser Zeremonienmeister (OW2)"
