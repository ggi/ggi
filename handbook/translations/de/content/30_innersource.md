# InnerSource

InnerSource erfreut sich in Unternehmen immer größerer Beliebtheit, da es einen Ansatz bietet, der auf erfolgreichen Open-Source-Praktiken in den Entwicklungsteams von Unternehmen basiert. Bei InnerSource geht es jedoch nicht darum, diese Praktiken einfach zu kopieren. Sie müssen an die einzigartige Kultur und interne Organisation eines Unternehmens angepasst werden. Schauen wir uns genauer an, was InnerSource ist und was nicht und welche damit verbundene Herausforderungen es gibt.

## Was ist InnerSource?

Der Begriff wurde erstmals von Tim O'Reilly im Jahr 2000 verwendet, der feststellt, dass Innersourcing "[...] * die Verwendung von Open-Source-Entwicklungstechniken innerhalb des Unternehmens ist.*"

Laut [InnerSource Commons](https://innersourcecommons.org/learn/books/innersource-patterns/) ist die Referenzgrundlage zum Thema InnerSource die "*Verwendung von Open-Source-Prinzipien und -Praktiken für die Softwareentwicklung innerhalb der Grenzen einer Organisation.*"

## Warum InnerSource?

Laut [InnerSource Commons](https://innersourcecommons.org/learn/books/innersource-patterns/), "*kann InnerSource für Unternehmen, die hauptsächlich Closed-Source-Software entwickeln, ein großartiges Werkzeug sein, um Silos aufzubrechen, die interne Zusammenarbeit zu fördern und zu skalieren, das Onboarding neuer Ingenieure zu beschleunigen und Möglichkeiten zu identifizieren, Software in die Open-Source-Welt zurückzugeben.*"

Es ist bemerkenswert, dass die Vorteile von InnerSource sich auf verschiedene Funktionen innerhalb eines Unternehmens auswirken können, nicht nur auf die Entwicklung. Dadurch haben einige Unternehmen konkrete Vorteile in Bereichen wie z.B.:

* Juristische Funktionen: Beschleunigung des Aufbaus funktionsübergreifender Kooperationen durch den Einsatz eines gebrauchsfertigen Rechtsrahmens (InnerSource-Lizenz).
* Personalverwaltung: Verwaltung knapper Kompetenzen durch ein zentrales erfahrenes Kernteam, das für die Bündelung von Aufwand und Fachwissen verantwortlich ist.

## Die InnerSource-Kontroverse

Um InnerSource ranken sich viele Mythen, die von Kritikern geäußert werden. Dabei bietet es Unternehmen, die InnerSource intern anwenden, große Vorteile, obwohl es sich nicht um Open Source handelt. Hier sind einige dieser Mythen:

* [MYTHOS] InnerSource geht auf Kosten von OpenSource (hauptsächlich ausgehend):
   * Software-Projekte verbleiben hinter der Firewall des Unternehmens.
   * Weniger externe Beiträge zu Open Source.
* [MYTHOS] Missbrauch des Open-Source-Gedankens statt Annäherung an ihn.
* [MYTHOS] Kein InnerSource-Projekt ist jemals zu einem Open-Source-Projekt geworden.
* [MYTHOS] Der Beweggrund für InnerSource ist, dass es Open Source ähnlich ist. Aber wenn ein/e Entwickler/in darauf Wert legt, soll er/sie in Wirklichkeit immer einen direkten Beitrag zu Open Source vorziehen.

Hier sind einige Fakten über die InnerSource-Praxis, die mit den meisten der bisherigen Mythen aufräumen:

* [FAKT] InnerSource ist ein Mittel, um hauptsächlich verschlossene Unternehmen zu Open Source zu bringen.
* [FAKT] Obwohl die meisten Open-Source-Beiträge von Freiwilligen geleistet werden, kann man mit dieser Liste der "wahrgenommenen Vorteile" bei Ingenieuren für die Teilnahme an Open Source werben.
* [FAKT] In einigen (oder den meisten?) Fällen folgen Unternehmen keiner geordneten und kontrollierten Entwicklungspraxis; GGI kann bei der Verwaltung helfen.
* [FAKT] Die Umstellung von geschlossenen auf offene Lizenzen wird noch *viel* Arbeit erfordern.
* [FAKT] Es gibt in der Tat Fälle, in denen InnerSource-Projekte als Open Source angeboten werden:
   * Twitter Bootstrap.
   * Kubernetes von Google.
   * Docker von dotCloud (früherer Name von Docker Inc.).
   * React Native.
* [FAKT] Open Source profitiert von der wachsenden Zahl an Softwareingenieuren, die sich mit Open-Source-Verfahren vertraut machen, da diese den Verfahren von InnerSource sehr ähnlich sind.

## Wer macht das?

Viele Unternehmen haben InnerSource-Initiativen oder ISPOs (InnerSource Program Offices) ins Leben gerufen, einige von ihnen schon seit langem, andere erst kürzlich. Hier ist eine unvollständige Liste hauptsächlich europäischer Unternehmen:

* Banco de Santander ([Quelle](https://patterns.innersourcecommons.org/p/innersource-portal))
* BBC ([Quelle](https://www.youtube.com/watch?v=pEGMxe6xz-0))
* Bosch ([Quelle](https://web.archive.org/web/20230429145619/https://www.bosch.com/research/know-how/open-and-inner-source/))
* Comcast ([Quelle](https://www.youtube.com/watch?v=msD-8-yrGfs&t=6s))
* Ericsson ([Quelle](https://innersourcecommons.org/learn/books/adopting-innersource-principles-and-case-studies/))
* Engie ([Quelle](https://github.com/customer-stories/engie))
* IBM ([Quelle](https://resources.github.com/innersource/fundamentals/))
* Mercedes ([Quelle](https://www.youtube.com/watch?v=hVcGABbmI4Y))
* Microsoft ([Quelle](https://www.youtube.com/watch?v=eZdx5MQCLA4))
* Nike ([Quelle](https://www.youtube.com/watch?v=srPG-Tq0HIs&list=PLq-odUc2x7i-A0sOgr-5JJUs5wkgdiXuR&index=46))
* Nokia ([Quelle](https://www.nokia.com/thought-leadership/articles/openness/openness-drives-innovation/))
* SNCF Connect & Tech ([Quelle 1](https://twitter.com/FrancoisN0/status/1645356213712846853), [Quelle 2](https://www.slideshare.net/FrancoisN0/opensource-innersource-pour-acclrer-les-dveloppements))
* Paypal ([Quelle](https://innersourcecommons.org/fr/learn/books/getting-started-with-innersource/))
* Philips ([Quelle](https://medium.com/philips-technology-blog/how-philips-used-innersource-to-maintain-its-edge-in-innovation-38c481e6fa03))
* Renault ([Quelle](https://www.youtube.com/watch?v=aCbv46TfanA))
* SAP ([Quelle](https://community.sap.com/topics/open-source/publications))
* Siemens ([Quelle](https://jfrog.com/blog/creating-an-inner-source-hub-at-siemens))
* Société Générale ([Quelle](https://github.com/customer-stories/societe-generale))
* Thales ([Quelle](https://www.youtube.com/watch?v=aCbv46TfanA))
* VeePee ([Quelle](https://about.gitlab.com/customers/veepee))

## InnerSource Commons, eine grundlegende Referenz

Eine aktive und dynamische Gemeinschaft von InnerSource-Praktikern, die nach Open Source Prinzipien arbeitet, finden Sie unter [InnerSource Commons](https://innersourcecommons.org). Sie stellen viele nützliche Ressourcen zur Verfügung, um sich in die Materie einzuarbeiten, darunter [Muster](https://innersourcecommons.org/learn/patterns/), einen [Lernpfad](https://innersourcecommons.org/de/learn/learning-path/) und kleine E-Books:

* [Getting Started with InnerSource](https://innersourcecommons.org/learn/books/getting-started-with-innersource/) von Andy Oram.
* [Understanding the InnerSource Checklist](https://innersourcecommons.org/learn/books/understanding-the-innersource-checklist/) von Silona Bonewald.

## Unterschiede in der Verwaltung von InnerSource

InnerSource bringt spezifische Herausforderungen mit sich, die bei Open Source nicht gegeben sind. Doch die meisten Unternehmen, die private Software entwickeln, haben diese Herausforderungen bereits bewältigt:

* Dedizierte, unternehmensspezifische Lizenz für Innersource-Projekte (für große Unternehmen mit mehreren juristischen Einheiten).
* Der öffentliche Charakter von Open Source bewahrt sie vor Herausforderungen bei Transferpreisen. Der private Charakter von InnerSource setzt Unternehmen, die in verschiedenen Rechtsordnungen tätig sind, der Haftung für Gewinnverlagerungen aus.
* Die Beweggründe für Beiträge sind sehr unterschiedlich:
   * InnerSource hat einen kleineren Stamm möglicher Mitwirkender, weil es auf eine Organisation beschränkt ist.
   * Wer seine beruflichen Fähigkeiten unter Beweis stellen will, muss einen Beitrag leisten. InnerSource beschränkt diesen Einfluss auf die Grenzen einer Organisation.
   * Der Beitrag zur Verbesserung der Gesellschaft ist eine weitere Triebfeder für Beiträge, der bei InnerSource begrenzt ist.
   * Die Motivation muss daher stärker sein und wird sich mehr auf Belohnungen und Zuweisungen stützen.
   * Der Umgang mit der Angst vor Perfektionismus, wie dem Hochstapler-Syndrom, ist in InnerSource aufgrund der begrenzten Sichtbarkeit des Codes einfacher.
* Die Auslagerung von Arbeitskräften wird immer häufiger, was sich in mehrfacher Hinsicht auf die Verwaltung auswirkt.
* Die Bewertung der Eignung für das Unternehmen ist bei InnerSource einfacher, da sie intern entwickelt wird.
* Die Auffindbarkeit wird zu einem Problem. Die Indexierung von Informationen ist für Unternehmen weniger wichtig. Öffentliche Suchmaschinen wie DuckDuckGo, Google oder Bing leisten eine viel bessere Arbeit, die InnerSource nicht nutzen kann.
* Da es intern bleibt, ist InnerSource in einer etwas besseren Position, den Export zu kontrollieren.
* Es ist eine Kontrolle der Weitergabe von geistigem Eigentum nach außen in Form von Quellcode erforderlich.

InnerSource entwickelt sich ständig weiter, da immer mehr Unternehmen die Prinzipien übernehmen und ihre Erfahrungen weitergeben. Eine spätere Version dieses Handbuchs wird eine Liste der Aktivitäten der GGI enthalten, die für InnerSource-Praktiker relevant sind.
