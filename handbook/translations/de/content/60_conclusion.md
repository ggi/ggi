# Fazit

Wie wir bereits gesagt haben, ist Open Source Good Governance kein Ziel; es ist eine Reise. Wir müssen uns um unsere gemeinsamen Güter kümmern, um die Gemeinschaften und das Ökosystem, die es antreiben, denn unser eigener gemeinsamer und damit individueller Erfolg hängt davon ab.

**Wir** als Softwarepraktiker und Open-Source-Enthusiasten verpflichten uns, das Handbuch der Good Governance Initiative weiter zu verbessern und an seiner Verbreitung und Reichweite zu arbeiten. Wir sind fest davon überzeugt, dass Organisationen, Einzelpersonen und Gemeinschaften Hand in Hand arbeiten müssen, um eine bessere und größere Reihe von Gemeingütern aufzubauen, die für alle verfügbar und vorteilhaft sind.

**Sie** sind herzlich eingeladen, der OSPO Alliance beizutreten, zu unserer Arbeit beizutragen, die Message zu verbreiten und Botschafter eines besseren Open-Source-Bewusstseins und einer besseren Governance in Ihrem eigenen Ökosystem zu sein. Es gibt eine breite Palette von Ressourcen, von Blogposts und Forschungsartikeln bis hin zu Konferenzen und Online-Schulungen. Wir stellen auch eine Reihe nützlicher Materialien auf [unserer Website](https://ospo-alliance.org) zur Verfügung und wir sind gerne bereit, so viel wie möglich zu helfen.

**Lasst uns gemeinsam die Zukunft der Good-Governance-Initiative definieren und aufbauen!**

## Kontakt

Der bevorzugte Weg, mit der OSPO Alliance in Kontakt zu treten, ist eine Nachricht auf unserer öffentlichen Mailingliste unter <https://accounts.eclipse.org/mailing-list/ospo.zone>. Sie können auch mit uns auf den üblichen Open-Source-Veranstaltungen diskutieren, an unseren monatlichen OSPO OnRamp-Webinaren teilnehmen oder mit einem Mitglied in Kontakt treten - sie werden Sie freundlicherweise an die richtige Person weiterleiten.

## Anhang: Vorlage für eine maßgeschneiderte Aktivitäts-Scorecard

Die neueste Version der Vorlage für die maßgeschneiderte Aktivitäts-Scorecard ist im Abschnitt `Ressourcen` des [Good Governance Initiative GitLab](https://gitlab.ow2.org/ggi/ggi) unter OW2 verfügbar.
