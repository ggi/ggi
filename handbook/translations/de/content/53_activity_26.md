## Zu Open-Source-Projekten beitragen

Aktivitäts-ID: [GGI-A-26](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_26.md).

### Beschreibung

Der Beitrag zu Open-Source-Projekten, die frei genutzt werden können, ist eines der wichtigsten Prinzipien der Good Governance. Es geht darum, nicht einfach ein passiver Verbraucher zu sein, sondern dem Projekt etwas zurückzugeben. Wenn jemand eine Funktion hinzufügt oder einen Fehler für seine eigenen Zwecke behebt, sollte er dies allgemein genug tun, um zum Projekt beizutragen. Den Entwicklern muss Zeit für ihre Beiträge eingeräumt werden.

Diese Aktivität deckt den folgenden Bereich ab:

- Die Arbeit mit Upstream-Open-Source-Projekten.
- Meldung von Fehlern und Funktionswünschen.
- Einbringen von Code und Fehlerkorrekturen.
- Teilnahme an Mailinglisten der Community.
- Austausch von Erfahrungen.

### Chancenbewertung

Die wichtigsten Vorteile dieser Aktivität sind:

- Sie erhöht das Allgemeinwissen und das Engagement für Open Source innerhalb des Unternehmens, da die Mitarbeiter beginnen, Beiträge zu leisten und sich an Open-Source-Projekten zu beteiligen. Sie erhalten ein Gefühl der Gemeinnützigkeit und verbessern ihren eigenen Ruf.
- Das Unternehmen erhöht seinen Bekanntheitsgrad und sein Ansehen, da die Beiträge durch das Projekt wandern. Dies zeigt, dass das Unternehmen tatsächlich an Open Source beteiligt ist, einen Beitrag leistet und Fairness und Transparenz fördert.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Es gibt eine klare und offizielle Methode für diejenigen, die einen Beitrag leisten wollen.
- [ ] Entwickler werden ermutigt, zu Open-Source-Projekten beizutragen, die sie nutzen.
- [ ] Es gibt ein Verfahren, das die Einhaltung von Rechtsvorschriften und die Sicherheit der Beiträge von Entwicklern gewährleistet.
- [ ] Leistungskennzahlen: Umfang der externen Beiträge (Code, Mailinglisten, Probleme, ...) von Einzelpersonen, Teams oder Unternehmen.

### Werkzeuge

Es kann sinnvoll sein, die Beiträge zu verfolgen, sowohl um den Überblick über die Beiträge zu behalten als auch, um über die Bemühungen des Unternehmens berichten zu können. Zu diesem Zweck können Dashboards und Software zur Aktivitätsverfolgung eingesetzt werden. Prüfen Sie:

- Bitergias [GrimoireLab](https://chaoss.github.io/grimoirelab/)
- [ScanCode](https://scancode-toolkit.readthedocs.io)

### Empfehlungen

Ermutigen Sie die Mitarbeiter des Unternehmens, zu externen Projekten beizutragen, indem Sie:

- Ihnen Zeit geben, um allgemeine, gut getestete Fehlerkorrekturen und Funktionen zu schreiben und sie der Gemeinschaft zurückzugeben.
- Schulung der Mitarbeiter in Bezug auf Beiträge zu Open-Source-Communities. Dabei geht es sowohl um technische Fähigkeiten (Verbesserung der Kenntnisse Ihres Teams) als auch um die Gemeinschaft (Zugehörigkeit zu den Open-Source-Communities, Verhaltenskodex, usw.).
- Bieten Sie Schulungen zu rechtlichen und technischen Fragen sowie die des geistigen Eigentums an und stellen Sie innerhalb des Unternehmens einen Ansprechpartner zur Verfügung, der bei diesen Themen helfen kann, wenn die Mitarbeiter Zweifel haben.
- Bieten Sie Anreize für veröffentlichte Arbeiten.
- Beachten Sie, dass die Beiträge des Unternehmens/der Einrichtung die Qualität des Codes und die Beteiligung widerspiegeln, stellen Sie also sicher, dass Ihr Entwicklungsteam einen ausreichend guten Code liefert.

### Hilfsmittel

- Die [CHAOSS](https://chaoss.community/) Initiative der Linux Foundation bietet Werkzeuge und Hinweise zur Verfolgung von Beiträgen in der Entwicklung.

### Empfohlene nächste Aktivitäten

- [GGI-A-31 - Öffentliche Darstellung der Verwendung von Open Source](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Nachdem es nun einen öffentlich sichtbaren Beitrag und ein Engagement der Organisation gibt, sollten Sie damit beginnen, darüber zu reden!
- [GGI-A-24 - Verwalten von Kennzahlen](https://ospo-alliance.org/ggi/activities/manage_key_indicators) Machen Sie den Beitrag zu OSS-Projekten sichtbar und messbar. Dies wird zur Verbreitung der Initiative beitragen und die Moral steigern.
- [GGI-A-27 - Gehören Sie zur Open-Source-Community](https://ospo-alliance.org/ggi/activities/belong_to_the_open_source_community) Zur OSS-Gemeinschaft beizutragen ist der erste Schritt, um ein Teil von ihr zu werden. Sobald man anfängt, beizutragen, wird man stärker in das Wohlergehen und die Verwaltung des Projekts einbezogen und kann schließlich zu einem Betreuer werden, der ein nachhaltiges und gesundes Projekt und einen Fahrplan gewährleistet.
- [GGI-A-29 - In Open-Source-Projekten engagieren](https://ospo-alliance.org/ggi/activities/engage_with_open_source_projects) Open-Source-Projekte legen Wert auf Meritokratie. Nachdem Sie nun bewiesen haben, dass Sie den Code und die Prozesse gut verstehen, können Sie sich in das Projekt einbringen und Ihre Beiträge offizieller machen.
- [GGI-A-36 - Open Source ermöglicht Innovation](https://ospo-alliance.org/ggi/activities/open_source_enabling_innovation) Die Mitwirkung an OSS-Projekten und die Interaktion mit externen Mitwirkenden ist ein Mittel zur Förderung der Innovation.
- [GGI-A-39 - Upstream first](https://ospo-alliance.org/ggi/activities/upstream_first) Ein Beitrag zu OSS-Projekten ist dann sinnvoll, wenn Aktualisierungen regelmäßig und systematisch im Upstream-Projekt zur Verfügung gestellt werden.
