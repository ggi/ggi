## Geschäftliche Open-Source-Software

Aktivitäts-ID: [GGI-A-20](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_20.md).

### Beschreibung

Bei dieser Aktivität geht es um die vorausschauende Auswahl von OSS-Lösungen, die entweder von Anbietern oder von der Gemeinschaft unterstützt werden, in geschäftsorientierten Bereichen. Sie kann auch die Festlegung von Vorzugsrichtlinien für die Auswahl von Open-Source-Anwendungssoftware für Unternehmen umfassen.

Während Open-Source-Software am häufigsten von IT-Fachleuten verwendet wird - Betriebssysteme, Middleware, DBMS, Systemverwaltung, Entwicklungswerkzeuge - ist sie in Bereichen, in denen Geschäftsleute die Hauptanwender sind, noch nicht anerkannt.

Die Aktivität betrifft Bereiche wie: Office-Suiten, Umgebungen für die Zusammenarbeit, Benutzerverwaltung, Workflow-Management Verwaltung von Arbeitsabläufen, Verwaltung von Kundenbeziehungen, E-Mail, elektronischer Geschäftsverkehr, usw.

### Chancenbewertung

In dem Maße, in dem Open Source zum Regelfall wird, reicht es weit über Betriebssysteme und Entwicklungswerkzeuge hinaus und findet zunehmend seinen Weg in den oberen Schichten der Informationssysteme, bis hin zu den Geschäftsanwendungen. Es ist wichtig zu ermitteln, welche OSS-Anwendungen erfolgreich eingesetzt werden, um die Anforderungen des Unternehmens zu erfüllen und wie sie zur bevorzugten Wahl eines Unternehmens werden können, um Kosten zu sparen.

Die Maßnahme kann Umschulungs- und Umstellungskosten mit sich bringen.

### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität:

- [ ] Es gibt eine Liste von empfohlenen OSS-Lösungen, um anstehende Bedürfnisse bei Geschäftsanwendungen zu erfüllen.
- [ ] Es wurde eine Vorzugsregelung für die Auswahl von quelloffener Geschäftsanwendungssoftware ausgearbeitet.
- [ ] Proprietäre Geschäftsanwendungen werden im Vergleich zu OSS-Entsprechungen bewertet.
- [ ] Im Beschaffungsprozess und in Ausschreibungen wird Open-Source-Lösungen der Vorzug gegeben (sofern rechtlich möglich).

### Werkzeuge

Zum jetzigen Zeitpunkt können wir uns kein Werkzeug vorstellen, das für diese Tätigkeit von Bedeutung oder von ihr betroffen ist.

### Empfehlungen

- Sprechen Sie mit Kollegen, lernen Sie von dem, was andere Unternehmen tun, die mit Ihrem vergleichbar sind.
- Besuchen Sie lokale Branchenveranstaltungen, um sich über OSS-Lösungen und professionelle Unterstützung zu informieren.
- Testen Sie zunächst Community-Editionen und Community-Unterstützung, bevor Sie sich für kostenpflichtige Unterstützungspakete entscheiden.

### Hilfsmittel

- [What is enterprise open source?](https://www.redhat.com/de/blog/what-enterprise-open-source): Eine kurze Lektüre über unternehmenstaugliche Open-Source-Lösungen.
- [101 Open Source Apps to Help your Business Thrive](https://digital.com/creating-an-llc/open-source-business/): Eine unverbindliche Liste von geschäftsorientierten Open-Source-Lösungen.

### Empfohlene nächste Aktivitäten

- [GGI-A-33 - Mit Open-Source-Anbietern zusammenarbeiten](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) Stärken Sie das Vertrauen in Ihre Open-Source-Assets, indem Sie sich mit Open-Source-Experten austauschen.
- [GGI-A-43 - Open-Source-Beschaffungspolitik](https://ospo-alliance.org/ggi/activities/open_source_procurement_policy) Die Nutzung von OSS Enterprise lässt sich optimieren, wenn man weiß, welche Ressourcen bereits vorhanden sind und, wenn man eine klare Beschaffungspolitik in diesem Bereich verfolgt.
