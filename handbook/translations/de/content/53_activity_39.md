## Upstream first

Aktivitäts-ID: [GGI-A-39](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_39.md).

### Beschreibung

Bei dieser Aktivität geht es um die Entwicklung eines Bewusstseins für die Vorteile von Beiträgen und die Durchsetzung des Upstream-First-Prinzips.

Beim Upstream-First-Prinzips müssen alle Entwicklungen an einem Open-Source-Projekt in der Qualität und Offenheit erfolgen, die erforderlich sind, um den Kernentwicklern eines Projekts vorgelegt und von diesen veröffentlicht zu werden.

### Chancenbewertung

Das Schreiben von Code mit Blick auf den Upstream führt zu:

- einer besseren Qualität des Codes,
- Code, der bereit ist, upstream eingereicht zu werden,
- Code, der in die Kernsoftware eingebunden wird,
- Code, der mit zukünftigen Versionen kompatibel sein wird,
- Anerkennung durch die Projektgemeinschaft und eine bessere und profitablere Zusammenarbeit.

> "Upstream First ist mehr als nur "nett sein". Es bedeutet, dass Sie ein Mitspracherecht bei dem Projekt haben. Es bedeutet Vorhersehbarkeit. Es bedeutet, dass Sie die Kontrolle haben. Es bedeutet, dass Sie agieren und nicht reagieren. Es bedeutet, dass Sie Open Source verstehen."
>
> &mdash; <cite>[Maximilian Michels](https://maximilianmichels.com/2021/upstream-first/)</cite>


### Fortschrittsbeurteilung

Die folgenden **Kontrollpunkte** zeigen den Fortschritt in dieser Aktivität: Ist Upstream First umgesetzt?

- [ ] Erheblicher Anstieg der Zahl der Pull-/Merge-Anträge, die bei Drittprojekten eingereicht werden.
- [ ] Es wurde eine Liste von Drittprojekten erstellt, für die zuerst ein Upstream-Antrag gestellt werden muss.

### Empfehlungen

- Ermittlung der Entwickler mit der größten Erfahrung in der Zusammenarbeit mit Upstream-Entwicklern.
- Erleichterung der Interaktion zwischen Entwicklern und Kernentwicklern (Veranstaltungen, Hackathons, usw.)

### Hilfsmittel

- Eine klare Erklärung des Upstream-First-Prinzips und warum es in das Kulturziel passt: <https://maximilianmichels. com/2021/upstream-first/>.

> Upstream First bedeutet, dass Sie jedes Mal, wenn Sie ein Problem in Ihrer Kopie des Upstream-Codes lösen, von dem andere profitieren könnten, tragen Sie diese Änderungen zum Upstream bei, d.h. Sie senden einen Patch oder öffnen eine Pull-Anfrage an das Upstream-Repository.

- [What is Upstream and Downstream in Software Development?](https://reflectoring.io/upstream-downstream/) Eine glasklare Erklärung.
- Erläutert über die Chromium OS Design Dokumenten: [Upstream First](https://www.chromium.org/chromium-os/chromiumos-design-docs/upstream-first).
- Red Hat über Upstream und die Vorteile von [Upstream First](https://www.redhat.com/en/blog/what-open-source-upstream).

### Empfohlene nächste Aktivitäten

- [GGI-A-25 - Förderung bewährter Verfahrensweisen bei der Open-Source-Entwicklung](https://ospo-alliance.org/ggi/activities/promote_open_source_development_best_practices) Der Beitrag zum Upstream ist eine wichtige bewährte Verfahrensweise bei Open Source. Machen Sie dies auch zu einem Teil der bewährten Verfahrensweisen Ihres Unternehmens, denn es hilft bei externen Beiträgen, interner Gesamtqualität und Wissensaustausch.
