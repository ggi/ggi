#
msgid ""
msgstr ""
"PO-Revision-Date: 2024-06-12 18:09+0000\n"
"Last-Translator: Paolo Vecchi <paologithub@omniscloud.eu>\n"
"Language-Team: Italian <https://hosted.weblate.org/projects/ospo-zone-ggi/54_activity_33/it/>\n"
"Language: it\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.6-dev\n"

#. Activity's title
#: ../content/54_activity_33.md:block 1 (header)
msgid "Engage with open source vendors"
msgstr "Gestire le relazioni con i fornitori open source"

#. Just translate "Activity ID:" and leave the activty's code as it is
#: ../content/54_activity_33.md:block 2 (paragraph)
msgid "Activity ID: [GGI-A-33](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_33.md)."
msgstr "ID attività: [GGI-A-33](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_33.md)."

#: ../content/54_activity_33.md:block 3 (header)
msgid "Description"
msgstr "Descrizione"

#. Description
#: ../content/54_activity_33.md:block 4 (paragraph)
msgid "Secure contracts with open source vendors that provide software critical to you. Companies and entities that produce open source software need to thrive to provide maintenance and development of new features. Their specific expertise is required on the project, and the community of users relies on their continued business and contributions."
msgstr "Acquisite contratti con i fornitori di software open source che forniscono software di importanza cruciale per voi. Le aziende e le entità che producono software open source devono prosperare per garantire la manutenzione e lo sviluppo di nuove funzionalità. Le loro competenze specifiche sono necessarie per il progetto e la comunità di utenti fa affidamento sulla continuità delle loro attività e sui loro contributi."

#: ../content/54_activity_33.md:block 5 (paragraph)
msgid "Engaging with open source vendors takes several forms:"
msgstr "L'impegno con i fornitori open source assume diverse forme:"

#. Forms to engage with open source vendors
#: ../content/54_activity_33.md:block 6 (unordered list)
msgid "Subscribing support plans."
msgstr "Sottoscrivere piani di supporto."

#. Forms to engage with open source vendors
#: ../content/54_activity_33.md:block 6 (unordered list)
msgid "Contracting local service companies."
msgstr "Appaltare i servizi a società locali."

#. Forms to engage with open source vendors
#: ../content/54_activity_33.md:block 6 (unordered list)
msgid "Sponsoring developments."
msgstr "Sponsorizzare lo sviluppo software."

#. Forms to engage with open source vendors
#: ../content/54_activity_33.md:block 6 (unordered list)
msgid "Paying for a commercial licence."
msgstr "Pagare per servizi commerciali aggiuntivi."

#. Description
#: ../content/54_activity_33.md:block 7 (paragraph)
msgid "This activity implies considering open source projects as fully-featured products worth paying for, much like any proprietary products -- although usually far less expensive."
msgstr "Questa attività implica la considerazione dei progetti open source come se fossero prodotti con un preciso valore economico, come i software proprietari, ma con costi generalmente nettamente inferiori."

#: ../content/54_activity_33.md:block 8 (header)
msgid "Opportunity Assessment"
msgstr "Valutazione delle opportunità"

#. Opportunity Assessment
#: ../content/54_activity_33.md:block 9 (paragraph)
msgid "The objective of this activity is to ensure professional support of open source software used in the organisation. It has several benefits:"
msgstr "L'obiettivo di questa attività è garantire un supporto professionale al software open source utilizzato nell'organizzazione. Presenta diversi vantaggi:"

#. Opportunity Assessment: benefits of professional support of open source software
#: ../content/54_activity_33.md:block 10 (unordered list)
msgid "Continuity of service through timely bug fixes."
msgstr "Continuità del servizio grazie alla tempestiva correzione dei bug."

#. Opportunity Assessment: benefits of professional support of open source software
#: ../content/54_activity_33.md:block 10 (unordered list)
msgid "Service performance through optimised installation."
msgstr "Prestazioni di servizio grazie all'ottimizzazione dell'installazione."

#. Opportunity Assessment: benefits of professional support of open source software
#: ../content/54_activity_33.md:block 10 (unordered list)
msgid "Clarification of the legal/commercial status of the software used."
msgstr "Chiarimento dello status legale/commerciale del software utilizzato."

#. Opportunity Assessment: benefits of professional support of open source software
#: ../content/54_activity_33.md:block 10 (unordered list)
msgid "Access to early information."
msgstr "Accesso alle nuove informazioni."

#. Opportunity Assessment: benefits of professional support of open source software
#: ../content/54_activity_33.md:block 10 (unordered list)
msgid "Stable budget forecast."
msgstr "Previsioni di bilancio stabili."

#. Opportunity Assessment
#: ../content/54_activity_33.md:block 11 (paragraph)
msgid "The cost is obviously that of the support plans selected. Another cost might be to depart from bulk outsourcing to large systems integrators in favour of fine-grained contracting with expert SMEs."
msgstr "Il costo è ovviamente quello dei piani di supporto scelti. Un altro costo potrebbe essere quello di abbandonare l'outsourcing di massa a grandi integratori di sistemi a favore di contratti di precisione con PMI esperte."

#: ../content/54_activity_33.md:block 12 (header)
msgid "Progress Assessment"
msgstr "Valutazione dei progressi"

#. Progress Assessment
#: ../content/54_activity_33.md:block 13 (paragraph)
msgid "The following **verification points** demonstrate progress in this activity:"
msgstr "I seguenti **punti di verifica** dimostrano i progressi in questa attività:"

#. Progress assessment verification point
#: ../content/54_activity_33.md:block 14 (unordered list)
msgid "Open source used in the organisation is backed by commercial support."
msgstr "L'open source utilizzato nell'organizzazione è sostenuto da un supporto commerciale."

#. Progress assessment verification point
#: ../content/54_activity_33.md:block 14 (unordered list)
msgid "Support plans for some open source projects have been contracted."
msgstr "Sono stati stipulati piani di supporto per alcuni progetti open source."

#. Progress assessment verification point
#: ../content/54_activity_33.md:block 14 (unordered list)
msgid "Cost of open source support plans is a legitimate entry in the IT budget."
msgstr "Il costo dei piani di supporto open source è una voce legittima del budget IT."

#: ../content/54_activity_33.md:block 15 (header)
msgid "Recommendations"
msgstr "Raccomandazioni"

#. Recommendation
#: ../content/54_activity_33.md:block 16 (unordered list)
msgid "Whenever possible, find local expert SMEs."
msgstr "Quando possibile, trovare PMI esperte a livello locale."

#. Recommendation
#: ../content/54_activity_33.md:block 16 (unordered list)
msgid "Beware of large systems integrators reselling third-party expertise (reselling support plans that expert open source SMEs actually provide)."
msgstr "Diffidate dei grandi integratori di sistemi che rivendono competenze di terzi (rivendendo piani di assistenza che in realtà sono forniti da PMI esperte di open source)."

#: ../content/54_activity_33.md:block 17 (header)
msgid "Resources"
msgstr "Risorse"

#. Resource
#: ../content/54_activity_33.md:block 18 (paragraph)
msgid "A couple of links illustrating the commercial reality of open source software:"
msgstr "Un paio di link che illustrano la realtà commerciale del software open source:"

#. Resource
#: ../content/54_activity_33.md:block 19 (unordered list)
msgid "[A quick read to understand commercial open source](https://www.webiny.com/blog/what-is-commercial-open-source)."
msgstr "[Una lettura veloce per capire l'open source commerciale](https://www.webiny.com/blog/what-is-commercial-open-source)."

#~ msgid "[An investor's view of the community to business evolution of open source projects](https://a16z.com/2019/10/04/commercializing-open-source/)."
#~ msgstr "[Il punto di vista di un investitore sull'evoluzione della comunità verso il business dei progetti open source](https://a16z.com/2019/10/04/commercializing-open-source/)."
