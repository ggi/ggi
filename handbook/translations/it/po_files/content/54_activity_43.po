# Benedetto Padula <bennypadula@gmail.com>, 2023.
# Antonio Conti <antonioc@seacom.it>, 2023.
# Sébastien Lejeune <sebastien.lejeune@thalesgroup.com>, 2023.
# Paolo Vecchi <paologitlab@omniscloud.eu>, 2024.
# Paolo Vecchi <paologithub@omniscloud.eu>, 2024.
msgid ""
msgstr ""
"PO-Revision-Date: 2024-06-12 18:09+0000\n"
"Last-Translator: Paolo Vecchi <paologithub@omniscloud.eu>\n"
"Language-Team: Italian <https://hosted.weblate.org/projects/ospo-zone-ggi/54_activity_43/it/>\n"
"Language: it\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.6-dev\n"

#. Activity's title
#: ../content/54_activity_43.md:block 1 (header)
msgid "Open source procurement policy"
msgstr "Politiche di acquisizione di software open source"

#. Just translate "Activity ID:" and leave the activty's code as it is
#: ../content/54_activity_43.md:block 2 (paragraph)
msgid "Activity ID: [GGI-A-43](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_43.md)."
msgstr "Attività ID: [GGI-A-43](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_43.md)."

#: ../content/54_activity_43.md:block 3 (header)
msgid "Description"
msgstr "Descrizione"

#. Description
#: ../content/54_activity_43.md:block 4 (paragraph)
msgid "This activity is about implementing a process to select, acquire, purchase open source software and services. It is also about considering the actual cost of open source software and provisioning for it. OSS may be \"free\" at first sight, but it is not without internal and external costs such as integration, training, maintenance and support."
msgstr "Questa attività riguarda l'implementazione di un processo di selezione, acquisizione e acquisto di software e servizi open source. Si tratta anche di considerare il costo effettivo del software open source e di provvedere alla sua fornitura. L'OSS può essere \"gratuito\" ad una prima analisi, ma non è esente da costi interni ed esterni come l'integrazione, la formazione, la manutenzione e il supporto."

#. Description
#: ../content/54_activity_43.md:block 5 (paragraph)
msgid "Such policy requires that both open source and proprietary solutions are symmetrically considered when evaluating value for money as the optimum combination of the total cost of ownership and quality. Therefore, the IT Procurement department should actively and fairly consider open source options, while at the same time ensuring proprietary solutions are considered on an equal footing in purchasing decisions."
msgstr "Queste policy richiedono che sia le soluzioni open source che quelle proprietarie siano considerate simmetricamente quando si valuta il rapporto qualità-prezzo come la combinazione ottimale tra costo totale di proprietà e qualità. Pertanto, l'ufficio acquisti IT deve considerare attivamente ed equamente le opzioni open source, assicurando al contempo che le soluzioni proprietarie siano considerate sullo stesso livello nelle decisioni di acquisto."

#. Description
#: ../content/54_activity_43.md:block 6 (paragraph)
msgid "Open source preference can be explicitly stated based on the intrinsic flexibility of the open source option when there is no significant overall cost difference between proprietary and open source solutions."
msgstr "La preferenza per l'open source può essere esplicitamente dichiarata sulla base della flessibilità intrinseca dell'opzione open source quando non c'è una significativa differenza del costo complessivo tra le soluzioni proprietarie e quelle open source."

#. Description
#: ../content/54_activity_43.md:block 7 (paragraph)
msgid "Procurement departments must understand that companies offering support for OSS typically lack the commercial resources to participate in procurement competitions, and adapt their open source procurement policies and processes accordingly."
msgstr "I dipartimenti di approvvigionamento (Procurement departments) devono capire che le aziende che offrono supporto per gli OSS di solito non dispongono di risorse commerciali sufficienti per partecipare alle gare d'appalto e devono adattare di conseguenza le loro politiche e i loro processi di approvvigionamento per l'open source."

#: ../content/54_activity_43.md:block 8 (header)
msgid "Opportunity Assessment"
msgstr "Valutazione delle opportunità"

#. Opportunity Assessment
#: ../content/54_activity_43.md:block 9 (paragraph)
msgid "Several reasons justify the efforts to set up specific open source procurement policies:"
msgstr "Diverse ragioni giustificano gli sforzi per istituire specifiche politiche di approvvigionamento dell'open source:"

#. Reason justifying the efforts to set up specific open source procurement policies
#: ../content/54_activity_43.md:block 10 (unordered list)
msgid "Supply of commercial open source software and services is growing and cannot be ignored, and requires the implementation of dedicated procurement policies and processes."
msgstr "L'offerta di software e servizi commerciali open source è in crescita e non può essere ignorata, e richiede l'implementazione di politiche e processi di approvvigionamento dedicati."

#. Reason justifying the efforts to set up specific open source procurement policies
#: ../content/54_activity_43.md:block 10 (unordered list)
msgid "There is a growing supply of highly competitive commercial open source business solutions for corporate information systems."
msgstr "Esiste una crescente offerta di soluzioni commerciali open source altamente competitive per i sistemi informativi aziendali."

#. Reason justifying the efforts to set up specific open source procurement policies
#: ../content/54_activity_43.md:block 10 (unordered list)
msgid "Even after adopting a free-of-charge OSS component and integrating it into an application, internal or external resources must be provided to maintain that source code."
msgstr "Anche dopo aver adottato un componente OSS gratuito e averlo integrato in un'applicazione, è necessario fornire risorse interne o esterne per la manutenzione del codice sorgente."

#. Reason justifying the efforts to set up specific open source procurement policies
#: ../content/54_activity_43.md:block 10 (unordered list)
msgid "Total Cost of Ownership (TCO) is often (although not necessarily) lower for FOSS solutions: no licence fees to pay when purchasing/upgrading, open market for service providers, option to provide some or all of the solution yourself."
msgstr "Il costo totale di proprietà (TCO) è spesso (anche se non necessariamente) inferiore per le soluzioni FOSS: nessun costo di licenza da pagare al momento dell'acquisto/aggiornamento, mercato aperto per i fornitori di servizi, possibilità di fornire una parte o tutta la soluzione da soli."

#: ../content/54_activity_43.md:block 11 (header)
msgid "Progress Assessment"
msgstr "Valutazione dei progressi"

#. Progress Assessment
#: ../content/54_activity_43.md:block 12 (paragraph)
msgid "The following **verification points** demonstrate progress in this activity:"
msgstr "I seguenti **punti di verifica** dimostrano i progressi in questa attività:"

#. Progress assessment verification point
#: ../content/54_activity_43.md:block 13 (unordered list)
msgid "New call for proposals proactively request open source submissions."
msgstr "Nuove Call for Proposals (proposte di nuove iniziative) richiedono in modo proattivo la presentazione di contributi open source."

#. Progress assessment verification point
#: ../content/54_activity_43.md:block 13 (unordered list)
msgid "Procurement department has a way to evaluate open source vs proprietary solutions."
msgstr "Il reparto acquisti ha un modo per valutare le soluzioni open source rispetto a quelle proprietarie."

#. Progress assessment verification point
#: ../content/54_activity_43.md:block 13 (unordered list)
msgid "A simplified procurement process for open source software and services has been implemented and documented."
msgstr "È stato implementato e documentato un processo di approvvigionamento semplificato per software e servizi open source."

#. Progress assessment verification point
#: ../content/54_activity_43.md:block 13 (unordered list)
msgid "An approval process drawing from cross-functional expertise has been defined and documented."
msgstr "È stato definito e documentato un processo di approvazione che si avvale di competenze interfunzionali."

#: ../content/54_activity_43.md:block 14 (header)
msgid "Recommendations"
msgstr "Raccomandazioni"

#. Recommendation
#: ../content/54_activity_43.md:block 15 (unordered list)
msgid "\"Be sure to tap into the expertise of your IT, DevOps, cybersecurity, risk management, and procurement teams when creating the process.\" (from [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/))."
msgstr "\"Assicuratevi di attingere alle competenze dei vostri team IT, DevOps, cybersecurity, gestione del rischio e acquisti quando create il processo\" (da [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/))."

#. Recommendation
#: ../content/54_activity_43.md:block 15 (unordered list)
msgid "Competition law may require that \"open source\" not be specifically mentioned."
msgstr "La legge sulla concorrenza può richiedere che l'\"open source\" non sia specificamente menzionato."

#. Recommendation
#: ../content/54_activity_43.md:block 15 (unordered list)
msgid "Select technology upfront then go to RFP for customisation and support services."
msgstr "Selezionate la tecnologia in anticipo e poi affidatevi a RFP per la personalizzazione e i servizi di supporto."

#: ../content/54_activity_43.md:block 16 (header)
msgid "Resources"
msgstr "Risorse"

#. Resource
#: ../content/54_activity_43.md:block 17 (unordered list)
msgid "[Decision factors for open source software procurement](http://oss-watch.ac.uk/resources/procurement-infopack): not new, but still a great read by our colleagues at OSS-watch in the UK. Check out the [slides](http://oss-watch.ac.uk/files/procurement.odp)."
msgstr "[Fattori decisionali per l'acquisto di software open source](http://oss-watch.ac.uk/resources/procurement-infopack): un po' datato, ma è comunque un'ottima lettura da parte dei nostri colleghi di OSS-watch nel Regno Unito. Date un'occhiata alle [slide](http://oss-watch.ac.uk/files/procurement.odp)."

#. Resource
#: ../content/54_activity_43.md:block 17 (unordered list)
msgid "[5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/): a recent piece on open source procurement with useful hints."
msgstr "[5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/): un articolo recente sul procurement dell'open source con suggerimenti utili."

#: ../../content/54_activity_43.md:block 18 (header)
msgid "Proposed next activities"
msgstr "Attività successive proposte"

#. Proposed next activity
#: ../../content/54_activity_43.md:block 19 (unordered list)
msgid "[GGI-A-33 - Engage with open source vendors](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) Defining a procument policy helps you identify the OSS providers and communities that you need to care about, and to engage with."
msgstr "[GGI-A-33 - Engage with open source vendors](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) Definire una policy per gli acquisti aiuta ad identificare i fornitori e le comunità OSS di particolare interesse e come interagire con loro."
