## Politiche di acquisizione di software open source

Attività ID: [GGI-A-43](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_43.md).

### Descrizione

Questa attività riguarda l'implementazione di un processo di selezione, acquisizione e acquisto di software e servizi open source. Si tratta anche di considerare il costo effettivo del software open source e di provvedere alla sua fornitura. L'OSS può essere "gratuito" ad una prima analisi, ma non è esente da costi interni ed esterni come l'integrazione, la formazione, la manutenzione e il supporto.

Queste policy richiedono che sia le soluzioni open source che quelle proprietarie siano considerate simmetricamente quando si valuta il rapporto qualità-prezzo come la combinazione ottimale tra costo totale di proprietà e qualità. Pertanto, l'ufficio acquisti IT deve considerare attivamente ed equamente le opzioni open source, assicurando al contempo che le soluzioni proprietarie siano considerate sullo stesso livello nelle decisioni di acquisto.

La preferenza per l'open source può essere esplicitamente dichiarata sulla base della flessibilità intrinseca dell'opzione open source quando non c'è una significativa differenza del costo complessivo tra le soluzioni proprietarie e quelle open source.

I dipartimenti di approvvigionamento (Procurement departments) devono capire che le aziende che offrono supporto per gli OSS di solito non dispongono di risorse commerciali sufficienti per partecipare alle gare d'appalto e devono adattare di conseguenza le loro politiche e i loro processi di approvvigionamento per l'open source.

### Valutazione delle opportunità

Diverse ragioni giustificano gli sforzi per istituire specifiche politiche di approvvigionamento dell'open source:

- L'offerta di software e servizi commerciali open source è in crescita e non può essere ignorata, e richiede l'implementazione di politiche e processi di approvvigionamento dedicati.
- Esiste una crescente offerta di soluzioni commerciali open source altamente competitive per i sistemi informativi aziendali.
- Anche dopo aver adottato un componente OSS gratuito e averlo integrato in un'applicazione, è necessario fornire risorse interne o esterne per la manutenzione del codice sorgente.
- Il costo totale di proprietà (TCO) è spesso (anche se non necessariamente) inferiore per le soluzioni FOSS: nessun costo di licenza da pagare al momento dell'acquisto/aggiornamento, mercato aperto per i fornitori di servizi, possibilità di fornire una parte o tutta la soluzione da soli.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa attività:

- [ ] Nuove Call for Proposals (proposte di nuove iniziative) richiedono in modo proattivo la presentazione di contributi open source.
- [ ] Il reparto acquisti ha un modo per valutare le soluzioni open source rispetto a quelle proprietarie.
- [ ] È stato implementato e documentato un processo di approvvigionamento semplificato per software e servizi open source.
- [ ] È stato definito e documentato un processo di approvazione che si avvale di competenze interfunzionali.

### Raccomandazioni

- "Assicuratevi di attingere alle competenze dei vostri team IT, DevOps, cybersecurity, gestione del rischio e acquisti quando create il processo" (da [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/)).
- La legge sulla concorrenza può richiedere che l'"open source" non sia specificamente menzionato.
- Selezionate la tecnologia in anticipo e poi affidatevi a RFP per la personalizzazione e i servizi di supporto.

### Risorse

- [Fattori decisionali per l'acquisto di software open source](http://oss-watch.ac.uk/resources/procurement-infopack): un po' datato, ma è comunque un'ottima lettura da parte dei nostri colleghi di OSS-watch nel Regno Unito. Date un'occhiata alle [slide](http://oss-watch.ac.uk/files/procurement.odp).
- [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/): un articolo recente sul procurement dell'open source con suggerimenti utili.

### Attività successive proposte

- [GGI-A-33 - Engage with open source vendors](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) Definire una policy per gli acquisti aiuta ad identificare i fornitori e le comunità OSS di particolare interesse e come interagire con loro.
