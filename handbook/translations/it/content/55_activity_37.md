## Open source favorisce la trasformazione digitale

Activity ID: [GGI-A-37](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_37.md).

### Descrizione

> "La trasformazione digitale è l'adozione della tecnologia digitale per trasformare i servizi o le aziende, sostituendo processi non digitali o manuali con processi digitali o sostituendo la vecchia tecnologia digitale con una più recente" (Wikipedia)

Quando le organizzazioni più avanzate nella Trasformazione Digitale guidano congiuntamente il cambiamento attraverso il Business, l'IT e la Finanza per ancorare il digitale nel percorso, riesaminano:

- Modello di business: catena del valore con ecosistemi, as a service, SaaS.
- Finanza: opex/capex, persone, outsourcing.
- IT: innovazione, modernizzazione di legacy/asset.

L'open source è al centro della trasformazione digitale:

- Tecnologie, pratiche agili, gestione dei prodotti.
- Persone: collaborazione, comunicazione aperta, ciclo di sviluppo/decisione.
- Modelli di business: try & buy, open innovation.

In termini di competitività, i processi più visibili sono probabilmente quelli che hanno un impatto diretto sulla customer experience. E dobbiamo riconoscere che i grandi player, così come le start-up, offrendo un'esperienza del tutto inedita ai clienti, hanno cambiato drasticamente le loro aspettative.

L'esperienza del cliente e tutti gli altri processi di un'azienda dipendono interamente dall'IT. Ogni azienda deve trasformare il proprio IT, questo è il senso della trasformazione digitale. Le aziende che non l'hanno ancora fatto devono realizzare la loro trasformazione digitale il più velocemente possibile, altrimenti il rischio è di essere spazzate via dal mercato. La trasformazione digitale è una condizione di sopravvivenza. Poiché la posta in gioco è così alta, un'azienda non può affidare completamente la trasformazione digitale a un fornitore. Ogni azienda deve mettere mano all'IT, il che significa che ogni azienda deve mettere mano al software open source, perché non esiste IT senza software open source.

I vantaggi attesi dalla trasformazione digitale includono:

- Semplificare, automatizzare i processi principali e renderli in tempo reale.
- Consentire risposte rapide ai cambiamenti della concorrenza.
- Sfruttare l'intelligenza artificiale e i big data.

### Valutazione delle opportunità

La trasformazione digitale potrebbe essere gestita da:

- Segmenti dell'IT: IT di produzione, IT di supporto al business (CRM, fatturazione, acquisti...), IT di supporto (HR, finanza, contabilità...), Big Data.
- Tipo di tecnologia o processo a supporto dell'IT: infrastruttura (cloud), intelligenza artificiale, processi (Make-or-Buy, DevSecOps, SaaS).

L'introduzione dell'open source in un particolare segmento o tecnologia dell'IT rivela che si vuole mettere mano a questo segmento o tecnologia, perché si è valutato che questo particolare segmento o tecnologia dell'IT è importante per la competitività dell'azienda. È importante valutare la posizione della vostra azienda rispetto non solo ai vostri concorrenti, ma anche ad altri settori e ai principali attori in termini di esperienza del cliente e soluzioni di mercato.

### Valutazione dei progressi


   - [ ] Livello 1: valutazione della situazioneHo identificato:

   - i segmenti IT che sono importanti per la competitività della mia azienda, e
   - le tecnologie open source necessarie per sviluppare applicazioni in questi segmenti.
E così ho deciso:


   - su quali segmenti voglio gestire internamente lo sviluppo dei progetti, e
   - su quali tecnologie open source devo costruire le mie competenze interne.

   - [ ] Livello 2: coinvolgimentoSu alcune tecnologie open source selezionate, utilizzate all'interno dell'azienda, diversi sviluppatori sono stati formati e sono riconosciuti come validi collaboratori dalla comunità open source. In alcuni segmenti selezionati sono stati avviati progetti basati su tecnologie open source.

   - [ ] Livello 3: GeneralizzazionePer tutti i progetti, un'alternativa open source viene sistematicamente studiata durante la fase iniziale del progetto. Per facilitare lo studio di tali alternative open source da parte del team di progetto, un budget centrale e un team centrale di architetti, ospitati nel Dipartimento IT, sono dedicati a fornire assistenza ai progetti.

**KPI**:

- KPI 1. Rapporto per il quale è stata esaminata un'alternativa open source: (Numero di progetti / Numero totale di progetti).
- KPI 2. Rapporto per il quale è stata scelta l'alternativa open source: (Numero di progetti / Numero totale di progetti).

### Raccomandazioni

Al di là dei titoli, la Trasformazione Digitale è una mentalità che implica alcuni cambiamenti fondamentali, che dovrebbero provenire anche (o addirittura soprattutto) dai livelli più alti dell'organizzazione. Il management deve promuovere iniziative e nuove idee, gestire i rischi e potenzialmente aggiornare le procedure esistenti per adattarle ai nuovi concetti.

La passione è un enorme fattore di successo. Uno dei mezzi sviluppati dai principali attori del settore è la creazione di spazi aperti per le nuove idee, dove le persone possono presentare e lavorare liberamente sulle loro idee di trasformazione digitale. Il management dovrebbe incoraggiare queste iniziative.

### Risorse

- [Eclipse Foundation: Enabling Digital Transformation in Europe Through Global Open Source Collaboration](https://outreach.eclipse.foundation/hubfs/EuropeanOpenSourceWhitePaper-June2021.pdf).
- [Europe: Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).
- [Europe: Open source software strategy 2020-2023](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
