## Supervisione dell'open source

ID Activity: [GGI-A-19](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_19.md).

### Descrizione

Questa attività riguarda il controllo dell'uso dell'open source e la garanzia che il software sia gestito in modo proattivo. Ciò riguarda diverse prospettive, sia che si tratti di utilizzare strumenti e soluzioni aziendali OSS, sia che si tratti di includere OSS come componenti nei propri sviluppi o di modificare una versione di un software adattandola alle proprie esigenze. Si tratta anche di identificare le aree in cui l'open source è diventato una soluzione di fatto (a volte nascosta) e di valutarne l'idoneità.

Potrebbe essere necessario chiarire quanto segue:

- Viene fornita la funzionalità richiesta?
- Vengono fornite funzionalità aggiuntive non necessarie, ma che aumentano la complessità delle fasi di BUILD e RUN?
- Cosa richiede la licenza, quali sono i vincoli legali?
- In che misura la decisione rende la vostra organizzazione indipendente dai fornitori?
- Esiste un'opzione di supporto, pronta per le vostre esigenze aziendali, e quanto costa?
- TCO (Total Cost of Ownership).
- Il management conosce i vantaggi dell'open source, ad esempio al di là del "risparmio sui costi di licenza"? Essere a proprio agio con l'open source aiuta a trarre il massimo beneficio dalla collaborazione con le comunità di progetto e i fornitori.
- Valutare se ha senso condividere i costi di sviluppo restituendo i propri sviluppi alla comunità, tenendo conto di tutte le implicazioni, come la conformità alla licenza.
- Verificare la disponibilità di un supporto comunitario o professionale.

### Valutazione delle opportunità

Definire un processo decisionale specificamente rivolto all'open source è un modo per massimizzarne i vantaggi.

- In questo modo si evita l'emergere di usi incontrollati e di costi nascosti delle tecnologie open source.
- Porta a decisioni strategiche e organizzative informate e consapevoli degli OSS.

Costi: l'attività può mettere in discussione e riconsiderare l'uso de facto non ottimale dell'open source come inefficiente, rischioso, ecc.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa attività:

- [ ] L'OSS è diventato un'opzione conveniente quando la valutazione di nuovo software da implementare viene effettuata.
- [ ] L'OSS non viene visto come un'eccezione o una scelta pericolosa.
- [ ] L'OSS è diventato un'opzione "mainstream".
- [ ] Gli attori principali sono sufficientemente convinti che la soluzione open source offra vantaggi strategici che valgono la pena di essere investiti.
- [ ] È possibile dimostrare che il TCO della soluzione basata sull'open source offre alla vostra organizzazione un valore superiore rispetto all'alternativa.
- [ ] Viene valutato il modo in cui l'indipendenza dei fornitori fa risparmiare denaro o può potenzialmente farlo in futuro.
- [ ] È stato valutato che l'indipendenza della soluzione riduce i rischi di un cambiamento troppo costoso della soluzione (non sono possibili formati di dati chiusi).

### Strumenti

In questa fase, non possiamo pensare ad alcuno strumento rilevante o interessato da questa attività.

### Raccomandazioni

- Gestire in modo proattivo l'uso dell'open source richiede livelli base di consapevolezza e comprensione dei fondamenti dell'open source, che devono essere presi in considerazione in ogni decisione relativa all'OSS.
- Confrontate le funzionalità necessarie invece di cercare un'alternativa per una soluzione closed source conosciuta.
- Assicuratevi di avere supporto e ulteriori sviluppi.
- Considerate gli effetti della licenza della soluzione sulla vostra organizzazione.
- Convincere tutti gli attori chiave del valore dei vantaggi dell'open source, al di là del "risparmio sui costi di licenza".
- Siate onesti, non esagerando con i benefici attesi dall'open source.
- Nel processo decisionale è altrettanto importante valutare le diverse soluzioni open source per evitare delusioni dovute ad aspettative sbagliate, per chiarire cosa si richiede all'organizzazione e tutti i vantaggi che l'apertura delle soluzioni comporta. Questo deve essere identificato in modo che l'organizzazione possa valutarlo per il proprio contesto.

### Risorse

- [Top 5 Benefits of Open Source](https://www.openlogic.com/blog/top-5-benefits-open-source-software): Blog sponsorizzato, ma comunque interessante e di rapida lettura.
- [Weighing The Hidden Costs Of Open Source](https://www.itjungle.com/2021/02/15/weighing-the-hidden-costs-of-open-source/): uno studio sponsorizzato da IBM sui costi di supporto dell'OSS.
