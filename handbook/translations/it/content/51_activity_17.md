## Inventario delle competenze e delle risorse open source

ID Activity: [GGI-A-17](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_17.md).

### Descrizione

In qualsiasi fase, dal punto di vista della gestione, è utile avere una mappatura, un inventario delle risorse open source, degli asset, dell'utilizzo e del loro stato, nonché delle esigenze potenziali e delle soluzioni disponibili. Ciò include anche la valutazione dell'impegno e delle competenze necessarie per colmare i gap.

Questa attività mira a fare un'istantanea della situazione dell'open source all'interno dell'organizzazione e sul mercato e a valutare il legame tra loro.

- Inventario dell'utilizzo di OSS nella catena di sviluppo del software e nei prodotti e componenti software utilizzati in produzione.
- Identificare le tecnologie open source (soluzioni, framework, funzioni innovative) che potrebbero soddisfare le vostre esigenze e contribuire a migliorare il vostro processo.

Non incluso

- Identificare e qualificare gli ecosistemi e le comunità OSS correlate. (Culture Goal)
- Identificare le dipendenze da librerie e componenti OSS. (Trust Goal)
- Identificare le competenze tecniche (ad es. linguaggi, framework...) e soft (ad es. collaborazione, comunicazione) necessarie. (appartiene alle successive Activity: Crescita delle competenze OSS e Competenze per lo sviluppo di software open source)

### Valutazione delle opportunità

Un inventario delle risorse open source disponibili che aiuterà a ottimizzare gli investimenti e a dare priorità allo sviluppo delle competenze.

Questa attività crea le condizioni per migliorare la produttività dello sviluppo, data l'efficienza e la popolarità dei componenti, dei principi di sviluppo e degli strumenti OSS, in particolare nello sviluppo di applicazioni e infrastrutture moderne.

- Potrebbe essere necessario semplificare il portfolio di risorse OSS.
- Potrebbe essere necessario riqualificare il personale.
- Ciò consente di identificare le esigenze e di alimentare la roadmap IT.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa Attività:

- [ ] Esiste un elenco di risorse OSS utilizzabili "We use", "We integrate", "We produce", "We host", e le relative Competenze
- [ ] Siamo impegnati a migliorare l'efficienza utilizzando metodi e strumenti all'avanguardia.
- [ ] Abbiamo identificato risorse OSS che non sono ancora state elencate (che potrebbero essere emerse internamente: è possibile definire una politica in questo settore?)
- [ ] Chiediamo ai nuovi progetti di appoggiare o riutilizzare le risorse OSS esistenti.
- [ ] Abbiamo una percezione e una comprensione ragionevolmente sicura dell'ambito di utilizzo degli OSS nella nostra organizzazione.

### Strumenti

Ci sono molti modi diversi per stabilire tale inventario. Un modo potrebbe essere quello di classificare le risorse OSS in quattro categorie:

- OSS che utilizziamo: software che utilizziamo in produzione o in sviluppo
- OSS che integriamo: ad esempio, le librerie OSS che integriamo in un'applicazione personalizzata
- OSS che produciamo: ad esempio, una libreria che abbiamo pubblicato su GitHub o un progetto OSS che sviluppiamo o a cui contribuiamo regolarmente.
- OSS che ospitiamo: OSS che gestiamo per offrire un servizio interno come un CRM, GitLab, Nexus, ecc. Un esempio di tabella potrebbe essere il seguente:

| Utilizziamo | Integriamo | Produciamo | Ospitiamo | Competenze |
| --- | --- | --- | --- | --- |
| Firefox, <br />LibreOffice, <br />Postgresql | Libreria slf4j | Library YY su GH | GitLab, <br />Nexus | Java, <br />Python |

La stessa identificazione dovrebbe valere per le competenze

- Competenze ed esperienze disponibili nei team esistenti
- Competenze ed esperienze che potrebbero essere sviluppate o acquisite internamente (formazione, coaching, esperimento)
- Competenze ed esperienze che devono essere ricercate sul mercato o attraverso partnership/appalti

### Raccomandazioni

- Mantieni le cose semplici.
- Si tratta di un esercizio di livello relativamente alto, non di un inventario dettagliato per il reparto contabilità.
- Sebbene questa attività sia un buon punto di partenza, non è necessario completarla al 100% prima di avviare le altre attività.
- Gestire le problematiche, le risorse e le competenze relative allo **sviluppo del software** nell'Activity #42.
- L'inventario deve coprire tutte le categorie IT: sistemi operativi, middleware, DBMS, amministrazione di sistema, strumenti di sviluppo e di test, ecc.
- Iniziate a identificare le comunità affini: è più facile ottenere supporto e feedback dal progetto quando vi conoscono già.

### Risorse

- Un eccellente corso su [Free (/Libre), and Open Source Software (FOSS)](https://profriehle.com/open-courses/free-and-open-source-software), tenuto dal professor Dirk Riehle.

### Attività successive proposte

- [GGI-A-18 - Open source competency growth](https://ospo-alliance.org/ggi/activities/open_source_competency_growth) Identificazione delle competenze e delle risorse open source per permettere all'azienda di consolidare e rafforzare la sua consapevolezza e le sue capacità di agire.
- [GGI-A-19 - Open source supervision](https://ospo-alliance.org/ggi/activities/open_source_supervision) Una volta che l'inventario del software e delle competenze open source è stato completato, l'organizzazione può iniziare a gestirne l'uso correttamente.
- [GGI-A-28 - Human Resources perspective](https://ospo-alliance.org/ggi/activities/human_resources_perspective) L'ufficio del personale può iniziare a creare dei piani di sviluppo, contratti e processi basati sull'inventario creato in questa attività.
- [GGI-A-33 - Engage with open source vendors](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) Una volta che il software e le competenze open source sono state catalogate e valutate è possibile rendere più efficiente e produttiva la relazione con i fornitori.
- [GGI-A-42 - Manage open source skills and resources](https://ospo-alliance.org/ggi/activities/manage_open_source_skills_and_resources) Una volta che l'inventario del software open source e delle relative competenze sono state completate è possibile gestirle correttamente con le risorse interne.
