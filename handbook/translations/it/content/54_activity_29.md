## Impegnarsi in progetti open source

Attività ID: [GGI-A-29](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_29.md).

### Descrizione

Questa attività consiste nell'impiegare contributi significativi ad alcuni progetti OSS che sono importanti per voi. I contributi sono scalati e impegnati a livello di organizzazione (non a livello personale come nell'Activity #26). Possono assumere diverse forme, dal finanziamento diretto all'allocazione di risorse (ad esempio, persone, server, infrastrutture, comunicazione, ecc.), purché vadano a beneficio del progetto o dell'ecosistema in modo sostenibile ed efficiente.

Questa attività è il seguito dell'Activity #26 e porta i contributi dei progetti open source al livello dell'organizzazione, rendendoli più visibili, potenti e vantaggiosi. In questa attività, si suppone che i contributi apportino un miglioramento sostanziale e a lungo termine al progetto OSS: ad esempio, uno sviluppatore o un team che sviluppa una nuova funzionalità molto richiesta, risorse infrastrutturali, server per un nuovo servizio, acquisizione della manutenzione di un ramo ampiamente utilizzato.

L'idea è quella di allocare una percentuale di risorse per sponsorizzare gli sviluppatori open source che scrivono e mantengono librerie o progetti che utilizziamo.

Questa attività implica una mappatura dei software open source utilizzati e una valutazione della loro criticità per decidere quale supportare.

### Valutazione delle opportunità

> "If every company using open source contributed at least a little, we would have a healthy ecosystem. <https://news.ycombinator.com/item?id=25432248>"

Il sostegno ai progetti aiuta a garantirne la sostenibilità e fornisce l'accesso alle informazioni, contribuendo forse anche a influenzare e a dare priorità ad alcuni sviluppi (anche se questo non dovrebbe essere il motivo principale del sostegno ai progetti).

Potenziali benefici di questa attività: garantire che le segnalazioni di bug siano prioritarie e che gli sviluppi siano integrati nella versione stabile. Possibili costi associati all'attività: impegno di tempo nei progetti, impegno di denaro.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa attività:

- [ ] Progetto beneficiario individuato.
- [ ] Opzione di supporto decisa, come il contributo monetario diretto o il contributo di codice.
- [ ] Nominato il task leader.
- [ ] È stato dato un contributo.
- [ ] Il risultato del contributo è stato valutato.

Punti di verifica presi in prestito dal questionario di OpenChain [autocertificazione](https://certification.openchainproject.org/):

- [ ] Abbiamo una politica di contributo a progetti open source per conto dell'organizzazione.
- [ ] Abbiamo una procedura documentata che regola i contributi open source.
- [ ] Abbiamo una procedura documentata per sensibilizzare tutto il personale del software alla politica di contribuzione open source.

### Strumenti

Alcune organizzazioni offrono meccanismi di finanziamento di progetti open source (potrebbe essere conveniente se il vostro progetto target rientra nei loro portafogli).

- [Open Collective](https://opencollective.com/).
- [Software Freedom Conservancy](https://sfconservancy.org/).
- [Tidelift](https://tidelift.com/).

### Raccomandazioni

- Concentratevi sui progetti critici per l'organizzazione: sono questi i progetti che desiderate maggiormente aiutare con il vostro contributo.
- Progetti comunitari mirati.
- Questa attività richiede un minimo di familiarità con il progetto target.

### Risorse

- [Come sostenere i progetti open source ora](https://sourceforge.net/blog/support-open-source-projects-now/): Una breve pagina con idee sul finanziamento dei progetti open source.
- [Sostenere l'OSS: uno spazio per conversazioni sul sostegno all'open source](https://sustainoss.org)

### Attività successive proposte

- [GGI-A-26 - Contribute to open source projects](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) The most natural way to engage with an open source initiative is by contributing directly to the project. You will in return collect valuable feedback on your contributions.
- [GGI-A-30 - Support open source communities](https://ospo-alliance.org/ggi/activities/support_open_source_communities) Ci sono tanti modi per supportare delle iniziative open source che sono essenziali per la vostra organizzazione. Divenire attivi nelle comunità è un buon modo per scoprirle ed incoraggiarle.
