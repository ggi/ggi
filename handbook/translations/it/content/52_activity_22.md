## Gestire le vulnerabilità del software

ID attività: [GGI-A-22](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_22.md).

### Descrizione

Il codice di uno sviluppatore è sicuro quanto la parte meno sicura del suo codice. Casi recenti (ad esempio heartbleed [^heartbleed], equifax [^equifax]) hanno dimostrato l'importanza di verificare la presenza di vulnerabilità in parti di codice non direttamente sviluppate in azienda. Le conseguenze delle esposizioni vanno dalle fughe di dati (con un notevole impatto sulla reputazione) agli attacchi di malware (ransomware) e all'indisponibilità di servizi a rischio per l'azienda.

Il software open source è noto per avere una migliore gestione delle vulnerabilità rispetto al software proprietario, soprattutto perché:

- Sempre maggiore attenzione è rivolta a trovare e risolvere i problemi del codice e dei processi aperti.
- I progetti open source risolvono le vulnerabilità e rilasciano patch e nuove versioni molto più velocemente.

Ad esempio, uno [studio di WhiteSource](https://resources.whitesourcesoftware.com/blog-whitesource/3-reasons-why-open-source-is-safer-than-commercial-software) sul software proprietario ha mostrato che il 95% delle vulnerabilità riscontrate nei loro componenti open source aveva già rilasciato una correzione al momento dell'analisi. Il problema, quindi, è **gestire meglio le vulnerabilità sia nella base di codice che nelle sue dipendenze**, indipendentemente dal fatto che siano closed o open source.

Per ridurre questi rischi, è necessario istituire un programma di valutazione delle risorse software e un processo di verifica delle vulnerabilità eseguito regolarmente. Implementare strumenti che avvisino i team interessati, gestiscano le vulnerabilità note e prevengano le minacce derivanti dalle dipendenze del software.

### Valutazione delle opportunità

Qualsiasi azienda che utilizza software deve tenere sotto controllo le sue vulnerabilità:

- la sua infrastruttura (ad es. infrastruttura Cloud, infrastruttura di rete, archivi di dati),
- le sue applicazioni aziendali (HR, strumenti CRM, gestione dei dati interni e dei clienti),
- il suo codice interno: ad esempio il sito web dell'azienda, i progetti di sviluppo interno, ecc,
- e tutte le dipendenze dirette e indirette di software e servizi.

Il ROI delle vulnerabilità è poco conosciuto finché non accade qualcosa di grave. Per stimare il costo reale delle vulnerabilità bisogna considerare le conseguenze di una grave violazione dei dati o dell'indisponibilità dei servizi.

Allo stesso modo, è necessario evitare a tutti i costi una cultura di segretezza e occultamento dei problemi di sicurezza all'interno dell'azienda. Al contrario, le informazioni sullo stato di vulnerabilità devono essere condivise e discusse per trovare le risposte migliori dalle persone giuste, dagli sviluppatori ai dirigenti di livello superiore.

I vantaggi della prevenzione degli attacchi informatici attraverso un'attenta gestione delle vulnerabilità del software sono molteplici:

- Evitare impatti negativi sulla reputazione,
- Evitare perdite dovute ad azioni di Exploiting (DDoS, Ransomware, tempo per ricostruire un sistema IT alternativo dopo un attacco),
- Rispettare le norme sulla protezione dei dati.

La gestione delle vulnerabilità del software OSS è solo una parte del più ampio processo di cybersecurity che affronta globalmente la sicurezza dei sistemi e dei servizi dell'organizzazione.

### Valutazione dei progressi

Dovrebbe esserci una persona o un team dedicato al monitoraggio delle vulnerabilità ed all'implementazione di processi facili da usare su cui gli sviluppatori possano fare affidamento. La valutazione delle vulnerabilità è una parte standard del processo di integrazione continua e le persone sono in grado di monitorare lo stato attuale del rischio in una dashboard dedicata.

I seguenti **punti di verifica** dimostrano i progressi in questa Attività:

- [ ] L'attività è coperta quando tutti i software e i servizi interni sono valutati e monitorati per le vulnerabilità note.
- [ ] L'attività è coperta quando nella catena di produzione del software vengono implementati uno strumento e un processo dedicati per prevenire l'introduzione di problemi nelle routine di sviluppo quotidiane.
- [ ] Una persona o un team è responsabile della valutazione del rischio di CVE/vulnerabilità rispetto all'esposizione.
- [ ] Una persona o un team è responsabile dell'invio di CVE/vulnerabilità alle persone interessate (SysOps, DevOps, sviluppatori, ecc.).

### Strumenti

- Strumenti GitHub
   - GitHub fornisce linee guida e strumenti per proteggere il codice ospitato sulla piattaforma. Per ulteriori informazioni, consultare [GitHub docs](https://docs.github.com/en/github/administering-a-repository/about-securing-your-repository).
   - GitHub fornisce [Dependabot](https://docs.github.com/en/github/managing-security-vulnerabilities/about-alerts-for-vulnerable-dependencies) per identificare automaticamente le vulnerabilità nelle dipendenze.
- [Eclipse Steady](https://eclipse.github.io/steady/) è uno strumento gratuito e open source che analizza i progetti Java e Python alla ricerca di vulnerabilità e aiuta gli sviluppatori a mitigarle.
- [OWASP dependency-check](https://owasp.org/www-project-dependency-check/): uno scanner di vulnerabilità open source.
- [OSS Review Toolkit](https://github.com/oss-review-toolkit/ort): un orchestratore open source in grado di raccogliere avvisi di sicurezza per le dipendenze utilizzate dai servizi di dati sulle vulnerabilità configurati.

### Risorse

- Il [database delle vulnerabilità del MITRE](https://cve.mitre.org/) dei CVE. Si veda anche il [database della sicurezza del NIST](https://nvd.nist.gov/) delle NVD e risorse satellite come [CVE Details](https://www.cvedetails.com/).
- Si veda anche questa nuova iniziativa di Google: le [Vulnerabilità open source](https://osv.dev/).
- Il gruppo di lavoro OWASP pubblica un elenco di scanner di vulnerabilità [sul proprio sito web](https://owasp.org/www-community/Vulnerability_Scanning_Tools), sia dal mondo commerciale che quello Open Source.
- J. Williams e A. Dabirsiaghi. The unfortunate reality of insecure libraries, 2012.
- [Rilevazione, valutazione e mitigazione delle vulnerabilità nelle dipendenze open source](https://link.springer.com/article/10.1007/s10664-020-09830-x), Serena Elisa Ponta, Henrik Plate & Antonino Sabetta, Empirical Software Engineering volume 25, pagine 3175-3215(2020).
- [A Manually-Curated Dataset of Fixes to Vulnerabilities of open source Software](https://arxiv.org/abs/1902.02595), Serena E. Ponta, Henrik Plate, Antonino Sabetta, Michele Bezzi, Cédric Dangremont. Esiste anche un [toolkit in sviluppo per implementare il suddetto dataset](https://sap.github.io/project-kb/).

### Attività successive proposte

- [GGI-A-24 - Manage key indicators](https://ospo-alliance.org/ggi/activities/manage_key_indicators) Rendi le vulnerabilità identificate visibili. Questo aiuta le persone a realizzare quanto è o non è sicuro li loro software e dimostra l'importanza di selezionare oculatamente le dipendenze software.

[^heartbleed]: https://it.wikipedia.org/wiki/Heartbleed
[^equifax]: https://arstechnica.com/information-technology/2017/09/massive-equifax-breach-caused-by-failure-to-patch-two-month-old-bug/
