## Contribuire a progetti open source

Activity ID: [GGI-A-26](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_26.md).

### Descrizione

Contribuire a progetti open source di libero utilizzo è uno dei principi chiave della buona governance. Il punto è evitare di essere un semplice consumatore passivo e dare un contributo ai progetti. Quando le persone aggiungono una funzionalità o correggono un bug per i propri scopi, dovrebbero renderlo abbastanza generico da contribuire al progetto. Agli sviluppatori deve essere concesso il tempo necessario per contribuire.

Questa attività copre i seguenti ambiti:

- Lavorare con progetti open source upstream.
- Segnalazione di bug e richieste di funzionalità.
- Contribuire al codice e alla correzione di bug.
- Partecipare alle mailing list della community.
- Condividere le esperienze.

### Valutazione delle opportunità

I principali benefici di questa attività sono:

- Aumenta la conoscenza generale e l'impegno verso l'open source all'interno dell'azienda, in quanto le persone iniziano a contribuire e a essere coinvolte in progetti open source. Si percepisce un senso di utilità pubblica e si migliora la propria reputazione personale.
- L'azienda aumenta la propria visibilità e reputazione man mano che i contributi si fanno strada nel progetto. Questo dimostra che l'azienda è effettivamente coinvolta nell'open source, contribuisce e promuove correttezza e trasparenza.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa Attività:

- [ ] Esiste un percorso chiaro e ufficiale per le persone disposte a contribuire.
- [ ] Gli sviluppatori sono incoraggiati a contribuire ai progetti open source che utilizzano.
- [ ] È in atto un processo per garantire la conformità legale e la sicurezza dei contributi da parte degli sviluppatori.
- [ ] KPI: Volume dei contributi esterni (codice, mailing list, issue...) da parte di individui, team o entità.

### Strumenti

Può essere utile seguire i contributi, sia per tenere traccia di ciò che viene apportato sia per poter comunicare l'impegno dell'azienda. A questo scopo si possono utilizzare dashboard e software di tracciamento delle attività. Controllare:

- [GrimoireLab](https://chaoss.github.io/grimoirelab/) di Bitergia
- [ScanCode](https://scancode-toolkit.readthedocs.io)

### Raccomandazioni

Incoraggiare le persone all'interno dell'entità a contribuire a progetti esterni:

- Permettere di scrivere correzioni di bug e funzionalità generiche e ben testate e di contribuire alla community.
- Fornire formazione alle persone sul contributo alle comunità open source. Questo riguarda sia le competenze tecniche (migliorare le conoscenze del team) che la community (appartenenza alle community open source, codice di condotta, ecc.).
- Fornire formazione su questioni legali, di proprietà intellettuale e tecniche e create un contatto all'interno dell'azienda che possa aiutarvi su questi temi in caso di dubbi.
- Fornire incentivi per i lavori pubblicati.
- Si noti che i contributi dell'azienda/ente rifletteranno la qualità del codice e il suo coinvolgimento, quindi assicuratevi che il vostro team di sviluppo fornisca codice sufficientemente buono.

### Risorse

- L'iniziativa [CHAOSS](https://chaoss.community/) della Linux Foundation offre alcuni strumenti e indicazioni su come tracciare i contributi allo sviluppo.

### Attività successive proposte

- [GGI-A-31 - Publicly assert use of open source](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Adesso che ci sono delle contribuzioni visibili pubblicamente e che vi è una chiara direzione aziendale, iniziate un piano di comunicazione per promuovere quelle attività!
- [GGI-A-24 - Manage key indicators](https://ospo-alliance.org/ggi/activities/manage_key_indicators) Rendete le contribuzioni a progetti OSS visibili e misurabili. Questo aiuterà la promozione dell'iniziativa ed aiuterà moralmente le persone coinvolte.
- [GGI-A-27 - Belong to the open source community](https://ospo-alliance.org/ggi/activities/belong_to_the_open_source_community) Contributing to the OSS community is the first step to becoming part of it. Once people start contributing, they become more involved in the project's health and governance and can eventually become maintainers, ensuring a sustainable and healthy project and roadmap.
- [GGI-A-29 - Engage with open source projects](https://ospo-alliance.org/ggi/activities/engage_with_open_source_projects) I progetti open source apprezzano e danno importanza alla meritocrazia. Adesso che avete dimostrato una buona comprensione del codice e dei processi, potete venire coinvolti in modo più ufficiale nel progetto.
- [GGI-A-36 - Open source enabling innovation](https://ospo-alliance.org/ggi/activities/open_source_enabling_innovation) Contribuire a progetti OSS ed interagire con contributori esterni è un modo per facilitare l'innovazione.
- [GGI-A-39 - Upstream first](https://ospo-alliance.org/ggi/activities/upstream_first) Contribuire a progetti OSS ha senso se gli aggiornamenti vengono resi disponibili al progetto principale in modo regolare ed ufficiale a livello aziendale.
