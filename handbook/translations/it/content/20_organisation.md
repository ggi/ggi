# Organizzazione

## Terminologia

Il progetto della metodologia OSS Good Governance è strutturato attorno a quattro concetti chiave: Obiettivi (Goals), Attività canoniche (Canonical Activities), Scorecard di attività personalizzate (Customised Activity Scorecards) e Iterazione (Iteration).

* **Obiettivi (Goals)**: Un obiettivo è un insieme di attività associate a un'area di interesse comune; esistono cinque obiettivi: Obiettivo Utilizzo, Obiettivo Fiducia, Obiettivo Cultura, Obiettivo Coinvolgimento e Obiettivo Strategia. Gli obiettivi possono essere raggiunti indipendentemente, in parallelo e perfezionati in modo iterativo attraverso le attività.
* **Attività canoniche (Canonical Activities)**: all'interno di un obiettivo, un'attività affronta un singolo problema o argomento di sviluppo, come la gestione della conformità legale, che può essere utilizzato come passo incrementale verso gli obiettivi del programma. L'insieme completo delle attività definite dal GGI è chiamato Attività canoniche.
* **Scorecard di attività personalizzate (CAS - Customised Activity Scorecard)**: per implementare il GGI in una determinata organizzazione, le attività canoniche devono essere adattate alle specificità del contesto, costruendo così una serie di schede di valutazione delle attività personalizzate. La scheda di valutazione dell'attività personalizzata descrive come l'attività verrà implementata nel contesto dell'organizzazione e come verranno monitorati i progressi.
* **Iterazione (Iteration)**: L'OSS Good Governance è un sistema di gestione e come tale richiede una valutazione, un riesame e una revisione periodica. Pensate al sistema contabile di un'organizzazione, è un processo continuo con almeno un punto di controllo annuale, il bilancio; allo stesso modo, il processo di buona governance dell'OSS richiede almeno una revisione annuale, ma le revisioni possono essere parziali o più frequenti a seconda delle attività.

## Obiettivi

Le Attività canoniche definite dal GGI sono organizzate in obiettivi. Ogni obiettivo affronta un'area specifica di progresso all'interno del processo. Dall'Uso alla Strategia, gli Obiettivi coprono questioni relative a tutte le parti interessate, dai team di sviluppo fino al management di livello superiore.

* **Obiettivo Utilizzo (Usage Goal)**: questo obiettivo riguarda le fasi preliminari dell'utilizzo del software open source. Le attività relative all'Obiettivo Utilizzo coprono i primi passi di un programma open source, identificando l'efficienza con cui l'open source viene utilizzato e i vantaggi che apporta all'organizzazione. Include la formazione e la gestione delle conoscenze, la produzione di inventari dell'open source già utilizzato all'interno dell'azienda e la presentazione di alcuni concetti di open source che possono essere utilizzati nel corso del processo.
* **Obiettivo Fiducia (Trust Goal)**: questo obiettivo riguarda l'utilizzo sicuro dell'open source. Trust Goal si occupa della conformità legale, della gestione delle dipendenze e delle vulnerabilità e, in generale, mira a creare fiducia nel modo in cui l'organizzazione utilizza e gestisce l'open source.
* **Obiettivo Culturale (Culture Goal)**: l'obiettivo culturale comprende attività volte a mettere i team a proprio agio con l'open source, a partecipare individualmente alle attività di collaborazione, a comprendere e a implementare le migliori pratiche dell'open source. Questo obiettivo promuove un senso di appartenenza alla comunità open source tra gli individui.
* **Obiettivo Coinvolgimento (Engagement Goal)**: Questo obiettivo mira a impegnare un'organizzazione nell'ecosistema open source. Vengono stanziate risorse umane e finanziarie per contribuire ai progetti open source. In questo caso, l'organizzazione assume il proprio ruolo civico nell'open source e la responsabilità di garantire la sostenibilità dell'ecosistema.
* **Obiettivo Strategia (Strategy goal)**: questo obiettivo consiste nel rendere l'open source visibile e accettabile ai livelli più alti della gestione aziendale. Si tratta di riconoscere che l'open source è un fattore strategico di sovranità digitale, di innovazione dei processi e, in generale, una fonte di attrattiva e di buona volontà.

## Attività canoniche

Le Canonical Activities (attività canoniche) sono al centro del progetto GGI. Nella sua versione iniziale, la Metodologia GGI prevede cinque Canonical Activities per ogni obiettivo, per un totale di 25. Le Canonical Activities sono descritte utilizzando le seguenti sezioni predefinite:

* *Description*: una sintesi dell'argomento trattato dall'attività e delle fasi di completamento.
* *Opportunity Assessment*: descrive perché e quando è importante intraprendere questa attività.
* *Progress Assessment*: descrive come misurare i progressi dell'attività e valutarne il successo.
* *Tools*: un elenco di tecnologie o strumenti che possono aiutare a realizzare questa attività.
* *Recommendations*: suggerimenti e buone pratiche raccolte dai partecipanti al GGI.
* *Resources*: link e riferimenti per approfondire l'argomento trattato nell'attività.

### Descrizione

Questa sezione fornisce una descrizione di alto livello delle Activity, una sintesi dell'argomento per definire lo scopo dell'attività nel contesto dell'approccio open source all'interno di un obiettivo.

### Valutazione delle opportunità

Per aiutare a strutturare un approccio iterativo, ogni attività ha una sezione "Opportunity Assessment" (Valutazione dell'opportunità), con una o più domande allegate. La valutazione dell'opportunità si concentra sul motivo per cui è importante intraprendere l'attività e su quali esigenze essa risponde. La valutazione dell'opportunità aiuterà a definire gli sforzi previsti, le risorse necessarie e a valutare i costi e il ROI atteso.

### Valutazione dei progressi

Questa fase si concentra sulla definizione di obiettivi, KPI (Key Performance Indicators[^kpi]) e sulla definizione di *punti di verifica* che aiutino a valutare i progressi dell'attività. I punti di verifica sono suggerimenti che possono aiutare a definire una tabella di marcia per il processo di buona governance, le sue priorità e le modalità di misurazione dei progressi.

### Strumenti

Di seguito sono elencati gli strumenti che possono aiutare a svolgere l'attività o a gestire una fase specifica dell'attività. Gli strumenti non sono una raccomandazione obbligatoria, né pretendono di essere esaustivi, ma sono suggerimenti o categorie da elaborare in base al contesto esistente.

### Raccomandazioni

Questa sezione viene regolarmente aggiornata con i feedback degli utenti e con tutti i tipi di raccomandazioni che possono aiutare a gestire l'attività.

### Risorse

Vengono proposte risorse per alimentare l'approccio con studi di base, documenti di riferimento, eventi o contenuti online per arricchire e sviluppare l'approccio relativo all'attività. Le risorse non sono esaustive, sono punti di partenza o suggerimenti per ampliare la semantica dell'attività in base al proprio contesto.

## Scorecards personalizzate delle attività

Le Customised Activity Scorecards (CAS) sono leggermente più dettagliate delle attività canoniche. Un CAS include dettagli specifici dell'organizzazione che implementa il GGI. L'uso delle CAS è descritto nella sezione Metodologia.

[^kpi]: Gli indicatori chiave di performance, o Key Performance Indicators, possono permettere di misurare l'efficienza di persone o dipartimenti nell'implementare in modo efficace le attività e mansioni a loro assegnate.
