## Gestire la conformità legale

ID Attività: [GGI-A-21](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_21.md).

### Descrizione

Le organizzazioni devono implementare un processo di conformità legale per tutelare il loro utilizzo e la loro partecipazione a progetti open source.

La gestione matura e professionale della conformità legale, all'interno dell'organizzazione e lungo la catena di fornitura, riguarda:

- Esecuzione di un'analisi approfondita della proprietà intellettuale che includa l'identificazione della licenza e la verifica della compatibilità.
- Garantire che l'organizzazione possa utilizzare, integrare, modificare e ridistribuire in modo sicuro componenti open source come parte dei suoi prodotti o servizi.
- Fornire a dipendenti e collaboratori un processo trasparente su come creare e contribuire al software open source.

*Analisi della composizione del software (SCA)*: Una parte significativa dei problemi legali e di proprietà intellettuale deriva dall'uso di componenti rilasciati sotto licenze che sono incompatibili tra loro o incompatibili con il modo in cui l'organizzazione vuole usare e ridistribuire i componenti. La SCA è il primo passo per risolvere questi problemi, perché " bisogna conoscere il problema per poterlo risolvere". Il processo consiste nell'identificare tutti i componenti coinvolti in un progetto in un documento di distinta base, comprese le dipendenze di build e test.

*Controllo delle licenze*: Un processo di controllo delle licenze utilizza uno strumento per analizzare automaticamente la base di codice e identificare le licenze e i diritti d'autore al suo interno. Se eseguito regolarmente e idealmente integrato nelle catene di compilazione e integrazione continue, consente di individuare tempestivamente i problemi di proprietà intellettuale.

### Valutazione delle opportunità

Con l'uso sempre più diffuso degli OSS nei sistemi informativi di un'organizzazione, è essenziale valutare e gestire la potenziale esposizione legale.

Tuttavia, la verifica delle licenze e dei diritti d'autore può essere complicata e costosa. Gli sviluppatori devono essere in grado di verificare rapidamente la proprietà intellettuale e le questioni legali. Avere un team e un responsabile aziendale dedicato alla proprietà intellettuale e alle questioni legali garantisce una gestione proattiva e coerente delle questioni legali, aiuta a garantire l'uso e i contributi dei componenti open source e fornisce una chiara visione strategica.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa Attività:

- [ ] Per i progetti è disponibile un semplice processo di verifica delle licenze.
- [ ] Per i progetti è disponibile un semplice processo di verifica della proprietà intellettuale.
- [ ] All'interno dell'organizzazione esiste un team o una persona responsabile della conformità legale.
- [ ] Sono previsti audit periodici per valutare la conformità legale.

Altri modi per impostare i punti di verifica:

- [ ] There is an easy-to-access legal/IP team.
- [ ] Tutti i progetti forniscono le informazioni richieste affinché le persone possano utilizzarle e contribuire al progetto.
- [ ] C'è un contatto nel team per le domande relative alla proprietà intellettuale e alle licenze.
- [ ] Esiste un dipendente aziendale dedicato alla proprietà intellettuale e alle licenze.
- [ ] Esiste un team dedicato alle domande relative alla proprietà intellettuale ed alle licenze.

### Strumenti

- [ScanCode](https://scancode-toolkit.readthedocs.io)
- [Fossology](https://www.fossology.org/)
- [SW360](https://www.eclipse.org/sw360/)
- [Fossa](https://github.com/fossas/fossa-cli)
- [OSS Review Toolkit](https://oss-review-toolkit.org)

### Raccomandazioni

- Informare le persone sui rischi associati alla concessione di licenze in conflitto con gli obiettivi aziendali.
- Proporre una soluzione semplice per i progetti per impostare il controllo delle licenze sul loro codice.
- Comunicare la sua importanza e aiutare i progetti ad aggiungerlo ai loro sistemi di CI.
- Fornire un modello o linee guida ufficiali per la struttura del progetto.
- Impostare controlli automatici per assicurarsi che tutti i progetti siano conformi alle linee guida.
- Considerare la possibilità di condurre un audit interno per identificare le licenze dell'infrastruttura aziendale.
- Fornire una formazione di base sulla proprietà intellettuale e sulle licenze ad almeno una persona per team.
- Fornire una formazione completa sulla proprietà intellettuale e sulle licenze per il responsabile.
- Creare un processo di escalation delle questioni relative alla proprietà intellettuale e alle licenze per il responsabile.

Ricordate che la conformità non riguarda solo la legge, ma anche la proprietà intellettuale. Ecco quindi alcune domande che possono aiutare a comprendere le conseguenze della conformità legale:

- Se distribuisco un componente open source e non rispetto le condizioni della licenza, violo la licenza stessa --> implicazioni legali.
- Se utilizzo un componente open source all'interno di un progetto che desidero distribuire/pubblicare, tale licenza può obbligare a rendere visibili elementi del codice che non voglio rendere open source --> Impatto sulla riservatezza per il vantaggio tattico della mia azienda e con terze parti (implicazioni legali).
- È una discussione aperta il fatto che l'uso di una licenza open source per un progetto che voglio pubblicare garantisca una proprietà intellettuale rilevante --> implicazioni sulla proprietà intellettuale.
- Se rendo un progetto open source *prima di* qualsiasi processo di brevettazione, questo *probabilmente* esclude la creazione di brevetti riguardanti il progetto --> implicazioni sulla proprietà intellettuale.
- Se rendo un progetto open source *dopo* qualsiasi processo di brevettazione, questo *probabilmente* permette la creazione di brevetti (difensivi) relativi a quel progetto --> potenziale proprietà intellettuale.
- In progetti complessi che coinvolgono molti componenti con molte dipendenze, la moltitudine di licenze open source può presentare incompatibilità tra le licenze --> implicazioni legali (cfr. Attività GGI-A-23 - Manage software dependencies).

### Risorse

- Un ampio elenco di strumenti è disponibile nella pagina [Existing OSS compliance group](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence-Compliance-Tools/).
- [Recommended Open Source Compliance Practices for the enterprise](https://www.ibrahimatlinux.com/wp-content/uploads/2022/01/recommended-oss-compliance-practices.pdf). Un libro di Ibrahim Haddad, della Linux Foundation, sulle pratiche di conformità all'open source per le aziende. [OpenChain Project](https://www.openchainproject.org/)

### Attività successive proposte

- [GGI-A-24 - Manage key indicators](https://ospo-alliance.org/ggi/activities/manage_key_indicators) Evidenzia e rendi misurabile le esigenze di conformità legale, dei correlati processi e risultati. Questo aiuterà gli interlocutori a capirne l'importanza fin dall'inizio del processo.
