## Promuovere le migliori pratiche di sviluppo open source

Attività ID: [GGI-A-25](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_25.md).

### Descrizione

Questa attività consiste nel definire, promuovere attivamente ed implementare le migliori pratiche open source all'interno dei team di sviluppo.

Come punto di partenza, si potrebbero prendere in considerazione i seguenti argomenti:

- Documentazione per utenti e sviluppatori.
- Organizzazione corretta del progetto su un repository accessibile al pubblico.
- Promuovere e implementare il riutilizzo controllato.
- Fornire una documentazione completa e aggiornata sul prodotto.
- Configuration Management: flussi di lavoro git, modelli collaborativi.
- Release management: rilascio anticipato e frequente, versioni stabili o di sviluppo, ecc.

I progetti OSS hanno un modus operandi speciale, simile a un [bazar](http://www.catb.org/~esr/writings/cathedral-bazaar/). Per consentire e favorire questa collaborazione e questa mentalità, si raccomandano alcune pratiche che facilitano lo sviluppo collaborativo e decentralizzato e i contributi di sviluppatori terzi…

#### Documenti per la community

Assicuratevi che tutti i progetti all'interno dell'azienda propongano i seguenti documenti:

- README -- descrizione rapida del progetto, come interagire, link alle risorse.
- Contributing -- introduzione per le persone disposte a contribuire.
- Code Of Conduct: cosa è accettabile o meno come comportamento all'interno della comunità.
- LICENSE -- la licenza predefinita del repository.

#### REUSE best practices

[REUSE](https://reuse.software) è un'iniziativa della [Free Software Foundation Europe](https://fsfe.org/) per migliorare il riutilizzo del software e semplificare la conformità agli OSS e alle licenze.

### Valutazione delle opportunità

Sebbene dipenda fortemente dalla conoscenza comune dell'OSS da parte del team, la formazione delle persone e la creazione di processi che impongano queste pratiche è sempre utile. È ancora più importante quando:

- potenziali utenti e contributori non sono noti,
- gli sviluppatori non sono abituati allo sviluppo open source.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa Attività:

- [ ] Il progetto stabilisce un elenco di best practice open source da rispettare.
- [ ] Il progetto monitora il suo allineamento con le migliori pratiche.
- [ ] Il team di sviluppo si è sensibilizzato al rispetto delle best practice OSS.
- [ ] Le nuove best practice vengono valutate regolarmente e si cerca di implementarle.

### Strumenti

- Lo strumento [REUSE helper tool](https://github.com/fsfe/reuse-tool) aiuta a rendere un repository conforme alle migliori pratiche [REUSE](https://reuse.software). Può essere incluso in molti processi di sviluppo per confermare lo stato attuale.
- [ScanCode](https://scancode-toolkit.readthedocs.io) ha la capacità di elencare tutti i documenti comunitari e legali presenti nell'archivio: vedere [descrizione della funzione](https://scancode-toolkit.readthedocs.io/en/latest/cli-reference/scan-options-pre.html#classify).
- GitHub ha una bella funzione per [controllare i documenti mancanti della comunità](https://docs.github.com/articles/viewing-your-community-profile). Si trova nella pagina del repository > "Insights" > "Community".

### Raccomandazioni

- L'elenco delle migliori pratiche dipende dal contesto e dall'ambito del programma e deve essere rivalutato regolarmente in un'ottica di miglioramento continuo. Le pratiche devono essere monitorate e valutate regolarmente per monitorare i progressi.
- Formare le persone sul riutilizzo degli OSS (come consumatori) e sugli ecosistemi (come contributori).
- Implementare REUSE.software come nell'activity #14.
- Impostare un processo per gestire i rischi legali associati al riutilizzo e ai contributi.
- Incoraggiare esplicitamente le persone a contribuire a progetti esterni.
- Fornire un modello o linee guida ufficiali per la struttura del progetto.
- Impostare controlli automatici per assicurarsi che tutti i progetti siano conformi alle linee guida.

### Risorse

- [elenco di best practice open source di OW2](https://www.ow2.org/view/MRL/Full_List_of_Best_Practices) dalla metodologia di valutazione Market Readiness Levels.
- [Sito ufficiale di REUSE](https://reuse.software) con specifiche, tutorial e FAQ.
- [Linee guida della comunità di GitHub](https://opensource.guide/).
- Un esempio di [migliori pratiche di gestione della configurazione con GitHub](https://dev.to/datreeio/top-10-github-best-practices-3kl2).

### Attività successive proposte

- [GGI-A-42 - Manage open source skills and resources](https://ospo-alliance.org/ggi/activities/manage_open_source_skills_and_resources) Potete aggiungere l'elenco delle migliori pratiche di sviluppo open source identificate nel materiale formativo.
- [GGI-A-44 - Run code reviews](https://ospo-alliance.org/ggi/activities/run_code_reviews/) Le procedure di revisione del codice sono elementi fondamentali delle migliori pratiche di sviluppo.
