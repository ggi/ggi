## Principio detto "Upstream first"

Activity ID: [GGI-A-39](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_39.md).

### Descrizione

Questa attività si occupa di sviluppare la consapevolezza dei benefici del ritorno del contributo e di far rispettare il principio "upstream first".

Con l'approccio upstream first, tutto lo sviluppo di un progetto open source deve essere realizzato con il livello di qualità e apertura richiesto per essere sottoposto agli sviluppatori principali del progetto e da questi pubblicato.

### Valutazione delle opportunità

Scrivere codice secondo l'approccio upstream first porta a:

- codice di migliore qualità,
- pronto per essere inviato in upstream,
- codice unito (merged) al software principale,
- compatibile con le versioni future,
- riconoscimento da parte della comunità di progetto e una collaborazione migliore e più proficua.

> Upstream First è più di un semplice "essere gentili". Significa avere voce in capitolo nel progetto. Significa prevedibilità. Significa avere il controllo. Significa agire piuttosto che reagire. Significa capire l'open source. ([Maximilian Michels](https://maximilianmichels.com/2021/upstream-first/))

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi di questa attività: La fase a monte è stata implementata?

- [ ] Aumento significativo del numero di richieste di pull/merge inviate a progetti di terze parti.
- [ ] È stato redatto un elenco di progetti di terzi per i quali è necessario applicare il principio "upstream first".

### Raccomandazioni

- Identificare gli sviluppatori con maggiore esperienza nell'interazione con gli sviluppatori upstream.
- Facilitare l'interazione tra sviluppatori e sviluppatori principali (eventi, hackathon, ecc.)

### Risorse

- Una chiara spiegazione del principio Upstream First e del perché si inserisce nell'Obiettivo Cultura: <https://maximilianmichels.com/2021/upstream-first/>.

> Upstream First significa che ogni volta che si risolve un problema nella propria copia del codice upstream da cui altri potrebbero trarre beneficio, si contribuiscono le modifiche a monte, cioè si invia una patch o si apre una richiesta di pull al repository upstream.

- [Cos'è l'Upstream e il Downstream nello sviluppo del software?](https://reflectoring.io/upstream-downstream/) Una spiegazione chiarissima.
- Spiegato dai documenti di progettazione di Chromium OS: [Upstream First](https://www.chromium.org/chromium-os/chromiumos-design-docs/upstream-first).
- Red Hat su upstream e i vantaggi di [upstream first](https://www.redhat.com/en/blog/what-open-source-upstream).

### Attività successive proposte

- [GGI-A-25 - Promote open source development best practices](https://ospo-alliance.org/ggi/activities/promote_open_source_development_best_practices) Contribuire al progetto originale (upstream) è una delle principali migliori pratiche. Fatelo parte delle migliori pratiche aziendali, aiuterà con contribuzioni esterne ed internamente migliorerà la qualità del codice e la condivisione delle informazioni.
