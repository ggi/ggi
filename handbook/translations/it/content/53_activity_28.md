## Prospettiva del dipartimento risorse umane

Attività ID: [GGI-A-28](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_28.md).

### Descrizione

Il passaggio alla cultura open source ha un impatto profondo sulle risorse umane:

- **Nuovi processi e contratti**: I contratti devono essere adattati per consentire e promuovere i contributi esterni. Ciò include le questioni relative alla proprietà intellettuale e alle licenze per il lavoro svolto all'interno dell'azienda, ma anche la possibilità per il dipendente o l'appaltatore di avere i propri progetti.
- **Diversi tipi di persone**: Le persone che lavorano con l'open source hanno spesso incentivi e mentalità diverse rispetto a quelle delle aziende proprietarie. I processi e le mentalità devono adattarsi a questo paradigma orientato alla reputazione della comunità, per attrarre nuovi tipi di talenti e trattenerli.
- **Sviluppo della carriera**: è necessario offrire un percorso di carriera che valorizzi i dipendenti per le loro competenze tecniche e trasversali e per le competenze attese dall'organizzazione (collaborazione per guidare gli sforzi della comunità, comunicazione per agire come portavoce dell'azienda, ecc.) In ogni caso, le risorse umane hanno un ruolo chiave nel rendere l'open source un obiettivo culturale.

**Forza lavoro** Per uno sviluppatore che ha lavorato a lungo sulla stessa soluzione proprietaria, passare all'open source può sembrare un cambiamento e richiedere un adattamento. Ma per la maggior parte degli sviluppatori il software open source porta solo vantaggi.

Gli sviluppatori che escono dalla scuola o dall'università oggi hanno sempre lavorato sull'open source. All'interno di un'azienda, la grande maggioranza degli sviluppatori utilizza linguaggi open source e importa librerie o snippet open source ogni giorno. È infatti molto più facile incollare righe di codice open source in un programma che attivare il processo di sourcing interno, che passa attraverso molteplici convalide attraverso la linea manageriale.

L'open source rende il lavoro dello sviluppatore più interessante, perché lo sviluppatore è sempre alla ricerca di ciò che i suoi colleghi al di fuori dell'azienda hanno inventato, e quindi rimane all'avanguardia della tecnologia.

Per un'organizzazione, è necessaria una strategia delle risorse umane per 1/ qualificare o riqualificare la forza lavoro esistente 2/ riflettere e posizionare l'azienda sull'assunzione di nuovi talenti, e dunque comunicare qual è l'attrattiva dell'azienda quando si tratta di open source.

> "Getting people with a good FLOSS mindset, who already understand the code, and know how to work well with others is wonderful. The alternative of evangelising / training / interning is worth doing but more expensive & time consuming."
>
> &mdash; <cite>CEO di un fornitore di software OSS</cite>

Ciò dimostra che l'assunzione di persone con un DNA open source è un percorso di accelerazione da considerare nella strategia delle risorse umane.

#### Processi

- Stabilire o rivedere le descrizioni delle mansioni (competenze tecniche, soft skill, competenze ed esperienze)
- Programmi di formazione: autoformazione, formazione formale, coaching manageriale, peer mapping, comunità
- Stabilire o rivedere il percorso di carriera: competenze, risultati/impatti chiave e fasi di carriera

### Valutazione delle opportunità

1. Pratiche di sviluppo: il problema probabilmente non è tanto quello di spronare gli sviluppatori a usare più open source, quanto piuttosto quello di assicurarsi che lo usino in modo sicuro, nel rispetto dei termini di licenza di ogni tecnologia open source e senza abbandonare i tradizionali controlli di sicurezza (le linee di codice open source potrebbero contenere codice malevolo),
1. Rivedere le pratiche di collaborazione: con le pratiche di sviluppo, l'opportunità è quella di estendere l'agilità e la collaborazione ad altre linee di business dell'organizzazione. Per promuovere questi comportamenti si ricorre spesso all'inner sourcing, anche se questo potrebbe essere un mezzo passo avanti verso la cultura dell'open source,
1. Cultura dell'organizzazione: alla fine, tutto dipende dalla cultura della vostra organizzazione: l'open source può essere il fiore all'occhiello di valori come l'apertura, la collaborazione, l'etica, la sostenibilità.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa Attività:

- [ ] Sono disponibili corsi di formazione per presentare sia i vantaggi che i vincoli (conformità ai termini di licenza della proprietà intellettuale) legati all'open source.
- [ ] Ogni sviluppatore, architect, responsabile di progetto (o Product Owner/Business Owner) comprende i vantaggi e i vincoli (conformità ai termini di licenza della proprietà intellettuale) legati all'open source.
- [ ] Gli sviluppatori sono incoraggiati a contribuire alle comunità open source e ad assumersene la responsabilità, e potrebbero ricevere una formazione adeguata per farlo.
- [ ] Le abilità e le competenze si riflettono nelle descrizioni delle mansioni e nelle fasi di carriera dell'organizzazione.
- [ ] L'esperienza acquisita dagli sviluppatori nell'open source (contributi alle comunità open source, partecipazione al processo di compliance interna, portavoce esterni dell'azienda, ...) viene presa in considerazione nel processo di valutazione delle risorse umane.

### Strumenti

- Matrice di competenze.
- Programmi di formazione pubblica (es. scuola open source).
- Sourcing: GitHub, GitLab, LinkedIn, Meetup, Epitech, Epita…
- Modelli di contratto (clausola di fedeltà).
- Descrizioni di lavoro (modelli) e fasi di carriera (modelli).

### Raccomandazioni

Nella maggior parte dei casi, oggi gli sviluppatori conoscono già alcuni principi dell'open source e sono disposti a lavorare con e sul software open source. Tuttavia, ci sono ancora alcune azioni che il management dovrebbe intraprendere:

- Preferenza per l'esperienza OSS nelle assunzioni, anche se il lavoro per cui lo sviluppatore viene assunto riguarda solo la tecnologia proprietaria. È probabile che, con la trasformazione digitale, lo sviluppatore debba un giorno lavorare sull'open source.
- Programma di formazione OSS: Ogni sviluppatore, ogni architetto, ogni responsabile di progetto (o Product Owner/Business Owner) dovrebbe avere accesso a risorse di formazione (video o formazione faccia a faccia) che presentino i vantaggi dell'open source e anche i vincoli in termini di proprietà intellettuale e conformità alle licenze.
- La formazione dovrebbe essere disponibile per gli sviluppatori che vogliono contribuire alle comunità open source e far parte degli organi di governance di queste comunità (certificazioni Linux).
- Riconoscimento, nei processi di valutazione personale delle risorse umane, del contributo del dipendente (sviluppatore o architetto) ai temi legati all'open source, come i contributi alle comunità open source e il rispetto dei termini di licenza della proprietà intellettuale. La maggior parte degli argomenti sono condivisi e rientrano nei percorsi di carriera tecnici, mentre alcuni potrebbero o dovrebbero essere specifici.
- Il segreto meglio custodito e la posizione dell'azienda: è necessario affrontare gli aspetti della comunicazione (quanto è importante per la vostra organizzazione, tanto da essere riflesso nel vostro rapporto annuale), come influisce sulla vostra posizione di comunicazione (un collaboratore open source potrebbe essere un portavoce della vostra azienda, compresi i contatti con la stampa).

### Risorse

- Per quanto riguarda la capacità delle persone di parlare al di fuori dell'azienda durante gli eventi, si veda Activity 31: "(Obiettivo di coinvolgimento) Affermare pubblicamente l'uso dell'open source".
