## Gestire le competenze e le risorse open source

Activity ID: [GGI-A-42](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_42.md).

### Descrizione

Questa attività è incentrata su competenze e risorse in **software development** . Include le tecnologie e le competenze specifiche degli sviluppatori, nonché i processi di sviluppo, i metodi e gli strumenti generali.

Per le tecnologie open source è disponibile una grande quantità di documentazione, forum e discussioni derivanti dall'ecosistema e risorse pubbliche. Per beneficiare appieno dell'approccio open source, è necessario stabilire una roadmap delle risorse attuali e degli obiettivi desiderati per impostare un programma coerente di competenze, metodi e strumenti di sviluppo all'interno dei team.

**Settori di applicazione**

È necessario stabilire le aree in cui il programma sarà applicato e come migliorerà la qualità e l'efficienza del codice e delle pratiche. Ad esempio, il programma non avrà gli stessi benefici se c'è un solo sviluppatore che lavora su componenti open source, o se l'intero ciclo di vita dello sviluppo è ottimizzato per includere le migliori pratiche open source.

È necessario definire l'ambito di applicazione dello sviluppo open source: componenti tecnici, applicazioni, modernizzazione o creazione di nuovi sviluppi. Esempi di pratiche di sviluppo che possono beneficiare dell'open source sono:

- Amministrazione del cloud.
- Applicazioni cloud-native, come innovare con queste tecnologie.
- DevOps, Continuous Integration / Continuous Delivery.

**Categorie**

- Competenze e risorse necessarie per sviluppare software open source: proprietà intellettuale, licenze, pratiche.
- Competenze e risorse necessarie per sviluppare software utilizzando componenti, linguaggi e tecnologie open source.
- Competenze e risorse necessarie per utilizzare metodi e processi open source.

### Valutazione delle opportunità

Gli strumenti open source sono sempre più diffusi tra gli sviluppatori. Questa Attività affronta la necessità di evitare la proliferazione di strumenti eterogenei all'interno di un team di sviluppo. Aiuta a definire una politica in questo ambito. Aiuta a ottimizzare la formazione e la creazione di esperienza. Un inventario delle competenze viene utilizzato per l'assunzione, la formazione e la pianificazione della successione nel caso in cui un dipendente che ricopre un ruolo chiave lasci l'azienda.

Avremmo bisogno di una metodologia per mappare le competenze di sviluppo del software open source.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa Attività:

- [ ] È presente una descrizione della catena di produzione dell'open source (la "filiera del software"),
- [ ] Esiste un piano (o una lista di desideri) per la razionalizzazione delle risorse di sviluppo,
- [ ] Esiste un inventario delle competenze che riassume le competenze, l'istruzione e l'esperienza degli attuali sviluppatori,
- [ ] Esiste un elenco dei desideri di formazione e un programma che si occupa delle carenze di competenze,
- [ ] Esiste un elenco di best practice di sviluppo open source mancanti e un piano per applicarle.

### Raccomandazioni

- Iniziare in modo semplice, far crescere l'analisi e la roadmap in modo costante.
- Nel reclutamento, ponete una forte enfasi sulle competenze e sull'esperienza open source. È sempre più facile quando le persone hanno già un DNA open source piuttosto che formare e addestrare le persone.
- Verificate i programmi di formazione dei fornitori di software e delle scuole open source.

### Risorse

Ulteriori informazioni:

- Un'introduzione a [cos'è uno Skills Inventory?](https://managementisajourney.com/management-toolbox-better-decision-making-with-a-skills-inventory) di Robert Tanner.
- Un articolo sulle competenze open source: [5 competenze open source per migliorare il vostro gioco e il vostro curriculum](https://sourceforge.net/blog/5-open-source-skills-game-resume/)

Questa attività può includere risorse e competenze tecniche quali:

- **Linguaggi popolari** (come Java, PHP, Perl, Python).
- **Framework open source** (Spring, AngularJS, Symfony) e strumenti di test.
- **Metodi di sviluppo e best practice** Agile, DevOps e open source.

### Attività successive proposte

- [GGI-A-28 - Human Resources perspective](https://ospo-alliance.org/ggi/activities/human_resources_perspective) Once open source resources have been identified internally to help with open source awareness, make the Human Resources department value them as well, for both existing and future employees.
