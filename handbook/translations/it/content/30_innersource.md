# InnerSource

L'innerSource è una pratica che sta guadagnando sempre più popolarità all'interno delle aziende in quanto offre un approccio basato sulle pratiche di comprovato successo utilizzate dai gruppi di sviluppo di software Open Source. "Fare InnerSource" non vuol semplicemente dire copiare le medesime pratiche, è necessario adeguarle alla cultura ed ai processi interni dell'azienda. Vediamo più in dettaglio cosa l'InnerSource è e non è e quali sono le complessità che bisogna gestire.

## Cos'è l'InnerSource?

Il termine è stato coniato da Tim O'Reilly nel 2000 dicendo che l'InnerSourcing è "[...] *l'uso di tecniche di sviluppo del software Open Source all'interno delle aziende.*"

Secondo [InnerSource Commons](https://innersourcecommons.org/learn/books/innersource-patterns/), la pubblicazione di riferimento sulla materia, InnerSource è "*l'uso dei principi e delle pratiche Open Source per lo sviluppo di software Open Source all'interno dei confini, materiali e procedurali, dell'azienda.*"

## Perché InnerSource?

Ancora secondo l'[InnerSource Commons](https://innersourcecommons.org/learn/books/innersource-patterns/), "*per aziende che principalmente scrivono software proprietario o "closed source", InnerSource è uno strumento perfetto per eliminare i silo, incoraggiare ed estendere le collaborazioni interne, velocizzare l'integrazione di un nuovo specialista ed per identificare le opportunità per contribuire a progetti Open Source.*"

E' di particolare interesse notare che l'InnerSource può avere impatti benefici per varie mansioni all'interno dell'azienda, non solo per gli sviluppatori. Come risulto diverse aziende hanno trovato vantaggi concreti in area come:

* Direzione legale: facilitazione ed accelerazione delle cooperazioni tra dipartimenti aziendali grazie all'implementazione di un framework legale pronto all'uso (licenza InnerSource).
* Risorse umane: gestione delle competenze interne tramite un core team con esperienza e responsabilità nel raggruppare e valorizzare gli skill disponibili.

## Controversie relative all'InnerSource

InnerSource è circondato da miti comuni che vengono diffusi dai suoi detrattori. Mentre non è realmente Open Source mostra dei potenziali benefici per organizzazioni che implementano questo approccio internamente. Alcuni di quei miti sono:

* [MITO] InnerSource viene implementato a danno dell'Open Source (principalmente contribuzioni esterne):
   * I progetti di sviluppo software rimangono all'interno dell'azienda.
   * Riduce contribuzioni ai progetti Open Source esterni.
* [MITO] Si appropria dello spirito dell'Open Source invece di contribuirvi attivamente.
* [MITO] Nessun progetto InnerSource è diventato un progetto Open Source.
* [MITO] Il motivo per applicare l'InnerSource è che è similare all'Open Source. In realtà, se uno sviluppatore lo apprezza, allora una contribuzione diretta al progetto Open Source dovrebbe essere sempre preferita.

Ecco alcuni fatti sull'InnerSource che invalidano i miti elencati precedentemente:

* [FATTO] InnerSource è un mezzo per far avvicinare aziende che sviluppano software Closed Source al mondo Open Source.
* [FATTO] Mentre la maggior parte delle contribuzioni a progetti Open Source sono fatte da volontari, possiamo promuovere la partecipazione nell'Open Source a sviluppatori utilizzando questo elenco di "benefici percepiti".
* [FATTO] In tanti, o forse nella maggior parte dei casi, le aziende non seguono metodologie di sviluppo strutturate a questa guida (GGI) può aiutare le aziende a farlo.
* [FATTO] C'è comunque da fare *tanto* lavoro per convertire il software sviluppato da licenza chiusa ad aperta.
* [FATTO] Ci sono tanti esempi di progetti InnerSource rilasciati con licenze Open Source:
   * Twitter Bootstrap.
   * Kubernetes creato da Google.
   * Docker di dotCloud (Nome precedentemente usato da Docker Inc.).
   * React Native.
* [FATTO] Open Source beneficia dall'aumento di sviluppatori abituati a sviluppare software Open Source ed InnerSource può aiutare in quanto sono basati su concetti similari.

## Chi lo fa?

Tante aziende hanno creato iniziative InnerSource o ISPO (InnerSource Program Office), alcune da tanto tempo mentre altre hanno iniziato di recente. La seguente lista, focalizzata principalmente su aziende Europee, ne contiene solo alcuni esempi:

* Banco de Santander ([source](https://patterns.innersourcecommons.org/p/innersource-portal))
* BBC - British Broadcasting Corporation ([source](https://www.youtube.com/watch?v=pEGMxe6xz-0))
* Bosch ([source](https://web.archive.org/web/20230429145619/https://www.bosch.com/research/know-how/open-and-inner-source/))
* Comcast ([source](https://www.youtube.com/watch?v=msD-8-yrGfs&t=6s))
* Ericsson ([source](https://innersourcecommons.org/learn/books/adopting-innersource-principles-and-case-studies/))
* Engie ([source](https://github.com/customer-stories/engie))
* IBM ([source](https://resources.github.com/innersource/fundamentals/))
* Mercedes ([source](https://www.youtube.com/watch?v=hVcGABbmI4Y))
* Microsoft ([source](https://www.youtube.com/watch?v=eZdx5MQCLA4))
* Nike ([source](https://www.youtube.com/watch?v=srPG-Tq0HIs&list=PLq-odUc2x7i-A0sOgr-5JJUs5wkgdiXuR&index=46))
* Nokia ([source](https://www.nokia.com/thought-leadership/articles/openness/openness-drives-innovation/))
* SNCF Connect & Tech ([source 1](https://twitter.com/FrancoisN0/status/1645356213712846853), [source 2](https://www.slideshare.net/FrancoisN0/opensource-innersource-pour-acclrer-les-dveloppements))
* Paypal ([source](https://innersourcecommons.org/fr/learn/books/getting-started-with-innersource/))
* Philips([source](https://medium.com/philips-technology-blog/how-philips-used-innersource-to-maintain-its-edge-in-innovation-38c481e6fa03))
* Renault ([source](https://www.youtube.com/watch?v=aCbv46TfanA))
* SAP ([source](https://community.sap.com/topics/open-source/publications))
* Siemens([source](https://jfrog.com/blog/creating-an-inner-source-hub-at-siemens))
* Société Générale([source](https://github.com/customer-stories/societe-generale))
* Thales ([source](https://www.youtube.com/watch?v=aCbv46TfanA))
* VeePee([source](https://about.gitlab.com/customers/veepee))

## InnerSource Commons, un riferimento essenziale

Una comunità attiva e vibrante di persone che seguono i principi InnerSource, che funziona applicando i principi open source, può essere trovata su [InnerSource Commons](https://innersourcecommons.org). I partecipanti mettono a disposizione tante risorse utili a farvi approfondire la materia incluso [patterns](https://innersourcecommons.org/learn/patterns/), un [learning path](https://innersourcecommons.org/learn/learning-path/) e piccole guide:

* [Getting Started with InnerSource](https://innersourcecommons.org/learn/books/getting-started-with-innersource/) di Andy Oram.
* [Understanding the InnerSource Checklist](https://innersourcecommons.org/learn/books/understanding-the-innersource-checklist/) di Silona Bonewald.

## Differenze nella governance InnerSource

Con l'InnerSource, le aziende che sviluppano software proprietario, devono gestire problematiche specifiche che non si riscontrano con l'open source:

* Licenze software specifiche ad uso aziendale per progetti InnerSource (per grandi aziende formate da diverse entità legali).
* La natura delle licenze open source evitano la problematica di dover gestire il trasferimento dei costi infragruppo/intercompany. La natura privata dell'InnerSource espone le aziende che operano in diverse giurisdizioni ad ulteriori verifiche relative a potenziali trasferimenti degli utili.
* Le motivazioni per contribuire sono variegate:
   * L'innerSource dispone di un numero di contributori ridotto in quanto è limitato alle risorse disponibili all'interno dell'organizzazione.
   * Mostrare le proprie capacità professionali è una delle motivazioni che spinge a contribuire. Con l'InnerSource l'impatto e la visibilità sono ristretti all'ambito aziendale.
   * Contribuire a migliorare la società è un'altra motivazione che è limitata con l'InnerSource.
   * Creare motivazioni personali richiede sforzi maggiori focalizzati principalmente su ricompense ed incarichi adeguati.
   * Con l'InnerSource, per alcuni perfezionisti, può risultare più facile gestire la sindrome dell'impostore in quanto la visibilità del codice è limitata.
* L'esternalizzazione delle mansioni è più frequente e questo influenza la governance in molti modi.
* Valutare l'adeguatezza del software ai fini aziendali è più facile con l'InnerSource in quanto viene sviluppato internamente.
* La reperibilità delle informazioni relative ai progetti è una problematica spesso non prioritaria all'interno delle aziende. Motori di ricerca pubblici come DuckDuckGo, Google o Bing forniscono un servizio migliore e sono strumenti dei quali l'InnerSource non si può avvantaggiare.
* L'InnerSource permette un miglior controllo di quello che viene rilasciato esternamente in quanto tutti i processi sono gestiti internamente.
* Procedure per l'implementazione di un controllo perimetrale, per evitare che segreti commerciali o proprietà intellettuali vengano rivelate attraverso il codice sorgente, devono essere implementate.

L'innerSource è in evoluzione continua grazie alla condivisione delle esperienze da parte di aziende che adottano i suoi principi. In una prossima versione di questa guida verrà incluso un'elenco delle attività rilevanti per chi implementa l'InnerSource all'interno della loro organizzazione.
