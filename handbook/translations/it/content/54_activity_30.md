## Sostenere le comunità open source

ID attività: [GGI-A-30](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_30.md).

### Descrizione

Questa attività consiste nel confrontarsi con i rappresentanti istituzionali del mondo open source.

Si ottiene attraverso:

- Aderire alle fondazioni OSS (compresi i costi finanziari dell'adesione).
- Attività di sostegno e promozione delle fondazioni.

Questa attività comporta l'assegnazione ai team di sviluppo e IT di tempo e budget per partecipare alle comunità open source.

### Valutazione delle opportunità

Le comunità open source sono in prima linea nell'evoluzione dell'ecosistema open source. Impegnarsi con le comunità open source presenta diversi vantaggi:

- aiuta a tenersi informati e aggiornati,
- migliora il profilo dell'organizzazione,
- L'adesione comporta dei vantaggi,
- fornisce ulteriore struttura e motivazione al team IT open source.

I costi comprendono:

- quote associative,
- Il tempo del personale e il budget per le trasferte sono stati stanziati per partecipare alle attività della comunità,
- monitorare gli impegni in materia di proprietà intellettuale.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa Attività:

- [ ] L'organizzazione è membro firmatario di una fondazione open source.
- [ ] L'organizzazione partecipa alla governance.
- [ ] Il software sviluppato dall'organizzazione è stato inviato o aggiunto alla base di codice di una fondazione.
- [ ] L'adesione è riconosciuta sui siti web dell'organizzazione e della comunità.
- [ ] Esecuzione della valutazione costi/benefici dell'adesione.
- [ ] È stato nominato un punto di contatto per la comunità.

### Raccomandazioni

- Entrate a far parte di una comunità compatibile con le vostre dimensioni e risorse, ossia una comunità che possa ascoltare la vostra voce e in cui possiate essere un contributo riconosciuto.

### Risorse

- Date un'occhiata a questa [useful page](https://www.linuxfoundation.org/tools/participating-in-open-source-communities/) della Linux Foundation sul perché e come unirsi a una comunità open source.

### Attività successive proposte

- [GGI-A-31 - Publicly assert use of open source](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Adesso che supportate ufficialmente delle comunità OSS, fatelo sapere! Fa bene alla vostra reputazione e fa bene ai progetti in termini di crescita e disseminazione del messaggio.
