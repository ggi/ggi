## Impostare una strategia per la governance aziendale dell'open source

Activity ID: [GGI-A-16](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_16.md).

### Descrizione

La definizione di una strategia di alto livello per la governance dell'open source all'interno dell'azienda assicura la coerenza e la visibilità degli approcci sia verso l'uso interno che verso i contributi e il coinvolgimento esterni. Rende più efficace la comunicazione aziendale offrendo una visione e una leadership chiare e consolidate.

Il passaggio all'open source comporta numerosi vantaggi, ma anche alcuni doveri e un cambiamento nella cultura aziendale. Può avere un impatto sui modelli di business e influenzare il modo in cui un'organizzazione presenta il suo valore e la sua offerta, nonché la sua posizione nei confronti dei clienti e dei concorrenti.

Questa attività comprende i seguenti compiti:

- Istituire un responsabile OSS, con la sponsorizzazione e il sostegno del top management.
- Stabilire e pubblicare una chiara tabella di marcia per l'open source, con obiettivi dichiarati e benefici attesi.
- Assicuratevi che tutto il management di alto livello ne sia a conoscenza e agisca di conseguenza.
- Promuovere l'OSS all'interno dell'azienda: incoraggiare le persone a usarlo, favorire le iniziative interne e il livello di conoscenza.
- Promuovere l'OSS all'esterno dell'azienda: attraverso dichiarazioni e comunicazioni ufficiali e un coinvolgimento visibile nelle iniziative OSS.

La definizione, la pubblicazione e l'applicazione di una strategia chiara e coerente favorisce il coinvolgimento di tutte le persone all'interno dell'azienda e facilita ulteriori iniziative da parte dei team.

### Valutazione delle opportunità

È un buon momento per lavorare su questa attività se:

- Non c'è uno sforzo coordinato da parte del management e l'open source è ancora visto come una soluzione ad hoc.
- Esistono già iniziative interne, ma non penetrano fino ai livelli superiori della dirigenza.
- L'iniziativa è stata avviata da tempo, ma incontra molti ostacoli e non produce ancora i risultati attesi.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa Attività:

- [ ] Esiste una chiara carta di governance dell'open source per l'azienda. La carta dovrebbe contenere:
   - cosa ottenere,
   - per chi lo facciamo,
   - quale sia il potere degli strategist e cosa no.
- [ ] Una roadmap open source è ampiamente disponibile e accettata in tutta l'azienda.

### Raccomandazioni

- Creare un gruppo di persone e processi per definire e monitorare la governance dell'open source all'interno dell'azienda. Assicuratevi che ci sia un chiaro impegno da parte del top management nei confronti delle iniziative open source.
- Condividere la strategia open source all'interno dell'organizzazione, renderla una questione importante e un vero impegno aziendale.
- Assicurarsi che la roadmap e la strategia siano ben comprese da tutti, dai team di sviluppo al management e al personale dell'infrastruttura.
- Comunicare i propri progressi, in modo che le persone sappiano a che punto è l'organizzazione rispetto al proprio impegno. Pubblicare regolarmente aggiornamenti e indicatori.

### Risorse

- [Lista di controllo e riferimenti per l'Open Governance](https://opengovernance.dev/).
- [L'open source comme enjeu de souveraineté numérique, di Cédric Thomas, CEO di OW2, Workshop presso Orange Labs, Parigi, 28 gennaio 2020](https://www.ow2.org/download/OSS_Governance/Level_5/2001-OSSetSouveraineteNumerique-RC3.pdf) (solo in francese).
- [Una serie di guide per la gestione dell'open source in azienda, a cura della Linux Foundation](https://todogroup.org/guides/).
- [Un bell'esempio di documento strategico open source, a cura del gruppo LF Energy](https://www.lfenergy.org/wp-content/uploads/sites/67/2019/07/Open-Source-Strategy-V1.0.pdf)

### Attività successive proposte

- [GGI-A-35 - Open source and digital sovereignty](https://ospo-alliance.org/ggi/activities/open_source_and_digital_sovereignty) Una chiara strategia per la governance dell'open source all'interno dell'azienda aiuta ad acquisire la sovranità digitale. Adesso è un buon momento per definire quelle strategia nel contesto aziendale.
- [GGI-A-34 - C-Level awareness](https://ospo-alliance.org/ggi/activities/c-level_awareness) La partecipazione dei dirigenti è necessaria per implementare correttamente una strategia open source aziendale. Educarli e coinvolgerli è un passo importante per il raggiungimento dell'obbiettivo.
