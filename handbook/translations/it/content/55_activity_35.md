## Open source e sovranità digitale

Activity ID: [GGI-A-35](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_35.md).

### Descrizione

La sovranità digitale può essere definita come

> "Capacità e opportunità di individui e istituzioni di svolgere il proprio ruolo nel mondo digitale in modo indipendente, intenzionale e sicuro" &mdash; Centro di competenza per l'informatica pubblica, Germania

Per condurre correttamente la propria attività, qualsiasi entità deve fare affidamento su altri partner, servizi, prodotti e strumenti. Esaminare i legami e i vincoli di queste dipendenze permette all'organizzazione di valutare e controllare la propria dipendenza da fattori esterni, migliorando così la propria autonomia e resilienza.

Ad esempio, il vendor lock-in è un forte fattore di dipendenza che può ostacolare i processi e il valore aggiunto dell'organizzazione e, pertanto, deve essere evitato. L'open source è una delle vie d'uscita da questo blocco. L'open source svolge un ruolo significativo nella sovranità digitale, consentendo una maggiore scelta tra soluzioni, fornitori e integratori e un maggiore controllo sulle roadmap IT.

Va notato che la sovranità digitale non è una questione di fiducia: è ovvio che dobbiamo fidarci dei nostri partner e fornitori, ma il rapporto diventa ancora migliore quando si basa sul consenso e sul riconoscimento reciproco, piuttosto che su contratti e costrizioni forzate.

Ecco alcuni vantaggi di una migliore sovranità digitale:

- Migliorare la capacità dell'organizzazione di fare le proprie scelte senza vincoli.
- Migliorare la resilienza dell'azienda nei confronti di attori e fattori esterni.
- Migliorare la posizione negoziale nei rapporti con partner e fornitori di servizi.

### Valutazione delle opportunità

- Quanto è difficile/costoso abbandonare una soluzione?
- I fornitori di soluzioni potrebbero imporre condizioni indesiderate sul loro servizio (ad esempio, cambio di licenza, aggiornamento dei contratti)?
- I fornitori di soluzioni potrebbero aumentare unilateralmente i loro prezzi, semplicemente perché non abbiamo scelta?

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa Attività:

- [ ] Esiste una valutazione delle dipendenze critiche per i fornitori e i partner dell'organizzazione.
- [ ] Esiste un piano di backup per queste dipendenze identificate.
- [ ] Esiste un requisito dichiarato di sovranità digitale quando si studiano nuove soluzioni.

### Raccomandazioni

- Identificare i principali rischi di dipendenza dai fornitori di servizi e dalle entità terze.
- Mantenere un elenco di alternative open-source ai servizi critici.
- Aggiungere un requisito nella selezione di nuovi strumenti e servizi utilizzati all'interno dell'ente, affermando la necessità di sovranità digitale.

### Risorse

- [A Primer on Digital Sovereignty & Open Source: part I](https://www.opensourcerers.org/2021/08/09/a-promer-on-digital-sovereignty/) e [A Primer on Digital Sovereignty & Open Source: part II](https://www.opensourcerers.org/2021/08/16/a-primer-on-digital-sovereignty-open-source/), dal sito Open-Sourcerers.
- Un eccellente articolo di superuser.openstack.org su [The Role of Open Source in Digital Sovereignty](https://superuser.openstack.org/articles/the-role-of-open-source-in-digital-sovereignty-openinfra-live-recap/). Ecco un breve estratto:
   > La sovranità digitale è una preoccupazione fondamentale per il XXI secolo, soprattutto per l'Europa. L'open source ha un ruolo importante nel consentire la sovranità digitale, permettendo a tutti di accedere alla tecnologia necessaria, ma anche fornendo la trasparenza della governance e l'interoperabilità necessarie per il successo di queste soluzioni.
- Il punto di vista dell'Unione Europea sulla sovranità digitale, dall'[Osservatorio Open Source (OSOR)](https://joinup.ec.europa.eu/collection/open-source-observatory-osor): Open Source, sovranità digitale e interoperabilità: La dichiarazione di Berlino.
- La posizione dell'UNICEF su [Open Source for Digital Sovereignty](https://www.unicef.org/innovation/stories/open-source-digital-sovereignty).
