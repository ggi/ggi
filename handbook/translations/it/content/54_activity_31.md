## Dichiarare pubblicamente l'uso dell'open source

ID attività: [GGI-A-31](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_31.md).

### Descrizione

Questa attività riguarda il riconoscimento dell'uso di OSS in un sistema informativo, in applicazioni e in nuovi prodotti.

- Fornire storie di successo.
- Presentare agli eventi.
- Finanziamento della partecipazione a eventi.

### Valutazione delle opportunità

È ormai generalmente accettato che la maggior parte dei sistemi informativi si basa su OSS e che le nuove applicazioni sono in gran parte realizzate riutilizzando OSS.

Il vantaggio principale di questa attività è quello di creare condizioni di parità tra OSS e software proprietario, per far sì che l'OSS riceva la stessa attenzione e sia gestito in modo altrettanto professionale del software proprietario.

Un vantaggio secondario è che contribuisce ad aumentare il profilo dell'ecosistema OSS e, poiché gli utenti OSS sono identificati come "innovatori", aumenta anche l'attrattiva dell'organizzazione.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa attività:

- [ ] I fornitori commerciali open source sono autorizzati a utilizzare il nome dell'organizzazione come riferimento del cliente.
- [ ] I collaboratori sono autorizzati a farlo e a esprimersi con il nome dell'organizzazione.
- [ ] L'uso degli OSS è apertamente menzionato nella relazione annuale del dipartimento IT.
- [ ] Non ci sono ostacoli al fatto che l'organizzazione spieghi l'uso degli OSS nei media (interviste, eventi OSS e di settore, ecc.).

### Raccomandazioni

- L'obiettivo di questa attività non è che l'organizzazione diventi un organismo di attivismo OSS, ma che si assicuri che non ci siano ostacoli al riconoscimento dell'uso dell'OSS da parte del pubblico.

### Risorse

- Esempio di [CERN](https://superuser.openstack.org/articles/cern-openstack-update/) che afferma pubblicamente il proprio utilizzo di OpenStack
