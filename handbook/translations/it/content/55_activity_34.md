## Consapevolezza dei dirigenti

Attività ID: [GGI-A-34](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_34.md).

### Descrizione

L'iniziativa open source di un'organizzazione produrrà i suoi benefici strategici solo se sarà applicata ai livelli più alti, integrando il DNA open source nella strategia e nel lavoro interno dell'azienda. Questo impegno non può avvenire se i dirigenti di alto livello e il top management non ne fanno parte. La formazione e la mentalità dell'open source devono essere estese anche a coloro che determinano le politiche, le decisioni e la strategia generale, sia all'interno che all'esterno dell'azienda.

Questo impegno garantisce che i miglioramenti pratici, i cambiamenti di mentalità e le nuove iniziative siano accolti con un sostegno costante, benevolo e sostenibile da parte della gerarchia, che porta a una maggiore partecipazione dei lavoratori. Modella il modo in cui gli attori esterni vedono l'organizzazione, apportando benefici alla reputazione e all'ecosistema. È anche un mezzo per affermare l'iniziativa e i suoi benefici a medio e lungo termine.

### Valutazione delle opportunità

Questa attività diventa essenziale se/quando:

- L'organizzazione ha fissato obiettivi globali relativi alla gestione dell'open source, ma fatica a raggiungerli. È improbabile che l'iniziativa possa raggiungere qualcosa senza una buona conoscenza e un chiaro impegno da parte dei dirigenti di livello superiore.
- L'iniziativa è già partita e sta facendo progressi, ma i livelli gerarchici superiori non la seguono adeguatamente.

Se tutto va bene, dovrebbe risultare evidente che l'utilizzo dell'open source, se non ad hoc, richiede un approccio coerente e ben ponderato, data la gamma di team e di cambiamenti culturali che può apportare.

### Valutazione dei progressi

I seguenti **punti di verifica** dimostrano i progressi in questa Attività:

- [ ] Esiste un ufficio o un funzionario di governance incaricato di definire una strategia open source uniforme in tutta l'azienda e di garantire che l'ambito di applicazione sia chiaro.
- [ ] Esiste un impegno chiaro e vincolante da parte della gerarchia nei confronti della strategia OSS.
- [ ] La gerarchia comunica in modo trasparente il proprio impegno nei confronti del programma.
- [ ] La gerarchia aziendale è disponibile per discutere del software open source. Può essere sollecitata e stimolata per i suoi impegni.
- [ ] Esiste un budget e un finanziamento adeguati per l'iniziativa.

### Raccomandazioni

Esempi di azioni associate a questa attività sono:

- Condurre una formazione per demistificare l'OSS al top management.
- Ottenere un'approvazione esplicita e pratica per l'uso e la strategia OSS.
- Menzionare e sostenere esplicitamente il programma OSS nelle comunicazioni interne.
- Menzionare e sostenere esplicitamente il programma OSS nelle comunicazioni pubbliche.

L'open source è un *fattore strategico* che fa crescere la *cultura d'impresa:*. Che cosa significa?

- L'open source può essere sfruttato come un meccanismo per rinegoziare le offerte dei fornitori e ridurre i costi di acquisizione del software.
   - L'open source dovrebbe essere di competenza dei *Software Asset Manager* o dei *reparti acquisti*?
- Le licenze open source sanciscono le libertà che offrono i vantaggi dell'open source, ma comportano anche *obblighi *. Se non vengono rispettate in modo appropriato, le responsabilità possono creare rischi legali, commerciali e di immagine per un'organizzazione.
   - Vi saranno situazioni in cui la licenza Open Source renderà visibili parti del codice che dovrebbero rimanere riservate?
   - Avrà un impatto sul portafoglio brevetti della mia organizzazione?
   - Come devono essere formati e supportati i team di progetto su questo tema?
- Contribuire a progetti open source esterni è il punto in cui risiede il maggior valore dell'open source.
   - In che modo la mia azienda dovrebbe incoraggiare (e monitorare) questo aspetto?
   - Come dovrebbero usare gli sviluppatori applicazioni come GitHub, GitLab, Slack, Discord, Telegram o qualsiasi altro strumento che i progetti open source utilizzano abitualmente?
   - L'open source può influire sulle politiche delle risorse umane dell'azienda?
- Naturalmente non si tratta solo di contribuire, che dire dei miei progetti open source?
   - Sono pronto a fare innovazione *open*?
   - Come gestiranno i miei progetti i contributi *in entrata*?
   - Devo impegnarmi per creare una community per un determinato progetto?
   - Come devo guidare la community, quale ruolo devono avere i suoi membri?
   - Sono pronto a cedere le decisioni sulla roadmap a una community?
   - L'open source può essere uno strumento prezioso per ridurre la compartimentalizzazione dei team aziendali?
   - Devo gestire il trasferimento dell'open source da un'entità aziendale a un'altra?

### Attività successive proposte

- [GGI-A-31 - Publicly assert use of open source](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) I dirigenti sono i rappresentanti aziendali con maggior autorevolezza. Fate in modo che sia loro stessi a comunicare il coinvolgimento dell'azienda nell'open source.
