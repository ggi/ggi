# InnerSource

L'InnerSource gagne aujourd’hui en popularité au sein des entreprises, car il propose une approche basée sur des pratiques open source réussies au sein des équipes de développement des organisations. Cependant, faire de l'InnerSource n’est pas simplement copier-coller ces pratiques. Elles doivent être adaptées à la culture et à l’organisation interne de l’entreprise. Examinons de plus près ce qu’est et n’est pas l'InnerSource, et quels sont les défis associés.

## Qu'est-ce que l'InnerSource ?

Le terme a été utilisé pour la première fois par Tim O’Reilly en 2000, déclarant que l’Innersourcing est « [...] *l’utilisation de techniques de développement open source au sein de l’entreprise.* »

Selon [InnerSource Commons](https ://innersourcecommons.org/learn/books/innersource-patterns/), la fondation de référence sur le sujet, l'InnerSource est «*l’utilisation de principes et de pratiques open source pour le développement de logiciels dans les limites d’une organisation.*»

## Pourquoi l'InnerSource ?

Toujours selon [InnerSource Commons](https ://innersourcecommons.org/learn/books/innersource-patterns/), « *pour les entreprises qui construisent principalement des logiciels au code source fermé, l'InnerSource peut être un excellent outil pour aider à briser les silos, encourager et faire évoluer la collaboration interne, accélérer l’intégration des nouveaux ingénieurs et identifier les opportunités de contribuer au monde de l’open source.* »

Il est intéressant de noter que les avantages de l’InnerSource peuvent avoir un impact sur diverses fonctions au sein d’une entreprise, et pas seulement sur l’ingénierie. En conséquence, certaines entreprises ont trouvé des avantages concrets dans des domaines tels que :

* Fonctions juridiques : accélérer la mise en place de collaborations transverses grâce à l’utilisation d’un cadre juridique prêt à l’emploi (licence InnerSource).
* Ressources humaines : gestion des compétences rares par le biais d’une équipe centrale expérimentée chargée de mettre en commun les efforts et l’expertise.

## La controverse InnerSource

L'InnerSource est entouré de mythes courants que l’on peut entendre de la part de ses détracteurs. Bien qu’il ne s’agisse pas d’un véritable open source, il présente de grands avantages potentiels pour les organisations qui déploient une telle approche en interne. Voici quelques-uns de ces mythes :

* [MYTHE] L’InnerSource se fait au détriment de l’open source (principalement vers l'extérieur) :
   * Les projets logiciels restent derrière le pare-feu de l’entreprise.
   * Moins de contributions externes à l’open source.
* [MYTHE] Détourner l’esprit de l’open source plutôt que de l’approcher.
* [MYTHE] Aucun projet InnerSource n’est jamais devenu un projet open source.
* [MYTHE] La raison pour faire de l'InnerSource est que c'est analogue à l’open source. Mais en réalité, si un développeur l’apprécie, alors une contribution directe en open source devrait toujours être préférée.

Voici quelques faits sur la pratique de l’InnerSource qui brisent la plupart des mythes précédents :

* [FAIT] L'InnerSource est un moyen d’initier des entreprises majoritairement fermées à l’open source.
* [FAIT] Bien que la majorité des contributions à l’open source soit faite par des bénévoles, nous pouvons inciter les ingénieurs à la participation à l’open source en utilisant cette liste d'« avantages perçus ».
* [FAIT] Dans certains cas (ou la plupart ?), les entreprises ne suivent pas une pratique de développement ordonnée et contrôlée et ce guide (GGI) peut être un moyen de les aider à gérer cela.
* [FAIT] Il faudra encore *beaucoup* de travail dans la conversion des licences fermées en licences ouvertes.
* [FAIT] Il y a en effet des cas de projets InnerSource passés en open source :
   * Twitter Bootstrap.
   * Kubernetes de Google.
   * Docker de dotCloud (nom précédent de Docker Inc.).
   * React Native.
* [FAIT] L’open source bénéficie de l’augmentation du nombre d’ingénieurs logiciels qui se familiarisent avec les pratiques open source, car celles-ci sont très similaires à l'InnerSource .

## Qui fait de l'InnerSource ?

De nombreuses entreprises ont lancé des initiatives InnerSource ou ISPO (InnerSource Program Office), certaines depuis longtemps, d’autres plus récemment. Voici une liste non éthique, principalement axée sur les entreprises européennes :

* Banco de Santander ([source](https ://patterns.innersourcecommons.org/p/innersource-portal))
* BBC ([source](https ://www.youtube.com/watch ?v=pEGMxe6xz-0))
* Bosch ([source](https ://web.archive.org/web/20230429145619/https ://www.bosch.com/research/know-how/open-and-inner-source/))
* Comcast ([source](https ://www.youtube.com/watch ?v=msD-8-yrGfs&t=6s))
* Ericsson ([source](https ://innersourcecommons.org/learn/books/adopting-innersource-principles-and-case-studies/))
* Engie ([source](https ://github.com/customer-stories/engie))
* IBM ([source](https ://resources.github.com/innersource/fundamentals/))
* Mercedes ([source](https ://www.youtube.com/watch ?v=hVcGABbmI4Y))
* Microsoft ([source](https://www.youtube.com/watch?v=eZdx5MQCLA4))
* Nike ([source](https ://www.youtube.com/watch ?v=srPG-Tq0HIs&list=PLq-odUc2x7i-A0sOgr-5JJUs5wkgdiXuR&index=46))
* Nokia ([source](https ://www.nokia.com/thought-leadership/articles/openness/openness-drives-innovation/))
* SNCF Connect & Tech ([source 1](https ://twitter.com/FrancoisN0/status/1645356213712846853), [source 2](https ://www.slideshare.net/FrancoisN0/opensource-innersource-pour-acclrer-les-dveloppements))
* Paypal ([source](https://innersourcecommons.org/fr/learn/books/getting-started-with-innersource/))
* Philips([source](https ://medium.com/philips-technology-blog/how-philips-used-innersource-to-maintain-its-edge-in-innovation-38c481e6fa03))
* Renault ([source](https ://www.youtube.com/watch ?v=aCbv46TfanA))
* SAP ([source](https ://community.sap.com/topics/open-source/publications))
* Siemens([source](https ://jfrog.com/blog/creating-an-inner-source-hub-at-siemens))
* Société Générale([source](https ://github.com/customer-stories/societe-generale))
* Thales ([source](https ://www.youtube.com/watch ?v=aCbv46TfanA))
* VeePee([source](https ://about.gitlab.com/customers/veepee))

## InnerSource Commons, une référence incontournable

Une communauté active et dynamique de praticiens d’InnerSource, qui travaille selon les principes de l’Open Source, peut être trouvée sur [InnerSource Commons](https ://innersourcecommons.org). Ils fournissent de nombreuses ressources utiles pour vous mettre à jour sur le sujet, y compris [patterns](https ://innersourcecommons.org/learn/patterns/), un [parcours d’apprentissage](https ://innersourcecommons.org/learn/learning-path/) et de petits ebooks :

* [Getting Started with InnerSource](https ://innersourcecommons.org/learn/books/getting-started-with-innersource/) by Andy Oram.
* [Understanding the InnerSource Checklist](https ://innersourcecommons.org/learn/books/understanding-the-innersource-checklist/) by Silona Bonewald.

## Différences de gouvernance de l’InnerSource

L'InnerSource apporte des défis spécifiques qui ne sont pas rencontrés dans l’Open Source. Pourtant, la plupart des organisations qui créent des logiciels privatifs y sont déjà confrontées :

* Licence dédiée et spécifique à l’entreprise pour les projets Innersource (pour les grandes entreprises ayant plusieurs entités juridiques).
* La nature publique de l’open source le met à l’abri des problèmes de prix de transfert. La nature privée de l’InnerSource expose les sociétés opérant dans différentes juridictions à des responsabilités de transfert de bénéfices.
* Les motivations des contributions sont très différentes :
   * L'InnerSource concerne un plus petit nombre de contributeurs potentiels, car il est limité à l’organisation.
   * Montrer ses compétences professionnelles est un moteur pour contribuer. L'InnerSource cantonne cet impact aux limites de l’organisation uniquement.
   * Contribuer à l’amélioration de la société est un autre moteur de contribution qui est limité dans l'InnerSource.
   * La motivation a donc besoin d’un effort plus important et reposera davantage sur les récompenses et les missions.
   * Il est plus facile de gérer les craintes de perfectionnisme comme le syndrome de l’imposteur en InnerSource en raison de la visibilité limitée du code.
* L’externalisation de la main-d’œuvre est plus fréquente, ce qui affecte la gouvernance de plusieurs façons.
* L’évaluation de l’adéquation à l'entreprise est plus facile pour l'InnerSource car elle est développée en interne.
* La facilité de découverte tend à devenir un problème. L’indexation de l’information est moins une priorité pour les sociétés. Les moteurs de recherche publics comme DuckDuckGo, Google ou Bing font un bien meilleur travail que l’InnerSource ne peut pas exploiter.
* L'InnerSource est un peu mieux placée pour contrôler l’exportation puisqu’elle vit à l’interne.
* Le contrôle délimité de la fuite de Propriété Intellectuelle par le code source est nécessaire.

L'InnerSource continue à évoluer au fur et à mesure que de plus en plus d’entreprises adoptent ses principes et partagent leurs expériences. Une version ultérieure de ce manuel fournira une liste organisée des activités du GGI pertinentes pour les praticiens d’InnerSource.
