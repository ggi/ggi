## Faire des revues de code

Identifiant de l'activité : [GGI-A-44](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_44.md).

### Description

La revue de code est une tâche de routine qui implique une revue manuelle et/ou automatisée du code source d'une application avant la sortie d'un produit ou la livraison d'un projet au client. Dans le cas des logiciels libres, la revue de code ne se limite pas à la détection d'erreurs de manière opportuniste ; il s'agit d'une approche intégrée au développement collaboratif réalisée au niveau de l'équipe.

Les revues de code devraient s'appliquer au code développé en interne ainsi qu'au code réutilisé à partir de sources externes, car elles améliorent la confiance générale dans le code et renforcent l'appropriation. C'est également un excellent moyen d'améliorer les compétences et les connaissances globales au sein de l'équipe et de favoriser la collaboration entre les membres de l'équipe.

### Évaluation de l’opportunité

Les revues de code sont utiles chaque fois que l'organisation développe un logiciel ou réutilise des éléments de logiciel externes. Bien qu'il s'agisse d'une étape standard du processus d'ingénierie logicielle, les revues de code dans le contexte de l'open source apportent des avantages spécifiques tels que :

- Lors de la publication de code source interne, vérifier que les directives de qualité adéquates sont respectées.
- Lors de la contribution à un projet open source existant, vérifier que les directives du projet ciblé sont respectées.
- La documentation disponible publiquement est mise à jour en conséquence.

C'est également une excellente occasion de partager et de faire respecter certaines règles de conformité juridique de votre organisation, telles que :

- Ne jamais supprimer les en-têtes de licence ou les droits d'auteur existants trouvés dans le code open source réutilisé.
- Ne pas copier et coller du code source de Stack Overflow sans l'autorisation préalable de l'équipe juridique.
- Inclure la ligne de copyright adéquate lorsque c'est nécessaire.

Les revues de code apportent confiance et sécurité dans le code. Si les gens ne sont pas sûrs de la qualité ou des risques potentiels de l'utilisation d'un produit logiciel, ils devraient procéder à des examens du code et collecter les commentaires des pairs.

### Suivi de l'avancement

Les **points de contrôle** suivants montrent une progression de cette Activité :

- [ ] La révision du code open source est reconnue comme une étape nécessaire.
- [ ] Des revues du code open source sont planifiées (régulièrement ou à des moments critiques).
- [ ] Un processus pour mener des revues de code open source a été défini et accepté collectivement.
- [ ] Les revues de code open source sont une partie standard du processus de développement.

### Recommandations

- La revue de code est une tâche collective qui fonctionne mieux dans un bon environnement collaboratif.
- N'hésitez pas à utiliser les outils et les modèles existants dans le monde du logiciel libre, où les revues de code sont un standard depuis des années (décennies).

### Ressources

- [Qu'est-ce qu'une Revue de Code ?](https://openpracticelibrary.com/practice/code-review/) : une lecture didactique sur la revue de code trouvée sur la bibliothèque de pratiques ouvertes de Red Hat.
- [Bonnes pratiques de Revue de Code](https://www.perforce.com/blog/qac/9-best-practices-for-code-review) : une autre perspective intéressante sur ce qu'est la revue de code.

### Activités à venir

- [GGI-A-26 - Contribuer aux projets open source](https://ospo-alliance.org/ggi/activities/contribute_to_open_source_projects) La revue de code est une pratique courante dans les projets open source, car elle améliore la qualité du code et le partage des connaissances. Les contributeurs qui effectuent des révisions de code se sentent généralement plus à l’aise avec les contributions externes et la collaboration.
