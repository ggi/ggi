## Gérer la conformité juridique

Identifiant de l'activité : [GGI-A-21](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_21.md).

### Description

Les organisations doivent mettre en œuvre un processus de conformité juridique pour sécuriser leur utilisation et leur participation aux projets open source.

La gestion mature et professionnelle de la conformité légale, au sein de l'organisation et tout au long de la chaîne d'approvisionnement, consiste à :

- Réalisation d'une analyse approfondie de la propriété intellectuelle qui comprend l'identification des licences et la vérification de la compatibilité.
- Garantir que l'organisation peut utiliser, intégrer, modifier et redistribuer en toute sécurité des composants open source dans le cadre de ses produits ou services.
- Fournir aux employés et aux contractants un processus transparent sur la façon de créer et de contribuer aux logiciels open source.

*Analyse de la composition du logiciel (SCA)* : Une part importante des problèmes juridiques et de PI (propriété intellectuelle) résulte de l'utilisation de composants publiés sous des licences qui sont soit incompatibles entre elles, soit incompatibles avec la façon dont l'organisation souhaite utiliser et redistribuer les composants. Le SCA est la première étape de la résolution de ces problèmes, car « il faut connaître le problème pour le résoudre ». Le processus consiste à identifier tous les composants impliqués dans un projet dans un document de nomenclature, y compris les dépendances de construction (build) et de test.

*Vérification des licences* : Un processus de vérification des licences utilise un outil pour analyser automatiquement la base de code et identifier les licences et les droits d'auteur qu'elle contient. S'il est exécuté régulièrement et idéalement intégré aux chaînes de construction (build) et d'intégration continue, il permet de détecter rapidement les problèmes de propriété intellectuelle.

### Évaluation de l’opportunité

Avec l'utilisation croissante des logiciels libres dans les systèmes d'information d'une organisation, il est essentiel d'évaluer et de gérer les risques juridiques potentiels.

Cependant, la vérification des licences et des droits d'auteur peut être délicate et coûteuse. Les développeurs doivent être en mesure de vérifier rapidement les questions juridiques et de PI. Disposer d'une équipe et d'un responsable dédiés à la propriété intellectuelle et aux questions juridiques garantit une gestion proactive et cohérente des questions juridiques, contribue à sécuriser l'utilisation et les contributions aux composants open source et fournit une vision stratégique claire.

### Suivi de l'avancement

Les **points de contrôle** suivants montrent une progression de cette Activité :

- [ ] Un processus de vérification de licence facile à utiliser est disponible pour les projets ;
- [ ] Un processus de vérification de la PI facile à utiliser est disponible pour les projets ;
- [ ] Il existe une équipe ou une personne responsable de la conformité juridique au sein de l’organisation ;
- [ ] Des audits réguliers visant à évaluer la conformité juridique sont prévus.

Autres moyens de mettre en place des points de contrôle :

- [ ] Il existe un processus de vérification des licences facile à utiliser ;
- [ ] Il existe une équipe juridique/PI pouvant être consultée.
- [ ] Tous les projets fournissent les informations nécessaires pour que les personnes puissent utiliser le projet et y contribuer ;
- [ ] Il y a un contact dans l'équipe pour les questions liées à la PI et aux licences ;
- [ ] Il y a un responsable de l'entreprise qui se consacre à la PI et aux licences ;
- [ ] Il existe une équipe dédiée aux questions relatives à la PI et aux licences.

### Outils

- [ScanCode](https://scancode-toolkit.readthedocs.io)
- [Fossology](https://www.fossology.org/)
- [SW360](https://www.eclipse.org/sw360/)
- [Fossa](https://github.com/fossas/fossa-cli)
- [OSS Review Toolkit](https://oss-review-toolkit.org)

### Recommandations

- Informer les gens sur les risques associés aux licences en conflit avec les objectifs commerciaux ;
- Proposer une solution facile pour que les projets puissent mettre en place une vérification des licences sur leur base de code ;
- Communiquer sur son importance et aider les projets à l’ajouter à leurs systèmes d’intégration continue ;
- Fournir un modèle ou des directives officielles de structure d'un projet.
- Mettre en œuvre des vérifications automatisées pour garantir que chaque projet est conforme aux directives.
- Envisager de réaliser un audit interne pour identifier les licences de l’infrastructure de l’entreprise ;
- Fournir une formation de base sur la PI et les licences à au moins une personne par équipe ;
- Fournir une formation complète sur la PI et les licences au responsable ;
- Mettre en place un processus de remontée des problèmes de PI et de licence vers le responsable.

N’oubliez pas que la conformité n’est pas seulement une question de droit, mais aussi de PI (propriété intellectuelle). Voici donc quelques questions pour vous aider à comprendre les conséquences de la conformité juridique :

- Si je distribue un composant open source et que je ne respecte pas les conditions de la licence, j’enfreins la licence → incidences juridiques ;
- Si j’utilise un composant open source au sein d’un projet que je souhaite distribuer/publier, cette licence peut obliger à une visibilité sur des éléments de code que je ne souhaite pas rendre open source → Impact de la confidentialité pour l’avantage tactique de mon entreprise et avec des tiers (implications légales) ;
- Il s'agit d'une discussion ouverte sur la question de savoir si l'utilisation d'une licence open source pour un projet que je veux publier octroie une PI pertinente → incidences en matière de PI. ;
- Si je publie un projet en open source *avant* tout processus de brevet, cela exclut *probablement* la création de brevets concernant le projet → incidences en matière de PI ;
- Si je publie un projet en open source *après* tout processus de brevet, cela permet *probablement* la création de brevets (défensifs) concernant ce projet → potentiel de PI ;
- Dans les projets complexes qui font intervenir de nombreux composants avec de nombreuses dépendances, la multitude de licences open source peut présenter des incompatibilités entre les licences → incidences juridiques (cf. activité GGI-A-23 - Gérer les dépendances logicielles).

### Ressources

- Il existe une large liste d'outils sur la page [Outils de conformité OSS existants](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence-Compliance-Tools/).
- [Pratiques recommandées de conformité des logiciels libres pour l'entreprise](https://www.ibrahimatlinux.com/wp-content/uploads/2022/01/recommended-oss-compliance-practices.pdf). Un livre d'Ibrahim Haddad, de la Fondation Linux, sur les pratiques de conformité des logiciels libres pour l'entreprise. [OpenChain Project](https://www.openchainproject.org/).

### Activités à venir

- [GGI-A-24 - Gérer les indicateurs clés](https://ospo-alliance.org/ggi/activities/manage_key_indicators) Rendez visibles et mesurables les préoccupations, les processus et les résultats en matière de conformité légale. Cela aidera les gens à se rendre compte de son importance plus tôt dans le processus.
