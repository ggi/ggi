## Promouvoir les bonnes pratiques de développement open source

Identifiant de l'activité : [GGI-A-25](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_25.md).

### Description

Cette activité consiste à définir, promouvoir activement et mettre en œuvre les bonnes pratiques open source au sein des équipes de développement.

Comme point de départ, les sujets suivants pourraient faire l'objet d'une attention particulière :

- Documentation pour les utilisateurs et les développeurs.
- Organisation correcte du projet sur un dépôt accessible au public.
- Promouvoir et mettre en œuvre une réutilisation contrôlée.
- Fournir une documentation complète et à jour du produit.
- Gestion de la configuration : flux de travail git, modèles de collaboration.
- Gestion des versions : publier tôt et souvent, des versions stables vs des versions de développement, etc.

Les projets OSS ont un modus operandi particulier, de type [bazar](http://www.catb.org/~esr/writings/cathedral-bazaar/). Afin de permettre et d'encourager cette collaboration et cet état d'esprit, certaines pratiques sont recommandées pour faciliter le développement collaboratif et décentralisé et les contributions de développeurs tiers…

#### Documents communautaires

Assurez-vous que tous les projets au sein de l'entreprise proposent les documents suivants :

- README - description rapide du projet, comment interagir, liens vers des ressources.
- Contribuer (« CONTRIBUTING ») - introduction pour les personnes souhaitant contribuer.
- Code de conduite (« CODE_OF_CONDUCT ») - ce qui est acceptable ou non comme comportement au sein de la communauté.
- LICENSE - la licence par défaut du référentiel.

#### Les bonnes pratiques de REUTILISATION

[REUSE](https://reuse.software) est une initiative de la [Free Software Foundation Europe](https://fsfe.org/) pour améliorer la réutilisation des logiciels et rationaliser la conformité des licences open source.

### Évaluation de l’opportunité

Bien que cela dépende fortement de la connaissance commune du logiciel libre au sein de l'équipe, la formation des personnes et la création de processus qui consolident ces pratiques sont toujours bénéfiques. C'est encore plus important lorsque :

- les utilisateurs et contributeurs potentiels ne sont pas connus,
- les développeurs ne sont pas habitués au développement open source.

### Suivi de l'avancement

Les **points de contrôle** suivants montrent une progression de cette Activité :

- [ ] Le projet établit une liste des bonnes pratiques de l'open source à respecter.
- [ ] Le projet surveille son alignement sur les bonnes pratiques.
- [ ] L'équipe de développement a été sensibilisée au respect des bonnes pratiques du logiciel libre.
- [ ] De nouvelles bonnes pratiques sont régulièrement évaluées, et un effort est fait pour les mettre en œuvre.

### Outils

- L'outil [REUSE helper tool](https://github.com/fsfe/reuse-tool) aide à rendre un dépôt conforme aux bonnes pratiques [REUSE](https://reuse.software). Il peut être inclus dans de nombreux processus de développement pour confirmer le statut actuel.
- [ScanCode](https://scancode-toolkit.readthedocs.io) a la capacité de lister tous les documents communautaires et légaux dans le dépôt : voir la [description de ses fonctionnalités](https://scancode-toolkit.readthedocs.io/en/latest/cli-reference/scan-options-pre.html#classify).
- GitHub dispose d'une fonctionnalité intéressante permettant de [vérifier les documents communautaires manquants](https://docs.github.com/articles/viewing-your-community-profile). Elle se trouve sur la page du dépôt > « Insights » > « Community ».

### Recommandations

- La liste des bonnes pratiques dépend du contexte et du domaine du programme et devrait être réévaluée régulièrement dans une optique d'amélioration continue. Les pratiques doivent être surveillées et évaluées régulièrement pour suivre les progrès.
- Former les gens à la réutilisation des logiciels libres (en tant que consommateurs) et aux écosystèmes (en tant que contributeurs).
- Mettre en œuvre REUSE.software.
- Mettre en place un processus de gestion des risques juridiques associés à la réutilisation et aux contributions.
- Encourager explicitement les gens à contribuer à des projets externes.
- Fournir un modèle ou des directives officielles de structure d'un projet.
- Mettre en œuvre des vérifications automatisées pour garantir que chaque projet est conforme aux directives.

### Ressources

- [Liste des bonnes pratiques open source par OW2](https://www.ow2.org/view/MRL/Full_List_of_Best_Practices), tirée de la méthodologie Market Readiness Levels.
- [Site officiel de REUSE](https://reuse.software) avec spécifications, tutoriel, FAQ.
- [Directives communautaires de GitHub](https://opensource.guide/).
- Un exemple de [bonnes pratiques en gestion de configuration avec GitHub](https://dev.to/datreeio/top-10-github-best-practices-3kl2).

### Activités à venir

- [GGI-A-42 - Gérer les compétences et les ressources en matière d'open source](https://ospo-alliance.org/ggi/activities/manage_open_source_skills_and_resources) Vous pouvez ajouter la liste des bonnes pratiques de développement open source identifiées au matériel de formation général.
- [GGI-A-44 - Faire des revues de code](https://ospo-alliance.org/ggi/activities/run_code_reviews/) La revue de code est un élément essentiel des meilleures pratiques de développement.
