## Vers une stratégie de gouvernance open source d'Entreprise

Identifiant de l'activité : [GGI-A-16](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_16.md).

### Description

Définir une stratégie de gouvernance des logiciels open source au sein de l'entreprise garantit des approches cohérentes et visibles à la fois pour une utilisation interne et pour des contributions et des implications externes. Une vision et une direction claires et bien établies rendent la communication plus efficace.

Le passage à l'open source s'accompagne de nombreux avantages, mais aussi de certains devoirs et d'un changement de culture de l'entreprise. Il peut avoir un impact sur les modèles commerciaux et influencer la manière dont une organisation présente sa valeur et son offre, ainsi que sa position vis-à-vis de ses clients et de ses concurrents.

Cette activité comprend les tâches suivantes :

- Mettre en place un responsable open source, avec le parrainage et le soutien de la direction générale.
- Établir et publier une feuille de route claire pour l'open source, avec des objectifs précis et des avantages attendus.
- S’assurer que tous les cadres dirigeants en prennent connaissance et agissent conformément à cette feuille de route.
- Promouvoir les logiciels open source au sein de l'entreprise : encourager leur usage, les initiatives internes et le niveau de connaissances.
- Promouvoir le logiciel libre à l'extérieur de l'entreprise : par des déclarations et une communication officielles, et par une participation visible aux initiatives OSS.

Définir, publier et appliquer une stratégie claire et cohérente favorise également l'adhésion de toutes les personnes au sein de l'entreprise et facilite les initiatives des équipes.

### Évaluation de l’opportunité

C'est le bon moment pour travailler sur cette activité si :

- Il n'y a pas d'effort coordonné de la part de la direction, et l'open source est toujours considéré comme une solution ad hoc.
- Il existe déjà des initiatives internes, mais elles ne remontent pas jusqu'aux niveaux supérieurs de la direction.
- L'initiative a été lancée il y a quelque temps, mais elle se heurte à de nombreux obstacles et ne donne toujours pas les résultats escomptés.

### Suivi de l'avancement

Les **points de contrôle** suivants montrent une progression de cette Activité :

- [ ] Il existe une charte de gouvernance open source claire pour l'entreprise. Cette charte doit contenir :
   - les objectifs à atteindre,
   - pour qui faisons nous cela,
   - ce qui est du pouvoir du ou des stratèges et ce qui ne l'est pas.
- [ ] Une feuille de route open source est largement disponible et acceptée dans toute l'entreprise.

### Recommandations

- Mettre en place un groupe de personnes et des processus pour définir et surveiller la gouvernance de l'open source au sein de l'entreprise. S'assurer qu'il existe bien un réel engagement du top management sur les initiatives open source.
- Communiquer sur la stratégie open source au sein de l'organisation, en faire une préoccupation majeure et un véritable engagement de l'entreprise.
- Veiller à ce que la feuille de route et la stratégie soient bien comprises par tout le monde, des équipes de développement aux gestionnaires et techniciens d’infrastructure.
- Communiquer sur ses progrès, afin que les gens sachent où en est l'organisation par rapport à son engagement. Publier régulièrement des mises à jour et des indicateurs.

### Ressources

- [Liste de vérifications et références pour une gouvernance open source](https://opengovernance.dev/).
- [L'open source comme enjeu de souveraineté numérique, par Cédric Thomas, OW2 CEO, Workshop à Orange Labs, Paris, 28 janvier 2020](https://www.ow2.org/download/OSS_Governance/Level_5/2001-OSSetSouveraineteNumerique-RC3.pdf).
- [Une série de guides pour gérer les logiciels open source dans l'entreprise, par la Fondation Linux](https://todogroup.org/guides/).
- [Un bon exemple de document sur la stratégie open source, par le groupe LF Energy](https://www.lfenergy.org/wp-content/uploads/sites/67/2019/07/Open-Source-Strategy-V1.0.pdf)

### Activités à venir

- [GGI-A-35 - Open source et souveraineté numérique](https://ospo-alliance.org/ggi/activities/open_source_and_digital_sovereignty) Une bonne stratégie de gouvernance de l’open source d’entreprise devrait améliorer l’open source et la souveraineté numérique. C’est le bon moment pour les définir dans le contexte de l’organisation.
- [GGI-A-34 - Implication des dirigeants](https://ospo-alliance.org/ggi/activities/c-level_awareness) L’engagement des dirigeants de l'entreprise sera nécessaire pour mettre en œuvre correctement la stratégie open source de l'organisation. Les éduquer et les impliquer est une bonne prochaine étape pour réussir ici.
