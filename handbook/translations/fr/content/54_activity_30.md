## Soutenir les communautés open source

Identifiant de l'activité : [GGI-A-30](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_30.md).

### Description

Cette activité consiste à s'engager auprès des représentants institutionnels du monde de l'open source.

Elle est réalisée par :

- L’adhésion aux fondations OSS (y compris le coût financier de l'adhésion).
- Le soutien et la défense des activités des fondations.

Cette activité implique d'allouer aux équipes IT et de développement du temps et un budget pour participer aux communautés open source.

### Évaluation de l’opportunité

Les communautés open source sont à l'avant-garde de l'évolution de l'écosystème open source. S'engager dans les communautés présente plusieurs avantages :

- cela permet de rester informé et à jour,
- le profil de l'organisation est amélioré,
- l'adhésion induit des bénéfices,
- cela fournit une structure et une motivation supplémentaires à l'équipe informatique open source.

Les coûts comprennent :

- les frais d'adhésion,
- le temps de personnel et un budget de voyage alloué pour participer aux activités de la communauté,
- le suivi des engagements en matière de propriété intellectuelle.

### Suivi de l'avancement

Les **points de contrôle** suivants montrent une progression de cette Activité :

- [ ] L'organisation est un membre officiel d'une fondation open source.
- [ ] L'organisation participe à la gouvernance d'une fondation / communauté.
- [ ] Les logiciels développés par l'organisation sont soumis à la base de code d'une fondation ou ont été ajoutés à celle-ci.
- [ ] L'adhésion est reconnue sur les sites web de l'organisation et de la communauté.
- [ ] Une évaluation des coûts/avantages de l'adhésion a été réalisée.
- [ ] Un point de contact pour la communauté a été désigné.

### Recommandations

- Rejoignez une communauté adaptée à votre taille et vos ressources, c’est-à-dire une communauté qui peut entendre votre voix et où vous pouvez être un contributeur reconnu.

### Ressources

- Consultez cette [page utile (en anglais)](https://www.linuxfoundation.org/tools/participating-in-open-source-communities/) de la Fondation Linux sur le pourquoi et le comment de l’adhésion à une communauté open source.

### Activités à venir

- [GGI-A-31 - Affirmer publiquement l’utilisation de l’open source](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Maintenant que vous soutenez officiellement certaines communautés OSS, faites le savoir ! C’est bon pour votre réputation, et c’est bon pour les projets en termes de santé et de diffusion.
