## S'engager dans des projets open source

Identifiant de l'activité : [GGI-A-29](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_29.md).

### Description

Cette activité consiste à engager des contributions significatives à certains projets OSS qui sont importants pour vous. Les contributions sont remontées et soumises au niveau de l'organisation (et non au niveau personnel comme dans #26). Elles peuvent prendre plusieurs formes, du financement direct à l'allocation de ressources (personnes, serveurs, infrastructure, communication, etc.), pour autant qu'elles bénéficient au projet ou à l'écosystème de manière durable et efficace.

Cette activité va au-delà de l'activité #26 pour faire émaner de l'organisation les contributions aux projets open source, les rendant plus visibles, puissantes et bénéfiques. Dans cette activité, les contributions sont censées apporter une amélioration substantielle et à long terme au projet OSS : par exemple, un développeur ou une équipe qui développe une nouvelle fonctionnalité très attendue, des actifs d'infrastructure, des serveurs pour un nouveau service, la reprise de la maintenance d'une branche largement utilisée.

L'idée est de réserver un pourcentage de ses ressources pour parrainer des développeurs de logiciels libres qui écrivent et maintiennent des bibliothèques ou des projets que l'on utilise.

Cette activité implique d'avoir une cartographie des logiciels open source utilisés, et une évaluation de leur criticité pour décider lequel soutenir.

### Évaluation de l’opportunité

> Si chaque entreprise utilisant des logiciels libres contribuait au moins un peu, nous aurions un écosystème sain. <https://news.ycombinator.com/item?id=25432248>

Soutenir des projets permet d'assurer leur pérennité et d'avoir accès à des informations, voire d'influencer et prioriser certains développements (bien que cela ne doive pas être la raison principale du soutien aux projets).

Avantages potentiels de cette activité : garantir que les rapports de bogues sont traités en priorité et que les développements sont intégrés dans la version stable. Coûts potentiels associés à cette activité : consacrer du temps aux projets, engagement financier.

### Suivi de l'avancement

Les **points de contrôle** suivants dénotent une progression de cette activité :

- [ ] Projet bénéficiaire identifié.
- [ ] Option de soutien décidée, telle qu'une contribution monétaire directe ou une contribution au code.
- [ ] Nomination d'un chef de projet.
- [ ] Une certaine contribution a été apportée.
- [ ] Le résultat de la contribution a été évalué.

Points de vérification empruntés au questionnaire d'OpenChain [auto-certification](https://certification.openchainproject.org/) :

- [ ] Nous avons une politique de contribution aux projets open source au nom de l'organisation.
- [ ] Nous disposons d'une procédure documentée régissant les contributions aux projets open source.
- [ ] Nous disposons d'une procédure documentée pour sensibiliser tout le personnel du logiciel à la politique de contribution aux projets open source.

### Outils

Certaines organisations proposent des mécanismes de financement de projets open source (cela pourrait être pratique si votre projet cible figure dans leurs portefeuilles).

- [Collectif ouvert](https://opencollective.com/).
- [Software Freedom Conservancy](https://sfconservancy.org/).
- [Tidelift](https://tidelift.com/).

### Recommandations

- Concentrez-vous sur les projets essentiels pour l'organisation : ce sont les projets que vous souhaitez le plus aider par vos contributions.
- Ciblez les projets communautaires.
- Cette activité nécessite une connaissance minimale d'un projet cible.

### Ressources

- [Comment soutenir les projets open source maintenant](https://sourceforge.net/blog/support-open-source-projects-now/) : une courte page avec des idées sur le financement des projets open source.
- [Sustain OSS : un espace pour les conversations sur le soutien de l'open source](https://sustainoss.org)

### Activités à venir

- [GGI-A-26 - Contribuez à des projets open source](https://ospo-alliance.org/ggi/activities/contribute_to_open_source_vendors) La façon la plus naturelle de s’engager dans une initiative open source est de contribuer directement au projet. En retour, vous recueillerez de précieux commentaires sur vos contributions.
- [GGI-A-30 - Supportez les communautés open source](https://ospo-alliance.org/ggi/activities/support_open_source_communities) Il existe de nombreuses façons de soutenir les initiatives open source qui sont essentielles à votre organisation. S’engager auprès des communautés est un bon moyen de les découvrir et de les encourager.
