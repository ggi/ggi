## Logiciel open source professionnel

Identifiant de l'activité : [GGI-A-20](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_20.md).

### Description

Cette activité vise au choix proactif de solutions OSS, commerciales ou communautaires, dans le domaine professionnel. Elle couvre également la définition de politiques préférentielles quant à la sélection de solutions métier open source.

Alors que le logiciel open source est le plus souvent utilisé par des professionnels de l'IT (systèmes d'exploitation, intergiciel, bases de données, administration système, outils de développement), il a encore besoin d'être reconnu dans les domaines où les professionnels métier sont prépondérants.

Alors que le logiciel open source est le plus souvent utilisé par des professionnels de l'IT (systèmes d'exploitation, intergiciel, bases de données, administration système, outils de développement), il a encore besoin d'être reconnu dans les domaines où les professionnels métier sont prépondérants.

### Évaluation de l’opportunité

À mesure que l'open source se généralise, il se répand bien au-delà des systèmes d'exploitation et outils de développement, trouvant ses usages dans les couches supérieures des systèmes d'information, jusqu'aux applications métier. Il est pertinent d'identifier quelles applications OSS sont utilisées avec succès pour répondre aux besoins de l'organisation, et comment elles peuvent y devenir un choix préférentiel en termes économiques.

L'activité peut induire des besoins en formation et des coûts de migration.

### Suivi de l'avancement

Les **points de contrôle** suivants montrent une progression de cette Activité :

- [ ] Il existe une liste des solutions OSS recommandées pour couvrir des besoins identifiés en matière d'applications métier.
- [ ] Une ébauche de politique de sélection des applications métier open source est définie.
- [ ] Les solutions métier propriétaires utilisées font l'objet d'évaluation par comparaison avec des équivalents OSS.
- [ ] Les processus d'approvisionnement et d'appel à proposition spécifient une préférence pour l'open source (si légalement possible).

### Outils

Dans l'état actuel, nous ne connaissons pas d'outil pertinent lié à cette activité.

### Recommandations

- Discuter avec ses collègues, tirer les leçons de ce que font des organisations comparables à la vôtre.
- Visiter les événements industriels locaux pour y rechercher des solutions open source et du support professionnel.
- Essayer les versions et le support communautaires préalablement à la souscription de contrats de support payants.

### Ressources

- [Qu’est-ce que l’open source d’entreprise ?](https://www.redhat.com/en/blog/what-enterprise-open-source) : courte lecture à propos de l’open source pour l’entreprise.
- [101 Applications open source pour aider votre business à prospérer](https://digital.com/creating-an-llc/open-source-business/) : une liste indicative de solutions open source orientées business.

### Activités à venir

- [GGI-A-33 - Fournisseurs open source](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) Renforcez la confiance dans vos ressources open source en faisant appel à des professionnels de l’open source.
- [GGI-A-43 - Politique d'approvisionnement open source](https://ospo-alliance.org/ggi/activities/open_source_procurement_policy) L’utilisation de l'OSS par l'Entreprise sera optimisée en sachant quels actifs sont déjà présents et en ayant une politique d’approvisionnement claire en la matière.
