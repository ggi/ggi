## Gérer les indicateurs clés

Identifiant de l'activité : [GGI-A-24](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_24.md).

### Description

Cette activité permet de collecter et de surveiller un ensemble d’indicateurs qui éclairent les décisions de gestion quotidiennes et les options stratégiques concernant les logiciels libres gérés de manière professionnelle.

Les métriques clés relatives aux logiciels libres constituent la toile de fond de l’efficacité de la mise en œuvre des programmes de gouvernance. L’activité couvre la sélection de quelques indicateurs, leur publication aux équipes et à la direction, et l’envoi de mises à jour régulières sur l’initiative, par exemple via une lettre d’information ou des actualités de l’entreprise.

Cette activité nécessite :

- que les parties prenantes discutent et définissent les objectifs du programme ;
- la mise en place d'un outil de mesure et de collecte de données connecté à l'infrastructure de développement ;
- la publication d’au moins un tableau de bord pour les parties prenantes et pour toutes les personnes impliquées dans l’initiative.

Les indicateurs sont basés sur des données qui doivent être collectées auprès de sources pertinentes. Il existe par chance de très nombreuses sources dans le domaine du génie logiciel libre. En voici quelques exemples :

- l’environnement de développement, la chaîne de production CI/CD ;
- le département des ressources humaines ;
- les outils de test et d’analyse de la composition du logiciel ;
- les dépôts logiciels.

Exemples d’indicateurs :

- Nombre de dépendances résolues, affichées par type de licence ;
- Nombre de dépendances périmées/vulnérables ;
- Nombre de problèmes de licence/PI (propriété intellectuelle) détectés ;
- Contributions apportées à des projets externes ;
- Durée d’ouverture des bugs ;
- Nombre de contributeurs sur un composant, nombre de commits, etc.

Cette activité consiste à définir ces exigences et besoins de mesure, et à mettre en place un tableau de bord qui montre de manière simple et efficace les principaux indicateurs du programme.

### Évaluation de l’opportunité

Les indicateurs clés permettent de comprendre et de mieux gérer les ressources consacrées aux logiciels libres, et de mesurer les résultats afin de communiquer efficacement et de récolter tous les bénéfices de l’investissement. En communiquant largement, davantage de personnes peuvent suivre l’initiative et se sentiront impliquées, ce qui en fait finalement une préoccupation et un objectif au niveau de l’organisation.

Si chaque activité est assortie de critères d’évaluation qui permettent de répondre aux questions sur les progrès réalisés, il est toujours nécessaire d’effectuer un suivi à l’aide de chiffres et d’indicateurs quantitatifs.

Qu’il s’agisse d’une petite start-up ou d’une grande entreprise internationale, les indicateurs clés permettent aux équipes de rester concentrées et de suivre les performances. Les indicateurs sont essentiels, car ils soutiennent la prise de décision et constituent la base du suivi des décisions déjà prises.

Avec des chiffres et des graphiques simples et pratiques, les membres de l’ensemble de l’organisation pourront suivre et synchroniser les efforts en matière d’open source, ce qui en fait une préoccupation et une action communes. Cela permet également aux différents acteurs de mieux entrer en piste, de contribuer au projet et d’en tirer les bénéfices globaux.

### Suivi de l'avancement

Les **points de contrôle** suivants dénotent une progression de cette activité :

- [ ] Une liste de métriques / mesures et la manière de les collecter ont été établies ;
- [ ] Des outils pour collecter, stocker, traiter et afficher les indicateurs sont utilisés ;
- [ ] Il existe un tableau de bord généralisé, accessible à tous les participants, qui montre les progrès réalisés dans le cadre de l’initiative.

### Outils

- [GrimoireLab](https://chaoss.github.io/grimoirelab) de Bitergia ;
- Les outils génériques de BI (elasticsearch, grafana, visualisations R/Python…) conviennent également, lorsque les connecteurs appropriés sont configurés en fonction des objectifs définis.

### Recommandations

- Rédiger les objectifs et la feuille de route de la gouvernance open source ;
- Communiquer en interne sur les actions et le statut de l’initiative ;
- Impliquer les personnes dans la définition des KPI, afin de s’assurer :
   - qu'ils soient bien compris ;
   - qu’ils fournissent une vue complète des besoins ;
   - qu'ils soient pris en compte et suivis.
- Construire au moins un tableau de bord qui peut être affiché pour tout le monde (par exemple sur un écran dans la salle), avec des indicateurs essentiels pour montrer les progrès et la situation globale.

### Ressources

- La [communauté CHAOSS](https://chaoss.community/) a beaucoup de bonnes références et ressources liées aux indicateurs open source.
- Consultez les métriques pour les [attributs de projet](https://www.ow2.org/view/MRL/Stage2-ProjectAttributes) de la méthodologie OW2 [Market Readiness Levels](https://www.ow2.org/view/MRL/Overview).
- [Une nouvelle façon de mesurer l’ouverture : The Open Governance Index](https://timreview.ca/article/512) par Liz Laffan est une lecture intéressante sur l’ouverture dans les projets open source.
- [Governance Indicators : A Users’ Guide](https://anfrel.org/wp-content/uploads/2012/02/2007_UNDP_goveranceindicators.pdf) est le guide de l’ONU sur les indicateurs de gouvernance. Bien qu’il s’applique à la démocratie, à la corruption et à la transparence des nations, les principes de base de la mesure et des indicateurs appliqués à la gouvernance valent la peine d’être lus.

### Activités à venir

- [GGI-A-37 - L’open source au service de la transformation numérique](https://ospo-alliance.org/ggi/activities/open_source_enabling_digital_transformation) Utilisez les métriques produites dans le cadre de votre stratégie open source générale.
