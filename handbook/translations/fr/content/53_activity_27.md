## Appartenir à la communauté open source

Identifiant de l'activité : [GGI-A-27](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_27.md).

### Description

Cette activité vise à accroître chez les développeurs le sentiment d'appartenance à la grande communauté open source. Comme pour toute communauté, les personnes et les entités doivent participer et contribuer en retour à l'ensemble. Cela renforce les liens entre les praticiens et apporte de la durabilité et de l'activité à l'écosystème. D'un point de vue plus technique, cela permet de choisir les priorités et la feuille de route des projets, d'améliorer le niveau de connaissances générales et de compréhension technique.

Cette activité couvre les points suivants :

- **Identifier les événements** qui valent la peine d'être suivis. La mise en relation de personnes, l'apprentissage de nouvelles technologies et la création d'un réseau sont des facteurs clés pour profiter pleinement des avantages de l'open source.
- Envisagez **l'adhésion à des fondations**. Les fondations et organisations open source sont un élément clé de l'écosystème open source. Elles fournissent des ressources techniques et organisationnelles aux projets, et constituent un bon endroit neutre où les adhérents peuvent discuter de problèmes et de solutions communes, ou travailler sur des normes.
- Surveillez les **groupes de travail**. Les groupes de travail sont des espaces de collaboration neutres où les experts interagissent sur un domaine spécifique comme l'IoT, la modélisation ou la science. Ils constituent un mécanisme très efficace et rentable pour aborder ensemble des préoccupations générales, bien que spécifiques à un domaine.
- **Participation au budget**. En dernier ressort, l'argent est le catalyseur. Planifiez les dépenses nécessaires, accordez du temps rémunéré aux personnes pour ces activités, anticipez les prochaines actions, afin que le programme ne doive pas s'arrêter après quelques mois par manque de financement.

### Évaluation de l’opportunité

L'open source fonctionne mieux lorsqu'il est réalisé en relation avec la communauté open source au sens large. Cela facilite la correction des bugs, le partage des solutions, etc.

C'est aussi un bon moyen pour les entreprises de montrer leur soutien aux valeurs de l'open source. Communiquer sur l'implication de l'entreprise est important à la fois pour la réputation de l'entreprise et pour l'écosystème open-source.

### Suivi de l'avancement

Les **points de contrôle** suivants montrent une progression de cette Activité :

- [ ] Une liste d'événements auxquels les gens pourraient assister est rédigée.
- [ ] Il existe un suivi des conférences publiques données par les membres de l'équipe.
- [ ] Les personnes peuvent soumettre des demandes de participation à des événements.
- [ ] Les personnes peuvent soumettre des projets de sponsoring / adhésion.

### Recommandations

- Sonder les personnes pour savoir quels événements les intéressent ou seraient les plus bénéfiques pour leur travail.
- Mettre en place une communication interne (bulletin d'information, centre de ressources, invitations…) pour que les gens soient au courant des initiatives et puissent y participer.
- Assurez-vous que ces initiatives peuvent bénéficier à différents types de personnes (développeurs, administrateurs, support…), et pas seulement aux cadres dirigeants.

### Ressources

- [What motivates a developer to contribute to open source software?](https://clearcode.cc/blog/why-developers-contribute-open-source-software/) Un article de Michael Sweeney sur clearcode.cc.
- [Why companies contribute to open source](https://blogs.vmware.com/opensource/2020/12/01/why-companies-contribute-to-open-source/) : Un article de Velichka Atanasova de VMWare.
- [Why your employees should be contributing to open source](https://www.cloudbees.com/blog/why-your-employees-should-be-contributing-to-open-source/) : Une bonne lecture par Robert Kowalski de CloudBees.
- [7 ways your company can support open source](https://www.infoworld.com/article/2612259/7-ways-your-company-can-support-open-source.html) : Un article de Simon Phipps pour InfoWorld.
- [Events: the life force of open source](https://www.redhat.com/en/blog/events-life-force-open-source) : Un article de Donna Benjamin de RedHat.

### Activités à venir

- [GGI-A-28 - Point de vue Ressources Humaines](https://ospo-alliance.org/ggi/activities/human_resources_perspective) Si l’organisation appartient à la communauté OSS, il est plus facile d’attirer des personnes qualifiées, en fonction de la communauté dans laquelle vous êtes impliqué.
- [GGI-A-31 - Affirmer publiquement l’utilisation de l’open source](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Maintenant que vous faites partie de la communauté OSS, faites le savoir ! C’est bon pour votre réputation, et c’est bon pour le projet en termes de santé et de diffusion.
