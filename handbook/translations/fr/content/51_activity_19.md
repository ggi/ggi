## Supervision de l'open source

Identifiant de l'activité : [GGI-A-19](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_19.md).

### Description

Cette activité vise à contrôler l'usage de l'open source et garantir que le logiciel open source est géré de manière adéquate. Différentes perspectives sont concernées, que ce soit l'usage d'outils ou solutions métier open source, l'inclusion de composants open source dans les développements internes, la modification d'une version de logiciel pour l'adapter à ses besoins, etc. Il est aussi question d'identifier les domaines dans lesquels l'open source est devenu une solution de fait (parfois cachée), et d'y évaluer sa pertinence.

Il peut être nécessaire de clarifier les points suivants :

- La fonctionnalité requise est-elle fournie ?
- Y a-t-il des fonctionnalités complémentaires dont on n'avait pas besoin, et qui accroissent la complexité des phases d'assemblage (build) ou d'exécution ?
- Quelles sont les contraintes liées à la licence, et leurs implications légales ?
- À quel point la solution accroît-elle l'indépendance de votre organisation vis-à-vis de ses fournisseurs ?
- Existe-t-il une offre de support en phase avec vos besoins métier, et quel est son coût ?
- TCO (Coût Total de Possession) ;
- L'encadrement est-il informé des avantages de l'open source, du moins au-delà des « économies de coût de licence » ? Être à l'aise avec l'open source aide à tirer le meilleur profit de la coopération avec les communautés, projets et éditeurs ;
- Évaluer s'il est pertinent de partager les coûts de développement en reversant ses propres développements à la communauté, en tenant compte de toutes les implications, comme la conformité à la licence.
- Vérifier la disponibilité de support, communautaire ou professionnel.

### Évaluation de l’opportunité

Définir un processus de décision orienté vers l'open source est un moyen de maximiser ses profits.

- Cela évite l'émergence d'usages sans contrôle et de coûts cachés des technologies open source.
- Cela conduit à des décisions stratégiques et organisationnelles informées et conscientes quant à l'open source.

Coûts : l'activité peut remettre en question et conduire à reconsidérer l'usage de facto de l'open source comme sous-optimal, inefficace, risqué, etc.

### Suivi de l'avancement

Les **points de contrôle** suivants dénotent une progression de cette activité :

- [ ] L'open source est devenu un choix confortable quand il n'est plus perçu comme une exception ou un choix dangereux.
- [ ] L'open source est devenu un choix confortable quand il n'est plus perçu comme une exception ou un choix dangereux.
- [ ] L'open source est devenu une option « mainstream » ;
- [ ] Les acteurs clés sont suffisamment convaincus que l'open source a des avantages stratégiques justifiant qu'on y investisse.
- [ ] Il est possible de démontrer que le TCO d'une solution basée sur l'open source est inférieur à celui d'alternatives propriétaires.
- [ ] Il est possible d'évaluer à quel degré le gain d'indépendance vis-à-vis des fournisseurs permet ou va permettre des économies ;
- [ ] Il est possible d’évaluer à quel degré l’indépendance qu’apporte la solution réduit les coûts potentiels d’un changement de solution (ouverture des formats).

### Outils

A ce stade, nous ne pensons à aucun outil pertinent ou concerné par cette activité.

### Recommandations

- Gérer de manière proactive l'usage de l'open source nécessite un niveau minimum de compréhension de ses fondamentaux, préalable à toute décision le concernant.
- Se concentrer sur les fonctionnalités nécessaires plutôt que la recherche d'une alternative à une solution propriétaire.
- S'assurer de la disponibilité de support et de la pérennité des développements futurs.
- Considérer les effets sur votre organisation de la licence de la solution.
- Convaincre les acteurs clés des avantages de l'open source, bien au-delà de l' « économie sur les coûts de licence ».
- Être honnête, en n'exagérant pas les bénéfices attendus de l'open source.
- Dans le processus de décision, il est important d'évaluer différentes solutions open source, pour éviter les désillusions liées à des attentes erronées, éclaircir ce que l'organisation devra faire, rendre perceptibles les avantages apportés par l'ouverture des solutions. L'organisation doit faire en sorte d'adapter cette évaluation à son contexte propre.

### Ressources

- [Les 5 principaux avantages de l'Open Source](https://www.openlogic.com/blog/top-5-benefits-open-source-software) : Blog sponsorisé mais intéressant, lecture courte.
- [Évaluer les coûts cachés de l'Open Source](https://www.itjungle.com/2021/02/15/weighing-the-hidden-costs-of-open-source/) : une analyse des coûts de support de l'OSS, sponsorisée par IBM.
