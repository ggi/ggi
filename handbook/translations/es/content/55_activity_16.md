## Establecer una estrategia para la gobernanza corporativa del código abierto

ID de la Actividad: [GGI-A-16](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_16.md).

### Descripción

Definir una estrategia de alto nivel para la gobernanza del código abierto dentro de la empresa garantiza la coherencia y visibilidad de los enfoques tanto hacia el uso interno como hacia las contribuciones y participación externas. Hace que la comunicación de la empresa sea más eficaz al ofrecer una visión y un liderazgo claros y establecidos.

El cambio hacia el código abierto conlleva numerosas ventajas, así como algunas obligaciones y un cambio en la cultura de la empresa. Puede repercutir en los modelos de negocio e influir en la forma en que una organización presenta su valor y su oferta, y en su posición frente a sus clientes y competidores.

Esta actividad incluye las siguientes tareas:

- Establecer un responsable de OSS, con el patrocinio y respaldo de la dirección (superior).
- Establezca y publique una hoja de ruta clara para el código abierto, con objetivos claros y beneficios esperados.
- Asegúrese de que todos los altos directivos la conocen y actúan de acuerdo con ella.
- Promover el OSS dentro de la empresa: animar a la gente a utilizarlo, fomentar las iniciativas internas y el nivel de conocimientos.
- Promover el OSS fuera de la empresa: mediante declaraciones y comunicaciones oficiales, y una participación visible en iniciativas de OSS.

Definir, publicar y hacer cumplir una estrategia clara y coherente también ayuda a que todo el personal de la empresa la acepte y facilita otras iniciativas de los equipos.

### Evaluación de oportunidades

Es un buen momento para trabajar en esta actividad si:

- No existe un esfuerzo coordinado por parte de la dirección, y el código abierto sigue considerándose una solución ad hoc.
- Ya existen iniciativas internas, pero no penetran hasta los niveles superiores de dirección.
- La iniciativa se puso en marcha hace algún tiempo, pero se enfrenta a numerosos obstáculos y sigue sin dar los resultados esperados.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] La empresa cuenta con una declaración clara de intenciones respecto al gobierno del código abierto. La declaración debe contener:
   - lo que hay que conseguir,
   - para quién lo hacemos,
   - cuál es el poder del estratega o estrategas y qué no.
- [ ] Una hoja de ruta del código abierto está ampliamente disponible y aceptada en toda la empresa.

### Recomendaciones

- Establezca un grupo de personas y procesos para definir y supervisar la gobernanza del código abierto en la empresa. Asegúrese de que existe un compromiso claro de la alta dirección con las iniciativas de código abierto.
- Comunicar la estrategia de código abierto dentro de la organización, convertirla en una preocupación importante y en un verdadero compromiso corporativo.
- Garantizar que la hoja de ruta y la estrategia sean bien comprendidas por todos, desde los equipos de desarrollo hasta el personal de gestión e infraestructura.
- Comunicar sus avances, para que la gente sepa en qué punto se encuentra la organización con respecto a su compromiso. Publique periódicamente actualizaciones e indicadores.

### Recursos

- [Lista de control y referencias para la gobernanza abierta](https://opengovernance.dev/).
- [L'open source comme enjeu de souveraineté numérique, por Cédric Thomas, CEO de OW2, Taller en Orange Labs, París, 28 de enero de 2020](https://www.ow2.org/download/OSS_Governance/Level_5/2001-OSSetSouveraineteNumerique-RC3.pdf) (sólo en francés).
- [Una serie de guías para gestionar el código abierto en la empresa, por la Fundación Linux](https://todogroup.org/guides/).
- [Un buen ejemplo de documento estratégico de código abierto, por el grupo LF Energy](https://www.lfenergy.org/wp-content/uploads/sites/67/2019/07/Open-Source-Strategy-V1.0.pdf)

### Próximas actividades propuestas

- [GGI-A-35 - Código abierto y soberanía digital](https://ospo-alliance.org/ggi/activities/open_source_and_digital_sovereignty) Una estrategia adecuada para la gobernanza corporativa del código abierto debería mejorar el código abierto y la soberanía digital. Ahora es un buen momento para definirlas en el contexto de la organización.
- [GGI-A-34 - C-Level awareness](https://ospo-alliance.org/ggi/activities/c-level_awareness) Será necesario el compromiso de los ejecutivos de nivel C para implementar adecuadamente la estrategia corporativa de código abierto. Educarles e implicarles es un buen paso para lograr el éxito.
