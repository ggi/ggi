# Introducción

Este documento presenta una metodología para implantar una gestión profesional del software de código abierto en una organización. Aborda la necesidad de utilizar software de código abierto de forma adecuada y justa, salvaguardar a empresa de amenazas técnicas, legales y de propiedad intelectual y maximizar las ventajas del software de código abierto. Donde sea que se encuentre la organización respecto a estos temas, este documento propone orientación e ideas para avanzar y hacer de su viaje un éxito.

## Contexto

La mayoría de los grandes usuarios e integradores de sistemas ya utilizan Software Libre y de Código Abierto (FOSS) ya sea en sus sistemas de información o en sus divisiones de productos y servicios. El cumplimento de las condiciones legales del código abierto viene siendo una preocupación creciente, y muchas empresas grandes han nombrado responsables de cumplimento. Sin embargo aunque asegurar su cadena de producción de software código abierto - que es la esencia del cumplimiento - es fundamental, los usuarios *tienen* que contribuir a las comunidades para la sostenibilidad del ecosistema de código abierto. Vemos a la gobernanza de código abierto acompasando a todo el ecosistema, participando en las comunidades locales, nutriendo una relación sana con los proveedores de software de código abierto y sus servicios asociados. Esto eleva a la conformidad al siguiente nivel y es de lo que trata un "buen" gobierno del código abierto.

Esta iniciativa abarca más allá del cumplimento y la responsabilidad legal. Trata acerca de desarrollar conciencia en las comunidades de usuarios finales (muchas veces desarrolladores de software a su vez) e integradores de sistemas. Y de desarrollar relaciones mutuamente beneficiosas en el seno del ecosistema europeo de FOSS.

La Buena Gobernanza del OSS habilita a organizaciones de todo tipo -- empresas pequeñas y grandes, ayuntamientos, universidades, asociaciones, etc. -- para maximizar los beneficios derivados del código abierto ayudo a alinear personas, procesos, tecnología y estrategia. Y en esta área, la de maximizar as ventajas del código abierto, especialmente en Europa, todo el mundo está aún aprendiendo e innovando, y nadie sabe donde se encuentra realmente respecto al estado del arte.

Esta iniciativa intenta ayudar a las organizaciones a lograr estos objetivos mediante:

* Un catálogo estructurado de **actividades**, un plan rector para la implantación de una gestión profesional del software de código abierto.
* Una **herramienta de gestión** para definir, monitorizar, informar y comunicar el progreso.
* Un **camino claro y práctico para la mejora**, con pasos pequeños y accesibles para mitigar riesgos, educar a las personas, adaptar procesos y comunicar interna y externamente al dominio de la organización.
* **Orientación** y una serie de **referencias seleccionadas** acerca del licenciamiento de código abierto, prácticas recomendadas, formación, y participación en el ecosistema para elevar la concienciación y la cultura del código abierto, consolidar el conocimiento interno y extender el liderazgo.

Este guía se ha desarrollado contemplando los siguientes requisitos:

* Se cubre cualquier tipo de organización: desde PYMEs a grandes empresas y ONGs, desde autoridades locales (ayuntamientos) a grandes instituciones ( europeas o gubernamentales). El marco de trabajo proporciona piezas para componer una estrategia y pistas para a su realización, pero *cómo* se ejecuten las actividades depende totalmente del contexto propio del programa y se deja a criterio de su gestor. Podría ser útil buscar servicios de consultoría e intercambiar ideas con otros pares.
* No se da nada por sentado acerca del nivel de conocimiento técnico en la organización o el dominio de actividad. Por ejemplo, algunas organizaciones necesitarán establecer un currículo formativo completo mientras que otras quizá simplemente propongan material adaptado a los equipos.

Algunas actividades no serán relevantes en todas las situaciones pero el marco de trabajo en sí sigue proporcionando una hoja de ruta completa y allana el camino a estrategias personalizadas.

## Acerca de la Iniciativa de Buen Gobierno

En OW2, una iniciativa es un esfuerzo conjunto para abordar una necesidad de mercado. La [Iniciativa por el Buen Gobierno](https://www.ow2.org/view/OSS_Governance) propone un marco metodológico para implantar en las organizaciones una gestión profesional del software de fuentes abiertas.

La Iniciativa de Buen Gobierno se basa en un modelo completo inspirado en la conocida jerarquía de necesidades y motivaciones humanas de Abraham Maslow; tal como ilustra la siguiente imagen.

![Maslow y la GGI](resources/images/ggi_maslow.png)

Mediante ideas, directrices y actividades, la Iniciativa de Buen Gobierno proporciona un patrón para la implantación de entidades organizativas comisionadas para la gestión profesional del software de fuentes abiertas, también llamadas OSPO (por las siglas en inglés de Oficina de Programa de Software de Fuentes Abiertas). La metodología es también un sistema de gestión para definir prioridades y monitorizar y compartir avances.

A medida que las organizaciones implantan la metodología de Buen Gobierno del OSS ampliarán sus habilidades en varias direcciones, incluyendo:

* **usar** software de fuentes abiertas de forma adecuada y segura en la empresa para mejorar la reutilización y mantenibilidad y la velocidad del desarrollo del software;
* **mitigar** los riesgos legales y técnicos asociados al código externo y a la colaboración;
* **identificar** la formación necesaria para los equipos, desde los programadores a los jefes de equipo y gestores, para que todos compartan la misma visión;
* **priorizar** objetivos y actividades, para desarrollar una estrategia eficiente de código abierto;
* **comunicar** eficientemente en la empresa y al mundo exterior para aprovechar la estrategia de código abierto;
* **mejorar** la competitividad y hacer la organización atractiva para el talento estrella del código abierto.

## Acerca de la OSPO Alliance

Las principales organizaciones europeas sin ánimo de lucro del código abierto, incluyendo a OW2, la Eclipse Foundation, el OpenForum Europe y la Foundation for Public Code, lanzaron la **OSPO Alliance** con la misión de desarrollar conciencia por el código abierto en Europa y en el mundo y de promover una gestión estructurada y profesional del código abierto en empresas y administraciones.

Mientras la iniciativa por el Buen Gobierno se enfoca en desarrollar una metodología de gestión, la OSPO Alliance tiene el objetivo más amplio de ayudar a empresas, particularmente en los sectores no tecnológicos, e instituciones públicas, a descubrir y comprender el código abierto, comenzar a beneficiarse de él en sus actividades y crecer para auspiciar sus propias OSPOs.

A OSPO Alliance ha establecido el sítio web **OSPO.Alliance** alojado en <https://ospo-alliance.org>. La OSPO Alliance sirve a la comunidad con un lugar seguro en el que debatir e intercambiar ideas acerca de temas de OSPOs y proporciona un repositorio para un conjunto completo de recursos para corporaciones, instituciones públicas y organizaciones académicas y de investigación. La OSPO Alliance conecta con OSPOs en toda Europa y en el mundo, así como con organizaciones comunitarias que la apoyan. Promueve buenas prácticas y fomenta la contribución a la sostentabilidad del ecosistema de código abierto. Visite el sitio web [OSPO Alliance](https://ospo-alliance.org) para una rápida visión general de las estructuras complementarias de buenas prácticas de gestión de TI.

El sitio web [OSPO Alliance](https://ospo-alliance.org) es también el lugar donde recabamos comentarios de la comunidad sobre la iniciativa y sus contenidos (actividades, acerbo de conocimiento).

## Traducciones

Este libro se escribió originalmente en inglés. También está disponible en francés, alemán, portugués, holandés, italiano y español, gracias a un trabajo comunitario en curso para traducir el Manual de la GGI. Dado que los avances evolucionan rápidamente, le recomendamos que consulte nuestra página web oficial para obtener una lista completa de las traducciones disponibles.

> Vea <https://ospo-alliance.org/ggi/>

El manual de GGI se traduce empelando [Weblate](https://hosted.weblate.org/), un proyecto y plataforma de código abierto que ofrece hospedaje gratuito para proyectos de código abierto. Queremos agradecerles profundamente, así como a todos nuestros colaboradores de traducción. Sois alucinantes.

> Vea <https://hosted.weblate.org/projects/ospo-zone-ggi/#languages>

## Contribuidores

Las siguientes grandes personas han contribuido al manual de la Iniciativa para la Buena Gobernanza:

* Frédéric Aatz (Microsoft Francia)
* Boris Baldassari (Castalia Solutions, Eclipse Foundation)
* Philippe Bareille (Ayuntamiento de París)
* Gaël Blondelle (Eclipse Foundation)
* Vicky Brasseur (Wipro)
* Philippe Carré (Nokia)
* Pierre-Yves Gibello (OW2)
* Michael Jaeger (Siemens)
* Sébastien Lejeune (Thales)
* Max Mehl (Free Software Foundation Europe)
* Catherine Nuel (OW2)
* Hervé Pacault (Orange)
* Stefano Pampaloni (RIOS)
* Christian Paterson (OpenUp)
* Simon Phipps (Meshed Insights)
* Silvério Santos (Orange Business)
* Cédric Thomas (OW2)
* Nicolas Toussaint (Orange Business)
* Florent Zara (Eclipse Foundation)
* Igor Zubiaurre (Bitergia)

## Licencia

Esta obra está licenciada mediante licencia [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0). Del sitio web de Creative Commons:

> Tiene Ud. derecho a:
>
> * Compartir — copiar y redistribuir el material en cualquier soporte o formato
> * Adaptar — remezclar, transformar, y crear partiendo del material
>
> para cualquier propósito, incluso comercialmente.
>
> Usted debe dar crédito de manera adecuada, brindar un enlace a la licencia, e indicar si se han realizado cambios. Puede hacerlo en cualquier forma razonable, pero no de forma tal que sugiera que usted o su uso tienen el apoyo de la licenciante. [Original: https://creativecommons.org/licenses/by/4.0/deed.es].

Todo el contenido es Copyright de OSPO Alliance y otros.
