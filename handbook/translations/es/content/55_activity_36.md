## El código abierto hace posible la innovación

Identificativo de actividad: [GGI-A-36](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_36.md).

### Descripción

> "La innovación es la aplicación práctica de las ideas que dan lugar a la introducción de nuevos bienes o servicios o a la mejora de la oferta de bienes o servicios."
>
> &mdash; <cite>Schumpeter, Joseph A.</cite>

El código abierto puede ser un factor clave para la innovación a través de la diversidad, la colaboración, y el intercambio fluido de ideas. Personas de distintas entornos, y ámbitos, pueden tener perspectivas diferentes, y aportar respuestas nuevas, mejoradas o incluso disruptivas a problemas conocidos. Se puede hacer posible la innovación escuchando distintos puntos de vista, y promoviendo activamente la colaboración abierta en proyectos y temas.

Del mismo modo, participar en la elaboración, e implementación de normas abiertas es un gran promotor de buenas prácticas, e ideas para mejorar el trabajo diario de la empresa. También permite a la entidad impulsar, e influir en la innovación hacia donde, y en lo, que necesita, así como mejora la visibilidad, y la reputación mundiales.

A través de la innovación, el código abierto permite, no sólo transformar los bienes, o servicios, que vuestra empresa comercializa, sino también crear, o modificar, todo el ecosistema en el que la empresa quiere prosperar.

Por ejemplo, al publicar Android como código abierto, Google invita a cientos de miles de empresas a crear sus propios servicios basados en esta tecnología de código abierto. Google crea, así, todo un ecosistema del que pueden beneficiarse todos los participantes. Por supuesto, muy pocas empresas son lo suficientemente poderosas, como para crear un ecosistema por decisión propia. Pero hay muchos ejemplos de alianzas, entre empresas, para crear un ecosistema de este tipo.

### Evaluación de oportunidades

Es importante evaluar la posición de vuestra empresa comparada a la de los competidores, socios, y clientes, porque a menudo sería arriesgado para una empresa alejarse demasiado de las normas y tecnologías utilizadas por clientes, socios, y competidores. Evidentemente, innovar significa ser diferente, pero lo que difiere no debe constituir un ámbito demasiado amplio; de lo contrario, la empresa no se beneficiará de los desarrollos de logiciales realizados por las demás empresas del ecosistema, ni del impulso empresarial, que este proporciona.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta actividad:

- [ ] Se han identificado las tecnologías — y las comunidades de código abierto, que las desarrollan —, que afectan la empresa.
- [ ] Los avances, y publicaciones, de estas comunidades de código abierto son objeto de seguimiento — incluso yo conozco la estrategia de ellas, antes de que se hagan públicos los lanzamientos.
- [ ] Los empleados de la organización son miembros de (algunas de) estas comunidades de código abierto, e influyen en sus hojas de ruta, y decisiones técnicas, aportando líneas de código, y participando en los órganos de gobierno de dichas comunidades.

### Recomendaciones

De todas las tecnologías necesarias, para dirigir vuestra empresa, debéis identificar:

- las tecnologías, que podrían ser las mismas que las de vuestros competidores,
- las tecnologías, que deben ser específicas de vuestra empresa.

Manteneos al día sobre las tecnologías emergentes. El código abierto ha estado impulsando la innovación durante la última década, y muchas herramientas potentes de todos los días proceden de ahí (pensad en Docker, Kubernetes, los proyectos de Big Data de Apache o Linux). No hay necesidad de saber todo sobre todo, pero uno debe saber lo suficiente del estado de la técnica, para identificar nuevas tendencias interesantes.

Permitid, y animad, a la gente a presentar ideas innovadoras, y a sacarlas adelante. Si es posible, destinad recursos a estas iniciativas, y hacedlas crecer. Confiad en la pasión, y la voluntad de la gente para crear, y fomentar ideas, y tendencias emergentes.

### Recursos

- [4 innovations we owe to open source](https://www.techrepublic.com/article/4-innovations-we-owe-to-open-source/).
- [The Innovations of Open Source](https://dirkriehle.com/publications/2019-selected/the-innovations-of-open-source/), del profesor Dirk Riehle.
- [Open source technology, enabling innovation](https://www.raconteur.net/technology/cloud/open-source-technology/).
- [Can Open Source Innovation Work in the Enterprise?](https://www.threefivetwo.com/blog/can-open-source-innovation-work-in-the-enterprise).
- [Europe: Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).
- [Europe: Open source software strategy 2020-2023](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
