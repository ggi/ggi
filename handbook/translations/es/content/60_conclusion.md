# Conclusión

Como hemos dicho antes, el buen gobierno del código abierto no es un destino; es un viaje. Tenemos que preocuparnos por nuestros bienes comunes, por las comunidades y el ecosistema que lo hacen posible, porque de ello depende nuestro propio éxito común y, por tanto, individual.

**Nosotros**, como profesionales del software y entusiastas del código abierto, nos comprometemos a seguir mejorando el manual de la Iniciativa para el Buen Gobierno y a trabajar en su difusión y alcance. Creemos firmemente que las organizaciones, los individuos y las comunidades deben trabajar codo con codo para construir un mejor y mayor conjunto de bienes comunes, disponibles y beneficiosos para todos.

**Usted** es bienvenido a unirse a la Alianza OSPO, contribuir a nuestro trabajo, correr la voz y ser el embajador de una mejor concienciación y gobernanza del código abierto dentro de su propio ecosistema. Hay una gran variedad de recursos disponibles, desde entradas de blog y artículos de investigación hasta conferencias y cursos de formación en línea. También ofrecemos un conjunto de material útil en [nuestro sitio web](https://ospo-alliance.org), y estamos dispuestos a ayudar en todo lo que podamos.

**¡Definamos y construyamos juntos el futuro de la Iniciativa para el Buen Gobierno!**

## Contacte con

La forma preferida de ponerse en contacto con la Alianza OSPO es enviar un mensaje a nuestra lista de correo pública en <https://accounts.eclipse.org/mailing-list/ospo.zone>. También puede venir a debatir con nosotros en los eventos habituales sobre código abierto, participar en nuestros seminarios web mensuales OSPO OnRamp o ponerse en contacto con cualquier miembro, que amablemente le redirigirá a la persona adecuada.

## Apéndice: Plantilla de cuadro de mando de actividades personalizado

La última versión de la plantilla del cuadro de mando de actividades personalizado está disponible en la sección `resources` del [Gitlab de la Good Governance Initiative](https://gitlab.ow2.org/ggi/ggi-castalia) en OW2.
