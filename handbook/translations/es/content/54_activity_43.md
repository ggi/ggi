## Política de adquisición de código abierto

Identificativo de actividad: [GGI-A-43](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_43.md).

### Descripción

Esta actividad consiste en implementar un proceso para seleccionar, adquirir, y comprar logiciales o servicios de código abierto. También se trata de considerar el coste real del logicial de código abierto, y prepararse para ello. El logicial do código abierto puede ser «gratuito» a primera vista, pero no está exento de costes internos y externos, como integración, formación, mantenimiento, y soporte.

Dicha política exige, que, tanto las soluciones de código abierto, como las propietarias, sean tomadas simétricamente a la hora de evaluar la relación calidad-precio, en tanto que combinación óptima del coste total de propiedad, y calidad. Por ende, el departamento de compras de TI debe considerar de forma activa, y equitativa, las alternativas de código abierto, garantiendo, a la vez, que las soluciones propietarias sean puestas en pie de igualdad en las decisiones de compra.

La preferencia por el código abierto puede afirmarse explícitamente basándose en la flexibilidad intrínseca de la opción de código abierto, cuando no existe una diferencia significativa de coste global, entre las soluciones propietarias, y las de código abierto.

Los departamentos de adquisiciones tienen, que entender, que las empresas, que brindan soporte a los logiciales de código abierto suelen carecer de músculo comercial para participar en concursos de adquisiciones y adaptar sus políticas y procesos de adquisición de código abierto en consecuencia.

### Evaluación de oportunidades

Varias razones justifican los esfuerzos por establecer políticas específicas de adquisición de código abierto:

- El suministro de software y servicios comerciales de código abierto está creciendo y no puede ignorarse, y requiere la aplicación de políticas y procesos de adquisición específicos.
- Hay una oferta creciente de soluciones comerciales de código abierto altamente competitivas para sistemas de información empresariales.
- Incluso después de adoptar un componente logicial de código abierto gratuito, e integrarlo en una aplicación, hay que proporcionar recursos internos, o externos, para mantener ese código fuente.
- Con las soluciones FOSS, el Coste Total de Propiedad (TCO) suele ser inferior (aunque no necesariamente): no pagar licencias al comprar/actualizar, mercado abierto para proveedores de servicios, opción de proveer, uno mismo, una parte, o la totalidad de la solución.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta actividad:

- [ ] Las convocatorias de ofertas nuevas solicitan de manera proactiva candidaturas de código abierto.
- [ ] El departamento de compras tiene una forma de evaluar las soluciones de código abierto frente a las propietarias.
- [ ] Se ha implantado, y documentado, un proceso simplificado de adquisición de logiciales, y servicios, de código abierto.
- [ ] Se ha definido, y documentado, un proceso de aprobación basado en la experiencia interfuncional.

### Recomendaciones

- «Aseguraos de aprovechar la experiencia de vuestros equipos de TI, DevOps, ciberseguridad, gestión de riesgos, y adquisiciones, al crear el proceso.» (de [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/), en inglés).
- La legislación sobre competencia puede exigir que no se mencione, específicamente, «código abierto».
- Seleccionad la tecnología primero, y, a continuación, solicitad propuestas de servicios de personalización, y asistencia.

### Recursos

- [Decision factors for open source software procurement](http://oss-watch.ac.uk/resources/procurement-infopack): no es nuevo, pero sigue siendo una gran lectura de nuestros colegas de OSS-watch en el Reino Unido. Echad un vistazo a las [diapositivas](http://oss-watch.ac.uk/files/procurement.odp).
- [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/): un artículo reciente sobre la contratación de código abierto, con consejos útiles.

### Próximas actividades propuestas

- [GGI-A-33 - Comprometerse con los proveedores de código abierto](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) La definición de una política de documentación le ayuda a identificar a los proveedores y comunidades de OSS que deben importarle y con los que debe comprometerse.
