## Conciencia de la gerencia

ID de la actividad: [GGI-A-34](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_actividad_34.md).

### Descripción

La iniciativa de código abierto de la organización sólo dará sus beneficios estratégicos si se aplica en sus niveles más altos integrando el ADN de código abierto en la estrategia y el trabajo interno de la empresa. Ese compromiso no puede suceder sin si los altos ejecutivos o la gerencia. La formación y la mentalidad de código abierto también deben extenderse a quienes configuran las políticas, decisiones y estrategia general, tanto hacia adentro como hacia afuera de la empresa.

Este compromiso garantiza que las mejoras prácticas, los cambios de mentalidad y las nuevas iniciativas se cumplan con un apoyo constante, benevolente y sostenible de la jerarquía, generando una participación más ferviente de los trabajadores. Modela cómo los actores externos ven la organización, aportando beneficios reputacionales y de ecosistema. También es un medio para establecer la iniciativa y sus beneficios a medio y largo plazo.

### Evaluación de oportunidades

Esta actividad se vuelve esencial si/cuando:

- La organización ha establecido objetivos globales relevantes para la gestión del código abierto, pero se debate por alcanzarlos. Es improbable que la iniciativa pueda lograr nada sin buenos conocimientos y un claro compromiso por parte de los altos ejecutivos.
- La iniciativa ya ha comenzado y progresa, pero los niveles superiores de la jerarquía no hacen un seguimiento adecuado.

Con suerte, deberá hacerse evidente que todo menos el uso ad hoc de la fuente abierta requiere un enfoque coherente y bien pensado, dado el rango de equipos y cambio cultural que puede traer.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] Existe una oficina/funcionario de gobierno con mandato, para establecer una estrategia uniforme de código abierto, en toda la empresa, y garantizar, que el ámbito sea claro.
- [ ] Hay un compromiso claro y vinculante de la jerarquía con la estrategia para el código abierto.
- [ ] Hay una comunicación transparente por parte de la jerarquía acerca de su compromiso con el programa.
- [ ] La jerarquía está disponible para hablar acerca del software de código abierto. Se la puede requerir y confrontar con sus promesas.
- [ ] Hay un presupuesto y financiación adecuados para la iniciativa.

### Recomendaciones

Los ejemplos de acciones asociadas a esta actividad incluyen:

- Formar a la gerencia para desmitificar el código abierto.
- Obtener un respaldo explícito y práctico para la estrategia y uso del código abierto.
- Mencionar y respaldar explícitamente el programa de código abierto en comunicaciones internas.
- Mencionar y respaldar explícitamente el programa de código abierto en comunicaciones públicas.

El código abierto es un *facilitador estratégico* que embarca a la *cultura empresarial*. ¿Qué significa esto?

- El código abierto puede aprovecharse como mecanismo para desbaratar a los proveedores y reducir los costes de adquisición de logiciales.
   - ¿Debería el código abierto ser competencia de los *gestores de activos de software* o de los *departamentos de compras*?
- Las licencias de código abierto consagran las libertades que aportan las ventajas del código abierto, pero también conllevan *obligaciones*. Si no se cumplen adecuadamente, las responsabilidades pueden crear riesgos legales, comerciales y de imagen para una organización.
   - ¿Concederán las condiciones de la licencia visibilidad a recursos que deberían permanecer confidenciales?
   - ¿Afectará eso la cartera de patentes de mi organización?
   - ¿Cómo se debe formar y apoyar a los equipos de proyecto en esta materia?
- Contribuir de vuelta a proyectos externos de código abierto es donde reside el mayor valor del código abierto.
   - ¿Cómo debería mi empresa fomentar (y seguir) esto?
   - ¿Cómo deben utilizar los programadores GitHub, GitLab, Slack, Discord, Telegram, o cualquiera de las demás herramientas que utilizan, habitualmente, los proyectos de código abierto?
   - ¿Puede el código abierto afectar a las políticas de recursos humanos de la empresa?
- Por supuesto, no se trata solo de contribuir, ¿qué hay de mis propios proyectos de código abierto?
   - ¿Estoy listo para hacer innovación *abierta*?
   - ¿Cómo gestionarán mis proyectos las contribuciones *entrantes*?
   - ¿Debo invertir esfuerzo en nutrir una comunidad para un proyecto dado?
   - ¿Cómo debo dirigir la comunidad, qué papel deben tener los miembros de la comunidad?
   - ¿Estoy listo para ceder las decisiones de la hoja de ruta a una comunidad?
   - ¿Puede la fuente abierta ser una herramienta valiosa para reducir el aislamiento entre los equipos de la empresa?
   - ¿Necesito manejar la transferencia de código abierto de una entidad a otra?

### Próximas actividades propuestas

- [GGI-A-31 - Afirmar públicamente el uso del código abierto](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Los ejecutivos de nivel C son representantes destacados de una organización. Haz que comuniquen la implicación de la organización en el código abierto.
