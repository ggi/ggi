## Contribuir a los proyectos de código abierto

ID de la actividad: [GGI-A-26](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_26.md).

### Descripción

Contribuir a proyectos de código abierto que se utilizan libremente es uno de los principios fundamentales del buen gobierno. El punto es evitar ser un simple consumidor pasivo y devolver a los proyectos. Cuando la gente agrega una característica o arregla un defecto para su propio fin, deben hacerlo lo suficientemente genérico para contribuir al proyecto. Los desarrolladores deben tener tiempo para las contribuciones.

Esta actividad abarca el siguiente ámbito:

- Trabajar en origen con proyectos de código abierto.
- Notificar defectos y solicitar funciones.
- Contribuir con código y corrección de defectos.
- Participar en las listas de correo de la comunidad.
- Compartir experiencias.

### Evaluación de oportunidades

Los principales beneficios de esta actividad son:

- Aumenta el conocimiento general y el compromiso con el código abierto dentro de la empresa, ya que la gente empieza a contribuir y a implicarse en proyectos de código abierto. Consiguen una sensación de utilidad pública y mejoran su reputación personal.
- La empresa aumenta su visibilidad y reputación a medida que las contribuciones se abren camino a través del proyecto contribuido. Esto demuestra que la empresa está realmente implicada en el código abierto, contribuye y promueve la equidad y la transparencia.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] Existe una vía clara y oficial para las personas que desean contribuir.
- [ ] Se anima a los desarrolladores a contribuir a los proyectos de código abierto que utilizan.
- [ ] Existe un proceso para garantizar el cumplimiento y la seguridad jurídicos de las contribuciones de los desarrolladores.
- [ ] KPI: Volumen de contribuciones externas (código, listas de correo, incidencias..) por individuo, equipo o entidad.

### Herramientas

Puede ser útil hacer seguimiento de las contribuciones, tanto para llevar la cuenta de lo que se aporta como para poder comunicar el esfuerzo de la empresa. Para ello pueden utilizarse cuadros de mando y programas informáticos para el seguimiento de actividad. Compruébe:

- [GrimoireLab](https://chaoss.github.io/grimoirelab/) de Bitergia
- [ScanCode](https://scancode-toolkit.readthedocs.io)

### Recomendaciones

Animar al personal de la entidad a contribuir a proyectos externos:

- Permitiéndoles invertir tiempo para escribir correcciones de errores y funciones genéricas y bien probadas, y contribuirlas a la comunidad.
- Capacitando al personal para contribuir a las comunidades de código abierto. Se trata tanto de las habilidades técnicas (mejorando el conocimiento de su equipo) como de la comunidad (pertenencia a las comunidades de código abierto, código de conducta, etc.).
- Proporcione formación sobre cuestiones jurídicas, de propiedad intelectual y técnicas, y establezca un contacto dentro de la empresa para ayudar con estos temas si la gente tiene dudas.
- Incentivar la publicación de trabajos.
- Tenga en cuenta que las contribuciones de la empresa/entidad reflejarán la calidad de su código y su implicación, así que asegúrese de que su equipo de desarrollo proporciona un código lo suficientemente bueno.

### Recursos

- La iniciativa [CHAOSS](https://chaoss.community/) de la Fundación Linux tiene algunas herramientas e indicaciones sobre cómo hacer seguimiento de las contribuciones al desarrollo.

### Próximas actividades propuestas

- [GGI-A-31 - Reafirmar públicamente el uso del código abierto](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Ahora que existe una contribución y un compromiso públicamente visibles por parte de la organización, ¡empieza a comunicarlo!
- [GGI-A-24 - Gestión de indicadores clave](https://ospo-alliance.org/ggi/activities/manage_key_indicators) Hacer visible y mensurable la contribución a los proyectos de OSS. Esto ayudará a difundir la iniciativa y a elevar la moral de la gente.
- [GGI-A-27 - Pertenecer a la comunidad de software libre](https://ospo-alliance.org/ggi/activities/belong_to_the_open_source_community) Contribuir a la comunidad de software libre es el primer paso para formar parte de ella. Una vez que la gente empieza a contribuir, se involucra más en la salud y la gobernanza del proyecto y puede llegar a convertirse en mantenedor, garantizando un proyecto y una hoja de ruta sostenibles y saludables.
- [GGI-A-29 - Implicarse en proyectos de código abierto](https://ospo-alliance.org/ggi/activities/engage_with_open_source_projects) Los proyectos de código abierto valoran la meritocracia. Ahora que has demostrado un buen conocimiento del código y los procesos, puedes implicarte en el proyecto y oficializar tus contribuciones.
- [GGI-A-36 - El código abierto como factor de innovación](https://ospo-alliance.org/ggi/activities/open_source_enabling_innovation) Contribuir a proyectos de OSS e interactuar con colaboradores externos es un factor de fomento de la innovación.
- [GGI-A-39 - Upstream primero](https://ospo-alliance.org/ggi/activities/upstream_first) Contribuir a proyectos de OSS realmente tiene sentido si las actualizaciones están disponibles en el proyecto upstream, de forma regular e institucionalizada.
