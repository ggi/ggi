## Gestionar competencias y recursos de código abierto

ID de la actividad: [GGI-A-42](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_42.md).

### Descripción

Esta actividad se centra en las habilidades y en los recursos de **desarrollo de software**. Incluye las tecnologías y habilidades específicas de desarrollo de los desarrolladores, así como de los procesos, métodos y herramientas de desarrollo en general.

Para las tecnologías de código abierto hay disponible una amplia cantidad de documentación, foros y debates que provienen del ecosistema, y recursos públicos. Para beneficiarse plenamente de su enfoque de código abierto, es necesario establecer una mapa de ruta de sus recursos actuales y metas deseadas para establecer un programa consistente de habilidades, métodos y herramientas de desarrollo en los equipos.

**Ámbitos de la aplicación**

Es necesario establecer los dominios en los que se aplicará el programa, y como irá mejorando la calidade y eficiencia del código y de las prácticas. Como ejemplo, el programa no tendrá los mismos beneficios si solo hay un único programador trabajando en componentes de código abierto que si todo o ciclo de vida del desarrollo se optimiza para incluir las buenas prácticas del código abierto.

Es necesario definir el ámbito de aplicación para el desarrollo de código abierto: componentes técnicos, aplicaciones, modernización o nuevos desarrollos. Algunos ejemplos de prácticas de desarrollo que se podrían beneficiar del código abierto serían:

- Administración de la nube.
- Aplicaciones nativas en la nube; cómo innovar con estas tecnologías.
- DevOps, Integración Contínua / Entrega Contínua.

**Categorías**

- Competencias y recursos necesarios para desarrollar software de código abierto: propiedad intelectual, licencias, prácticas.
- Habilidades y recursos necesarios para desarrollar software utilizando componentes, lenguajes y tecnologías de código abierto.
- Habilidades y recursos necesarios para utilizar métodos y procesos de código abierto.

### Evaluación de oportunidades

Las herramientas de código abierto son cada vez más populares entre los desarrolladores. Esta actividad aborda la necesidad de evitar la proliferación de herramientas heterogéneas dentro de un equipo de desarrollo. Ayuda a definir una política en este ámbito. Ayuda a optimizar la formación y la adquisición de experiencia. Se emplea un inventario de competencias para la contratación, la formación y la planificación de la sucesión en caso de que un empleado clave abandone la empresa.

Necesitaríamos una metodología para mapear las competencias de desarrollo de software de código abierto.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] Hay una descripción de la cadena de producción del software libre (la "cadena de suministro de software"),
- [ ] Existe un plan (o una lista de deseos) para la racionalización de los recursos de desarrollo,
- [ ] Existe un inventario de competencias en el que se resumen las aptitudes, formación y experiencia de los desarrolladores actuales,
- [ ] Existe una lista de deseos en materia de formación y un programa que aborda las carencias de competencias,
- [ ] Hay una lista de buenas prácticas de desarrollo de código abierto que faltan y un plan para aplicarlas.

### Recomendaciones

- Empiece por lo básico, haga crecer el análisis y la hoja de ruta de forma constante.
- A la hora de contratar, haga especial hincapié en las aptitudes y la experiencia en código abierto. Siempre es más fácil cuando las personas ya tienen un ADN de código abierto que formarlas y entrenarlas.
- Consulte los programas de formación de los proveedores de software y las escuelas de código abierto.

### Recursos

Más información:

- Una introducción a [¿qué es un inventario de competencias?](https://managementisajourney.com/management-toolbox-better-decision-making-with-a-skills-inventory) de Robert Tanner.
- Un artículo sobre competencias en código abierto: [5 habilidades de código abierto para mejorar su juego y su currículum](https://sourceforge.net/blog/5-open-source-skills-game-resume/)

Esta actividad puede incluir recursos y competencias técnicas como:

- **Lenguajes populares** (como Java, PHP, Perl, Python).
- **Marcos de trabajo de código abierto** (Spring, AngularJS, Symfony) y herramientas de prueba.
- **Métodos y buenas prácticas de desarrollo** ágil, DevOps y de código abierto.

### Próximas actividades propuestas

- [GGI-A-28 - Perspectiva de los Recursos Humanos](https://ospo-alliance.org/ggi/activities/hr_perspective) Una vez identificados internamente los recursos de código abierto para ayudar a la concienciación sobre el código abierto, haga que el departamento de Recursos Humanos también los valore, tanto para los empleados actuales como para los futuros.
