# InnerSource

InnerSource es cada vez más popular en las empresas, ya que ofrece un enfoque basado en prácticas de código abierto que han tenido éxito en los equipos de desarrollo de las organizaciones. Sin embargo, hacer InnerSource no es simplemente copiar y pegar esas prácticas. Hay que adaptarlas a la cultura y la organización interna propias de cada empresa. Veamos con más detalle qué es y qué no es InnerSource, y cuáles son los retos asociados.

## ¿Qué es InnerSource?

El término fue utilizado por primera vez por Tim O'Reilly en 2000, al afirmar que Innersourcing es "[...] *el uso de técnicas de desarrollo de código abierto dentro de la empresa.*"

Según [InnerSource Commons](https://innersourcecommons.org/learn/books/innersource-patterns/), la fundación de referencia sobre el tema, InnerSource es "*uso de principios y prácticas de código abierto para el desarrollo de software dentro de los confines de una organización.*"

## ¿Por qué InnerSource?

Sin embargo, según [InnerSource Commons](https://innersourcecommons.org/learn/books/innersource-patterns/), "*para las empresas que construyen principalmente software de código cerrado, InnerSource puede ser una gran herramienta para ayudar a romper los silos, fomentar y ampliar la colaboración interna, acelerar la incorporación de nuevos ingenieros e identificar oportunidades para contribuir con software al mundo del código abierto*"

Es interesante observar que las ventajas de InnerSource pueden repercutir en diversas funciones de una empresa, no sólo en la ingeniería. Como resultado, algunas empresas han encontrado ventajas concretas en áreas como:

* Funciones jurídicas: acelerar el establecimiento de colaboraciones interfuncionales mediante el uso de un marco jurídico listo para usar (licencia InnerSource).
* Recursos humanos: gestión de competencias escasas a través de un equipo central experimentado responsable de aunar esfuerzos y conocimientos.

## La polémica del InnerSource

InnerSource está rodeado de mitos que uno puede oír de sus detractores. Aunque no es verdadero código abierto, muestra grandes beneficios potenciales para las organizaciones que despliegan este enfoque internamente. He aquí algunos de estos mitos:

* [MITO] InnerSource se hace a expensas del código abierto (principalmente saliente):
   * Los proyectos de software permanecen detrás del cortafuegos de la empresa.
   * Menos contribuciones externas al código abierto.
* [MITO] Secuestrar el espíritu del código abierto frente a acercarse a él.
* [MITO] Ningún proyecto de InnerSource se ha convertido nunca en un proyecto de código abierto.
* [MITO] El motivo para hacer InnerSource es que es similar al código abierto. Pero en realidad, si un desarrollador lo valora, siempre debería preferirse una contribución directa de código abierto.

He aquí algunos datos sobre la práctica de InnerSource que desmienten la mayoría de los mitos anteriores:

* [HECHO] InnerSource es una forma de dar la bienvenida al código abierto a empresas principalmente cerradas.
* [HECHO] Aunque la mayoría de las contribuciones al código abierto las realizan voluntarios, podemos publicitar la participación en el código abierto a los ingenieros utilizando esta lista de "beneficios percibidos".
* [HECHO] En algunos casos (¿o en la mayoría?), las empresas no siguen una práctica de desarrollo ordenada y controlada, y esto (GGI) puede ser una forma de ayudarles a gestionarlo.
* [HECHO] Aún será necesario * mucho * de trabajo en la conversión de licencias cerradas a abiertas.
* [HECHO] Efectivamente, hay casos de proyectos InnerSource de código abierto:
   * Twitter Bootstrap.
   * Kubernetes de Google.
   * Docker de dotCloud (nombre anterior de Docker Inc.).
   * React Native.
* [HECHO] El código abierto se beneficia del aumento de ingenieros de software que se familiarizan con las prácticas del código abierto, ya que las de InnerSource son muy similares.

## ¿Quién lo hace?

Muchas empresas han puesto en marcha iniciativas InnerSource o ISPO (InnerSource Program Office), algunas desde hace tiempo, otras más recientemente. He aquí una lista no exhaustiva, centrada principalmente en empresas europeas:

* Banco de Santander ([fuente](https://patterns.innersourcecommons.org/p/innersource-portal))
* BBC ([fuente](https://www.youtube.com/watch?v=pEGMxe6xz-0))
* Bosch ([fuente](https://web.archive.org/web/20230429145619/https://www.bosch.com/research/know-how/open-and-inner-source/))
* Comcast ([fuente](https://www.youtube.com/watch?v=msD-8-yrGfs&t=6s))
* Ericsson ([fuente](https://innersourcecommons.org/learn/books/adopting-innersource-principles-and-case-studies/))
* Engie ([fuente](https://github.com/customer-stories/engie))
* IBM ([fuente](https://resources.github.com/innersource/fundamentals/))
* Mercedes ([fuente](https://www.youtube.com/watch?v=hVcGABbmI4Y))
* Microsoft ([fuente](https://www.youtube.com/watch?v=eZdx5MQCLA4))
* Nike ([fuente](https://www.youtube.com/watch?v=srPG-Tq0HIs&list=PLq-odUc2x7i-A0sOgr-5JJUs5wkgdiXuR&index=46))
* Nokia ([fuente](https://www.nokia.com/thought-leadership/articles/openness/openness-drives-innovation/))
* SNCF Connect & Tech ([fuente 1](https://twitter.com/FrancoisN0/status/1645356213712846853), [fuente 2](https://www.slideshare.net/FrancoisN0/opensource-innersource-pour-acclrer-les-dveloppements))
* Paypal ([fuente](https://innersourcecommons.org/fr/learn/books/getting-started-with-innersource/))
* Philips([fuente](https://medium.com/philips-technology-blog/how-philips-used-innersource-to-maintain-its-edge-in-innovation-38c481e6fa03))
* Renault ([fuente](https://www.youtube.com/watch?v=aCbv46TfanA))
* SAP ([fuente](https://community.sap.com/topics/open-source/publications))
* Siemens([fuente](https://jfrog.com/blog/creating-an-inner-source-hub-at-siemens))
* Société Générale([fuente](https://github.com/customer-stories/societe-generale))
* Thales ([fuente](https://www.youtube.com/watch?v=aCbv46TfanA))
* VeePee([fuente](https://about.gitlab.com/customers/veepee))

## InnerSource Commons, una referencia esencial

En [InnerSource Commons](https://innersourcecommons.org) se puede encontrar una comunidad activa y vibrante de practicantes de InnerSource, que trabaja según los principios del código abierto. Proporcionan un montón de recursos útiles para ponerse al día en la materia, incluyendo [patrones](https://innersourcecommons.org/learn/patterns/), una [ruta de aprendizaje](https://innersourcecommons.org/learn/learning-path/) y pequeños libros electrónicos:

* [Comenzando con InnerSource](https://innersourcecommons.org/learn/books/getting-started-with-innersource/) por Andy Oram.
* [Comprensión de la lista de verificación de InnerSource](https://innersourcecommons.org/learn/books/understanding-the-innersource-checklist/) por Silona Bonewald.

## Diferencias en la gestión de InnerSource

InnerSource trae desafíos específicos que no se enfrentan en Open Source. Sin embargo, la mayoría de las organizaciones que crean software privativo ya se están ocupando de ellos:

* Licencia dedicada y específica de la empresa para proyectos Innersource (para grandes empresas con múltiples entidades jurídicas).
* La naturaleza pública del código abierto lo salva de los desafíos de los precios de transferencia. La naturaleza privada de InnerSource expone a las empresas que operan en diferentes jurisdicciones a responsabilidades por transferencia de beneficios.
* Las motivaciones para contribuir son muy diferentes:
   * InnerSource tiene un grupo más reducido de posibles contribuyentes porque se limita a la organización.
   * Mostrar las propias habilidades profesionales es un motor para contribuir. InnerSource limita este impacto únicamente a los límites de la organización.
   * Contribuir a mejorar la sociedad es otro de los motores de las contribuciones limitadas en InnerSource.
   * Por lo tanto, la motivación requiere un mayor esfuerzo y se basará más en recompensas y tareas.
   * Afrontar los miedos al perfeccionismo, como el síndrome del impostor, es más fácil en InnerSource debido a la visibilidad limitada del código.
* La externalización de la mano de obra es más frecuente, lo que afecta a la gobernanza de varias maneras.
* Evaluar la adecuación de la empresa es más fácil para InnerSource porque se desarrolla internamente.
* La facilidad de búsqueda tiende a convertirse en un problema. Indexar la información es menos prioritario para las empresas. Los motores de búsqueda públicos como DuckDuckGo, Google o Bing hacen un trabajo mucho mejor que InnerSource no puede aprovechar.
* InnerSource está en una posición ligeramente mejor para controlar la exportación, ya que vive dentro de la empresa.
* Se necesita un control de los límites de la IP que se filtra como código fuente.

InnerSource sigue evolucionando a medida que más empresas adoptan sus principios y comparten sus experiencias. En una versión posterior de este manual se ofrecerá una lista de las actividades del GGI relevantes para los profesionales de InnerSource.
