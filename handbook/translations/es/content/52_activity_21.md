## Gestionar el cumplimiento de la legislación

ID de la Actividad: [GGI-A-21](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_21.md).

### Descripción

Las organizaciones necesitan implementar un proceso de cumplimiento legal para asegurar su uso y participación en proyectos de código abierto.

Una gestión madura y profesional del cumplimento legal, en la organización y en toda la cadena de suministro, abarca:

- Realizar un análisis exhaustivo de la propiedad intelectual que incluya la identificación de licencias y su comprobación de compatibilidad.
- Asegurar que la organización puede utilizar, integrar, modificar y redistribuir componentes de código abierto como parte de sus productos o servicios con seguridad.
- Proporcionar a los empleados propios y subcontratados un proceso transparente para crear y contribuir al software de código abierto.

*Análisis de composición de software (SCA)*: una parte importante de los problemas legales y de propiedad intelectual se derivan del uso de componentes publicados bajo licencias que son incompatibles entre ellos o incompatibles con la forma en que la organización desea usar y redistribuir los componentes. SCA es el primer paso para resolver esos problemas, ya que "necesita conocer el problema para solucionarlo". El proceso consiste en identificar todos los componentes involucrados en un proyecto en una Lista de Materiales, incluidas las dependencias de construcción y pruebas.

*Comprobación de licencias*: Un proceso de comprobación de licencias utiliza una herramienta para analizar automáticamente la base de código e identificar las licencias y los derechos de autor que contiene. Si se ejecuta con regularidad, e idealmente se integra en cadenas de construcción e integración continuas, permite detectar problemas de propiedad intelectual en una fase temprana.

### Evaluación de oportunidades

Con el creciente uso de OSS en los sistemas de información de las organizaciones, es esencial evaluar y gestionar la posible exposición legal.

Sin embargo, comprobar las licencias y los derechos de autor puede ser difícil y costoso. Los desarrolladores necesitan ser capaces de comprobar rápidamente las preguntas IP y legales. Contar con un equipo y un responsable corporativo dedicado a cuestiones legales y de propiedad intelectual garantiza una gestión proactiva y coherente de las cuestiones jurídicas, ayuda a asegurar las contribuciones y el uso de los componentes de código abierto y proporciona una visión estratégica clara.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] Hay un proceso de verificación de licencias fácil de usar disponible para proyectos.
- [ ] Hay un proceso fácil de usar de comprobación de la propiedad intelectual disponible para proyectos.
- [ ] Existe un equipo o persona responsable del cumplimiento legal dentro de la organización.
- [ ] Se programan auditorías periódicas para evaluar el cumplimiento legal.

Otras formas de establecer puntos de verificación:

- [ ] Existe un proceso de verificación de licencias fácil de usar.
- [ ] Hay un equipo jurídico/IP fácil de usar.
- [ ] Todos los proyectos proporcionan la información necesaria para que las personas utilicen y contribuyan al proyecto.
- [ ] Hay un contacto en el equipo para preguntas relativas a la propiedad intelectual y el licenciamiento.
- [ ] Hay un funcionario corporativo dedicado a la propiedad intelectual y el licenciamiento.
- [ ] Hay un equipo dedicado a cuestiones relativas a la propiedad intelectual y al licenciamiento.

### Herramientas

- [ScanCode](https://scancode-toolkit.readthedocs.io)
- [Fossology](https://www.fossology.org/)
- [SW360](https://www.eclipse.org/sw360/)
- [Fossa](https://github.com/fossas/fossa-cli)
- [OSS Review Toolkit](https://oss-review-toolkit.org)

### Recomendaciones

- Informar al personal sobre los riesgos asociados al licenciamiento conflictivo con los objetivos empresariales.
- Proponga una solución sencilla para que los proyectos establezcan la comprobación de licencias en su base de código.
- Comunique su importancia y ayude a los proyectos a agregarlo a sus sistemas de integración continua.
- Proporcione una plantilla o directrices oficiales para la estructura del proyecto.
- Establezca comprobaciones automáticas para asegurarse de que todos los proyectos cumplen las directrices.
- Considere realizar una auditoría interna para identificar las licencias de la infraestructura de la empresa.
- Proporcione formación básica sobre propiedad intelectual y licencias al menos a una persona por equipo.
- Proporcione formación completa sobre propiedad intelectual y licencias al funcionario corporativo.
- Establezca un proceso para escalar al funcionario corporativo los asuntos relacionados con la propiedad intelectual y las licencias.

Recuerde que el cumplimiento no es sólo una cuestión jurídica, sino también de propiedad intelectual. Así que aquí van algunas cuestiones para ayudar a entender las consecuencias del cumplimiento legal:

- Si distribuyo un componente de código abierto y no respeto las condiciones de la licencia, infrinjo la licencia --> implicaciones legales.
- Si utilizo un componente de código abierto dentro de un proyecto que deseo distribuir/publicar, esa licencia puede obligar a tener visibilidad sobre elementos de código que no deseo abrir --> Impacto de confidencialidad para la ventaja táctica de mi empresa y con terceros (implicaciones legales).
- Es un debate abierto si el uso de una licencia de código abierto para un proyecto que quiero publicar otorga PI relevante --> implicaciones de PI.
- Si hago que un proyecto sea de código abierto *antes* de cualquier proceso de patente, eso *probablemente* excluye la creación de patentes relativas al proyecto --> implicaciones de PI.
- Si hago que un proyecto sea de código abierto *después* de cualquier proceso de patente, eso *probablemente* permita la creación de patentes (defensivas) relativas a ese proyecto --> potencial de PI.
- En proyectos complejos que incorporan muchos componentes con muchas dependencias, la multitud de licencias de código abierto puede presentar incompatibilidades entre licencias --> implicaciones legales (cf. actividad GGI-A-23 - Gestionar las dependencias de software).

### Recursos

- Existe una amplia lista de herramientas en la [página existente del grupo de cumplimiento de OSS](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence-Compliance-Tools/).
- [Prácticas de cumplimiento de código abierto recomendadas para la empresa](https://www.ibrahimatlinux.com/wp-content/uploads/2022/01/recommended-oss-compliance-practices.pdf). Un libro de Ibrahim Haddad, de la Fundación Linux, sobre prácticas de cumplimiento de código abierto para la empresa. [Proyecto OpenChain](https://www.openchainproject.org/)

### Próximas actividades propuestas

- [GGI-A-24 - Gestión de indicadores clave](https://ospo-alliance.org/ggi/activities/manage_key_indicators) Haga visibles y medibles las preocupaciones, los procesos y los resultados relacionados con el cumplimiento de la legislación. Esto ayudará a las personas a darse cuenta de su importancia en una fase más temprana del proceso.
