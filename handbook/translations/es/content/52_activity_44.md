## Revisión del código

ID de la Actividad: [GGI-A-44](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_44.md).

### Descripción

La revisión del código es una tarea rutinaria que implica la revisión manual y/o automatizada del código fuente de una aplicación antes de lanzar un producto o entregar un proyecto al cliente. En el caso del software de código abierto, la revisión del código es algo más que detectar errores de forma oportunista: es un enfoque integrado del desarrollo colaborativo llevado a cabo a nivel de equipo.

Las revisiones del código deben aplicarse tanto al código desarrollado internamente como al reutilizado de fuentes externas, ya que mejora la confianza general en el código y refuerza la propiedad. También es una forma excelente de mejorar las habilidades y conocimientos globales dentro del equipo y fomentar la colaboración en equipo.

### Evaluación de oportunidades

Las revisiones del código son valiosas siempre que la organización desarrolle software o reutilice piezas de software externas. Aunque se trata de un paso estándar en el proceso de ingeniería de software, las revisiones de código en el contexto del código abierto aportan ventajas específicas como:

- Al publicar código fuente interno, compruebe que se respetan las directrices de calidad adecuadas.
- Cuando contribuya a un proyecto de código abierto existente, compruebe que se respetan las directrices del proyecto en cuestión.
- La documentación disponible al público se actualiza en consecuencia.

También es una excelente oportunidad para compartir y hacer cumplir algunas de las normas de la política de cumplimiento legal de su empresa, como:

- No elimine nunca las cabeceras de licencia o derechos de autor existentes en el código fuente abierto reutilizado.
- No copies ni pegues código fuente de Stack Overflow sin permiso previo del equipo legal.
- Incluya la línea de copyright correcta cuando sea necesario.

Las revisiones del código aportarán confianza al código. Si las personas no están seguras de la calidad o de los riesgos potenciales de utilizar un producto de software, deben realizar revisiones del código y entre iguales.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] La revisión del código fuente abierto se reconoce como un paso necesario.
- [ ] Se planifican revisiones del código fuente abierto (de forma periódica o en momentos críticos).
- [ ] Se ha definido y aceptado colectivamente un proceso para realizar revisiones del código fuente abierto.
- [ ] Las revisiones de código abierto son una parte habitual del proceso de desarrollo.

### Recomendaciones

- La revisión del código es una tarea colectiva que funciona mejor en un buen entorno de colaboración.
- No dude en utilizar las herramientas y patrones existentes en el mundo del código abierto, donde las revisiones de código son un estándar desde hace años (décadas).

### Recursos

- [What is Code Review?](https://openpracticelibrary.com/practice/code-review/): una lectura didáctica sobre la revisión de código que se encuentra en la Open Practice Library de Red Hat.
- [Best Practices for Code Reviews](https://www.perforce.com/blog/qac/9-best-practices-for-code-review): otra perspectiva interesante sobre lo que es la revisión de código.

### Próximas actividades propuestas

- [GGI-A-26 - Contribuir a proyectos de código abierto](https://ospo-alliance.org/ggi/activities/contribute_to_open_source_projects) La revisión del código es una práctica común en los proyectos de código abierto, ya que mejora la calidad del código y el intercambio de conocimientos. Los colaboradores que realizan revisiones del código suelen sentirse más cómodos con las contribuciones externas y la colaboración.
