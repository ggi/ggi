## Hacer público el uso del código abierto

ID de la actividad: [GGI-A-31](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_31.md).

### Descripción

Esta actividad consiste en reconocer el uso de OSS en un sistema de información, en aplicaciones y en nuevos productos.

- Proporcionar historias de éxito.
- Presentaciones en eventos.
- Financiación de la participación en actos.

### Evaluación de oportunidades

En la actualidad se acepta generalmente que la mayoría de los sistemas de información funcionan con OSS, y que las nuevas aplicaciones se hacen en su mayor parte reutilizando OSS.

El principal beneficio de esta actividad es crear igualdad de condiciones entre el OSS y el software privativo, para asegurarse de que se presta la misma atención al OSS y se gestiona con la misma profesionalidad que el software privativo.

Un beneficio secundario es que contribuye en gran medida a elevar el perfil del ecosistema de OSS y, dado que los usuarios de OSS son identificados como "innovadores", también aumenta el atractivo de la organización.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta actividad:

- [ ] Los proveedores comerciales de código abierto tienen autorización para utilizar el nombre de la organización como referencia del cliente.
- [ ] Los colaboradores pueden hacerlo y expresarse bajo el nombre de la organización.
- [ ] El uso de OSS se menciona abiertamente en el informe anual del departamento de TI.
- [ ] No hay ningún obstáculo para que la organización explique su uso del OSS en los medios de comunicación (entrevistas, eventos sobre OSS y el sector, etc.).

### Recomendaciones

- El objetivo de esta actividad no es que la organización se convierta en un organismo de activismo de OSS, sino asegurarse de que no haya ningún obstáculo para que el público reconozca su uso de OSS.

### Recursos

- Ejemplo del [CERN](https://superuser.openstack.org/articles/cern-openstack-update/) afirmando públicamente su uso de OpenStack
