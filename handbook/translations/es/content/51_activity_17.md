## Inventario de competencias y recursos de código abierto

ID de la Actividad: [GGI-A-17](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_17.md).

### Descripción

En cualquier etapa, desde el punto de vista de la gestión, es útil tener un mapa, un inventario de los recursos de código abierto, los activos, el uso y su estado, así como las necesidades potenciales y las soluciones disponibles. También incluye la evaluación del esfuerzo y las habilidades necesarias para cubrir la brecha.

Esta actividad tiene como objetivo tomar una instantánea de la situación del código abierto dentro de la organización y en el mercado y evaluar el puente entre ambos.

- Inventario del uso de OSS en la cadena de desarrollo de software, así como en los productos y componentes de software utilizados en la producción.
- Identifique las tecnologías de código abierto (soluciones, marcos de trabajo, características innovadoras) que podrían ajustarse a sus necesidades y ayudar a mejorar su proceso.

No se incluye

- Identificar y calificar los ecosistemas y comunidades de OSS relacionados. (Objetivo Cultura)
- Identificar las dependencias de las bibliotecas y componentes de OSS. (Objetivo Confianza)
- Identificar las habilidades técnicas (por ejemplo, lenguajes, marcos..) y blandas (por ejemplo, colaboración, comunicación) necesarias. (pertenece a las siguientes actividades: Crecimiento de la competencia de OSS y habilidades de desarrollo de software de código abierto)

### Evaluación de oportunidades

Un inventario de los recursos de código abierto disponibles que ayudará a optimizar la inversión y a priorizar el desarrollo de competencias.

Esta actividad crea las condiciones para mejorar la productividad del desarrollo, dada la eficacia y la popularidad de los componentes, los principios de desarrollo y las herramientas de OSS, especialmente en el desarrollo de aplicaciones e infraestructuras modernas.

- Esto puede requerir la simplificación de la cartera de recursos de OSS.
- Esto puede requerir una nueva formación del personal.
- Esto permite identificar las necesidades y alimenta su hoja de ruta informática.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] Existe una lista viable de recursos de OSS que "utilizamos", "integramos", "producimos", "alojamos" y las habilidades relacionadas
- [ ] Estamos en el camino de mejorar la eficiencia utilizando métodos y herramientas de última generación.
- [ ] Hemos identificado recursos de OSS no contabilizados hasta ahora (que pueden haberse colado, y ¿tenemos elementos para definir la política en este ámbito?)
- [ ] Pedimos a los nuevos proyectos que respalden o reutilicen los recursos de OSS existentes.
- [ ] Tenemos una percepción y una comprensión razonablemente seguras del alcance del uso del OSS en nuestra organización.

### Herramientas

Hay muchas formas diferentes de establecer dicho inventario. Una forma sería clasificar los recursos de OSS en cuatro categorías:

- OSS que utilizamos: software que utilizamos en producción o en desarrollo
- OSS que integramos: por ejemplo, bibliotecas de OSS que integramos en una aplicación a medida
- OSS que producimos: por ejemplo, una biblioteca que hemos publicado en GitHub o un proyecto de OSS que desarrollamos o al que contribuimos regularmente.
- OSS que alojamos: OSS que ejecutamos para ofrecer un servicio interno como un CRM, GitLab, nexus, etc. Una tabla de ejemplo sería como la siguiente:

| Utilizamos | Integramos | Producimos | Alojamos | Habilidades |
| --- | --- | --- | --- | --- |
| Firefox, <br />OpenOffice, <br />Postgresql | Biblioteca slf4j | Biblioteca YY en GH | GitLab, <br />Nexus | Java, <br />Python |

La misma identificación debe aplicarse a las competencias

- Competencias y experiencias disponibles a través de los equipos existentes
- Habilidades y experiencias que podrían desarrollarse o adquirirse internamente (formación, entrenamiento, experimentación)
- Competencias y experiencias que deben buscarse en el mercado o a través de la asociación/contratación

### Recomendaciones

- Mantenga las cosas simples.
- Se trata de un ejercicio de relativo alto nivel, no de un inventario detallado para el departamento de contabilidad.
- Si bien esta actividad es un buen punto de partida, no es necesario completarla al 100% antes de iniciar otras actividades.
- Manejar asuntos, recursos y habilidades relacionadas con el **desarrollo de software** en la actividad #42.
- El inventario debe abarcar todas las categorías de TI: sistemas operativos, middlewares, DBMS, administración de sistemas, herramientas de desarrollo y pruebas, etc.
- Empiece a identificar a las comunidades afines: es más fácil conseguir apoyo y comentarios del proyecto cuando ya le conocen.

### Recursos

- Un curso excelente sobre [Software (/Libre), y de Código Abierto (FOSS)](https://nythesis.com/open-courses/free-and-open-source-software/), por el profesor Dirk Riehle.

### Próximas actividades propuestas

- [GGI-A-18 - Crecimiento de la competencia en código abierto](https://ospo-alliance.org/ggi/activities/open_source_competency_growth) La identificación de habilidades y recursos de código abierto permite a la organización empezar a consolidar y reforzar su concienciación y competencia.
- [GGI-A-19 - Supervisión del código abierto](https://ospo-alliance.org/ggi/activities/open_source_supervision) Una vez completado el inventario de software de código abierto y de competencias, se puede empezar a controlar y gestionar el uso de OSS dentro de la organización.
- [GGI-A-28 - Perspectiva de los recursos humanos](https://ospo-alliance.org/ggi/activities/human_resources_perspective) El departamento de Recursos Humanos puede elaborar planes, contratos y procesos de desarrollo proporcionados y adecuados basándose en el inventario elaborado en esta actividad.
- [GGI-A-33 - Comprometerse con los proveedores de código abierto](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) Antes de definir una relación externa con un proveedor, uno debe conocer su software de código abierto y sus competencias.
- [GGI-A-42 - Gestionar habilidades y recursos de código abierto](https://ospo-alliance.org/ggi/activities/manage_open_source_skills_and_resources) Una vez completado el inventario de activos y habilidades de código abierto, se puede empezar a gestionarlos adecuadamente, aprovechando los recursos internos existentes.
