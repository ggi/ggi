## Participar en proyectos de código abierto

ID de la actividad: [GGI-A-29](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_29.md).

### Descripción

Esta actividad consiste en comprometerse a realizar contribuciones significativas a algunos proyectos de OSS que sean importantes para usted. Las contribuciones se escalan y se comprometen a nivel de organización (no a nivel personal como en nº 26). Pueden adoptar diversas formas, desde la financiación directa hasta la asignación de recursos (por ejemplo, personas, servidores, infraestructura, comunicación, etc.), siempre que beneficien al proyecto o ecosistema de forma sostenible y eficiente.

Esta actividad es una continuación de la actividad nº 26 y lleva las contribuciones de los proyectos de código abierto al nivel de la organización, haciéndolas más visibles, potentes y beneficiosas. En esta actividad, se supone que las contribuciones aportan una mejora sustancial y a largo plazo al proyecto de OSS: por ejemplo, un desarrollador o equipo que desarrolla una nueva característica muy deseada, activos de infraestructura, servidores para un nuevo servicio, asunción del mantenimiento de una rama muy utilizada.

La idea es destinar un porcentaje de los recursos a patrocinar a desarrolladores de código abierto que escriben y mantienen bibliotecas o proyectos que utilizamos.

Esta actividad implica tener un mapa del software de código abierto utilizado, y una evaluación de su criticidad para decidir cuál apoyar.

### Evaluación de oportunidades

> Si cada empresa que utiliza código abierto contribuyera al menos un poco, tendríamos un ecosistema sano. <https://news.ycombinator.com/item?id=25432248>

Apoyar proyectos ayuda a garantizar su sostenibilidad y facilita el acceso a la información, e incluso puede ayudar a influir y priorizar algunos desarrollos (aunque ésta no debería ser la razón principal para apoyar proyectos).

Posibles beneficios de esta actividad: asegurar que se da prioridad a los informes de defecto y que los desarrollos se integran en la versión estable. Posibles costes asociados a la actividad: dedicar tiempo y/o dinero a los proyectos.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta actividad:

- [ ] Proyecto beneficiario identificado.
- [ ] Opción de apoyo decidida, como por ejemplo contribución monetaria directa o contribución de código.
- [ ] Se ha nombrado al responsable de la tarea.
- [ ] Ha habido alguna contribución.
- [ ] Se ha evaluado el resultado de la contribución.

Puntos de verificación tomados del cuestionario de OpenChain [autocertificación](https://certification.openchainproject.org/):

- [ ] Tenemos una política de contribución a proyectos de código abierto en nombre de la organización.
- [ ] Disponemos de un procedimiento documentado que regula las contribuciones al código abierto.
- [ ] Disponemos de un procedimiento documentado para que todo el personal de software conozca la política de contribución al código abierto.

### Herramientas

Algunas organizaciones ofrecen mecanismos para financiar proyectos de código abierto (podría ser conveniente si su proyecto objetivo está en sus carteras).

- [Colectivo Abierto](https://opencollective.com/).
- [Software Freedom Conservancy](https://sfconservancy.org/).
- [Tidelift](https://tidelift.com/).

### Recomendaciones

- Concéntrese en los proyectos críticos para la organización: son los proyectos a los que más desea ayudar con sus contribuciones.
- Elija proyectos comunitarios.
- Esta actividad requiere una familiaridad mínima con un proyecto objetivo.

### Recursos

- [Cómo apoyar ahora proyectos de código abierto](https://sourceforge.net/blog/support-open-source-projects-now/): Una breve página con ideas para financiar proyectos de código abierto.
- [Sustain OSS: un espacio de debate sobre el mantenimiento del código abierto](https://sustainoss.org)

### Próximas actividades propuestas

- [GGI-A-26 - Contribuir a proyectos de código abierto](https://ospo-alliance.org/ggi/activities/contribute_to_open_source_vendors) La forma más natural de comprometerse con una iniciativa de código abierto es contribuir directamente al proyecto. A cambio, recibirás valiosos comentarios sobre tus aportaciones.
- [GGI-A-30 - Apoyo a las comunidades de código abierto](https://ospo-alliance.org/ggi/activities/support_open_source_communities) Hay muchas formas de apoyar las iniciativas de código abierto que son esenciales para su organización. Comprometerse con las comunidades es una buena forma de descubrirlas y fomentarlas.
