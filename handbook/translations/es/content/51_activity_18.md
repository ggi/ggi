## Aumento de la competencia de código abierto

ID de la actividad: [GGI-A-18](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_18.md).

### Descripción

Esta actividad consiste en planificar e iniciar las habilidades técnicas y la experiencia inicial con OSS una vez que se ha realizado un inventario (nº 17). También es la oportunidad de empezar a establecer una hoja de ruta de desarrollo de habilidades básicas y ligeras.

- Determinar cuáles son las habilidades y la formación necesarias.
- Establezca un proyecto piloto para poner en marcha el enfoque, aprenda de la práctica y fije un primer hito.
- Aprovechar la experiencia adquirida y crear un corpus de conocimientos.
- Empezar a identificando y documentando los próximos pasos para una adopción más amplia.
- Elaborar una estrategia en los próximos meses o un año para conseguir apoyo financiero y de gestión.

El alcance de la actividad:

- Linux, Apache, Debian, habilidades de administración.
- Bases de datos de código abierto MariaDB, MySQL, PostgreSQL, etc.
- Tecnologías de virtualización y nube de código abierto.
- Pila LAMP y sus alternativas.

### Evaluación de oportunidades

Como cualquier tecnología informática, y probablemente aún más, el código abierto aporta innovación. El código abierto crece rápido y cambia con rapidez. Requiere que las organizaciones se mantengan al día.

Esta actividad ayuda a identificar las áreas en las que la formación podría ayudar a las personas a ser más eficientes y sentirse más seguras utilizando el código abierto. Ayuda a tomar decisiones sobre el desarrollo de los empleados. Sembrar habilidades básicas de código abierto permite evaluar la oportunidad de:

- Ampliar las soluciones informáticas con tecnologías de mercado existentes desarrolladas por el ecosistema.
- Desarrollar nuevas formas de colaboración dentro y fuera de la organización.
- Adquirir competencias en tecnologías nuevas e innovadoras.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta actividad:

- [ ] Se elabora una matriz de competencias.
- [ ] El alcance de las tecnologías de OSS utilizadas se define de forma proactiva, es decir, se evita el uso incontrolado de tecnologías de OSS.
- [ ] Se adquiere un nivel satisfactorio de pericia para estas tecnologías.
- [ ] Los equipos han recibido una formación de "fundamentos del código abierto" para empezar.

### Herramientas

Una herramienta clave en este sentido es la denominada matriz (o mapa) de actividades (o competencias).

Esta actividad puede ser realizada por:

- utilizando tutoriales en línea (muchos gratuitos en Internet),
- participando en congresos de desarrolladores,
- recibir formación de proveedores, etc.

### Recomendaciones

- Utilizar y desarrollar componentes de código abierto de forma segura y eficiente requiere una mentalidad abierta y colaborativa que debe reconocerse y propagarse tanto desde arriba (gerencia) como desde abajo (desarrolladores).
- Asegúrese de que la dirección apoya y promueve activamente el enfoque. No pasará nada si no hay compromiso por parte de la jerarquía.
- Implique a la gente (desarrolladores, partes interesadas) en el proceso: organizar mesas redondas y escuchar ideas.
- Conceda tiempo y recursos para que la gente descubra, pruebe y juegue con estos nuevos conceptos. Si es posible, que sea divertido: la gamificación y las recompensas son buenos incentivos.

Un proyecto piloto con los siguientes pasos podría servir de catalizador:

- Identificar la tecnología o el marco de trabajo con el que empezar.
- Encuentre formación en línea, tutoriales y ejemplos de código para experimentar.
- Construir un prototipo de la solución final.
- Identificar a algunos expertos a quienes desafiar y entrenar en la implementación.

### Recursos

- [Qué es una matriz de competencias](https://blog.kenjo.io/what-is-a-competency-matrix): una rápida lectura introductoria.
- [Cómo hacer una matriz de competencias para su equipo](http://www.managersresourcehandbook.com/download/Skills-Matrix-Template.pdf): una plantilla con comentarios.
- [MOOC sobre cultura libre](https://librecours.net/parcours/upload-lc000/) (sólo en francés): se trata de un curso de 6 partes sobre cultura libre, introducción a los derechos de autor, propiedad intelectual y licencias de código abierto

### Próximas actividades propuestas

- [GGI-A-28 - Perspectiva de los recursos humanos](https://ospo-alliance.org/ggi/activities/human_resources_perspective) Planificar cuidadosamente el conjunto de habilidades internas y el crecimiento de las competencias disponibles forma parte de la perspectiva de los recursos humanos.
