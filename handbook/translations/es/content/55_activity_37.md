## El código abierto al servicio de la transformación digital

Identificativo de la actividad: [GGI-A-37](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_37.md).

### Descripción

> «La Transformación Digital es la adopción de tecnología digital, para transformar servicios, o empresas, mediante la sustitución de procesos no digitales, o manuales, por procesos digitales, o la sustitución de tecnología digital antigua, por tecnología digital reciente.» (Wikipedia)

Cuando las organizaciones más avanzadas en Transformación Digital impulsan conjuntamente el cambio, a través de su negocio, TI, y finanzas, para anclar lo digital por el camino, reconsideran su:

- Modelo de negocio: cadena de valor con ecosistemas, como servicio, SaaS.
- Finanzas: opex/capex, personal, externalización.
- TI: innovación, modernización de legados/activos.

El código abierto está en el centro de la transformación digital:

- Tecnologías, prácticas ágiles, gestión de productos.
- Personas: colaboración, comunicación abierta, ciclo de desarrollo/decisión.
- Modelos de negocio: probar antes de comprar, innovación abierta.

En términos de competitividad, los procesos más visibles son, probablemente, los, que repercuten directamente en la experiencia del cliente. Tenemos, que reconocer, que los grandes actores, así como las empresas emergentes, al ofrecer una experiencia al cliente totalmente sin precedentes, han cambiado, drásticamente, las expectativas de los ellos.

La experiencia del cliente, así como todos los demás procesos de una empresa, dependen, totalmente, de las TI. Todas las empresas tienen que transformar sus TI, y de eso trata la transformación digital. Las empresas que aún no lo han hecho, tienen ahora que lograr su transformación digital, lo más rápido posible, de contrario corren el riesgo de ser eliminadas del mercado. La transformación digital es una condición para la supervivencia. Dado que es mucho lo que está en juego, una empresa no puede dejar por completo la transformación digital en manos de un proveedor. Toda empresa tiene que ponerse manos a la obra con sus TI, lo que significa, que toda empresa tiene que ponerse manos a la obra con el software de código abierto porque no hay TI sin software de código abierto.

En los beneficios esperados de la transformación digital se incluye:

- Simplificar, automatizar los procesos centrales, convertirlos en procesos en tiempo real.
- Permitir respuestas rápidas, a los cambios de la competencia.
- Sacad partido de la inteligencia artificial, y los macrodatos.

### Evaluación de oportunidades

La transformación digital podría gestionarse por:

- Segmentos de la TI: TI de producción, TI de apoyo al negocio (gestión de relaciones con el cliente, facturación, compras…), TI de apoyo (recursos humanos, finanzas, contabilidad…), Macrodatos.
- Tipo de tecnología o proceso que soporta la TI: Infraestructura (nube), Inteligencia Artificial, Procesos (Make-or-Buy, DevSecOps, SaaS).

Inyectar código abierto en un segmento o tecnología concretos de vuestra TI revela que queréis entrar de lleno en este segmento o tecnología, porque habéis evaluado, que este segmento, o tecnología concretos de vuestra TI son importantes para la competitividad de vuestra empresa. Es importante evaluar la posición de la empresa en comparación, no sólo, con las de los competidores, sino también con otras industrias, y actores clave en términos de experiencia del cliente, y soluciones de mercado.

### Evaluación del progreso


- [ ] Nivel 1: Evaluación de la situación

  He identificado:
   - los segmentos de TI, que son importantes para la competitividad de mi empresa, y
   - las tecnologías de código abierto necesarias para desarrollar aplicaciones en estos segmentos.
   
   Y así he decidido:
   - sobre qué segmentos quiero gestionar internamente el desarrollo de proyectos, y
   - en qué tecnologías de código abierto necesito adquirir experiencia interna.

- [ ] Nivel 2: Participación

   En algunas tecnologías de código abierto utilizadas en la empresa, varios desarrolladores han recibido formación, y son reconocidos como valiosos colaboradores, por la comunidad de código abierto. En algunos segmentos determinados, se han puesto en marcha proyectos basados en tecnologías de código abierto.

- [ ] Nivel 3: Generalización

   En todos los proyectos, se estudia, sistemáticamente, una alternativa de código abierto, durante la fase inicial del proyecto. Para facilitar al equipo del proyecto el estudio de dicha alternativa de código abierto, se dedica un presupuesto central, y un equipo central de arquitectos, alojados en el Departamento de TI, a prestar asistencia a los proyectos.

**KPIs**:

- KPI 1. Cociente para el que se investigó una alternativa de código abierto: (Número de proyectos / Número total de proyectos).
- KPI 2. Cociente por el que se eligió la alternativa de código abierto: (Número de proyectos / Número total de proyectos).

### Recomendaciones

Más allá del titular, la Transformación Digital es una mentalidad, que implica algunos cambios fundamentales, y esto también (o incluso principalmente) deberá provenir de las capas superiores de la organización. La dirección debe promover iniciativas, nuevas ideas, gestionar los riesgos, y, potencialmente, actualizar los procedimientos existentes, para adaptarlos a los nuevos conceptos.

La pasión es un gran factor de éxito. Uno de los medios desarrollados por los principales actores en este campo es la creación de espacios abiertos a nuevas ideas, donde la gente pueda presentar, y trabajar libremente sus ideas sobre la transformación digital. La dirección debe fomentar este tipo de iniciativas.

### Recursos

- [Fundación Eclipse: Habilitar la transformación digital en Europa mediante la colaboración global del código abierto](https://outreach.eclipse.foundation/hubfs/EuropeanOpenSourceWhitePaper-June2021.pdf).
- [Europe: Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).
- [Europe: Open source software strategy 2020-2023](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
