## Comprometerse con los proveedores de código abierto

ID de la actividad: [GGI-A-33](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_33.md).

### Descripción

Asegúrese contratos con proveedores de software de código abierto que proporcionen software crítico para usted. Las empresas y entidades que producen software de código abierto necesitan prosperar para proporcionar mantenimiento y desarrollo de nuevas funciones. Su experiencia específica es necesaria en el proyecto, y la comunidad de usuarios confía en su actividad y contribuciones continuas.

La colaboración con proveedores de código abierto adopta varias formas:

- Suscripción de planes de ayuda.
- Contratación de empresas locales de servicios.
- Desarrollos patrocinadores.
- Pagar por una licencia comercial.

Esta actividad implica considerar los proyectos de código abierto como productos completos por los que merece la pena pagar, al igual que por cualquier producto privativo, aunque normalmente mucho menos caro.

### Evaluación de oportunidades

El objetivo de esta actividad es garantizar un apoyo profesional al software de código abierto utilizado en la organización. Tiene varias ventajas:

- Continuidad del servicio mediante la oportuna corrección de defectos.
- Rendimiento del servicio gracias a una instalación optimizada.
- Aclaración de la situación legal/comercial del software utilizado.
- Acceso a información temprana.
- Previsión presupuestaria estable.

El coste es obviamente el de los planes de soporte seleccionados. Otro coste podría ser dejar de subcontratar en masa a grandes integradores de sistemas en favor de una contratación detallada con PYME expertas.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta actividad:

- [ ] El código abierto utilizado en la organización cuenta con soporte comercial.
- [ ] Se han contratado planes de soporte para algunos proyectos de código abierto.
- [ ] El coste de los planes de soporte de código abierto es una partida legítima en el presupuesto de TI.

### Recomendaciones

- Siempre que sea posible, busque PYME locales expertas.
- Cuidado con los grandes integradores de sistemas que revenden conocimientos de terceros (revenden planes de asistencia que en realidad ofrecen PYMES expertas en código abierto).

### Recursos

Un par de enlaces que ilustran la realidad comercial del software de fuente abierta:

- [Una lectura rápida para entender el código abierto comercial](https://www.webiny.com/blog/what-is-commercial-open-source).
