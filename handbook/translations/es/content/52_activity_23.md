## Gestionar las dependencias del software

ID de la Actividad: [GGI-A-23](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_23.md).

### Descripción

Un programa *identificador de dependencias* busca las dependencias utilizadas realmente en la base de código. Como resultado, la organización debe establecer y mantener una lista de dependencias conocidas de su base de código y observar la evolución de los proveedores identificados.

Establecer y mantener una lista de dependencias conocidas es un elemento facilitador y un requisito previo:

- Control de propiedad intelectual y licencias: algunas licencias no pueden mezclarse, ni siquiera como dependencia. Uno debe conocer sus dependencias para evaluar sus riesgos jurídicos asociados.
- Gestión de vulnerabilidades: todo el software es tan débil como su parte más débil: véase el ejemplo del [fallo Heartbleed](https://en.wikipedia.org/wiki/Heartbleed). Hay que conocer sus dependencias para evaluar los riesgos de seguridad asociados.
- Ciclo de vida y sostenibilidad: una comunidad activa en el proyecto de dependencia es un buen indicio de cara a la corrección de errores, optimizaciones y nuevas funciones.
- Selección meditada de las dependencias utilizadas, según criterios de "madurez": el objetivo es utilizar componentes de código abierto que sean seguros, con una base de código saneada y bien mantenida, y una comunidad viva, activa y reactiva que acepte contribuciones externas, etc.

### Evaluación de oportunidades

La identificación y el seguimiento de las dependencias es un paso necesario para mitigar los riesgos asociados a cualquier reutilización de código. Además, implantar herramientas y procesos para gestionar las dependencias de software es un requisito previo para gestionar adecuadamente la calidad, el cumplimiento y la seguridad.

Considere las siguientes preguntas:

- ¿Cuál es el riesgo de la empresa (coste, reputación, etc.) si el software se corrompe o sufre ataques técnicos o legales?
- ¿La base de código se considera crítica para las personas, la organización o el negocio?
- ¿Y si un componente del que depende una aplicación cambia su repositorio?

El primer paso mínimo es implementar una herramienta de análisis de composición de software (SCA). Quizá se requiera el apoyo de consultoras especializadas para un SCA o mapeo de dependencias completo.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta actividad:

- [ ] Se identifican las dependencias de todo el código desarrollado internamente.
- [ ] Se identifican las dependencias de todo el código externo ejecutado en la empresa.
- [ ] Existe un procedimiento de análisis de composición de software o de identificación de dependencias fácil de implementar para que los proyectos lo añadan a su proceso de integración continua.
- [ ] Se utilizan herramientas de análisis de dependencias.

### Herramientas

- [OWASP Dependency check](https://github.com/jeremylong/DependencyCheck): dependency-Check es una herramienta de análisis de composición de software (SCA) que intenta detectar vulnerabilidades de dominio público contenidas en las dependencias de un proyecto.
- [OSS Review Toolkit](https://oss-review-toolkit.org/): un conjunto de herramientas para ayudar a revisar las dependencias del software de código abierto.
- [Fossa](https://github.com/fossas/fossa-cli): análisis de dependencias rápido, portable y fiable. Soporta escaneo de licencias y vulnerabilidades. Independiente del lenguaje; se integra con más de 20 sistemas de compilación.
- [Software 360](https://projects.eclipse.org/projects/technology.sw360).
- [Herramienta de licencia Eclipse Dash](https://github.com/eclipse/dash-licenses): toma una lista de dependencias y solicita a [Clearly Defined](https://clearlydefined.io) que compruebe sus licencias.
- [Proyecto FOSSology](https://www.fossology.org/): FOSSology es un proyecto de código abierto cuya misión es avanzar en el cumplimiento de las licencias de código abierto.

### Recomendaciones

- Realice auditorías periódicas sobre las dependencias y los requisitos de propiedad intelectual para mitigar los riesgos jurídicos.
- Lo ideal es integrar la gestión de dependencias en el proceso de integración continua para que los problemas (nueva dependencia, incompatibilidad de licencia) se identifiquen y solucionen lo antes posible.
- Realice un seguimiento de las vulnerabilidades relacionadas con las dependencias, mantenga informados a los usuarios y a los desarrolladores.
- Informe a las personas sobre los riesgos asociados con un licenciamiento incorrecto.
- Proponga una solución sencilla para que los proyectos establezcan la comprobación de licencias en su base de código.
- Comunique su importancia y ayude a los proyectos a agregarlo a sus sistemas de integración continua.
- Establezca un KPI visible para los riesgos relacionados con dependencias.

### Recursos

- Página existente del grupo [Herramientas para el cumplimiento de licencias OSS](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence-Compliance-Tools/).
- [Cumplimiento de licencias de software libre y de código abierto: Herramientas para el análisis de la composición del software](https://www.computer.org/csdl/magazine/co/2020/10/09206429/1npxG2VFQSk), por Philippe Ombredanne, nexB Inc.
- [Modelo de madurez de la sostenibilidad del software](http://oss-watch.ac.uk/resources/ssmm).
- [CHAOS](https://chaoss.community/): Community Health Analytics Open Source Software.

### Próximas actividades propuestas

- [GGI-A-21 - Gestionar el cumplimiento legal](https://ospo-alliance.org/ggi/activities/manage_legal_compliance) Antes de poder hacer un seguimiento de las incompatibilidades de la propiedad intelectual y las licencias, es necesario identificar todas las dependencias de su software de código abierto.
- [GGI-A-22 - Gestionar las vulnerabilidades del software](https://ospo-alliance.org/ggi/activities/manage_software_vulnerabilities/) Antes de ser capaz de rastrear las vulnerabilidades en sus activos de código, uno necesita identificar todas las dependencias en su software de código abierto.
