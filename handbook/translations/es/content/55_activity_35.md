## Fuente abierta y soberanía digital

ID de la Actividad: [GGI-A-35](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_35.md).

### Descripción

La soberanía digital puede definirse como

> “La capacidad y la oportunidad de individuos e instituciones para ejercer su papel en el mundo digital de forma independiente, intencionada y segura.” & mdash; Centro de Competencia para las TIC Públicas, Alemania

Para gobernar adecuadamente su negocio, cualquier entidad tiene que depender de otros socios, servicios, productos y herramientas. La revisión de los vínculos y limitaciones de estas dependencias permite a la organización evaluar y controlar su dependencia de factores externos, mejorando así su autonomía y resiliencia.

Como ejemplo, el cautiverio respecto a un proveedor es un factor fuerte de dependencia que puede obstaculizar los procesos de la organización y el valor añadido y, como tal, debe evitarse. La fuente abierta es una de las maneras de salir de este bloqueo. La fuente abierta desempeña un papel importante en la soberanía digital, permitiendo una mayor elección entre soluciones, proveedores e integradores, y un mayor control sobre las hojas de ruta de TI.

Cabe señalar que la soberanía digital no es cuestión de confianza: obviamente necesitamos confiar en nuestros socios y proveedores, pero la relación mejora si se basa en el consentimiento mutuo y el reconocimiento, en lugar de en contratos forzados y restricciones.

He aquí algunas ventajas de una mejor soberanía digital:

- Mejorar la capacidad de la organización para tomar sus propias decisiones sin limitaciones.
- Mejorar la resiliencia de la empresa con respecto a los actores y factores externos.
- Mejorar la posición de negociación al tratar con socios y proveedores de servicios.

### Evaluación de oportunidades

- ¿Cuánto cuesta (dificultad y coste) abandonar una solución?
- ¿Podrían los proveedores de soluciones imponer condiciones no deseadas a su servicio (por ejemplo, cambio de licencia, actualizaciones de contratos)?
- ¿Podrían los proveedores de soluciones incrementar unilateralmente sus precios, simplemente porque no tenemos elección?

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] Hay una evaluación de dependencias críticas respecto a proveedores y socios de la organización.
- [ ] Hay una contingencia para estas dependencias identificadas.
- [ ] Existe un requisito expreso de soberanía digital cuando se investigan nuevas soluciones.

### Recomendaciones

- Identificar los principales riesgos de dependencia de proveedores de servicios y terceros.
- Mantener una lista de alternativas de código abierto a los servicios críticos.
- Añadir un requisito al seleccionar nuevas herramientas y servicios a utilizar dentro de la entidad, indicando la necesidad de soberanía digital.

### Recursos

- [A Primer on Digital Sovereignty & Open Source: part I](https://www.opensourcerers.org/2021/08/09/a-promer-on-digital-sovereignty/) y [A Primer on Digital Sovereignty & Open Source: part II](https://www.opensourcerers.org/2021/08/16/a-primer-on-digital-sovereignty-open-source/), del sitio web Open-Sourcerers .
- Un excelente artículo de superuser.openstack.org sobre [El papel de la fuente abierta en la soberanía digital](https://superuser.openstack.org/articles/the-role-of-open-source-in-digital-sovereignty-openinfra-live-recap/). He aquí un extracto corto:
   > La Soberanía Digital es una preocupación clave para el siglo XXI, especialmente para Europa. La fuente abierta tiene un papel importante que desempeñar para habilitar la soberanía digital, permitiendo a todos acceder a la tecnología necesaria, pero también proporcionando la transparencia de gobierno e interoperabilidad necesarias para que las soluciones tengan éxito.
- La postura de la Unión Europea sobre soberanía digital, del [Observador de Fuentes Abiertas (OSOR)](https://joinup.ec.europa.eu/collection/open-source-observatory-osor): Fuente abierta, soberanía digital e interoperabilidad: La Declaración de Berlín.
- La postura de UNICEF's sobre la [Soberanía Digital de Fuentes Abiertas](https://www.unicef.org/innovation/stories/open-source-digital-sovereignty).
