## Gestionar los indicadores clave

ID de la actividad: [GGI-A-24](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_24.md).

### Descripción

Esta actividad recopila y supervisa un conjunto de indicadores que informan las decisiones de gestión cotidiana y las opciones estratégicas relativas al software de código abierto gestionado profesionalmente.

Las métricas clave relacionadas con el software de código abierto conforman el contexto para valorar la aplicación de los programas de gobernanza. La actividad abarca la selección de algunos indicadores, la publicación para los equipos y la gestión, y el envío periódico de actualizaciones sobre la iniciativa, por ejemplo mediante un boletín informativo o noticias corporativas.

Esta actividad requiere:

- partes interesadas para debatir y definir los objetivos del programa,
- la implantación de una herramienta de medición y recopilación de datos conectada a la infraestructura de desarrollo,
- la publicación de al menos un cuadro de mando para las partes interesadas y para todas las personas involucradas en la iniciativa.

Los indicadores se basan en datos que deben recopilarse de fuentes pertinentes. Afortunadamente, hay muchas fuentes para la ingeniería de software de código abierto. Por ejemplo:

- el entorno de desarrollo, la cadena CI/CD de producción,
- el departamento de RRHH,
- las herramientas de pruebas y de análisis de composición de software,
- los repositorios.

Algunos ejemplos de indicadores son:

- Número de dependencias resueltas, mostradas por tipo de licencia.
- Número de dependencias obsoletas/vulnerables.
- Número de incidencias con licencias o propiedad intelectual detectadas.
- Contribuciones a proyectos externos.
- Vida de los defectos.
- Número de colaboradores en un componente, número de commits, etc.

Esta actividad consiste en definir estos requisitos y necesidades de medición, e implantar un cuadro de mando que muestre de forma sencilla y eficaz los principales indicadores del programa.

### Evaluación de oportunidades

Los indicadores clave ayudan a comprender y gestionar mejor los recursos dedicados al software de código abierto, y miden los resultados con el fin de comunicar de manera efectiva y obtener los beneficios completos de la inversión. Al divulgarse más personas pueden seguir la iniciativa y se sentirán implicadas, lo que lo convierte en una preocupación y un objetivo a nivel de organización.

Si bien cada actividad tiene criterios de evaluación que ayudan a responder a las preguntas sobre los progresos logrados, todavía es necesario supervisar los números e indicadores cuantitativos.

Ya sea en una pequeña startup o una gran empresa global, las métricas clave ayudan a mantener los equipos enfocados y supervisar el rendimiento. Las métricas son cruciales porque apoyan la toma de decisiones y son la base para supervisar las decisiones ya adoptadas.

Con números y gráficos simples y prácticos, todos los miembros de la organización podrán seguir y sincronizar los esfuerzos en materia de código abierto, lo que lo hará una preocupación y acción compartidas. Esto también facilita a diversos actores su incorporación, contribuir al proyecto y obtener los beneficios generales.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta actividad:

- [ ] Se ha establecido una lista de métricas y cómo recogerlas.
- [ ] Se utilizan herramientas para recoger, almacenar, procesar y mostrar indicadores.
- [ ] Existe un cuadro de mando general a disposición de todos los participantes que muestra el progreso realizados en la iniciativa.

### Herramientas

- [GrimoireLab](https://chaoss.github.io/grimoirelab) de Bitergia.
- Las herramientas genéricas de BI (elasticsearch, grafana, visualizaciones R/Python...) también son buenas opciones, cuando los conectores adecuados se configuran de acuerdo con los objetivos definidos.

### Recomendaciones

- Anote los objetivos y la hoja de ruta del gobierno del código abierto.
- Comunicar internamente las acciones y el estado de la iniciativa.
- Involucrar al personal en la definición de los indicadores clave, para asegurar que
   - se entienden bien,
   - proporcionan una visión completa de las necesidades y
   - se tengan en cuenta y se sigan.
- Construya al menos un cuadro de mando que pueda mostrarse a todo el mundo (por ejemplo, en una pantalla en la sala), con indicadores esenciales que muestren el progreso y la situación general.

### Recursos

- La [comunidad CHAOSS](https://chaoss.community/) tiene muchas buenas referencias y recursos relacionados con los indicadores de código abierto.
- Consulte las métricas para [Atributos del proyecto](https://www.ow2.org/view/MRL/Stage2-ProjectAttributes) de la [metodología](https://www.ow2.org/view/MRL/Overview) de Niveles de Preparación del Mercado de OW2.
- [Una nueva forma de medir la apertura: el índice de gobernanza abierta](https://timreview.ca/article/512) de Liz Laffan es una lectura interesante sobre la apertura en proyectos de código abierto.
- [Indicadores de Gobernanza: Una Guía de Usuario](https://anfrel.org/wp-content/uploads/2012/02/2007_UNDP_goveranceindicators.pdf) es la guía de la ONU sobre indicadores de gobernanza. Aunque se aplica a la democracia, la corrupción y la transparencia de las naciones, vale la pena leer los conceptos básicos de medición e indicadores aplicados a la gobernanza.

### Próximas actividades propuestas

- [GGI-A-37 - El código abierto al servicio de la transformación digital](https://ospo-alliance.org/ggi/activities/open_source_enabling_digital_transformation) Utilice las métricas producidas como parte de su estrategia general de código abierto.
