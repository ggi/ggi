## Gestionar las vulnerabilidades del software

ID de la actividad: [GGI-A-44](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_22.md).

### Descripción

Nuestro código es tan seguro como su parte menos segura. Casos recientes (por ejemplo, heartbleed[^heartbleed], equifax[^equifax]) han demostrado la importancia de verificar vulnerabilidades en partes del código que no desarrolla directamente la entidad. Las consecuencias de las exposiciones van desde fugas de datos (con un tremendo impacto en la reputación) hasta ataques de ransomware e indisponibilidad de servicios que amenaza el negocio.

Se sabe que el software de código abierto tiene una gestión de vulnerabilidades mejor que el software privativo, principalmente porque:

- Hay más ojos buscando problemas y soluciones en código y procesos abiertos.
- Los proyectos de código abierto corrigen vulnerabilidades y publican parches y versiones nuevas mucho más rápidamente.

Por ejemplo, un [estudio de WhiteSource](https://resources.whitesourcesoftware.com/blog-whitesource/3-reasons-why-open-source-is-safer-than-commercial-software) sobre software privativo mostró que el 95% de las vulnerabilidades encontradas en sus componentes de código abierto ya habían publicado una solución en el momento del análisis. La cuestión, por tanto, es **gestionar mejor las vulnerabilidades tanto en el código base como en sus dependencias**, independientemente de que sean de código cerrado o abierto.

Para mitigar estos riesgos, hay que establecer un programa de evaluación de sus activos de software y un proceso de verificación de vulnerabilidad ejecutado con regularidad. Implemente herramientas que alertan a los equipos afectados, gestione vulnerabilidades conocidas y evite amenazas derivadas de las dependencias del software.

### Evaluación de oportunidades

Cualquier empresa que use software tiene que vigilar sus vulnerabilidades en:

- su infraestructura (por ejemplo, infraestructura en la nube, infraestructura de red, almacenes de datos),
- sus aplicaciones empresariales (RRHH, herramientas CRM, gestión de datos internos y relacionados con los clientes),
- su código interno: por ejemplo, el sitio web de la empresa, proyectos de desarrollo interno, etc,
- y todas las dependencias directas e indirectas de software y servicios.

El ROI de las vulnerabilidades es poco conocido hasta que ocurre algo malo. Hay que tener en cuenta las consecuencias de una violación de datos importante o de la indisponibilidad de servicios para estimar el verdadero coste de las vulnerabilidades.

Del mismo modo, se debe evitar a toda costa una cultura de secreto y ocultamiento por cuestiones relacionadas con la seguridad dentro de la empresa. En cambio, la información sobre el estado de la vulnerabilidad debe compartirse y debatirse para encontrar las mejores respuestas por parte de las personas adecuadas, desde desarrolladores hasta altos ejecutivos.

Los beneficios de prevenir ciberataques gestionando cuidadosamente las vulnerabilidades del software son múltiples:

- Evite los riesgos reputacionales,
- Evite mermas en la explotación (DDoS, Ransomware, tiempo para reconstruir un sistema informático alternativo tras un ataque)),
- Cumpla con la normativa de protección de datos.

Gestionar vulnerabilidades de software OSS es sólo una parte del proceso de ciberseguridad más amplio que aborda globalmente la seguridad de los sistemas y servicios en la organización.

### Evaluación del progreso

Debe haber una persona o un equipo dedicado a supervisar las vulnerabilidades y procesos fáciles de usar en los que puedan confiar los desarrolladores. La evaluación de vulnerabilidades es una parte estándar del proceso de integración continua, y las personas pueden supervisar el estado actual del riesgo en un panel de control específico.

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] La actividad queda cubierta cuando se evalúan y supervisan todos los programas y servicios internos buscando vulnerabilidades conocidas.
- [ ] La actividad queda cubierta cuando se implantan una herramienta y un proceso dedicados en la cadena de producción de software para evitar la introducción de problemas en las rutinas diarias de desarrollo.
- [ ] Una persona o equipo es responsable de evaluar el riesgo de la CVE/vulnerabilidad frente a la exposición.
- [ ] Una persona o equipo se encarga de enviar la CVE/vulnerabilidad a los afectados (SysOps, DevOps, desarrolladores, etc.).

### Herramientas

- Herramientas de GitHub
   - GitHub proporciona pautas y herramientas para proteger el código alojado en la plataforma. Consulte [GitHub docs](https://docs.github.com/en/github/administering-a-repository/about-securing-your-repository) para obtener más información.
   - GitHub proporciona [Dependabot](https://docs.github.com/en/github/managing-security-vulnerabilities/about-alerts-for-vulnerable-dependencies) para identificar vulnerabilidades en dependencias automáticamente.
- [Eclipse Steady](https://eclipse.github.io/steady/) es una herramienta gratuita de código abierto que analiza los proyectos Java y Python buscando vulnerabilidades y ayuda a los desarrolladores a mitigarlos.
- [OWASP dependency-check](https://owasp.org/www-project-dependency-check/): un escáner de vulnerabilidades de código abierto.
- [OSS Review Toolkit](https://github.com/oss-review-toolkit/ort): un orquestador de código abierto capaz de recopilar avisos de seguridad para dependencias usadas desde servicios de datos de vulnerabilidad configurados.

### Recursos

- La [base de datos de CVEs de vulnerabilidades de MITRE](https://cve.mitre.org/) . Consulte también la [Base de datos de NVDs de seguridad del NIST](https://nvd.nist.gov/) , y recursos satélite como [CVE Details](https://www.cvedetails.com/).
- Consulte también esta nueva iniciativa de Google: las [vulnerabilidades de código abierto](https://osv.dev/).
- El grupo de trabajo OWASP publica una lista de escáneres de vulnerabilidades [en su sitio web](https://owasp.org/www-community/Vulnerability_Scanning_Tools), tanto del mundo comercial como de código abierto.
- J. Williams y A. Dabirsiaghi. La desafortunada realidad de las bibliotecas inseguras, 2012.
- [Detección, evaluación y mitigación de vulnerabilidades en dependencias de código abierto](https://link.springer.com/article/10.1007/s10664-020-09830-x), Serena Elisa Ponta, Henrik Plate & Antonino Sabetta, Ingeniería Empírica de Software volumen 25, páginas 3175–3215 (2020).
- [Un conjunto de datos curado manualmente de correcciones a vulnerabilidades de software de código abierto](https://arxiv.org/abs/1902.02595), Serena E. Ponta, Henrik Plate, Antonino Sabetta, Michele Bezzi, Cédric Dangremont. También hay un [kit de herramientas en desarrollo para implementar el conjunto de datos mencionado](https://sap.github.io/project-kb/).

### Próximas actividades propuestas

- [GGI-A-24 - Gestión de indicadores clave](https://ospo-alliance.org/ggi/activities/manage_key_indicators) Haga visibles las vulnerabilidades identificadas. Esto ayudará a la gente a darse cuenta de lo seguro o no que es su software, y demostrará la importancia de seleccionar las dependencias adecuadas.

[^heartbleed]: https://www.wikipedia.org/wiki/Heartbleed
[^equifax]: https://arstechnica.com/information-technology/2017/09/massive-equifax-breach-caused-by-failure-to-patch-two-month-old-bug/
