## Cambio en origen

ID de la Actividad: [GGI-A-39](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_39.md).

### Descripción

Esta actividad se ocupa de crear conciencia con respecto a los beneficios de contribuir y hacer cumplir el primer principio de cambio en origen.

Con el enfoque de cambios en origen, todo desarrollo en un proyecto de código abierto se hará con el nivel de calidad y apertura requerido para presentarlo a su núcleo de desarrolladores y que lo publiquen.

### Evaluación de oportunidades

Escribir código pensando en el origen produce:

- código de mejor calidad,
- código que está listo para presentarlo en origen,
- código que se integra con el software original,
- código que será compatible con la versión futura,
- reconocimiento por parte de la comunidad del proyecto y una cooperación mejor y más rentable.

> Cambios en Origen es más que "ser amable". Significa que Ud. tiene una opinión en el proyecto. Significa predictibilidad. Significa que Ud. tiene el control. Significa que Ud. actúa en lugar de reaccionar. Significa que Ud. entiende la fuente abierta.
>
> &mdash; <cite>[Maximilian Michels](https://maximilianmichels.com/2021/upstream-first/)</cite>


### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta actividad: ¿Se implementan los cambios en origen?

- [ ] Incremento significativo del número de solicitudes de integración presentadas a proyectos de terceros.
- [ ] Se ha esbozado una lista de proyectos de terceros para los que se deben aplicar los cambios en origen.

### Recomendaciones

- Identificar a los desarrolladores más expertos interactuando con sus homólogos originales.
- Facilitar que los desarrolladores interactúen con sus homólogos originales (eventos, hackatones, etc.)

### Recursos

- Una explicación clara del principio de Cambios en Origen y por qué se corresponde con el objetivo de Cultura: <https://maximilianmichels.com/2021/upstream-first/>.

> Cambios en Origen significa que cada vez que Ud. resuelve un problema en su copia del código original de la que otros podrían beneficiarse, contribuye estos cambios al origen. Es decir, solicita su integración con el repositorio original.

- [¿Qué significa "aguas arriba" o "aguas abajo" en un contexto de desarrollo de software?](https://reflectoring.io/upstream-downstream/) Una explicación cristalina.
- Explicado en los documentos de diseño del sistema operativo Chromium: [Upstream First](https://www.chromium.org/chromium-os/chromiumos-design-docs/upstream-first).
- Red Hat acerca del origen y los beneficios de [cambiar en origen](https://www.redhat.com/en/blog/ what-open-source-upstream).

### Próximas actividades propuestas

- [GGI-A-25 - Promover las buenas prácticas en el desarrollo del código abierto](https://ospo-alliance.org/ggi/activities/promote_open_source_development_best_practices) Contribuir a las fuentes es una de las mejores prácticas del código abierto. Inclúyala también en las buenas prácticas de la organización, ya que contribuirá a las aportaciones externas, a la calidad general interna y al intercambio de conocimientos.
