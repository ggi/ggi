## Pertenecer a la comunidad de código abierto

ID de la Actividad: [GGI-A-27](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_27.md).

### Descripción

Esta actividad trata de desarrollar entre los desarrolladores un sentimiento de pertenencia a la gran comunidad del código abierto. Como en toda comunidad, las personas y entidades tienen que participar y contribuir al conjunto. Refuerza los vínculos entre los profesionales y aporta sostenibilidad y actividad al ecosistema. Desde un punto de vista más técnico, permite elegir las prioridades y la hoja de ruta de los proyectos, mejora el nivel de conocimientos generales y la conciencia técnica.

Esta actividad abarca lo siguiente:

- **Identifique eventos** a los que merezca la pena asistir. Conectar a la gente, aprender sobre nuevas tecnologías y crear una red son factores clave para obtener todos los beneficios del código abierto.
- Considere la posibilidad de **afiliarse a fundaciones** . Las fundaciones y organizaciones son un componente clave del ecosistema del código abierto. Proporcionan recursos técnicos y organizativos a los proyectos, y son un buen lugar neutral para que los patrocinadores debatan problemas y soluciones comunes, o elaboren estándares.
- Siga a **grupos de trabajo**. Los grupos de trabajo son espacios neutrales de trabajo colaborativo donde los expertos interactúan en torno a un dominio específico como IoT, modelado o ciencia. Son un mecanismo muy eficiente para abordar conjuntamente las preocupaciones comunes y específicas de cada dominio.
- **Participación presupuestaria**. Al final del camino, el dinero es el facilitador. Planifique los gastos necesarios, conceda a las personas tiempo remunerado para estas actividades, prevea los próximos movimientos, para que el programa no tenga que detenerse tras unos meses con estrés de caja.

### Evaluación de oportunidades

El código abierto funciona mejor cuando se hace en relación con la comunidad de código abierto en general. Facilita la corrección de errores, el intercambio de soluciones, etc.

También es una buena forma de que las empresas muestren su apoyo a los valores del código abierto. Comunicar la implicación de la empresa es importante tanto para su reputación como para el ecosistema del código abierto.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] Se esboza una lista de actos a los que se podría asistir.
- [ ] Hay un seguimiento de las charlas públicas que dan los miembros del equipo.
- [ ] Las personas pueden presentar solicitudes de participación en eventos.
- [ ] La gente puede presentar proyectos para su patrocinio.

### Recomendaciones

- Encueste a la gente para saber qué actos les gustan o serían más beneficiosos para su trabajo.
- Establezca comunicaciones internas (boletín, centro de recursos, invitaciones...) para que la gente conozca las iniciativas y pueda participar.
- Asegúrese de que estas iniciativas puedan beneficiar a varios tipos de personas (desarrolladores, administradores, soporte...), no sólo a los altos ejecutivos.

### Recursos

- [¿Qué motiva a un desarrollador a contribuir al software de código abierto?](https://clearcode.cc/blog/why-developers-contribute-open-source-software/) Un artículo de Michael Sweeney en clearcode.cc.
- [Por qué las empresas contribuyen al código abierto](https://blogs.vmware.com/opensource/2020/12/01/why-companies-contribute-to-open-source/) Un artículo de Velichka Atanasova de VMWare.
- [Por qué sus empleados deberían contribuir al código abierto](https://www.cloudbees.com/blog/why-your-employees-should-be-contributing-to-open-source/) Una buena lectura de Robert Kowalski de CloudBees.
- [7 maneras en que su empresa puede apoyar el código abierto](https://www.infoworld.com/article/2612259/7-ways-your-company-can-support-open-source.html) Un artículo de Simon Phipps para InfoWorld.
- [Eventos: la fuerza vital del código abierto](https://www.redhat.com/en/blog/events-life-force-open-source) Un artículo de Donna Benjamin de RedHat.

### Próximas actividades propuestas

- [GGI-A-28 - Perspectiva de RRHH](https://ospo-alliance.org/ggi/activities/human_resources_perspective) Si la organización pertenece a la comunidad OSS, es más fácil atraer a personas cualificadas, en función de la comunidad en la que se participe.
- [GGI-A-31 - Afirmar públicamente el uso del código abierto](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Ahora que ya formas parte de la comunidad del OSS, ¡dalo a conocer! Es bueno para tu reputación, y es bueno para el proyecto en términos de salud y difusión.
