## Apoyar a las comunidades de código abierto

ID de la actividad: [GGI-A-30](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_30.md).

### Descripción

Esta actividad consiste en relacionarse con representantes institucionales del mundo del código abierto.

Se consigue mediante:

- Afiliación a fundaciones de OSS (incluido el coste económico de la afiliación).
- Apoyando y abogando por las actividades de las fundaciones.

Esta actividad implica asignar a los equipos de desarrollo y TI algo de tiempo y presupuesto para participar en comunidades de código abierto.

### Evaluación de oportunidades

Las comunidades de código abierto están a la vanguardia de la evolución del ecosistema del código abierto. Comprometerse con las comunidades de código abierto tiene varias ventajas:

- ayuda a mantenerse informado y al día,
- mejora el perfil de la organización,
- la afiliación conlleva ventajas,
- proporciona estructura y motivación adicionales al equipo informático de código abierto.

Los costes incluyen:

- cuotas de afiliación,
- tiempo del personal y algún presupuesto para viajes asignado para participar en actividades comunitarias,
- seguimiento del compromiso de propiedad intelectual.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] La organización es miembro signatario de una fundación de código abierto.
- [ ] La organización participa en la gobernanza.
- [ ] El software desarrollado por la organización se envía / se ha añadido a la base de código de una fundación.
- [ ] La afiliación se reconoce en los sitios web tanto de la organización como de la comunidad.
- [ ] Evaluación de costes y beneficios de la afiliación.
- [ ] Se ha designado un punto de contacto para la comunidad.

### Recomendaciones

- Únase a una comunidad compatible con su tamaño y sus recursos, es decir, una comunidad que pueda oír su voz y en la que pueda ser un colaborador reconocido.

### Recursos

- Consulte esta [página útil](https://www.linuxfoundation.org/tools/participating-in-open-source-communities/) de la Fundación Linux sobre por qué y cómo unirse a una comunidad de código abierto.

### Próximas actividades propuestas

- [GGI-A-31 - Afirmar públicamente el uso del código abierto](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Ahora que apoyas oficialmente a algunas comunidades de OSS, ¡dalo a conocer! Es bueno para tu reputación, y es bueno para los proyectos en términos de salud y difusión.
