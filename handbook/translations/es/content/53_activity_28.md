## Perspectiva de los recursos humanos

ID de la actividad: [GGI-A-28](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_28.md).

### Descripción

El cambio a la cultura del código abierto tiene profundas repercusiones en los recursos humanos:

- **Nuevos procesos y contratos**: Los contratos tienen que adaptarse para permitir y promover las contribuciones externas. Esto incluye cuestiones de propiedad intelectual y licencias para el trabajo realizado dentro de la empresa, pero también la posibilidad de que el empleado o contratista tenga sus propios proyectos.
- **Diferentes tipos de personas**: Las personas que trabajan con código abierto suelen tener incentivos y mentalidades diferentes a los de las personas puramente propietarias y corporativas. Los procesos y las mentalidades tienen que adaptarse a este paradigma orientado a la reputación de la comunidad, para atraer a nuevos tipos de talento y retenerlos a lo largo del tiempo.
- **Desarrollo profesional**: es necesario ofrecer una trayectoria profesional que fomente y valore las habilidades técnicas y sociales de los empleados, así como las competencias que espera su organización (colaboración para impulsar los esfuerzos de la comunidad, comunicación para actuar como portavoz de su empresa, etc.). Por supuesto, los Recursos Humanos tienen un papel clave a la hora de hacer posible el código abierto como objetivo cultural.

**Fuerza laboral** Para un desarrollador que ha estado trabajando en la misma solución privativa durante mucho tiempo, cambiar a código abierto puede parecer un gran cambio y requiere una adaptación. Pero para la mayoría de los desarrolladores, el software de código abierto solo acarrea beneficios.

Todos los desarrolladores que salen hoy de la escuela o de la universidad han estado siempre trabajando en código abierto. Dentro de una empresa, la gran mayoría de los desarrolladores utilizan lenguajes de código abierto e importan bibliotecas o fragmentos de código abierto todos los días. De hecho, es mucho más fácil pegar líneas de código fuente abierto en un programa que desencadenar el proceso de provisión interna, que escala a través de múltiples aprobaciones a lo largo de la línea de mando.

El código abierto hace que el trabajo del desarrollador sea más interesante, porque con el código abierto, un desarrollador siempre está pendiente de lo que han inventado sus compañeros de fuera de la empresa y en consecuencia se mantiene a la vanguardia de la tecnología.

Para una organización, tiene que haber una estrategia de Recursos Humanos para 1/ cualificar o recualificar a la mano de obra existente 2/ reflejar y posicionar a la empresa en la contratación de nuevos talentos, de ahí cuál es el atractivo de la empresa en lo que respecta al código abierto.

> "Obtener gente con una buena mentalidad FLOSS, que ya entienden el código, y saber cómo trabajar bien con otros es maravilloso. La alternativa de evangelizar / formar / entrenar vale la pena pero es más cara en dinero y tiempo."
>
> &mdash; <cite>Director ejecutivo de un proveedor de software OSS</cite>

Esto ilustra que la contratación de personas con ADN de código abierto es una vía de aceleración a tener en cuenta en la estrategia de los Recursos Humanos.

#### Procesos

- Establecer o revisar las descripciones de los puestos de trabajo (aptitudes técnicas, aptitudes interpersonales, competencias y experiencias)
- Programas de capacitación: autodidacta, formación formal, entrenamiento acompañado en gestión, mapeo de colegas, comunidades
- Establecer o revisar la trayectoria profesional: competencias, resultados/impacto clave y etapas de la carrera

### Evaluación de oportunidades

1. Enmarcar las prácticas de desarrollo: probablemente el problema no sea tanto incitar a los desarrolladores a utilizar más código abierto, sino asegurarse de que lo utilizan de forma segura, respetando las condiciones de licencia de cada tecnología de código abierto y sin renunciar a los controles de seguridad tradicionales (las líneas de código abierto podrían contener código malicioso),
1. Revisar las prácticas de colaboración: con las prácticas de desarrollo, la oportunidad es extender la agilidad y la colaboración a otras líneas de negocio de su organización. A menudo se recurre al InnerSource para fomentar estos comportamientos, aunque esto sea solo la mitad del camino hacia la cultura del código abierto,
1. Cultura de la organización: al final, todo depende de la cultura de su organización: el código abierto puede ser el buque insignia de valores como la apertura, la colaboración, la ética o la sostenibilidad.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] Hay formación disponible para presentar tanto las ventajas como las restricciones (cumplimiento de los términos de licencia de propiedad intelectual) relativas al código abierto.
- [ ] Cada desarrollador, cada arquitecto, cada jefe de proyecto (o responsable de producto/negocio), entiende los beneficios y las restricciones (cumplimiento de los términos de licencia de propiedad intelectual) relativos al código abierto.
- [ ] Se anima a los desarrolladores a contribuir a las comunidades de código abierto y a responsabilizarse de ellas, y podrían recibir la formación adecuada para hacerlo.
- [ ] Las cualificaciones y competencias se reflejan en las descripciones de los puestos de trabajo de la organización y en el escalafón profesional.
- [ ] La experiencia que los desarrolladores adquieren en código abierto (contribuciones a comunidades de código abierto, participación en el proceso de cumplimiento interno, portavoces externos de la empresa, ...) se tiene en cuenta en el proceso de evaluación de los Recursos Humanos.

### Herramientas

- Matriz de competencias.
- Programas de formación pública (ej. escuela de código abierto).
- Abastecimiento: GitHub, GitLab, LinkedIn, Meetups, Epitech, Epita…
- Modelos de contrato (Cláusula de fidelidad).
- Descripciones de puestos (plantillas) y etapas profesionales (plantillas).

### Recomendaciones

Hoy en día, la mayoría de los desarrolladores ya conocen algunos principios del código abierto y están dispuestos a trabajar con y en software de código abierto. Sin embargo, todavía hay algunas medidas que la dirección debería tomar:

- Preferencia por la experiencia en OSS a la hora de contratar, aunque el trabajo para el que se contrata al desarrollador sólo esté relacionado con tecnología privativa. Lo más probable es que, con la transformación digital, el desarrollador tenga que trabajar algún día sobre código abierto.
- Programa de formación en OSS: Cada desarrollador, cada arquitecto, cada jefe de proyecto (o responsable de producto/negocio), debe tener acceso a recursos de formación (vídeos o formación presencial) que presenten las ventajas del código abierto y también las restricciones en términos de cumplimiento de licencias y propiedad intelectual.
- Debe ofrecerse formación a los desarrolladores que quieran contribuir a las comunidades de código abierto y formar parte de los órganos de gobierno de estas comunidades (certificaciones Linux).
- Reconocimiento en los procesos de evaluación personal de los Recursos Humanos de la contribución del empleado (desarrollador o arquitecto) a temas relacionados con el código abierto, como las contribuciones a comunidades de código abierto y el cumplimiento de los términos de licencia de propiedad intelectual. La mayoría de los temas son compartidos y encajan en las trayectorias profesionales técnicas, mientras que algunos podrían o deberían ser específicos.
- El secreto mejor guardado y la postura de la empresa: necesidad de abordar los aspectos de la comunicación (cuán fundamental es esto para su organización que podría reflejarse en su informe anual), cómo afecta su postura de comunicación (un colaborador de código abierto podría ser un portavoz de su empresa, incluidos los contactos de prensa).

### Recursos

- En cuanto a la capacidad de las personas para hablar en actos fuera de la empresa, véase la Actividad 31: "(Objetivo de compromiso) Afirmar públicamente el uso del código abierto".
