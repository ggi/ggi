## Supervisión del código abierto

ID de la Actividad : [GGI-A-19](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_19.md).

### Descripción

Esta actividad consiste en controlar el uso del código abierto y garantizar una gestión proactiva del software de código abierto. Esto afecta a varias perspectivas, ya sea utilizar herramientas y soluciones empresariales de OSS, o incluir OSS como componentes en desarrollos propios o modificar una versión de un software adaptándolo a las propias necesidades, etc. También se trata de identificar las áreas en las que el código abierto se ha convertido en una solución de facto (a veces encubierta) y evaluar su idoneidad.

Puede que sea necesario aclarar lo siguiente:

- ¿Se proporciona la funcionalidad requerida?
- ¿Se proporciona funcionalidad adicional innecesaria pero que aumenta la complejidad de las fases de CONSTRUCCIÓN y EJECUCIÓN?
- ¿Qué exige la licencia? ¿Cuáles son las limitaciones legales?
- ¿En qué medida la decisión hace a su organización independiente de los proveedores?
- ¿Existe una opción de asistencia preparada para las necesidades de su empresa y cuánto cuesta?
- TCO (Coste Total de Propiedad).
- ¿Conoce la dirección las ventajas del código abierto, por ejemplo, más allá del "ahorro de costes de licencia"? Sentirse cómodo con el código abierto ayuda a sacar el máximo provecho de la colaboración con las comunidades y los proveedores de los proyectos .
- Ver si tiene sentido compartir los costes de desarrollo cediendo los desarrollos propios a la comunidad y todas sus implicaciones, como el cumplimiento de las licencias.
- Compruebe la disponibilidad de soporte comunitario o profesional.

### Evaluación de oportunidades

Definir un proceso de decisión específicamente dirigido al código abierto es una forma de maximizar sus beneficios.

- Evita el uso progresivo incontrolado y los costes ocultos de las tecnologías OSS.
- Conduce a decisiones estratégicas y organizativas informadas y conscientes del OSS.

Costes: la actividad puede cuestionar y reconsiderar el uso subóptimo de facto del código abierto por ineficiente, arriesgado, etc.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta actividad:

- [ ] El OSS se ha convertido en una opción cómoda a la hora de seleccionar nuevo software.
- [ ] El OSS no se considera una excepción ni una opción peligrosa.
- [ ] El OSS se ha convertido en una opción "corriente".
- [ ] Los actores clave están suficientemente convencidos de que la solución de código abierto tiene ventajas estratégicas en las que merece la pena invertir.
- [ ] Puede demostrarse que el coste total de propiedad de la solución basada en código abierto aporta a su organización un valor superior al de la alternativa.
- [ ] Se evalúa cómo la independencia de los proveedores ahorra dinero o puede ahorrarlo en el futuro.
- [ ] Se evalúa que la independencia de la solución reduce los riesgos de que sea demasiado costoso cambiar la solución (no es posible utilizar formatos de datos cerrados).

### Herramientas

En este momento, no podemos pensar en ninguna herramienta relevante o afectada por esta actividad.

### Recomendaciones

- Gestionar de forma proactiva el uso del código abierto requiere unos niveles básicos de concienciación y comprensión de los fundamentos del código abierto, ya que deben tenerse en cuenta en cualquier decisión sobre OSS.
- Compare la funcionalidad necesaria en lugar de buscar una alternativa para una solución de código cerrado conocida.
- Asegúrese de contar con apoyo y un desarrollo activo.
- Tenga en cuenta los efectos de la licencia de la solución en su organización.
- Convencer a todos los agentes clave del valor de las ventajas del código abierto, más allá del "ahorro de costes de licencia".
- Sea honesto, no exagere el efecto de la solución de código abierto.
- En el proceso de toma de decisiones es igualmente importante evaluar las distintas soluciones de código abierto para evitar decepciones por expectativas erróneas, dejar claro lo que se pide a la organización y todas las ventajas que aporta la apertura de las soluciones. Esto debe identificarse para que la organización pueda evaluarlo en función de su propio contexto.

### Recursos

- [Las 5 principales ventajas del código abierto](https://www.openlogic.com/blog/top-5-benefits-open-source-software): Blog patrocinado, pero aún así interesante, lectura rápida.
- [Weighing The Hidden Costs Of Open Source](https://www.itjungle.com/2021/02/15/weighing-the-hidden-costs-of-open-source/): un estudio patrocinado por IBM sobre los costes de soporte del OSS.
