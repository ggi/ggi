## Promover las mejores prácticas de desarrollo del código abierto

ID de la Actividad: [GGI-A-25](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_25.md).

### Descripción

Esta actividad consiste en definir, promover y aplicar activamente las mejores prácticas de código abierto dentro de los equipos de desarrollo.

Como punto de partida se podría considerar atender los siguientes temas:

- Documentación de usuario y desarrollador.
- Organización adecuada del proyecto en un repositorio de acceso público.
- Promover y aplicar la reutilización controlada.
- Proporcionar una documentación completa y actualizada del producto.
- Gestión de la configuración: flujos de trabajo git, patrones colaborativos.
- Gestión de lanzamientos: liberación temprana y frecuente, versiones estables frente a versiones en desarrollo, etc.

Los proyectos de la OSS tienen un modus operandi especial que recuerda a un [bazar](http://www.catb.org/~esr/writings/cathedral-bazaar/). Para permitir y fomentar esta colaboración y mentalidad, se recomiendan algunas prácticas que faciliten el desarrollo colaborativo y descentralizado y las contribuciones de desarrolladores externos…

#### Documentos de la comunidad

Asegúrese de que todos los proyectos dentro de la empresa presenten los siguientes documentos:

- README -- descripción rápida del proyecto, cómo interactuar, enlaces a recursos.
- Contributing -- introducción para personas que desean contribuir.
- Código de conducta -- comportamiento considerado aceptable, o no, en la comunidad.
- LICENSE -- la licencia por omisión del repositorio.

#### REUSE -- buenas prácticas para reutilizar

[REUSE](https://reuse.software) es una iniciativa de la [Free Software Foundation Europe](https://fsfe.org/) para mejorar la reutilización del software y simplificar el cumplimiento de las licencias.

### Evaluación de oportunidades

Aunque depende en gran medida del conocimiento común del OSS entre el equipo, capacitar a las personas y crear procesos que hagan cumplir estas prácticas es siempre beneficioso. Es aún más importante cuando:

- se desconocen los posibles usuarios y contribuyentes,
- los desarrolladores no están acostumbrados al desarrollo de código abierto.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] El proyecto establece una lista de buenas prácticas de código abierto que hay que cumplir.
- [ ] El proyecto supervisa su adecuación a las buenas prácticas.
- [ ] El equipo de desarrollo se ha concienciado para cumplir con las buenas prácticas de OSS.
- [ ] Se evalúan periódicamente buenas prácticas nuevas y se procura aplicarlas.

### Herramientas

- La herramienta [REUSE helper tool](https://github.com/fsfe/reuse-tool) ayuda a un repositorio a cumplir las buenas prácticas de [REUSE](https://reuse.software). Puede incluirse en muchos procesos de desarrollo para confirmar el estado actual.
- [ScanCode](https://scancode-toolkit.readthedocs.io) tiene la capacidad de listar todos los documentos comunitarios y legales del repositorio: véase su [descripción de carcaterísticas](https://scancode-toolkit.readthedocs.io/en/latest/cli-reference/scan-options-pre.html#classify).
- GitHub tiene una característica estupenda para [ver los documentos comunitarios ausentes](https://docs.github.com/articles/viewing-your-community-profile). Se puede encontrar en la página del repositorio > "Insights" > "Community".

### Recomendaciones

- La lista de buenas prácticas depende del contexto y dominio del programa y se debe revisar periódicamente al estilo de la mejora continua. Las prácticas deben vigilarse y evaluarse periódicamente para seguir el progreso.
- Formar al personal en reutilización (como consumidores) del OSS y en ecosistemas (como contribuyentes).
- Implementar REUSE.software como en la actividad #14.
- Establecer un proceso para gestionar los riesgos jurídicos asociados con la reutilización y las contribuciones.
- Alentar al personal a contribuir a proyectos externos.
- Proporcione una plantilla o directrices oficiales para la estructura del proyecto.
- Establezca comprobaciones automáticas para asegurarse de que todos los proyectos cumplen las directrices.

### Recursos

- [La lista de buenas prácticas de código abierto de OW2](https://www.ow2.org/view/MRL/Full_List_of_Best_Practices) de la metodología de evaluación de los Niveles de Preparación del mercado.
- [El sitio web oficial de REUSE](https://reuse.software) con especificación, tutorial y preguntas frecuentes.
- [Directrices de la comunidad de GitHub](https://opensource.guide/).
- Un ejemplo de [buenas prácticas de gestión de la configuración utilizando GitHub](https://dev.to/datreeio/top-10-github-best-practices-3kl2).

### Próximas actividades propuestas

- [GGI-A-42 - Gestión de habilidades y recursos de código abierto](https://ospo-alliance.org/ggi/activities/manage_open_source_skills_and_resources) Puede añadir la lista de mejores prácticas de desarrollo de código abierto identificadas al material de formación general.
- [GGI-A-44 - Revisión del código](https://ospo-alliance.org/ggi/activities/run_code_reviews/) La revisión del código es un elemento esencial de las mejores prácticas de desarrollo.
