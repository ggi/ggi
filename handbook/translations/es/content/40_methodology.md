# Metodología

Implementar la metodología de Buen Gobierno de OSS es a la postre una iniciativa con consecuencias e impacto. Implica varias categorías de personas, servicios y procesos de empresa, desde las prácticas cotidianas hasta la gestión de RRHH y desde los desarrolladores hasta el nivel ejecutivo. No hay realmente un mecanismo mágico para implementar el buen gobierno del código abierto. Diferentes tipos de organización, y culturas y circunstancias empresariales requerirán enfoques diferentes del buen gobierno del código abierto. Para cada organización, habrá limitaciones e expectativas distintas, llevando a diferentes rutas y maneras de gestionar el programa.

Con esto en mente, la Iniciativa para el Buen Gobierno proporciona un diseño genérico de actividades que se pueden adaptar al dominio, cultura y requisitos de propios de la organización. Si bien el diseño pretende ser completo la metodología se puede implementar progresivamente. Es posible iniciar el programa simplemente seleccionando los objetivos y actividades más relevantes en cada contexto específico. La idea es construir un primer borrador del libro de ruta para ayudar a establecer la iniciativa local.

Junto con este marco de trabajo también recomendamos encarecidamente contactar con colegas a través de una red bien establecida como la europea [OSPO Alliance](https://ospo-alliance.org), u otras iniciativas semejantes del TODO group, o de OSPO++. Lo que importa es poder intercambiar ideas con personas que lleven una iniciativa similar y compartir los problemas que surgen y las soluciones existentes.

## Preparar el terreno

Dada a ambición de la metodología GGI y su amplio impacto potencial es importante comunicarse con una variedad de personas dentro de una organización. Sería apropiado involucrarlas para establecer un conjunto inicial de expectativas y requisitos realistas para comenzar con buen pié, atraer interés y apoyos. Una buena idea podría ser publicar los cuadros de mando personalizados de actividades en la plataforma colaborativa de la organización para que se puedan utilizar para comunicarse con los afectados. Algunos consejos:

* Identifique a los principales afectados, que acuerden un conjunto de objetivos primarios. Involúcrelos en el éxito de la iniciativa como parte de sus propias agendas.
* Obtenga un compromiso inicial, acuerde las etapas y el ritmo y establezca controles regulares para informarles de los avances.
* Asegúrese de que entiendan los beneficios de lo que se puede lograr y lo que implica: la mejora esperada debe ser clara y el resultado visible.
* Establezca un primer diagnóstico del estado da arte del código abierto en la organización candidata. Resultado: un documento describiendo lo que este programa logrará, donde está la organización y a dónde pretende ir.

## Flujo de trabajo

Como profesionales de software modernos, nos gustan los métodos agilistas que definen incrementos pequeños y seguros, dado que es una buena práctica reevaluar la situación regularmente y proporcionar unos mínimos resultados intermedios significativos.

En el contexto de un programa vivo de OSPO esto es muy importante ya que muchos aspectos secundarios evolucionarán, desde la estrategia y respuesta de la organización al código abierto hasta la disponibilidad y compromiso de las personas. La reevaluación e iteración periódicas también permiten adaptarse a la aceptación del programa en curso, seguir mejor las tendencias y oportunidades actuales, y obtener beneficios incrementales para los afectados y para la organización en su totalidad.

Idealmente la metodología podría implementarse en cinco fases como se indica:

1. **Descubrimiento** Entender los conceptos-clave, asimilar la metodología, alinear las metas y las expectativas.
1. **Adecuación** Adaptación de la descripción de la actividad y la evaluación de oportunidades a las especificidades de la organización.
1. **Priorización** Identificación de objetivos y resultados clave, tareas y herramientas, planificación de hitos y esbozo del cronograma.
1. **Activación** Finalización del cuadro de mando, presupuesto, asignaciones, documentación en el gestor de tareas.
1. **Iteración** Evaluar y puntuar resultados, destacar asuntos, mejorar, ajustar. Iterar todos los trimestres u semestres.

Preparar la primera iteración del programa:

* Identifique un primer conjunto de tareas sobre las que trabajar, y priorícelas de acuerdo a las necesidades (brechas hasta el estado deseado) y el cronograma. Resultado: una lista de tareas sobre las que trabajar, durante la iteración.
* Defina un conjunto de requisitos y áreas de mejora, comuníquelo a los afectados y usuarios finales, obtenga su aprobación o compromiso.
* Rellene los cuadros de mando para trazar el progreso. Se puede descargar una plantilla del cuadro de mando de [repositório GGI](https://gitlab.ow2.org/ggi/ggi/-/tree/main/resources/).

Al final de cada iteración, haga una retrospectiva y prepárese para a próxima iteración:

* Comunique las últimas mejoras.
* Evalúe donde se encuentra, si se han concluido las tareas previstas, refine la hoja de ruta en consecuencia.
* Compruebe los asuntos pendientes, si es necesario solicite apoyo a otros actores o servicios.
* Redefina las tareas de acuerdo al contexto actualizado.
* Defina un nuevo subconjunto de tareas a ejecutar.

## Configuración manual: usar cuadros de mando personalizados de actividades

Un cuadro de mando personalizado de actividades es una forma que describir una actividad canónica adaptada a las especificidades de una organización. En conjunto, los cuadros de mando personalizados de actividades proporcionan una hoja de ruta para a gestión del software de código abierto.

`Observe que según la experiencia inicial con la metodología, adaptar una actividad canónica a un cuadro de mando específico para una organización puede llevar hasta una hora .`

Un cuadro de mando personalizado de actividades contiene las siguientes secciones:

* **Desambiguación del título** Antes de nada, dedique algunos minutos a entender de qué va la actividad y su relevancia, como puede encajar en su gestión general del OSS.
* **Descripción personalizada** Adapte la actividad a las especificidades de la organización. Defina el ámbito de la actividad, el caso de uso particular que va a abordar.
* **Evaluación de oportunidades** Explique porque es importante emprender esta actividad, qué necesidades aborda. ¿Cuales son los puntos de dolor? ¿Cuales son las oportunidades para avanzar? ¿Qué se puede ganar?
* **Objetivos** Defina un par de objetivos cruciales para a actividad. Puntos de dolor que arreglar, oportunidades de avance, deseos. Identifique tareas clave. Lo que intentamos lograr en esta iteración.
* **Herramientas** Tecnología, herramientas y productos empleados en la actividad.
* **Notas operacionales** Indicaciones sobre el enfoque, el método, la estrategia para avanzar en esta actividad.
* **Resultados clave** Defina resultados esperados medibles y verificables. Escoja resultados que indiquen el avance respecto a los objetivos. Indique aquí los KPIs.
* **Progreso y puntuación** Progreso es el grado de completitud del resultado, en %; Puntuación es la valoración personal del éxito.
* **Evaluación personal** Puede Ud. añadir una breve explicación para cada resultado y para el grado de satisfacción personal expresado en su puntuación.
* **Cronograma** Indique fechas de inicio y fin, fases, pasos críticos, hitos.
* **Esfuerzos** Evalúe el tiempo y los recursos materiales solicitados, internos y de terceros. ¿Qué esfuerzos se esperan? ¿Cuanto va a costar? ¿Qué recursos necesitamos?
* **Responsables** Diga quién participa. Asigne tareas o liderazgo y responsabilidades de la actividad.
* **Obstáculos** Identifique los retos principales, dificultades previstas, riesgos, bloqueos, incertidumbres, puntos de atención, dependencias críticas.
* **Estado** Escriba aquí una evaluación sucinta de cómo progresa la actividad: ¿adecuadamente? ¿atrasada? Etc.
* **Evaluación global de progreso** Su propia evaluación de progreso de la actividad a alto nivel, sucinta, orientada a la gestión.

## Configuración automática: usar la funcionalidad de despliegue de GGI

A partir de la versión 1.1 del Manual, la GGI propone [My GGI Board](https://gitlab.ow2.org/ggi/my-ggi-board), una herramienta automatizada para desplegar su propia instancia de GGI en la forma de un proyecto en GitLab. Configurar el proceso de instalación lleva menos de 10 minutos, está totalmente documentado y proporciona una manera simple y fiable de personalizar las actividades, acompañar su ejecución a medida que avanza y comunicar los resultados a los afectados. Se puede ver un ejemplo en vivo del despliegue en [el GitLab de la iniciativa](https://gitlab.ow2.org/ggi/my-ggi-board-test), con el sitio web auto-generado disponible en [sus páginas GitLab](https://ggi.ow2.io/my-ggi-board-test/).

![Actividades de implantación de GGI](resources/images/ggi_deploy_activities.png)

Aquí hay un flujo de trabajo estándar para usar la funcionalidad de despliegue:

1. Bifurque My GGI Board en su propia instancia o proyecto de GitLab y configúrelo siguiendo las instrucciones del fichero README del proyecto: <https://gitlab.ow2.org/ggi/my-ggi-board>. Esto causará lo siguiente:

  - Creará todas las actividades como asuntos/tickets del proyecto.
  - Creará un cuadro para ayudarle a visualizar y gestionar las actividades.
  - Creará un sitio web estático, publicado en las páginas de su instancia de GitLab, con la información extraída de las actividades.
  - Actualizará la descripción del proyecto con los enlaces adecuados al cuadro de actividades y a su sitio web estático.

2. A partir de ahí, puede Ud. empezar a mirar las actividades y rellenar la sección del cuadro de puntuación.

  - La sección del cuadro de puntuación es el equivalente electrónico (y simplificado) de las tarjetas de puntuación ODT mencionados anteriormente. Se emplean para adaptar la actividad a su contexto, listando los recursos locales, riesgos y oportunidades, y definiendo objetivos adaptados necesarios para completar la actividad.
  - Si alguna actividad no se aplica a su contexto, simplemente márquela como "No seleccionada", o ciérrela.
  - Este es un proceso que consume bastante tiempo, pero es muy necesario, ya que le ayudará, paso a paso, a trazar su propia hoja de ruta y plan.

3. Cuando se hayan definido las actividades, puede comenzar a implementar su propia OSPO. Seleccione algunas actividades que considere importantes para empezar, y cambie su etiqueta de avance de 'No iniciada' a 'En Curso'. Puede utilizar las funciones de GitLab para ayudarle a organizar el trabajo (comentarios, encargados, etc.) o cualquier otra herramienta. Es fácil de enlazar con las actividades, y hay un montón de estupendas integraciones disponibles.

4. Periódicamente (semanal, mensualmente, en función de su calendario), evalúe y revise las actividades actuales y, cuando se completen, cambie la etiqueta de "En curso" a "Realizada". Seleccione algunas otras y comience de nuevo en el paso 3 hasta completarlas todas.

El sitio web propone una visión general rápida de las actividades actuales y pasadas, y extrae la sección de cuadro de mando de los tickets para mostrar solo la información relevante a nivel local. Cuando se producen cambios en los asuntos (actividades), estos se actualizan automáticamente en el sitio web generado. Tenga en cuenta que las cadenas de integración continua para la generación automática del sitio web se ejecutan automáticamente todas las noches, pero puede iniciarlas fácilmente desde la sección CI / CD del proyecto GitLab. La siguiente imagen muestra la interfaz del sitio web generada automáticamente.

![Sitio web de despliegue de GGI](resources/images/ggi_deploy_website.png)

Puede preguntar u obtener asistencia para la función de despliegue en nuestra página de inicio de GitLab. Y agradecemos sus comentarios.

> Página principal de GGI Deploy: <https://gitlab.ow2.org/ggi/my-ggi-board>

## Disfrute

¡Comunique su éxito y disfrute de la tranquilidad de una estrategia de código abierto de vanguardia!

El Buen Gobierno de OSS es un método para implementar un programa de mejora continua, y como tal nunca termina. Sin embargo, es importante destacar los pasos intermedios y apreciar los cambios que produce, para hacer visible el progreso y compartir los resultados.

* Comuníquese con los afectados y usuarios finales para hacerles conocer las ventajas y beneficios que aporta el esfuerzo de la iniciativa.
* Fomentar la sostenibilidad del programa. Asegúrese de que las mejores prácticas y lecciones aprendidas del programa se apliquen y actualicen siempre.
* Comparta su experiencia con sus colegas: aporte comentarios al grupo de trabajo de GGI y dentro de su comunidad de adopción de OSPO, y comparta su enfoque.
