## Software empresarial de código abierto

ID de la Actividad: [GGI-A-20](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_20.md).

### Descripción

Esta actividad trata de seleccionar proactivamente soluciones de OSS, soportadas por proveedores o por la comunidad, en áreas orientadas a negocio. Puede también abarcar la definición de políticas de preferencia para la selección de aplicaciones de código abierto en negocios.

Aunque el software de código abierto es el más utilizado por los profesionales de TI -sistema operativo, middleware, DBMS, administración de sistemas, herramientas de desarrollo-, aún tiene que ser reconocido en áreas donde los profesionales de la empresa son los principales usuarios.

La actividad abarca áreas como: software ofimático, entornos colaborativos, administración de usuarios, gestión de flujos de trabajo, gestión de relaciones con clientes, correo electrónico, comercio electrónico, etc.

### Evaluación de oportunidades

A medida que el uso de código abierto se normaliza, llega mucho más allá de los sistemas operativos y herramientas de desarrollo. Paulatinamente va encontrando su camino hacia las capas superiores de los sistemas de información, incluyendo las aplicaciones de negocio. Es importante identificar qué aplicaciones de OSS se usar con éxito para atender las necesidades de la organización y como convertirse en la elección preferida para ahorrar costes en una organización.

La actividad puede conllevar algunos costes de formación y migración.

### Evaluación del progreso

Los siguientes **puntos de verificación** demuestran el progreso en esta Actividad:

- [ ] Existe una lista de soluciones OSS recomendadas para abordar necesidades pendientes en aplicaciones comerciales.
- [ ] Se ha esbozado una política de preferencia para la selección de software de aplicaciones de negocio de código abierto.
- [ ] Se están evaluando las aplicaciones de negocio privativas en uso contra sus equivalentes OSS.
- [ ] El proceso de compras y las solicitudes de ofertas especifican la preferencia por el código abierto (si es legalmente viable).

### Herramientas

En esta etapa, no podemos pensar en ninguna herramienta relevante o implicada en esta actividad.

### Recomendaciones

- Hable con colegas, aprenda de lo que hacen otras empresas comparables a la suya.
- Asista a eventos profesionales locales para descubrir más acerca de las soluciones y soporte profesional OSS.
- Experimente primero con las ediciones y soporte comunitarios antes de comprometerse con planes de soporte de pago.

### Recursos

- [What is enterprise open source?](https://www.redhat.com/pt-br/blog/what-enterprise-open-source): una lectura rápida sobre código abierto para empresas.
- [101 Open Source Apps to Help your Business Thrive](https://digital.com/creating-an-llc/open-source-business/): una lista indicativa de soluciones de código abierto para empresas.

### Próximas actividades propuestas

- [GGI-A-33 - Relación con proveedores de código abierto](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) Mejore la confianza en sus activos de código abierto relacionándose con profesionales del código abierto.
- [GGI-A-43 - Política de adquisiciones de código abierto](https://ospo-alliance.org/ggi/activities/open_source_procurement_policy) El uso de OSS por parte de las empresas se optimizará conociendo los activos que ya existen y teniendo una política de adquisiciones clara al respecto.
