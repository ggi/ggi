# Organización

## Terminología

El diseño de la metodología de Buen Gobierno de OSS está estructurado en torno a cuatro conceptos clave: objetivos, actividades canónicas, cuadros de mando de actividades personalizadas e iteración.

* **Objetivos**: un objetivo es un conjunto de actividades asociadas a un área de interés común. Existen cinco objetivos: objetivo de utilización, objetivo de confianza, objetivo de cultura, objetivo de desarrollo y objetivo de estrategia. Los objetivos se pueden alcanzar de forma independiente, en paralelo, o refinándolos iterativamente por actividades.
* **Actividades canónicas**: dentro de un objetivo, cada actividad aborda un único asunto de desarrollo - como la gestión de cumplimiento legal - que puede aprovecharse como un paso incremental en dirección a los objetivos del programa. El conjunto completo de actividades que define la GGI se denomina Actividades Canónicas.
* **Cuadro de mando personalizado actividades (CAS)**: para implementar GGI en una determinada organización, las actividades canónicas se deben adaptar a las especificidades del contexto, constituyendo así un conjunto de cuadros de mando personalizados de actividades. El cuadro de mando personalizado de actividades describe como se implementará la actividad en el contexto de una organización y como se monitorizará su progreso.
* **Iteración**: El Buen Gobierno de OSS es un sistema de gestión y como tal requiere evaluaciones y revisiones periódicas. Piense en el sistema de contabilidad de una organización, es un proceso continuo con al menos un punto de control anual, el balance; de la misma manera, el proceso de Buen Gobierno de OSS requiere al menos una revisión anual, no obstante, puede haber revisiones parciales o más frecuentes, dependiendo de las actividades.

## Objetivos

As actividades canónicas que define GGI se organizan en objetivos. Cada objetivo aborda un área específica de avance en el proceso. Desde la utilización a la estrategia, los objetivos cubren cuestiones relacionadas con todos los afectados, desde los equipos de desarrollo al nivel ejecutivo.

* Objetivo **Uso**: este objetivo cubre los pasos básicos en la utilización de software de código abierto. Las actividades relacionadas con el objetivo de utilización cubren los primeros pasos de un programa de código abierto, identificando la eficiencia de uso del código abierto y qué aporta éste a la organización. Incluye formación y gestión de conocimiento, produciendo inventarios de software de código abierto en uso interno, y presenta conceptos de código abierto que se pueden utilizar a lo largo de todo el proceso.
* Objetivo **Confianza**: este objetivo trata el uso seguro del código abierto. El objetivo de confianza trata el cumplimiento legal, la gestión de dependencias y vulnerabilidades, y trata de generar confianza respecto a la forma en que la organización emplea el código abierto.
* Objetivo **Cultura**: el objetivo de cultura incluye actividades orientadas a hacer que los equipos se sientan cómodos con el código abierto, participando individualmente en actividades colaborativas, comprendiendo e implementando buenas prácticas de código abierto. Este objetivo fomenta entre los individuos un sentimiento de pertenencia a la comunidad del código abierto.
* Objetivo **Involucración**: este objetivo busca involucrarse en el ecosistema del código abierto a nivel corporativo. Los recursos humanos y financieros están presupuestados para contribuir a proyectos de código abierto. Aquí, la organización afirma ser un ciudadano responsable del código abierto y reconoce su responsabilidad en asegurar la sostenibilidad del ecosistema de código abierto.
* Objetivo **Estrategia**: este objetivo trata de hacer al código abierto visible y aceptable en los niveles más altos de la gerencia corporativa. Se trata de reconocer que el código abierto es un facilitador estratégico de la soberanía digital, proceso de innovación y, en general, una fuente de atracción y buena voluntad.

## Actividades canónicas

Las actividades canónicas son centrales en el diseño de GGI. En su versión inicial, la Metodología GGI ofrece cinco actividades canónicas por objetivo, 25 en total. Las actividades canónicas se describen empleando las siguientes secciones predefinidas:

* *Descripción*: un resumen del asunto que la actividad aborda y de los pasos para completarla.
* *Evaluación de oportunidades*: describe cuando y por qué es importante realizar esta actividad.
* *Evaluación de progreso*: describe como medir el avance de la actividad y evaluar su éxito.
* *Herramientas*: una lista de tecnologías y/o herramientas que pueden ayudar a realizar esta actividad.
* *Recomendaciones*: sugerencias y buenas prácticas coleccionadas por los participantes en GGI.
* *Recursos*: enlaces y referencias para leer más sobre el tema tratado en la actividad.

### Descripción

Esta sección aporta una descripción de alto nivel de la actividad, un resumen del asunto para establecer el propósito de la actividad en contexto con el enfoque de código abierto en el marco de un objetivo.

### Evaluación de oportunidades

Para ayudar a estructurar un enfoque iterativo, cada actividad tiene una sección "Evaluación de oportunidades" con una o más cuestiones relacionadas. La evaluación de oportunidades se centra en por qué es importante realizar esta actividad; qué necesidades aborda. Evaluar la oportunidad ayudará a estimar esfuerzos, recursos necesarios, y ayudará a evaluar e los costes y el retorno esperado de la inversión (ROI).

### Evaluación del progreso

Este paso se centra en definir los objetivos, KPI (Indicadores Clave de Rendimiento[^kpi]), y en proporcionar *puntos de verificación* que ayuden a evaluar el progreso en la Actividad. Los puntos de verificación que se sugieren, pueden ayudar a definir una hoja de ruta para el proceso de Buena Gobernanza, sus prioridades y cómo se medirán los progresos.

### Herramientas

Aquí se listan las herramientas que pueden ayudar a entregar la actividad o instrumentar un paso específico de las actividades. Las herramientas no son obligatorias ni pretenden ser exhaustivas, sino que son sugerencias o categorías sobre las que trabajar en base a un contexto existente.

### Recomendaciones

Esta sección se actualiza regularmente con comentarios de los usuarios y todo tipo de recomendaciones que puedan ayudar a gestionar la actividad.

### Recursos

Los recursos se proponen para alimentar el enfoque con estudios de fondo, documentos de referencia, eventos o contenido on-line para enriquecer y desarrollar el enfoque relacionado con la actividad. Los recursos no son exhaustivos, sino puntos de partida o sugerencias para expandir la semántica de la actividad según su proprio contexto.

## Cuadros de mando personalizados de actividades

Los cuadros de mando personalizados de actividades (CAS) son ligeramente más detallados que las actividades canónicas. Un CAS incluye detalles específicos de la organización que implementa GGI. El uso de CAS se describe en la sección Metodología.

[^kpi]: Un indicador de rendimiento o indicador clave de rendimiento es un tipo de medida del rendimiento. Los KPI evalúan el progreso y el éxito de una organización o de una actividad concreta a la que se dedique.
