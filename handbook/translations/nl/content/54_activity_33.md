## Ga in gesprek met open source leveranciers

Activity ID: [GGI-A-33](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_33.md).

### Beschrijving

Beveilig contracten met open source verkopers die software leveren die voor u van kritiek belang is. Bedrijven en entiteiten die open source software produceren moeten gedijen om het onderhoud en de ontwikkeling van nieuwe functies te verzorgen. Hun specifieke expertise is nodig voor het project, en de gemeenschap van gebruikers vertrouwt op hun voortdurende activiteiten en bijdragen.

Samenwerken met leveranciers van open source neemt verschillende vormen aan:

- Inschrijving van ondersteuningsplannen.
- Het contracteren van lokale servicebedrijven.
- Sponsorontwikkelingen.
- Betalen voor een commerciële licentie.

Deze activiteit houdt in dat open source-projecten worden beschouwd als volledig functionele producten die de moeite waard zijn om voor te betalen, net zoals propriëtaire producten - hoewel gewoonlijk veel minder duur.

### Beoordeling van kansen

Het doel van deze activiteit is te zorgen voor professionele ondersteuning van open source software die in de organisatie wordt gebruikt. Het heeft verschillende voordelen:

- Continuïteit van de dienstverlening door tijdige bugfixes.
- Serviceprestaties door geoptimaliseerde installatie.
- Verduidelijking van de juridische/commerciële status van de gebruikte software.
- Toegang tot vroegtijdige informatie.
- Stabiele begrotingsraming.

De kosten zijn uiteraard die van de gekozen ondersteuningsplannen. Een andere kostenpost kan zijn om af te stappen van bulkuitbesteding aan grote systeemintegratoren ten gunste van fijnmazige contracten met deskundige KMO's.

### Voortgangsbeoordeling

Uit de volgende **verificatiepunten** blijkt dat op dit gebied vooruitgang is geboekt:

- [ ] Open source die in de organisatie wordt gebruikt, wordt ondersteund door commerciële ondersteuning.
- [ ] Voor sommige open source-projecten zijn ondersteuningsplannen gecontracteerd.
- [ ] De kosten van open source-ondersteuningsplannen zijn een legitieme post in het IT-budget.

### Aanbevelingen

- Zoek waar mogelijk lokale deskundige KMO's.
- Pas op voor grote systeemintegratoren die expertise van derden doorverkopen (het doorverkopen van ondersteuningsplannen die deskundige open source systeemintegratoren eigenlijk leveren).

### Middelen

Een paar links die de commerciële realiteit van open source software illustreren:

- [Een snelle lezing om commerciële open source te begrijpen](https://www.webiny.com/blog/what-is-commercial-open-source).
