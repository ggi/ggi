## C-Level bewustzijn

Activity ID: [GGI-A-34](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_34.md).

### Beschrijving

Het open source initiatief van de organisatie zal alleen zijn strategische voordelen opleveren als het op de hoogste niveaus wordt afgedwongen door het open source DNA te integreren in de strategie en de interne werking van het bedrijf. Een dergelijk engagement is niet mogelijk als de hogere leidinggevenden en het topmanagement er niet zelf deel van uitmaken. De training en de open source mentaliteit moeten ook worden uitgebreid naar degenen die het beleid, de beslissingen en de algemene strategie vormgeven, zowel binnen als buiten het bedrijf.

Dit engagement zorgt ervoor dat praktische verbeteringen, mentaliteitsveranderingen en nieuwe initiatieven op consistente, welwillende en duurzame steun van de hiërarchie kunnen rekenen, waardoor de betrokkenheid van de werknemers toeneemt. Het geeft vorm aan de manier waarop externe actoren de organisatie zien, wat de reputatie en het ecosysteem ten goede komt. Het is ook een middel om het initiatief en de voordelen ervan op middellange en lange termijn vast te leggen.

### Beoordeling van kansen

Deze activiteit wordt essentieel als/wanneer:

- De organisatie heeft globale doelen gesteld die relevant zijn voor open source management, maar worstelt om ze te bereiken. Het is onwaarschijnlijk dat het initiatief iets kan bereiken zonder goede kennis en een duidelijk commitment van hogerhand.
- Het initiatief is al gestart en boekt vooruitgang, maar de hogere niveaus van de hiërarchie geven er geen goed gevolg aan.

Hopelijk wordt het duidelijk dat alles behalve ad hoc gebruik van open source een consistente en goed doordachte aanpak vereist, gezien de verscheidenheid aan teams en de culturele verandering die het met zich mee kan brengen.

### Voortgangsbeoordeling

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] Er is een gemandateerde afdeling/functionaris die bevoegd is om een uniforme open source strategie in het hele bedrijf vast te stellen en ervoor te zorgen dat de reikwijdte duidelijk is.
- [ ] Er is een duidelijk, bindend engagement van de hiërarchie voor de OSS-strategie.
- [ ] De hiërarchie communiceert op transparante wijze over haar inzet voor het programma.
- [ ] De hiërarchie is beschikbaar om open source software te bespreken. Het kan gevraagd en uitgedaagd worden op zijn beloftes.
- [ ] Er is een passende begroting en financiering voor het initiatief.

### Aanbevelingen

Voorbeelden van acties in verband met deze activiteit zijn:

- Training geven om OSS te demystificeren voor het management op C-niveau.
- Verkrijgen van expliciete, praktische steun voor OSS-gebruik en -strategie.
- Het OSS-programma expliciet vermelden en onderschrijven in interne communicatie.
- Het OSS-programma expliciet vermelden en onderschrijven in openbare mededelingen.

Open source is een *strategische enabler* die de *bedrijfscultuur* omarmt. Wat betekent dit?

- Open source kan worden ingezet als een mechanisme om leveranciers te ontwrichten en de aanschafkosten van software te verlagen.
   - Moet open source vallen onder de bevoegdheid van *Software Asset Managers* of *inkoopafdelingen*?
- Open source licenties leggen de vrijheden vast die de voordelen van open source opleveren, maar ze brengen ook *verplichtingen met zich mee*. Indien deze niet naar behoren worden nagekomen, kunnen de verantwoordelijkheden juridische, commerciële en imagorisico's voor een organisatie met zich meebrengen.
   - Zullen licentievoorwaarden inzicht geven in bronnen die vertrouwelijk moeten blijven?
   - Zal dit gevolgen hebben voor de octrooiportefeuille van mijn organisatie?
   - Hoe moeten projectteams hierover worden opgeleid en ondersteund?
- Terug bijdragen aan externe open source projecten is waar de grootste waarde van open source ligt.
   - Hoe moet mijn bedrijf dit aanmoedigen (en bijhouden)?
   - Hoe moeten ontwikkelaars GitHub, GitLab, Slack, Discord, Telegram, of een van de andere tools waar open source projecten gewoonlijk gebruik van maken, gebruiken?
   - Kan open source invloed hebben op het HR-beleid van het bedrijf?
- Natuurlijk gaat het niet alleen om het teruggeven van bijdragen, hoe zit het met mijn eigen open source projecten?
   - Ben ik klaar voor *open* innovatie?
   - Hoe zullen mijn projecten *inkomende* bijdragen beheren?
   - Moet ik de moeite nemen om een gemeenschap te vormen voor een bepaald project?
   - Hoe moet ik de gemeenschap leiden, welke rol moeten de leden van de gemeenschap hebben?
   - Ben ik klaar om beslissingen over de routekaart over te dragen aan een gemeenschap?
   - Kan open source een waardevol instrument zijn om de siloïsering tussen bedrijfsteams te verminderen?
   - Moet ik de overdracht van open source van de ene bedrijfsentiteit naar de andere afhandelen?

### Voorgestelde volgende activiteiten

- [GGI-A-31 - Publiekelijk bevestigen van het gebruik van open source](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) C-level managers zijn prominente vertegenwoordigers van een organisatie. Laat ze communiceren over de betrokkenheid van de organisatie bij open source.
