## Behoor tot de open source gemeenschap

Activity ID: [GGI-A-27](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_27.md).

### Beschrijving

Deze activiteit gaat over het ontwikkelen van het gevoel onder ontwikkelaars om deel uit te maken van de grotere open source gemeenschap. Zoals bij elke gemeenschap, moeten mensen en entiteiten deelnemen en bijdragen aan het geheel. Het versterkt de banden tussen beoefenaars en brengt duurzaamheid en activiteit in het ecosysteem. Aan de meer technische kant maakt het het mogelijk om de prioriteiten en het stappenplan van projecten te kiezen, het niveau van algemene kennis en technisch bewustzijn te verbeteren.

Deze activiteit omvat het volgende:

- **Identificeer evenementen** die de moeite waard zijn om bij te wonen. Het verbinden van mensen, leren over nieuwe technologieën en het opbouwen van een netwerk zijn sleutelfactoren om alle voordelen van open source te benutten.
- Overweeg **stichtingslidmaatschappen**. Open source stichtingen en organisaties zijn een belangrijk onderdeel van het open source ecosysteem. Ze bieden technische en organisatorische hulpmiddelen voor projecten en zijn een goede neutrale plek voor sponsors om veelvoorkomende problemen en oplossingen te bespreken of om aan normen te werken.
- Bekijk **werkgroepen**. Werkgroepen zijn neutrale collaboratieve werkruimten waar experts samenwerken op een specifiek domein zoals IoT, modellering of wetenschap. Ze zijn een zeer efficiënt en kosteneffectief mechanisme om gemeenschappelijke, zij het domeinspecifieke problemen samen aan te pakken.
- **Budgetparticipatie**. Aan het einde van de reis is geld de katalysator. Plan benodigde uitgaven, geef mensen tijd voor deze activiteiten, anticipeer op volgende stappen, zodat het programma niet hoeft te stoppen na een paar maanden zonder financiering.

### Beoordeling van kansen

Open source werkt het beste in relatie met de open source gemeenschap in het algemeen. Het vergemakkelijkt het oplossen van bugs, het delen van oplossingen, enz.

Het is ook een goede manier voor bedrijven om hun steun aan open sourcewaarden te tonen. Communiceren over de betrokkenheid van het bedrijf is belangrijk voor zowel de reputatie van het bedrijf als voor het open source ecosysteem.

### Voortgangsbeoordeling

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] Er wordt een lijst opgesteld met evenementen die mensen kunnen bijwonen.
- [ ] Er is toezicht op de openbare lezingen die door teamleden worden gegeven.
- [ ] Mensen kunnen verzoeken voor deelname aan evenementen indienen.
- [ ] Mensen kunnen projecten indienen voor sponsoring.

### Aanbevelingen

- Onderzoek bij mensen om erachter te komen welke evenementen ze leuk vinden of die het meest nuttig zijn voor hun werk.
- Zet interne communicatie op (nieuwsbrief, informatiecentrum, uitnodigingen...) zodat mensen op de hoogte zijn van de initiatieven en kunnen deelnemen.
- Zorg ervoor dat deze initiatieven verschillende soorten mensen ten goede kunnen komen (ontwikkelaars, beheerders, ondersteuning...), niet alleen leidinggevenden op C-niveau.

### Middelen

- [What motivates a developer to contribute to open source software?](https://clearcode.cc/blog/why-developers-contribute-open-source-software/) Een artikel door Michael Sweeney op clearcode.cc.
- [Why companies contribute to open source](https://blogs.vmware.com/opensource/2020/12/01/why-companies-contribute-to-open-source/) Een artikel door Velichka Atanasova van VMWare.
- [Why your employees should be contributing to open source](https://www.cloudbees.com/blog/why-your-employees-should-be-contributing-to-open-source/) Een goed te lezen artikel door Robert Kowalski van CloudBees.
- [7 ways your company can support open source](https://www.infoworld.com/article/2612259/7-ways-your-company-can-support-open-source.html) Een artikel van Simon Phipps voor InfoWorld.
- [Events: the life force of open source](https://www.redhat.com/en/blog/events-life-force-open-source) Een artikel door Donna Benjamin van RedHat.

### Voorgestelde volgende activiteiten

- [GGI-A-28 - HR perspectief](https://ospo-alliance.org/ggi/activities/human_resources_perspective) Als de organisatie tot de open source gemeenschap behoort, is het gemakkelijker om bekwame mensen aan te trekken, op basis van de gemeenschap waarbij u betrokken bent.
- [GGI-A-31 - Publiekelijk bevestigen van het gebruik van open source](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Nu je onderdeel bent geworden van de open source gemeenschap, maak het bekend! Het is goed voor je reputatie en het is goed voor het project in termen van de gezondheid van het project en verdere verspreiding van het project.
