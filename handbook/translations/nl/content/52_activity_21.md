## Beheer de naleving van de wettelijke voorschriften

Activity ID: [GGI-A-21](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_21.md).

### Beschrijving

Organisaties moeten een juridisch compliance proces implementeren om hun gebruik en deelname aan open source projecten veilig te stellen.

Een volwassen en professioneel beheer van de compliance, in de organisatie en in de toeleveringsketen, gaat over:

- Het uitvoeren van een grondige analyse van de intellectuele eigendom, inclusief licentie-identificatie en compatibiliteitscontrole.
- Ervoor zorgen dat de organisatie open source componenten veilig kan gebruiken, integreren, wijzigen en herdistribueren als onderdeel van haar producten of diensten.
- Werknemers en contractanten een transparant proces bieden over hoe ze open source software kunnen maken en eraan kunnen bijdragen.

*Software Composition Analysis (SCA)*: Een aanzienlijk deel van de juridische en intellectueel eigendomsproblemen vloeit voort uit het gebruik van componenten die zijn vrijgegeven onder licenties die onderling incompatibel zijn of incompatibel zijn met de manier waarop de organisatie de componenten wil gebruiken en herdistribueren. SCA is de eerste stap om die problemen op te lossen, want "je moet het probleem kennen om het op te lossen". Het proces bestaat erin alle componenten die bij een project betrokken zijn te identificeren in een Bill of Material document, inclusief bouw- en testafhankelijkheden.

*Licentiecontrole*: Een licentiecontrole maakt gebruik van een tool om automatisch de codebasis te analyseren en licenties en auteursrechten te identificeren. Indien regelmatig uitgevoerd en idealiter geïntegreerd in continue bouw- en integratieketens, kunnen IP-problemen vroegtijdig worden opgespoord.

### Beoordeling van kansen

Met het steeds toenemende gebruik van OSS in de informatiesystemen van een organisatie is het van essentieel belang om de potentiële juridische risico's te beoordelen en te beheren.

Het controleren van licenties en auteursrechten kan echter lastig en duur zijn. Ontwikkelaars moeten IP en juridische vragen snel kunnen controleren. Een team en een bedrijfsfunctionaris die zich bezighouden met IP en juridische vragen zorgt voor een proactief en consistent beheer van juridische vragen, helpt het gebruik en de bijdragen van open source componenten veilig te stellen en biedt een duidelijke strategische visie.

### Voortgangsbeoordeling

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] Er is een gebruiksvriendelijk proces voor het controleren van licenties voor projecten.
- [ ] Er is een eenvoudig te gebruiken IP-controleproces beschikbaar voor projecten.
- [ ] Er is binnen de organisatie een team of persoon verantwoordelijk voor de juridische compliance.
- [ ] Er zijn regelmatig audits gepland om na te gaan of juridische compliance wordt nageleefd.

Andere manieren om verificatiepunten in te stellen:

- [ ] Er is een gebruiksvriendelijk proces voor het controleren van licenties.
- [ ] Er is een eenvoudig te gebruiken juridisch/IP-team.
- [ ] Alle projecten bieden de vereiste informatie voor mensen om het project te gebruiken en eraan bij te dragen.
- [ ] Er is een contactpersoon in het team voor vragen over intellectueel eigendom en licenties.
- [ ] Er is een bedrijfsfunctionaris die zich bezighoudt met intellectueel eigendom en licenties.
- [ ] Er is een speciaal team voor vragen over intellectueel eigendom en licenties.

### Hulpmiddelen

- [ScanCode](https://scancode-toolkit.readthedocs.io)
- [Fossologie](https://www.fossology.org/)
- [SW360](https://www.eclipse.org/sw360/)
- [Fossa](https://github.com/fossas/fossa-cli)
- [OSS Review Toolkit](https://oss-review-toolkit.org)

### Aanbevelingen

- Mensen informeren over de risico's van licenties die in strijd zijn met de bedrijfsdoelstellingen.
- Een eenvoudige oplossing voorstellen voor projecten om licentiecontrole op hun codebase in te stellen.
- Communiceer over het belang ervan en help projecten om het toe te voegen aan hun CI-systemen.
- Zorg voor een sjabloon of officiële richtlijnen voor de projectstructuur.
- Stel geautomatiseerde controles in om ervoor te zorgen dat alle projecten aan de richtlijnen voldoen.
- Overweeg een interne audit om de licenties van de bedrijfsinfrastructuur in kaart te brengen.
- Ten minste één persoon per team een basisopleiding intellectueel eigendom en licenties geven.
- Zorg voor een volledige intellectueel eigendom- en licentietraining voor de functionaris.
- Een proces opzetten om intellectueel eigendom- en licentiekwesties aan de functionaris voor te leggen.

Vergeet niet dat compliance niet alleen juridisch is, maar ook intellectueel eigendom. Daarom volgen hier enkele vragen om de gevolgen van wettelijke compliance te helpen begrijpen:

- Als ik een open source component verspreid en de licentievoorwaarden niet respecteer, schend ik de licentie --> juridische gevolgen.
- Als ik een open source component gebruik binnen een project dat ik wil verspreiden/publiceren, kan die licentie verplichten tot zichtbaarheid op elementen van code die ik niet open source wil maken --> Geheimhoudingsimpact voor het tactisch voordeel van mijn bedrijf en met derden (juridische implicaties).
- Het is een open discussie over de vraag of het gebruik van een open source licentie voor een project dat ik wil publiceren relevant intellectueel eigendom verleent --> intellectueel eigendom implicaties.
- * Als ik een project open source maak *voordat* enig octrooiproces plaatsvindt, sluit dat *waarschijnlijk* de creatie van octrooien betreffende het project uit --> intellectueel eigendom implicaties.
- * Als ik een project open source maak *na* enig octrooiproces, maakt dat *waarschijnlijk* de creatie van (defensieve) octrooien betreffende dat project mogelijk --> intellectueel eigendom potentieel.
- In complexe projecten die veel componenten met veel afhankelijkheden inbrengen, kan de veelheid van open source licenties onverenigbaarheden tussen licenties vertonen --> juridische implicaties (conform activiteit GGI-A-23 - Beheer van softwareafhankelijkheden).

### Middelen

- Er is een uitgebreide lijst van instrumenten op de [pagina van de bestaande OSS-nalevingsgroep](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence-Compliance-Tools/).
- [Recommended Open Source Compliance Practices for the enterprise](https://www.ibrahimatlinux.com/wp-content/uploads/2022/01/recommended-oss-compliance-practices.pdf). Een boek van Ibrahim Haddad, van de Linux Foundation, over open source compliance praktijken voor de onderneming. [OpenChain Project](https://www.openchainproject.org/)

### Voorgestelde volgende activiteiten

- [GGI-A-24 -Beheren van kritieke indicatoren](https://ospo-alliance.org/ggi/activities/manage_key_indicators) Maak de zorgen, processen en resultaten op het gebied van wettelijke naleving zichtbaar en meetbaar. Dit zal mensen helpen het belang ervan eerder in het proces te beseffen.
