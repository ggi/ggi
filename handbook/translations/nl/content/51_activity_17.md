## Inventarisatie van open source vaardigheden en -bronnen

Activity ID: [GGI-A-17](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_17.md).

### Beschrijving

In elk stadium is het vanuit managementperspectief nuttig om een overzicht te hebben, een inventaris van open source bronnen, middelen, gebruik en hun status, alsmede potentiële behoeften en beschikbare oplossingen. Het omvat ook een beoordeling van de vereiste inspanning en vaardigheden om de leemte op te vullen.

Deze activiteit heeft tot doel een momentopname te maken van de open source situatie binnen de organisatie en op de markt en de brug tussen beide te evalueren.

- Inventarisatie van het gebruik van OSS in de softwareontwikkelingsketen en in de softwareproducten en -componenten die in de productie worden gebruikt.
- Identificeer open source technologieën (oplossingen, frameworks, innovatieve functies) die in uw behoeften kunnen voorzien en uw proces kunnen helpen verbeteren.

Niet inbegrepen

- Identificeren en kwalificeren van verwante OSS-ecosystemen en -gemeenschappen. (Cultuurdoel)
- Identificeren van afhankelijkheden van OSS-bibliotheken en -componenten. (Vertrouwensdoel)
- Bepaal de technische (bv. talen, frameworks...) en zachte (bv. samenwerking, communicatie) vaardigheden die nodig zijn. (hoort bij volgende activiteiten: OSS competentiegroei en Open source software ontwikkelingsvaardigheden)

### Beoordeling van kansen

Een inventaris van beschikbare open source bronnen die zal helpen de investeringen te optimaliseren en prioriteit te geven aan de ontwikkeling van vaardigheden.

Deze activiteit schept de voorwaarden voor verbetering van de ontwikkelingsproductiviteit, gezien de efficiëntie en populariteit van OSS-componenten, ontwikkelingsbeginselen en -instrumenten, met name bij de ontwikkeling van moderne toepassingen en infrastructuren.

- Dit kan betekenen dat de portefeuille van OSS-middelen moet worden vereenvoudigd.
- Dit kan een herscholing van het personeel vereisen.
- Dit maakt het mogelijk de behoeften te identificeren en voedt uw IT-roadmap.

### Voortgangsbeoordeling

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] Er is een werkbare lijst van OSS-bronnen "Wij gebruiken", "Wij integreren", "Wij produceren", "Wij hosten", en de daarmee verband houdende vaardigheden
- [ ] Wij zijn op weg naar meer efficiëntie door gebruik te maken van de modernste methoden en instrumenten.
- [ ] We hebben OSS-middelen geïdentificeerd die tot nu toe niet in aanmerking zijn genomen (die er misschien ingeslopen zijn, en hebben we elementen om beleid op dit gebied te bepalen?)
- [ ] We vragen nieuwe projecten om bestaande OSS-bronnen te onderschrijven of te hergebruiken.
- [ ] Wij hebben een redelijk veilige perceptie van en inzicht in de reikwijdte van het gebruik van OSS in onze organisatie.

### Hulpmiddelen

Er zijn veel verschillende manieren om een dergelijke inventaris op te stellen. Eén manier is het indelen van OSS-bronnen in vier categorieën:

- OSS die we gebruiken: software die we in productie of ontwikkeling gebruiken
- OSS die we integreren: bijvoorbeeld OSS-bibliotheken die we integreren in een op maat gemaakte applicatie
- OSS die we produceren: bijvoorbeeld een bibliotheek die we op GitHub hebben gepubliceerd of een OSS-project dat we ontwikkelen of waaraan we regelmatig bijdragen.
- OSS die we hosten: OSS die we draaien om een interne dienst aan te bieden, zoals een CRM, GitLab, Nexus, enz. Een voorbeeldtabel zou er als volgt uitzien:

| We gebruiken | Wij integreren | Wij produceren | Wij hosten | Vaardigheden |
| --- | --- | --- | --- | --- |
| Firefox, <br />OpenOffice, <br />Postgresql | Bibliotheek slf4j | Bibliotheek YY op GH | GitLab, <br />Nexus | Java, <br />Python |

Dezelfde identificatie moet gelden voor vaardigheden

- Via de bestaande teams beschikbare vaardigheden en ervaringen
- Vaardigheden & ervaringen die intern kunnen worden ontwikkeld of opgedaan (opleiding, coaching, experiment)
- Vaardigheden en ervaringen die op de markt of via partnerschappen/contracten moeten worden gezocht

### Aanbevelingen

- Hou het simpel.
- Het is een oefening op relatief hoog niveau, geen gedetailleerde inventaris voor de boekhouding.
- Hoewel deze activiteit een goed uitgangspunt is, hoeft deze niet voor 100% te zijn afgerond voordat u met andere activiteiten begint.
- Behandelen van kwesties, middelen en vaardigheden in verband met **softwareontwikkeling** in activiteit #42.
- De inventaris moet alle IT-categorieën omvatten: besturingssystemen, middlewares, DBMS, systeembeheer, ontwikkelings- en testinstrumenten, enz.
- Begin met het identificeren van verwante gemeenschappen: het is gemakkelijker om steun en feedback van het project te krijgen als ze je al kennen.

### Middelen

- Een uitstekende cursus over [Free (/Libre), and Open Source Software (FOSS)](https://profriehle.com/open-courses/free-and-open-source-software), door professor Dirk Riehle.

### Voorgestelde volgende activiteiten

- [GGI-A-18 - Groei van open source competenties](https://ospo-alliance.org/ggi/activities/open_source_competency_growth) Door open source vaardigheden en -middelen te identificeren, kan de organisatie haar bewustzijn en haar competenties consolideren en versterken.
- [GGI-A-19 - Open source toezicht](https://ospo-alliance.org/ggi/activities/open_source_supervision) Zodra de inventarisatie van open source software en vaardigheden compleet is, kan men beginnen met het controleren en beheren van het gebruik van open source software binnen de organisatie.
- [GGI-A-28 - HR perspectief](https://ospo-alliance.org/ggi/activities/human_resources_perspective) De HR addeling kan proportionele en adequate ontwikkelingsplannen, contracten en processen opstellen op basis van de inventaris die bij deze activiteit wordt geproduceerd.
- [GGI-A-33 - Samenwerken met leveranciers van open source](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) Je moet hun open source software en -vaardigheden kennen voordat je een externe relatie met een leverancier aangaat.
- [GGI-A-42 - Beheer van open source vaardigheden en middelen](https://ospo-alliance.org/ggi/activities/manage_open_source_skills_and_resources) Zodra de inventaris van open source middelen en -vaardigheden compleet is, kan men beginnen deze op de juiste manier te beheren, voortbouwend op de bestaande interne bronnen.
