## Open source toezicht

Activiteit ID: [GGI-A-19](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_19.md).

### Beschrijving

Bij deze activiteit gaat het erom het gebruik van open source te controleren en ervoor te zorgen dat open source software proactief wordt beheerd. Dit heeft betrekking op verschillende perspectieven, of het nu gaat om het gebruik van OSS-tools en bedrijfsoplossingen, of om het opnemen van OSS als componenten in eigen ontwikkelingen of om het aanpassen van een versie van een software aan de eigen behoeften, enz. Het gaat ook over het identificeren van gebieden waar open source een (soms heimelijke) de facto oplossing is geworden en het beoordelen van de geschiktheid ervan.

Het kan nodig zijn het volgende te verduidelijken:

- Wordt de vereiste functionaliteit geboden?
- Wordt er extra functionaliteit geboden die niet nodig is, maar de complexiteit in de BUILD- en RUN-fase verhoogt?
- Wat vereist de licentie, wat zijn de wettelijke beperkingen?
- In hoeverre maakt het besluit uw organisatie leverancier-onafhankelijk?
- Bestaat er een ondersteuningsoptie, voldoende geschikt voor uw zakelijke behoeften, en hoeveel kost die?
- TCO (Total Cost of Ownership).
- Kent het management de voordelen van open source, bijvoorbeeld buiten de "besparing op licentiekosten"? Vertrouwd zijn met open source helpt om maximaal voordeel te halen uit de samenwerking met projectgemeenschappen en verkopers.
- Kijk of het zinvol is om de ontwikkelingskosten te delen door de ontwikkelingen aan de gemeenschap te geven, met alle gevolgen van dien, zoals de naleving van licenties.
- Ga na of er ondersteuning vanuit de gemeenschap of professionele ondersteuning beschikbaar is.

### Beoordeling van kansen

Het definiëren van een besluitvormingsproces dat specifiek gericht is op open source is een manier om de voordelen ervan te maximaliseren.

- Het voorkomt het ongecontroleerde sluipende gebruik en de verborgen kosten van OSS-technologieën.
- Het leidt tot geïnformeerde en OSS bewuste strategische en organisatorische beslissingen.

Kosten: de activiteit kan suboptimaal de facto gebruik van open source aanvechten en heroverwegen als inefficiënt, riskant, enz.

### Voortgangsbeoordeling

Uit de volgende **verificatiepunten** blijkt dat op dit gebied vooruitgang is geboekt:

- [ ] OSS is een comfortabele optie geworden bij de keuze voor nieuwe software.
- [ ] OSS wordt niet gezien als een uitzondering of een gevaarlijke keuze.
- [ ] OSS is een "mainstream" optie geworden.
- [ ] Belangrijke spelers zijn er voldoende van overtuigd dat de open source oplossing strategische voordelen heeft die het waard zijn om in te investeren.
- [ ] Er kan worden aangetoond dat de TCO van de op open source gebaseerde oplossing uw organisatie een hogere waarde geeft dan het alternatief.
- [ ] Er is een evaluatie van hoe de onafhankelijkheid van leveranciers geld bespaart of in de toekomst kan besparen.
- [ ] Er is een evaluatie dat de onafhankelijkheid van de oplossing het risico verkleint dat het te duur wordt om de oplossing te veranderen (geen gesloten gegevensformaten mogelijk).

### Hulpmiddelen

In dit stadium kunnen wij geen instrument bedenken dat voor deze activiteit relevant of van belang is.

### Aanbevelingen

- Proactief beheer van het gebruik van open source vereist een basisniveau van bewustzijn en begrip van de grondbeginselen van open source, omdat deze in overweging moeten worden genomen bij elke OSS-beslissing.
- Vergelijk de benodigde functionaliteit in plaats van een alternatief te zoeken voor een bekende closed source oplossing.
- Zorg voor ondersteuning en verdere ontwikkeling.
- Houd rekening met de gevolgen van de licentie van de oplossing voor uw organisatie.
- Overtuig alle hoofdrolspelers van de waarde van de voordelen van open source, naast "besparen op licentiekosten".
- Wees eerlijk, overdrijf het effect van de open source oplossing niet.
- In het besluitvormingsproces is het even belangrijk om verschillende open source oplossingen te beoordelen om teleurstelling door verkeerde verwachtingen te voorkomen, om duidelijk te maken wat de organisatie moet doen en welke voordelen de openheid van de oplossingen biedt. Dit moet in kaart worden gebracht zodat de organisatie het voor haar eigen context kan beoordelen.

### Middelen

- [Top 5 Voordelen van Open Source](https://www.openlogic.com/blog/top-5-benefits-open-source-software): Gesponsorde blog, maar toch interessant, vlug te lezen.
- [Weighing The Hidden Costs Of Open Source](https://www.itjungle.com/2021/02/15/weighing-the-hidden-costs-of-open-source/): een door IBM gesponsorde kijk op OSS-ondersteuningskosten.
