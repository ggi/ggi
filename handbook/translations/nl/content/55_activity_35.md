## Open source en digitale soevereiniteit

Activity ID: [GGI-A-35](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_35.md).

### Beschrijving

Digitale soevereiniteit kan worden gedefinieerd als de

> "Vermogen en mogelijkheid van individuen en instellingen om hun rol(len) in de digitale wereld zelfstandig, doelbewust en veilig uit te voeren." &mdash; Competence Centre for Public IT, Duitsland

Voor een goede bedrijfsvoering is elke entiteit afhankelijk van bepaalde andere partners, diensten, producten en instrumenten. Door de banden en beperkingen van deze afhankelijkheden te herzien kan de organisatie haar afhankelijkheid van externe factoren beoordelen en beheersen, en zo haar autonomie en veerkracht verbeteren.

Zo is vendor lock-in een sterke factor van afhankelijkheid die de processen en de toegevoegde waarde van de organisatie kan belemmeren en dus moet worden vermeden. Open source is een van de manieren om uit deze lock-in te komen. Open source speelt een belangrijke rol in digitale soevereiniteit, omdat het een grotere keuze tussen oplossingen, leveranciers en integratoren en meer controle over IT-roadmaps mogelijk maakt.

Er zij op gewezen dat digitale soevereiniteit geen kwestie van vertrouwen is: we moeten uiteraard onze partners en providers vertrouwen, maar de relatie wordt nog beter wanneer zij gebaseerd is op wederzijdse instemming en erkenning, in plaats van op gedwongen contracten en spanningen.

Hier zijn enkele voordelen van een betere digitale soevereiniteit:

- De organisatie beter in staat te stellen haar eigen keuzes te maken zonder beperkingen.
- De veerkracht van het bedrijf ten aanzien van externe actoren en factoren verbeteren.
- Verbetering van de onderhandelingspositie in de omgang met partners en dienstverleners.

### Beoordeling van kansen

- Hoe moeilijk/duur is het om van een oplossing af te stappen?
- Kunnen de aanbieders van oplossingen ongewenste voorwaarden stellen aan hun dienst (bv. wijziging van de licentie, bijwerking van de contracten)?
- Kunnen de aanbieders van oplossingen eenzijdig hun prijzen verhogen, gewoon omdat we geen keuze hebben?

### Voortgangsbeoordeling

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] Er is een beoordeling van kritische afhankelijkheden voor de leveranciers en partners van de organisatie.
- [ ] Er is een back-up plan voor deze geïdentificeerde afhankelijkheden.
- [ ] Bij het onderzoek naar nieuwe oplossingen wordt de digitale soevereiniteit geëist.

### Aanbevelingen

- Identificeren van belangrijke afhankelijkheidsrisico's van dienstverleners en entiteiten van derden.
- Een lijst bijhouden van open source alternatieven voor kritieke diensten.
- Een vereiste toevoegen bij de selectie van nieuwe instrumenten en diensten die binnen de entiteit worden gebruikt, met vermelding van de noodzaak van digitale soevereiniteit.

### Middelen

- [A Primer on Digital Sovereignty & Open Source: part I](https://www.opensourcerers.org/2021/08/09/a-promer-on-digital-sovereignty/) en [A Primer on Digital Sovereignty & Open Source: part II](https://www.opensourcerers.org/2021/08/16/a-primer-on-digital-sovereignty-open-source/), van de Open-Sourcerers website.
- Een uitstekend superuser.openstack.org artikel over [De rol van Open Source in digitale soevereiniteit](https://superuser.openstack.org/articles/the-role-of-open-source-in-digital-sovereignty-openinfra-live-recap/). Hier is een kort uittreksel:
   > Digitale soevereiniteit is een belangrijk aandachtspunt voor de 21e eeuw, vooral voor Europa. Open source kan een belangrijke rol spelen bij het mogelijk maken van digitale soevereiniteit, door iedereen toegang te geven tot de noodzakelijke technologie, maar ook door te zorgen voor de bestuurlijke transparantie en interoperabiliteit die nodig zijn voor het welslagen van die oplossingen.
- De visie van de Europese Unie op digitale soevereiniteit, van het [Open Source Observatory (OSOR)](https://joinup.ec.europa.eu/collection/open-source-observatory-osor): Open Source, digitale soevereiniteit en interoperabiliteit: De Verklaring van Berlijn.
- Het standpunt van UNICEF over [Open Source for Digital Sovereignty](https://www.unicef.org/innovation/stories/open-source-digital-sovereignty).
