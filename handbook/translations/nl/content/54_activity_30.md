## Ondersteun open source communities

Activity ID: [GGI-A-30](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_30.md).

### Beschrijving

Deze activiteit gaat over het contact met institutionele vertegenwoordigers van de open source wereld.

Het wordt bereikt door:

- Aansluiting bij OSS-stichtingen (inclusief de financiële kosten van het lidmaatschap).
- Ondersteunende, pleitende stichtingen activiteiten.

Deze activiteit houdt in dat de ontwikkelings- en IT-teams wat tijd en budget krijgen om deel te nemen aan open-sourcegemeenschappen.

### Beoordeling van kansen

Opensourcegemeenschappen lopen voorop in de evolutie van het open source ecosysteem. Samenwerken met opensourcegemeenschappen heeft verschillende voordelen:

- het helpt om op de hoogte en up-to-date te blijven,
- het versterkt het profiel van de organisatie,
- lidmaatschap komt met voordelen,
- het biedt extra structuur en motivatie aan het open source IT-team.

De kosten omvatten:

- lidmaatschapsgeld,
- personeelstijd en enig reisbudget voor deelname aan activiteiten binnen de community,
- toezicht op de IP-verbintenissen.

### Voortgangsbeoordeling

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] De organisatie is een ondertekend lid van een open source stichting.
- [ ] De organisatie neemt deel aan het bestuur.
- [ ] Door de organisatie ontwikkelde software is ingediend bij / toegevoegd aan de codebase van een stichting.
- [ ] Het lidmaatschap wordt erkend op de websites van zowel de organisatie als de gemeenschap.
- [ ] Kosten-batenanalyse van het lidmaatschap uitgevoerd.
- [ ] Er is een contactpersoon voor de gemeenschap aangewezen.

### Aanbevelingen

- Word lid van een gemeenschap die past bij uw omvang en middelen, d.w.z. een gemeenschap die uw stem kan horen en waar u een erkende bijdrage kunt leveren.

### Middelen

- Bekijk deze [nuttige pagina](https://www.linuxfoundation.org/tools/participating-in-open-source-communities/) van de Linux Foundation over het waarom en hoe van een open source gemeenschap.

### Voorgestelde volgende activiteiten

- [GGI-A-31 - Publiekelijk bevestigen van het gebruik van open source](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Nu je officieel een aantal open source gemeenschappen steunt, maak het bekend! Het is goed voor je reputatie, en het is goed voor de gezondheid en verspreiding van je project.
