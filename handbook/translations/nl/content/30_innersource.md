# InnerSource

InnerSource groeit tegenwoordig in populariteit binnen bedrijven, omdat het een aanpak biedt die is gebaseerd op succesvolle open source gebruiken binnen de ontwikkelingsteams van organisaties. InnerSource uitvoeren is echter niet alleen maar kopiëren en plakken van deze praktijken. Ze moeten worden aangepast aan de unieke cultuur en interne organisatie van bedrijven. Laten we eens nader bekijken wat InnerSource wel en niet is, en wat de bijbehorende uitdagingen zijn.

## Wat is InnerSource?

De term werd voor het eerst gebruikt door Tim O'Reilly in 2000, waarin hij stelde dat Innersourcing "[…] *het beste gebruik van open source ontwikkeltechnieken binnen het bedrijf.*" is

Volgens [InnerSource Commons](https://innersourcecommons.org/learn/books/innersource-patterns/), de referentiebasis over dit onderwerp, is InnerSource het "*gebruik van open source principes en -praktijken voor softwareontwikkeling binnen de grenzen van een organisatie.*"

## Waarom InnerSource?

Nog steeds volgens [InnerSource Commons](https://innersourcecommons.org/learn/books/innersource-patterns/): "*voor bedrijven die voornamelijk closed source software bouwen, kan InnerSource een geweldig hulpmiddel zijn om silo's te helpen afbreken, interne samenwerking aan te moedigen, de introductie van nieuwe engineers te versnellen en de mogelijkheden te identificeren om software terug te brengen naar de open source wereld.*"

Het is interessant om op te merken dat de voordelen van InnerSource van invloed kunnen zijn op verschillende functies binnen een bedrijf, en niet alleen op software engineering. Als gevolg hiervan hebben sommige bedrijven concrete voordelen gevonden op gebieden zoals:

* Juridische functies: het versnellen van de totstandkoming van cross-functionele samenwerkingen door het gebruik van een kant-en-klaar juridisch raamwerk (InnerSource-licentie).
* Human Resources: beheer van de schaarse vaardigheden via een centraal ervaren kernteam welke verantwoordelijk is voor het bundelen van inspanningen en expertise.

## De InnerSource controverse

InnerSource wordt omgeven door veelvoorkomende mythes welke je kunt horen van tegenstanders van het principe. Hoewel het geen echte open source is, laat het grote potentiële voordelen zien voor organisaties die een dergelijke aanpak intern inzetten. Hier zijn enkele van deze mythes:

* [MYTH] InnerSource gaat ten koste van open source (voornamelijk uitgaand):
   * Softwareprojecten blijven achter de bedrijfsfirewall.
   * Minder externe bijdragen aan open source.
* [MYTHE] De geest van open source kapen versus deze geest benaderen.
* [MYTHE] Geen enkel InnerSource-project is ooit een open source project geworden.
* [MYTHE] Het motief om InnerSource toe te passen is dat het vergelijkbaar is met open source. Maar als een ontwikkelaar er waarde aan hecht, zou een directe open source bijdrage in werkelijkheid altijd de voorkeur moeten hebben.

Hier zijn enkele feiten over de praktijk van InnerSource die de meeste van de voorgaande mythes doorbreken:

* [FEIT] InnerSource is een manier om meer gesloten bedrijven welkom te heten in de open source.
* [FEIT] Hoewel het merendeel van de open source bijdragen door vrijwilligers wordt gedaan, kunnen we de deelname aan de open source aan engineers bekendmaken met behulp van deze lijst met “waargenomen voordelen”.
* [FEIT] In sommige (of de meeste?) gevallen volgen bedrijven geen ordelijke en gecontroleerde ontwikkelingspraktijk en dit (GGI) kan een manier zijn om hen te helpen dit te beheren.
* [FEIT] Het zal nog steeds *veel* werk vergen bij de conversie van gesloten naar open licenties.
* [FEIT] Er zijn inderdaad gevallen waarin het InnerSource project open source is:
   * Twitter Bootstrap.
   * Kubernetes van Google.
   * Docker van dotCloud (oude naam van Docker Inc.).
   * Reageer "native".
* [FEIT] Open source profiteert van de toename van software engineers die vertrouwd raken met open source, aangezien die van InnerSource erg op elkaar lijken.

## Wie doet het?

Veel bedrijven hebben InnerSource initiatieven of een ISPO (InnerSource Program Office) gelanceerd, sommige al langere tijd, andere recenter. Hier is een niet volledige lijst, voornamelijk gericht op Europese bedrijven:

* Banco de Santander ([source](https://patterns.innersourcecommons.org/p/innersource-portal))
* BBC ([source](https://www.youtube.com/watch?v=pEGMxe6xz-0))
* Bosch ([source](https://web.archive.org/web/20230429145619/https://www.bosch.com/research/know-how/open-and-inner-source/))
* Comcast ([source](https://www.youtube.com/watch?v=msD-8-yrGfs&t=6s))
* Ericsson ([source](https://innersourcecommons.org/learn/books/adopting-innersource-principles-and-case-studies/))
* Engie ([source](https://github.com/customer-stories/engie))
* IBM ([source](https://resources.github.com/innersource/fundamentals/))
* Mercedes ([source](https://www.youtube.com/watch?v=hVcGABbmI4Y))
* Microsoft ([source](https://www.youtube.com/watch?v=eZdx5MQCLA4))
* Nike ([source](https://www.youtube.com/watch?v=srPG-Tq0HIs&list=PLq-odUc2x7i-A0sOgr-5JJUs5wkgdiXuR&index=46))
* Nokia ([source](https://www.nokia.com/thought-leadership/articles/openness/openness-drives-innovation/))
* SNCF Connect & Tech ([source 1](https://twitter.com/FrancoisN0/status/1645356213712846853), [source 2](https://www.slideshare.net/FrancoisN0/opensource-innersource-pour-acclrer-les-dveloppements))
* Paypal ([source](https://innersourcecommons.org/fr/learn/books/getting-started-with-innersource/))
* Philips([source](https://medium.com/philips-technology-blog/how-philips-used-innersource-to-maintain-its-edge-in-innovation-38c481e6fa03))
* Renault ([source](https://www.youtube.com/watch?v=aCbv46TfanA))
* SAP ([source](https://community.sap.com/topics/open-source/publications))
* Siemens([source](https://jfrog.com/blog/creating-an-inner-source-hub-at-siemens))
* Société Générale([source](https://github.com/customer-stories/societe-generale))
* Thales ([source](https://www.youtube.com/watch?v=aCbv46TfanA))
* VeePee([source](https://about.gitlab.com/customers/veepee))

## InnerSource Commons, een essentieel naslagwerk

Een actieve en levendige gemeenschap van InnerSource beoefenaars, die werkt volgens Open Source principes, is te vinden op [InnerSource Commons](https://innersourcecommons.org). Ze bieden veel nuttige bronnen om u op de hoogte te houden van de kwestie, waaronder [patterns](https://innersourcecommons.org/learn/patterns/), een [leertraject](https://innersourcecommons.org/leren/leertraject/) en enkele kleine e-books:

* [Getting Started with InnerSource](https://innersourcecommons.org/learn/books/getting-started-with-innersource/) door Andy Oram.
* [Understanding the InnerSource Checklist](https://innersourcecommons.org/learn/books/understanding-the-innersource-checklist/) door Silona Bonewald.

## Verschillen in de governance van InnerSource

InnerSource brengt specifieke uitdagingen met zich mee die in open source niet worden opgelost. Toch hebben de meeste organisaties die private software maken er al mee te maken:

* Toegewijde, bedrijfsspecifieke licentie voor Innersource projecten (voor grote bedrijven met meerdere rechtspersonen).
* Het publieke karakter van open source behoedt het voor uitdagingen op het gebied van interne verrekenprijzen. Het particuliere karakter van InnerSource stelt bedrijven die in verschillende rechtsgebieden actief zijn, bloot aan winstverschuivingsverplichtingen.
* De motivaties voor bijdragen zijn heel verschillend:
   * InnerSource heeft een kleinere pool van mogelijke bijdragers omdat deze beperkt is tot de eigen organisatie.
   * Het tonen van je professionele vaardigheden is een drijfveer om een bijdrage te leveren. InnerSource beperkt deze impact uitsluitend tot de grenzen van de organisatie.
   * Bijdragen aan het verbeteren van de samenleving is een andere drijfveer voor bijdragen die beperkt zijn in InnerSource.
   * Motivatie vergt daarom een grotere inspanning en zal meer afhankelijk zijn van beloningen en opdrachten.
   * Omgaan met angsten voor perfectionisme, zoals het impostor syndrome, is gemakkelijker in InnerSource vanwege de beperkte zichtbaarheid van de code.
* Uitbesteding van personeel komt vaker voor, wat de governance op verschillende manieren beïnvloedt.
* Het beoordelen van de geschiktheid van de onderneming is eenvoudiger voor InnerSource omdat het intern is ontwikkeld.
* Vindbaarheid dreigt een probleem te worden. Het indexeren van informatie heeft voor bedrijven minder prioriteit. Openbare zoekmachines zoals DuckDuckGo, Google of Bing doen veel beter werk dan InnerSource zal kunnen behalen.
* InnerSource verkeert in een iets betere positie om de export te controleren, omdat het in de eigen organisatie leeft.
* Grensbewaking van IP-lekken als broncode is nodig.

InnerSource blijft zich ontwikkelen naarmate meer bedrijven de principes overnemen en hun ervaringen delen. Een latere versie van dit handboek zal een samengestelde lijst bieden van de activiteiten van de GGI die relevant zijn voor InnerSource beoefenaars.
