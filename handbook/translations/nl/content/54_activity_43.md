## Open source inkoopbeleid

Activity ID: [GGI-A-43](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_43.md).

### Beschrijving

Deze activiteit gaat over het implementeren van een proces om open source software en diensten te selecteren, aan te schaffen en te kopen. Het gaat er ook om de werkelijke kosten van openbronsoftware en de voorzieningen daarvoor in aanmerking te nemen. OSS mag dan op het eerste gezicht "gratis" zijn, het is niet zonder interne en externe kosten zoals integratie, opleiding, onderhoud en ondersteuning.

Dit beleid vereist dat zowel open source als propriëtaire oplossingen symmetrisch worden bekeken bij de evaluatie van de prijs-kwaliteitverhouding als de optimale combinatie van de totale eigendomskosten en de kwaliteit. Daarom moet de IT-aankoopafdeling open sourceopties actief en eerlijk in overweging nemen, en er tegelijkertijd voor zorgen dat propriëtaire oplossingen bij aankoopbeslissingen op gelijke voet worden behandeld.

De voorkeur voor open source kan expliciet worden uitgesproken op basis van de intrinsieke flexibiliteit van de open source-optie wanneer er geen significant algemeen kostenverschil is tussen propriëtaire en open source-oplossingen.

Inkoopafdelingen moeten begrijpen dat bedrijven die ondersteuning bieden voor OSS meestal niet over de commerciële middelen beschikken om deel te nemen aan aanbestedingen, en hun beleid en processen voor open source-inkoop dienovereenkomstig aanpassen.

### Beoordeling van kansen

Verschillende redenen rechtvaardigen de inspanningen om een specifiek beleid voor open source-aankopen op te zetten:

- Het aanbod van commerciële openbronsoftware en -diensten groeit en kan niet worden genegeerd, en vereist de uitvoering van een specifiek aanbestedingsbeleid en -proces.
- Er is een groeiend aanbod van zeer concurrerende commerciële open source bedrijfsoplossingen voor bedrijfsinformatiesystemen.
- Zelfs nadat een gratis OSS-component is aangenomen en in een toepassing is geïntegreerd, moeten interne of externe middelen worden verstrekt om die broncode te onderhouden.
- Total Cost of Ownership (TCO) is vaak (hoewel niet noodzakelijk) lager voor FOSS-oplossingen: geen licentiekosten te betalen bij aankoop/upgrade, open markt voor dienstverleners, optie om de oplossing geheel of gedeeltelijk zelf te leveren.

### Voortgangsbeoordeling

Uit de volgende **verificatiepunten** blijkt dat op dit gebied vooruitgang is geboekt:

- [ ] In de nieuwe oproep tot het indienen van voorstellen wordt proactief om open source inzendingen gevraagd.
- [ ] De inkoopafdeling heeft een manier om open source versus propriëtaire oplossingen te evalueren.
- [ ] Er is een vereenvoudigde aanbestedingsprocedure voor openbronsoftware en -diensten ingevoerd en gedocumenteerd.
- [ ] Er is een goedkeuringsproces op basis van transversale expertise vastgesteld en gedocumenteerd.

### Aanbevelingen

- "Zorg ervoor dat u de expertise van uw IT-, DevOps-, cybersecurity-, risicomanagement- en inkoopteams aanboort bij het opstellen van het proces." (uit [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/).
- Het mededingingsrecht kan vereisen dat "open source" niet specifiek wordt vermeld.
- Selecteer vooraf technologie en ga dan naar een RFP voor maatwerk en ondersteunende diensten.

### Middelen

- [Decision factors for open source software procurement](http://oss-watch.ac.uk/resources/procurement-infopack): niet nieuw, maar toch een geweldig leesvoer van onze collega's van OSS-watch in het VK. Bekijk de [slides](http://oss-watch.ac.uk/files/procurement.odp).
- [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/): een recent stuk over open source inkoop met nuttige tips.

### Voorgestelde volgende activiteiten

- [GGI-A-33 - Samenwerken met leveranciers van open source](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) Door een inkoopbeleid te definiëren, kunt u de open source aanbieders en -gemeenschappen identificeren waar u aandacht aan wilt en daarmee samen te werken.
