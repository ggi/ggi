# Organisatie

## Terminologie

De blauwdruk van de OSS Good Governance-methodiek is gestructureerd rond vier sleutelconcepten: Doelen, Basisactiviteiten, Aangepaste Activiteiten-scorecards en Iteratie.

* **Doelen**: Een Doel is een reeks activiteiten die verband houden met een gemeenschappelijk aandachtsgebied, er zijn vijf doelen: Gebruik, Vertrouwen, Cultuur, Engagement en Strategie. Doelen kunnen onafhankelijk, parallel en iteratief worden bereikt door middel van Activiteiten.
* **Basisactiviteiten**: binnen een Doel behandelt een Activiteit een enkel aandachtspunt of ontwikkelingsonderwerp -- zoals compliance -- dat kan worden gebruikt als een stapsgewijze stap naar de doelstellingen van het programma. De volledige reeks Activiteiten zoals gedefinieerd door de GGI worden de Basisactiviteiten genoemd.
* **Aangepaste Activiteitenrapport**: Om GGI in een bepaalde organisatie te implementeren, moeten de Basisactiviteiten worden aangepast aan de specifieke kenmerken van de context, waardoor een reeks aangepaste activiteitenscorekaarten worden opgebouwd. De aangepaste activiteitenscorekaart beschrijft hoe de activiteit zal worden uitgevoerd in de context van de organisatie en hoe de vooruitgang zal worden gecontroleerd.
* **Iteratie**: De OSS Good Governance-methodiek is een managementsysteem en vereist als zodanig periodieke evaluatie, en revisie. Denk aan het boekhoudsysteem in een organisatie, het is een continu proces met ten minste één jaarlijks controlepunt, de balans; op dezelfde manier vereist het OSS Good Governance proces ten minste een jaarlijkse revisie, maar revisies kunnen gedeeltelijk of vaker plaatsvinden, afhankelijk van de activiteiten.

## Doelstellingen

De door de GGI gedefinieerde Basisactiviteiten zijn georganiseerd in doelen. Elk doel heeft betrekking op een specifiek voortgangsgebied binnen het proces. Van gebruik tot strategie, de doelen hebben betrekking op alle belanghebbenden, van ontwikkelingsteams tot het management op C-niveau.

* **Gebruik** Doel: Dit doel heeft betrekking op de basisstappen in het gebruik van open source software. De activiteiten in verband met het gebruiksdoel bestrijken de eerste stappen van een open source-programma, door na te gaan hoe efficiënt open source wordt gebruikt en wat het de organisatie oplevert. Het omvat training en kennisbeheer, het maken van inventarisaties van bestaande open source die al intern wordt gebruikt, en presenteert enkele open source-concepten die in het hele proces kunnen worden gebruikt.
* **Vertrouwen** Doel: Dit doel betreft het veilig gebruik van open source. Het doel vertrouwen gaat over compliance, beheer van afhankelijkheid en kwetsbaarheid en beoogt in het algemeen vertrouwen op te bouwen in de manier waarop de organisatie open source gebruikt en beheert.
* **Cultuur** Doel: De culturele doelstelling omvat activiteiten die erop gericht zijn teams vertrouwd te maken met open source, individuele deelname aan samenwerkingsactiviteiten, begrip en toepassing van beste praktijken inzake open source. Deze doelstelling bevordert een gevoel van verbondenheid met de open source gemeenschap onder individuen.
* **Engagement** Doel: Dit doel is de betrokkenheid bij het open source ecosysteem op bedrijfsniveau. Personele en financiële middelen worden gebudgetteerd om bij te dragen aan open source projecten. Hier bevestigt de organisatie dat ze een verantwoordelijke open source burger is en erkent ze haar verantwoordelijkheid om de duurzaamheid van het open source ecosysteem te verzekeren.
* **Strategie** doel: Dit doel gaat over het zichtbaar en aanvaardbaar maken van open source op de hoogste niveaus van het bedrijfsmanagement. Het gaat erom te erkennen dat open source een strategische facilitator is van digitale soevereiniteit, procesinnovatie en, in het algemeen, een bron van aantrekkelijkheid en goodwill.

## Basisactiviteiten

De Basisactiviteiten staan centraal in de GGI blauwdruk. In haar initiële versie voorziet de GGI methodologie vijf basisactiviteiten per doel, 25 in totaal. De basisactiviteiten worden beschreven aan de hand van de volgende voorgedefinieerde secties:

* *Beschrijving*: een samenvatting van het onderwerp van de activiteit en de stappen om deze te voltooien.
* *Beoordeling van kansen*: beschrijft waarom en wanneer het relevant is om deze activiteit te ondernemen.
* *Voortgangsbeoordeling*: beschrijft hoe de voortgang van de activiteit wordt gemeten en het succes ervan wordt beoordeeld.
* *Hulpmiddelen*: een lijst van technologieën of hulpmiddelen die kunnen helpen deze activiteit te verwezenlijken.
* *Aanbevelingen*: tips en beste praktijken verzameld bij GGI-deelnemers.
* *Bronnen*: links en referenties om meer te lezen over het onderwerp van de activiteit.

### Beschrijving

Dit deel geeft een beschrijving op hoog niveau van de activiteit, een samenvatting van het onderwerp om het doel van de activiteit in de context van de openbronaanpak binnen een doel te plaatsen.

### Beoordeling van kansen

Om een iteratieve aanpak te helpen structureren, heeft elke activiteit een onderdeel "opportuniteitsbeoordeling", met daaraan gekoppeld een of meer vragen. De beoordeling van de gelegenheid richt zich op de vraag waarom het relevant is deze activiteit te ondernemen, in welke behoeften deze voorziet. De beoordeling van de gelegenheid zal helpen bepalen welke inspanningen worden verwacht, welke middelen nodig zijn, en helpen de kosten en het verwachte rendement te evalueren.

### Voortgangsbeoordeling

Deze stap richt zich op het definiëren van doelstellingen, KPI's (Key Performance Idicators[^kpi]), en op het verstrekken van *verificatiepunten* die helpen de vooruitgang in de activiteit te evalueren. De verificatiepunten worden voorgesteld, ze kunnen helpen bij het definiëren van een routekaart voor het proces van goed bestuur, de prioriteiten ervan en hoe de vooruitgang zal worden gemeten.

### Hulpmiddelen

Hier worden hulpmiddelen genoemd die kunnen helpen bij het uitvoeren van de activiteit of een specifieke stap van de activiteiten. De hulpmiddelen zijn geen verplichte aanbeveling, en pretenderen ook niet volledig te zijn, maar zijn suggesties of categorieën die op basis van de bestaande context kunnen worden uitgewerkt.

### Aanbevelingen

Deze rubriek wordt regelmatig bijgewerkt met feedback van gebruikers en allerlei aanbevelingen die kunnen helpen bij het beheer van de activiteit.

### Middelen

Er worden middelen voorgesteld om de aanpak te voeden met achtergrondstudies, referentiedocumenten, evenementen of online-inhoud om de gerelateerde aanpak van de activiteit te verrijken en te ontwikkelen. De bronnen zijn niet uitputtend; het zijn startpunten of suggesties om de semantiek van de activiteit uit te breiden volgens de eigen context.

## Aangepaste activiteitenscorekaarten

Aangepaste activiteiten rapporten zijn iets gedetailleerder dan basisactiviteiten. Een AAR bevat details die specifiek zijn voor de organisatie die GGI implementeert. Het gebruik van de AAR wordt beschreven in de sectie methodologie.

[^kpi]: Een prestatie indicator of key performance indicator is een vorm van prestatiemeting. KPI's evalueren de voortgang en het succes van een organisatie of van een bepaalde activiteit waarbij zij betrokken is.
