## Neem deel aan open source projecten

Activity ID: [GGI-A-29](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_29.md).

### Beschrijving

Deze activiteit gaat over het committeren van significante bijdragen aan enkele OSS-projecten die belangrijk voor je zijn. Bijdragen worden opgeschaald en toegezegd op organisatieniveau (niet op persoonlijk niveau zoals in #26). Ze kunnen verschillende vormen aannemen, van directe financiering tot toewijzing van middelen (bv. mensen, servers, infrastructuur, communicatie, enz.), zolang ze het project of ecosysteem duurzaam en efficiënt ten goede komen.

Deze activiteit is een vervolg op activiteit #26 en brengt de bijdragen van open source projecten naar het niveau van de organisatie, waardoor ze zichtbaarder, krachtiger en nuttiger worden. Bij deze activiteit worden de bijdragen geacht een substantiële, langdurige verbetering voor het OSS-project op te leveren: bv. een ontwikkelaar of team dat een veelgevraagde nieuwe functie ontwikkelt, infrastructuuractiva, servers voor een nieuwe dienst, overname van het onderhoud van een veelgebruikte tak.

Het idee is om een percentage van de middelen opzij te zetten om open source ontwikkelaars te sponsoren die bibliotheken of projecten die wij gebruiken, schrijven en onderhouden.

Deze activiteit houdt in dat de gebruikte openbronsoftware in kaart moet worden gebracht en dat de kriticiteit ervan moet worden geëvalueerd om te beslissen welke software moet worden ondersteund.

### Beoordeling van kansen

> Als elk bedrijf dat open source gebruikt ten minste een beetje zou bijdragen, zouden we een gezond ecosysteem hebben. <https://news.ycombinator.com/item?id=25432248>

Ondersteuning van projecten draagt bij tot de duurzaamheid ervan en biedt toegang tot informatie, en kan zelfs helpen bij het beïnvloeden en prioriteren van sommige ontwikkelingen (hoewel dit niet de belangrijkste reden voor ondersteuning van projecten mag zijn).

Mogelijke voordelen van deze activiteit: ervoor zorgen dat prioriteit wordt gegeven aan bugrapporten en dat ontwikkelingen worden geïntegreerd in de stabiele versie. Mogelijke kosten van deze activiteit: tijd aan projecten besteden, geld vastleggen.

### Voortgangsbeoordeling

Uit de volgende **verificatiepunten** blijkt dat op dit gebied vooruitgang is geboekt:

- [ ] Project begunstigde geïdentificeerd.
- [ ] Ondersteuningsoptie besloten, zoals directe geldelijke bijdrage of bijdrage in code.
- [ ] Taakleider aangewezen.
- [ ] Er is een bijdrage geleverd.
- [ ] Het resultaat van de bijdrage is geëvalueerd.

Vragenlijst met verificatiepunten geleend van OpenChain [zelfcertificering](https://certification.openchainproject.org/):

- [ ] Wij hebben een beleid voor bijdragen aan open source projecten namens de organisatie.
- [ ] We hebben een gedocumenteerde procedure voor open source bijdragen.
- [ ] Wij hebben een gedocumenteerde procedure om alle softwaremedewerkers bewust te maken van het beleid inzake open source bijdragen.

### Hulpmiddelen

Sommige organisaties bieden mechanismen aan voor de financiering van open source projecten (het kan handig zijn als uw doelproject in hun portefeuille zit).

- [Open Collective](https://opencollective.com/).
- [Software Freedom Conservancy](https://sfconservancy.org/).
- [Tidelift](https://tidelift.com/).

### Aanbevelingen

- Concentreer u op projecten die cruciaal zijn voor de organisatie: dit zijn de projecten die u het meest wilt helpen met uw bijdragen.
- Gerichte gemeenschapsprojecten.
- Deze activiteit vereist een minimale vertrouwdheid met een doelproject.

### Middelen

- [How to support open source projects now](https://sourceforge.net/blog/support-open-source-projects-now/): Een korte pagina met ideeën over het financieren van open source projecten.
- [Sustain OSS: a space for conversations about sustaining open source](https://sustainoss.org)

### Voorgestelde volgende activiteiten

- [GGI-A-26 - Bijdragen aan open source projecten](https://ospo-alliance.org/ggi/activities/contribute_to_open_source_vendors) De meest natuurlijke manier om deel te nemen aan een open source initiatief is door rechtstreeks bij te dragen aan het project. In ruil daarvoor verzamel je waardevolle feedback op jouw bijdragen.
- [GGI-A-30 - Ondersteuning van open source gemeenschappen](https://ospo-alliance.org/ggi/activities/support_open_source_communities) Er zijn veel manieren om de open source initiatieven te ondersteunen die essentieel zijn voor uw organisatie. Betrokken raken bij de gemeenschappen is een goede manier om ze te ontdekken en te koesteren.
