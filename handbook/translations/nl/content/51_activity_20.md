## Open source bedrijfssoftware

Activiteit ID: [GGI-A-20](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_20.md).

### Beschrijving

Deze activiteit gaat over het proactief selecteren van OSS-oplossingen, hetzij door leveranciers, hetzij door de gemeenschap ondersteund, op bedrijfsgerichte gebieden. Zij kan ook betrekking hebben op het vaststellen van voorkeursbeleid voor de selectie van open source software voor bedrijfstoepassingen.

Hoewel open source software meestal wordt gebruikt door IT professionals -- besturingssysteem, middleware, DBMS, systeembeheer, ontwikkeltools -- moet het nog worden erkend in gebieden waar business professionals de primaire gebruikers zijn.

De activiteit betreft gebieden zoals: Office-suites, samenwerkingsomgevingen, gebruikersbeheer, workflowbeheer, klantenbeheer, e-mail, e-commerce, enz.

### Beoordeling van kansen

Naarmate open source mainstream wordt, reikt het veel verder dan besturingssystemen en ontwikkeltools, het vindt steeds meer zijn weg naar de bovenste lagen van de informatiesystemen, tot in de bedrijfsapplicaties. Het is relevant na te gaan welke OSS-toepassingen met succes worden gebruikt om aan de behoeften van de organisatie te voldoen en hoe zij de kostenbesparende voorkeurskeuze van een organisatie kunnen worden.

De activiteit kan enige omscholings- en migratiekosten met zich meebrengen.

### Voortgangsbeoordeling

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] Er is een lijst van aanbevolen OSS-oplossingen om in te spelen op hangende behoeften in zakelijke toepassingen.
- [ ] Er wordt een voorkeursbeleid voor de selectie van open source software voor bedrijfsapplicaties opgesteld.
- [ ] In gebruik zijnde bedrijfstoepassingen worden geëvalueerd ten opzichte van OSS-equivalenten.
- [ ] Bij aanbestedingen en oproepen tot het indienen van voorstellen wordt de voorkeur gegeven aan open source (indien wettelijk mogelijk).

### Hulpmiddelen

In dit stadium kunnen wij geen instrument bedenken dat relevant is voor of betrokken is bij deze activiteit.

### Aanbevelingen

- Praat met collega's, leer van wat andere bedrijven doen die vergelijkbaar zijn met het uwe.
- Bezoek lokale branche-evenementen om meer te weten te komen over OSS-oplossingen en professionele ondersteuning.
- Probeer eerst gemeenschapsedities en gemeenschapsondersteuning uit voordat u zich vastlegt op betaalde ondersteuningsplannen.

### Middelen

- [Wat is enterprise open source?](https://www.redhat.com/en/blog/what-enterprise-open-source): een snelle lezing over enterprise-ready open source.
- [101 Open Source Apps om uw bedrijf te helpen bloeien](https://digital.com/creating-an-llc/open-source-business/): Een indicatieve lijst van bedrijfsgerichte open source oplossingen.

### Voorgestelde volgende activiteiten

- [GGI-A-33 - Samenwerken met leveranciers van open source](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) Verbeter het vertrouwen in uw open source middelen door samen te werken met open source professionals.
- [GGI-A-43 - Open source inkoopbeleid](https://ospo-alliance.org/ggi/activities/open_source_procurement_policy) Het gebruik van open source zal worden geoptimaliseerd door te weten welke assets er al zijn en door hierover een duidelijk inkoopbeleid te voeren.
