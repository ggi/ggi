## Zet een strategie op voor de governance van open source binnen bedrijven

Activity ID: [GGI-A-16](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_16.md).

### Beschrijving

Het definiëren van een strategie op hoog niveau voor open source governance binnen het bedrijf zorgt voor consistentie en zichtbaarheid van de aanpak van zowel intern gebruik als externe bijdragen en betrokkenheid. Het maakt de communicatie van het bedrijf effectiever door een duidelijke en gevestigde visie en leiderschap te bieden.

De verschuiving naar open source brengt tal van voordelen met zich mee, maar ook enkele plichten en een verandering van de bedrijfscultuur. Het kan invloed hebben op bedrijfsmodellen en op de manier waarop een organisatie haar waarde en aanbod presenteert, en op haar positie ten opzichte van klanten en concurrenten.

Deze activiteit omvat de volgende taken:

- Stel een OSS-functionaris aan, met sponsoring en steun van het (top)management.
- Een duidelijke routekaart voor open source opstellen en publiceren, met duidelijke doelstellingen en verwachte voordelen.
- Zorg ervoor dat het hele topmanagement ervan op de hoogte is en ernaar handelt.
- OSS binnen het bedrijf bevorderen: mensen aanmoedigen om het te gebruiken, interne initiatieven en kennisniveau stimuleren.
- OSS buiten het bedrijf promoten: via officiële verklaringen en communicatie, en zichtbare betrokkenheid bij OSS initiatieven.

Het definiëren, publiceren en handhaven van een duidelijke en consistente strategie helpt ook de buy-in van alle mensen binnen het bedrijf en vergemakkelijkt verdere initiatieven van teams.

### Beoordeling van kansen

Het is een goed moment om aan deze activiteit te werken als:

- Er is geen gecoördineerde inspanning van het management, en open source wordt nog steeds gezien als een ad-hocoplossing.
- Er bestaan al interne initiatieven, maar die dringen niet door tot de hogere managementniveaus.
- Het initiatief is enige tijd geleden gestart, maar stuit op veel obstakels en levert nog steeds niet de verwachte resultaten op.

### Voortgangsbeoordeling

De volgende **verificatiepunten** tonen de vooruitgang in deze activiteit aan:

- [ ] Er is een duidelijk open source governance handvest voor het bedrijf. Het handvest moet bevatten:
   - wat te bereiken,
   - voor wie we dit doen,
   - wat de macht van de strateeg is en wat niet.
- [ ] Een open source roadmap is algemeen beschikbaar en geaccepteerd in het hele bedrijf.

### Aanbevelingen

- Zet een groep mensen en processen op om open source governance binnen het bedrijf te definiëren en te bewaken. Zorg voor een duidelijke betrokkenheid van het topmanagement bij de open source initiatieven.
- Communiceer over de open source strategie binnen de organisatie, maak het een belangrijk aandachtspunt en een echte bedrijfsverbintenis.
- Ervoor zorgen dat de roadmap en de strategie door iedereen goed begrepen worden, van de ontwikkelingsteams tot het management en de infrastructuurmedewerkers.
- Communiceren over de voortgang, zodat mensen weten waar de organisatie staat wat betreft haar engagement. Regelmatige updates en indicatoren publiceren.

### Middelen

- [Checklist en referenties voor Open Governance](https://opengovernance.dev/).
- [L'open source comme enjeu de souveraineté numérique, door Cédric Thomas, OW2 CEO, Workshop bij Orange Labs, Parijs, 28 januari 2020](https://www.ow2.org/download/OSS_Governance/Level_5/2001-OSSetSouveraineteNumerique-RC3.pdf) (alleen in het Frans).
- [Een reeks gidsen om open source binnen de onderneming te beheren, door de Linux Foundation](https://todogroup.org/guides/).
- [Een mooi voorbeeld van een open source strategiedocument, door de LF Energy groep](https://www.lfenergy.org/wp-content/uploads/sites/67/2019/07/Open-Source-Strategy-V1.0.pdf)

### Voorgestelde volgende activiteiten

- [GGI-A-35 - Open source en digitale soevereiniteit](https://ospo-alliance.org/ggi/activities/open_source_and_digital_sovereignty) Een goede strategie voor de open source governance binnen bedrijven zou de open source- en digitale soevereiniteit moeten verbeteren. Dit is een goed moment om deze te definiëren binnen de context van de organisatie.
- [GGI-A-34 -Kennis op C-niveau](https://ospo-alliance.org/ggi/activities/c-level_awareness) De betrokkenheid van leidinggevenden op C-niveau zal nodig zijn om de open source bedrijfsstrategie op de juiste manier te implementeren. Hen opleiden en betrekken is een goede volgende stap om hier succes te boeken.
