## Softwareafhankelijkheden beheren

Activity ID: [GGI-A-23](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_23.md).

### Beschrijving

Een *programma voor de identificatie van afhankelijkheden* zoekt naar de werkelijk gebruikte afhankelijkheden binnen de codebase. Bijgevolg moet de organisatie een lijst van bekende afhankelijkheden voor haar codebase opstellen en bijhouden, en de evolutie van de geïdentificeerde providers in het oog houden.

Het opstellen en bijhouden van een lijst van bekende afhankelijkheden is een voorwaarde voor:

- intellectueel eigendom en licentiecontrole: sommige licenties kunnen niet worden gemengd, zelfs niet als afhankelijkheid. Men moet de afhankelijkheden kennen om de bijbehorende juridische risico's te kunnen beoordelen.
- Beheer van kwetsbaarheden: het hele stuk software is zo zwak als zijn zwaktse onderdeel: zie het voorbeeld van het [Heartbleed-fout](https://en.wikipedia.org/wiki/Heartbleed). Men moet de afhankelijkheden ervan kennen om de bijbehorende veiligheidsrisico's te kunnen beoordelen.
- Levenscyclus en duurzaamheid: een actieve gemeenschap op het afhankelijkheidsproject is een goed teken voor bugcorrecties, optimalisaties en nieuwe functies.
- Doordachte selectie van gebruikte afhankelijkheden, volgens criteria van "volwassenheid" - het doel is open source componenten te gebruiken die veilig zijn, met een gezonde en goed onderhouden codebase, en een levende, actieve en reactieve gemeenschap die externe bijdragen aanvaardt, enz.

### Beoordeling van kansen

Het identificeren en bijhouden van afhankelijkheden is een vereiste stap om de risico's van hergebruik van code te beperken. Daarnaast is het implementeren van hulpmiddelen en processen om softwareafhankelijkheden te beheren een eerste vereiste om de kwaliteit, naleving en compliance goed te beheren.

Overweeg de volgende vragen:

- Wat is het risico voor het bedrijf (kosten, reputatie, enz.) als de software wordt beschadigd, aangevallen of aangeklaagd?
- Wordt de codebase beschouwd als kritisch voor mensen, de organisatie of het bedrijf?
- Wat als een component waarvan een applicatie afhankelijk is zijn repository verandert?

De minimale en eerste stap is de implementatie van een software compositie analyse (SCA) tool. Ondersteuning door gespecialiseerde adviesbureaus kan nodig zijn voor een volwaardige SCA of dependency mapping.

### Voortgangsbeoordeling

Uit de volgende **verificatiepunten** blijkt dat op dit gebied vooruitgang is geboekt:

- [ ] In alle intern ontwikkelde code worden afhankelijkheden geïdentificeerd.
- [ ] Afhankelijkheden worden vastgesteld in alle externe code die binnen het bedrijf wordt uitgevoerd.
- [ ] Er is een eenvoudig op te zetten analyse van de softwaresamenstelling of procedure voor de identificatie van afhankelijkheden beschikbaar die projecten kunnen toevoegen aan hun Continuous Integration proces.
- [ ] Er worden instrumenten voor afhankelijkheidsanalyse gebruikt.

### Hulpmiddelen

- [OWASP Dependency check](https://github.com/jeremylong/DependencyCheck): dependency-Check is een hulpmiddel voor Software Composition Analysis (SCA) dat probeert openbaar gemaakte kwetsbaarheden in de afhankelijkheden van een project op te sporen.
- [OSS Review Toolkit](https://oss-review-toolkit.org/): een pakket hulpmiddelen voor het beoordelen van afhankelijkheden van Open Source Software.
- [Fossa](https://github.com/fossas/fossa-cli): snelle, draagbare en betrouwbare afhankelijkheidsanalyse. Ondersteunt het scannen van licenties en kwetsbaarheden. Taal-agnostisch; integreert met 20+ bouwsystemen.
- [Software 360](https://projects.eclipse.org/projects/technology.sw360).
- [Eclipse Dash license tool](https://github.com/eclipse/dash-licenses): neemt een lijst van afhankelijkheden en vraagt [ClearlyDefined](https://clearlydefined.io) om hun licenties te controleren.
- [Het FOSSology Project](https://www.fossology.org/): FOSSology is een open source project met als missie het bevorderen van de naleving van open source licenties.

### Aanbevelingen

- Regelmatig audits uitvoeren over de afhankelijkheden en intellectueel eigendom vereisten om juridische risico's te beperken.
- Idealiter integreert u afhankelijkhedenbeheer in het continue integratieproces, zodat problemen (nieuwe afhankelijkheid, licentie-incompatibiliteit) zo snel mogelijk worden geïdentificeerd en opgelost.
- Houd kwetsbaarheden in verband met afhankelijkheden bij en houd gebruikers en ontwikkelaars op de hoogte.
- Mensen informeren over de risico's van verkeerde licenties.
- Een eenvoudige oplossing voorstellen voor projecten om licentiecontrole op hun codebase in te stellen.
- Communiceer over het belang ervan en help projecten om het toe te voegen aan hun CI-systemen.
- Zet een zichtbare KPI op voor afhankelijkheidsrisico's.

### Middelen

- Bestaande [OSS-licentie nalevingshulpmiddelen](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence-Compliance-Tools/) groepspagina.
- [Free and Open Source Software licence Compliance: Tools for Software Composition Analysis](https://www.computer.org/csdl/magazine/co/2020/10/09206429/1npxG2VFQSk), door Philippe Ombredanne, nexB Inc.
- [Software Sustainability Maturity Model](http://oss-watch.ac.uk/resources/ssmm).
- [CHAOS](https://chaoss.community/): Community Health Analytics Open Source Software.

### Voorgestelde volgende activiteiten

- [GGI-A-21 - Beheren van compliance](https://ospo-alliance.org/ggi/activities/manage_legal_compliance) Voordat je IP- en licentie incompatibiliteiten kunt opsporen, moet je alle afhankelijkheden in hun open source software identificeren.
- [GGI-A-22 - Beheer van kwetsbaarheden van software](https://ospo-alliance.org/ggi/activities/manage_software_vulnerabilities/) Voordat we kwetsbaarheden in hun code kunnen opsporen, moeten alle afhankelijkheden in hun open source software worden geïdentificeerd.
