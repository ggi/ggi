## Software de empresa de código aberto

ID da atividade: [GGI-A-20](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_20.md).

### Descrição

Esta atividade é sobre selecionar proativamente soluções de OSS, quer sejam apoiadas por vendedores ou pela comunidade, em áreas orientadas a negócios. Pode também abranger a definição de políticas de preferência para a seleção de software de aplicações de código aberto para empresas.

Embora o software de código aberto seja mais frequentemente utilizado por profissionais de TI -- sistemas operativos, middleware, SGBD, administração de sistemas, ferramentas de desenvolvimento -- tem ainda de ser reconhecido em áreas onde profissionais de negócios são os utilizadores principais.

A atividade abrange áreas como: suítes Office, ambientes de colaboração, gestão de utilizadores, gestão de fluxos de trabalho, gestão de relações com clientes, correio eletrónico, comércio eletrónico, etc.

### Avaliação de oportunidades

À medida que o código aberto se torna a corrente dominante, alcança muito além dos sistemas operativos e ferramentas de desenvolvimento e assim cada vez mais encontra o seu caminho às camadas superiores dos sistemas de informação, no cerne das aplicações de negócio. É relevante identificar quais aplicações de OSS são usados com sucesso para atender às necessidades da organização e como podem tornar-se a escolha preferida de uma organização que economiza custos.

A atividade pode trazer alguns custos de retreino e migração.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] Existe uma lista de soluções OSS recomendadas para responder a necessidades pendentes em aplicações comerciais.
- [ ] Uma política de preferência para a seleção de software de aplicações de negócios de código aberto é elaborada.
- [ ] Aplicações comerciais proprietárias em uso são avaliadas em relação a OSS equivalentes.
- [ ] O processo de aquisição e solicitações especificam a preferência de código aberto (se legalmente viável).

### Ferramentas

Nesta fase, não podemos pensar em nenhuma ferramenta relevante ou em questão com esta atividade.

### Recomendações

- Converse com colegas, aprenda com o que outras empresas comparáveis à sua fazem.
- Visite eventos locais do setor para saber mais sobre as soluções de OSS e apoio profissional.
- Experimente primeiro as edições comunitárias e o apoio comunitário antes de se comprometer com planos de apoio pagos.

### Recursos

- [What is enterprise open source?](https://www.redhat.com/pt-br/blog/what-enterprise-open-source): uma leitura rápida sobre código aberto para empresas.
- [101 Open Source Apps to Help your Business Thrive](https://digital.com/creating-an-llc/open-source-business/): uma lista indicativa de soluções de código aberto para empresas.

### Novas atividades propostas

- [GGI-A-33 - Engage with open source vendors](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) Improve confidence in your open source assets by engaging with open source professionals.
- [GGI-A-43 - Open source procurement policy](https://ospo-alliance.org/ggi/activities/open_source_procurement_policy) OSS Enterprise usage will be optimised by knowing what assets are already there, and having a clear procurement policy on the matter.
