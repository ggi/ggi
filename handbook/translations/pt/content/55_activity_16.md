## Estabelecer uma estratégia para a governação empresarial de código aberto

ID da atividade: [GGI-A-16](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_16.md).

### Descrição

A definição de uma estratégia de alto nível para a governação de código aberto numa empresa assegura a consistência e a visibilidade das abordagens tanto para a utilização interna como para as contribuições e o envolvimento externos. Faz a comunicação da empresa mais eficaz, oferecendo uma visão e liderança claras e estabelecidas.

A mudança para o código aberto implica inúmeros benefícios, bem como algumas tarefas e uma mudança na cultura da empresa. Pode ter um impacto nos modelos de negócio e influenciar a forma como uma organização apresenta o valor e oferta dela e na posição em relação aos clientes e concorrentes.

Esta atividade inclui as seguintes tarefas:

- Criar um oficial do OSS, com patrocínio e apoio da direcção (de topo).
- Estabelecer e publicar um roteiro claro para o código aberto, com objetivos declarados e benefícios esperados.
- Assegurar que toda a gestão de alto nível o conhece e atua de acordo com ele.
- Promover o OSS na empresa: encorajar as pessoas a utilizá-lo, fomentar iniciativas internas e nível de conhecimento.
- Promover o OSS fora da empresa: através de declarações e comunicação oficiais, e envolvimento visível nas iniciativas do OSS.

A definição, publicação e aplicação de uma estratégia clara e consistente também ajuda à adesão de todas as pessoas na empresa e facilita outras iniciativas das equipas.

### Avaliação de oportunidades

É uma boa altura para trabalhar nesta actividade se:

- Não houver um esforço coordenado por parte da administração e o código aberto ainda for visto como uma solução improvisada.
- Já existem iniciativas internas, mas que não penetram até aos níveis superiores da administração.
- A iniciativa foi iniciada há algum tempo, mas enfrenta muitos obstáculos e ainda não produz os resultados esperados.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] Existe uma carta clara de governação de código aberto para a empresa. A carta deve conter:
   - o que alcançar,
   - por quem o fazemos,
   - qual o poder do(s) estratega(s) e o que não é.
- [ ] Um roteiro de código aberto está amplamente disponível e é aceite em toda a empresa.

### Recomendações

- Estabeleça um grupo de pessoas e processos para definir e monitorizar a governação de código aberto na empresa. Certifique-se de que haja um compromisso claro da gestão de alto nível com as iniciativas de código aberto.
- Comunique sobre a estratégia de código aberto na organização, faça-o uma grande preocupação e um verdadeiro compromisso empresarial.
- Assegure que o roteiro e a estratégia são bem compreendidos por todos, desde as equipas de desenvolvimento até ao pessoal da administração e infraestrutura.
- Comunique sobre progressos, para que as pessoas saibam onde a organização está relativamente aos compromissos feitos. Publique atualizações e indicadores frequentemente.

### Recursos

- [Lista de verificação e referências para a governação aberta](https://opengovernance.dev/).
- [L'open source comme enjeu de souveraineté numérique, by Cédric Thomas, OW2 CEO, Workshop nos Orange Labs, Paris, 28. janeiro 2020](https://www.ow2.org/download/OSS_Governance/Level_5/2001-OSSetSouveraineteNumerique-RC3.pdf) (em francês).
- [A series of guides to manage open source within the enterprise, by the Linux Foundation](https://todogroup.org/guides/).
- [A fine example of open source strategy document, by the LF Energy group](https://www.lfenergy.org/wp-content/uploads/sites/67/2019/07/Open-Source-Strategy-V1.0.pdf)

### Novas atividades propostas

- [GGI-A-35 - Open source and digital sovereignty](https://ospo-alliance.org/ggi/activities/open_source_and_digital_sovereignty) A proper strategy for corporate open source governance should improve open source and digital sovereignty. Now is a good time to define these in the context of the organisation.
- [GGI-A-34 - C-Level awareness](https://ospo-alliance.org/ggi/activities/c-level_awareness) C-level executives engagement will be needed to properly implement the open source corporate strategy. Educating and involving them is a good next step to achieve success here.
