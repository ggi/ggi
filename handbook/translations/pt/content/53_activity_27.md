## Pertencer à comunidade de código aberto

ID da atividade: [GGI-A-27](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_27.md).

### Descrição

Esta atividade trata-se de desenvolver um maior sentimento de pertença a uma comunidade de código aberto entre os programadores. Como em qualquer comunidade, pessoas e entidades têm de participar e contribuir de volta para o coletivo. Reforça as ligações entre profissionais e traz sustentabilidade e atividade ao ecossistema. Numa vertente mais técnica, permite a escolha de prioridades e do roteiro dos projetos, melhora o nível de conhecimento geral e a consciência técnica.

Esta atividade abrange o seguinte:

- **Identificar eventos** que valham a pena participar. Conectar pessoas, aprender sobre novas tecnologias e construir uma rede são fatores-chave para obter todos os benefícios do software livre.
- Ponderar **associações a fundações***. As fundações e organizações de código aberto são uma componente chave do ecossistema de código aberto. Fornecem recursos técnicos e organizacionais a projetos e são um bom lugar neutro para os patrocinadores discutirem questões e soluções comuns, ou trabalharem em normas.
- Observar **grupos de trabalho***. Os grupos de trabalho são um espaço de trabalho colaborativo neutro onde os peritos interagem num domínio específico como a Internet das Coisas, a modelização ou a ciência. São um mecanismo muito eficiente e rentável para abordar preocupações comuns em conjunto, apesar de específicas do domínio.
- **Participação no orçamento***. No final da viagem, o dinheiro é o facilitador. Planear as despesas necessárias, dar tempo às pessoas pagas para estas atividades, antecipar os próximos movimentos, para que o programa não tenha de parar após alguns meses sem financiamento.

### Avaliação de oportunidades

O código aberto funciona melhor quando feito com a comunidade de código aberto em geral. Facilita a correção de bugs, partilha de soluções, etc.

É também uma boa forma de as empresas mostrarem o seu apoio aos valores do código aberto. A comunicação sobre o envolvimento da empresa é importante tanto para a reputação da empresa como para o ecossistema de código aberto.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] É elaborada uma lista de eventos a que as pessoas poderiam assistir.
- [ ] Há monitorização dos discursos públicos dados pelos membros da equipa.
- [ ] As pessoas podem enviar solicitações de participação em eventos.
- [ ] As pessoas podem apresentar solicitações de participação em eventos.

### Recomendações

- Pesquise as pessoas para conhecer que eventos gostam ou seriam as mais benéficas para o trabalho.
- Crie comunicação interna (boletim informativo, centro de recursos, convites...) para que as pessoas conheçam as iniciativas e possam participar.
- Certifique-se de que essas iniciativas podem beneficiar vários tipos de pessoas (programadores, administradores, suporte...), não apenas a administração superior.

### Recursos

- [What motivates a developer to contribute to open source software?](https://clearcode.cc/blog/why-developers-contribute-open-source-software/) Um artigo de Michael Sweeney no clearcode.cc.
- [Why companies contribute to open source](https://blogs.vmware.com/opensource/2020/12/01/why-companies-contribute-to-open-source/) Um artigo de Velichka Atanasova da VMWare.
- [Why your employees should be contributing to open source](https://www.cloudbees.com/blog/why-your-employees-should-be-contributing-to-open-source/) Uma boa leitura por Robert Kowalski de CloudBees.
- [7 ways your company can support open source](https://www.infoworld.com/article/2612259/7-ways-your-company-can-support-open-source.html) Um artigo de Simon Phipps para a InfoWorld.
- [Events: the life force of open source](https://www.redhat.com/en/blog/events-life-force-open-source) Um artigo de Donna Benjamin da RedHat.

### Novas atividades propostas

- [GGI-A-28 - HR perspective](https://ospo-alliance.org/ggi/activities/human_resources_perspective) If the organisation belongs to the OSS community, it is easier to attract skilled people, based on the community you are involved in.
- [GGI-A-31 - Publicly assert use of open source](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Now that you have become a part of the OSS community, make it known! It is good for your reputation, and it is good for the project in terms of health and dissemination.
