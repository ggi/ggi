## Envolver-se em projetos de código aberto

ID da atividade: [GGI-A-29](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_29.md).

### Descrição

Esta atividade trata-se de cometer contribuições significativas para alguns projetos do OSS que são importantes para si. As contribuições são aumentadas e comprometidas a nível da organização (não a nível pessoal como em #26). Podem assumir várias formas, desde o financiamento direto à alocação de recursos (por exemplo, pessoas, servidores, infraestruturas, comunicação, etc.), desde que beneficiem o projeto ou o ecossistema de forma sustentável e eficiente.

Esta atividade é um seguimento da atividade #26 e traz as contribuições dos projetos de código aberto ao nível da organização, fazendo-as mais visíveis, poderosas e benéficas. As contribuições nesta actividade devem trazer uma melhoria substancial e a longo prazo ao projeto de OSS: por exemplo, um programador ou uma equipa que desenvolve uma nova característica muito desejada, bens de infraestrutura, servidores para um novo serviço, assunção da manutenção de um ramo amplamente utilizado.

A intenção é de reservar uma percentagem dos recursos para patrocinar programadores de código aberto que escrevem e mantêm bibliotecas ou projetos que utilizamos.

Esta atividade implica ter um mapeamento do software de código aberto utilizado e uma avaliação da criticidade dele para decidir qual deles apoiar.

### Avaliação de oportunidades

> Se todas as empresas que utilizam código aberto contribuíssem pelo menos um pouco, teríamos um ecossistema saudável. <https://news.ycombinator.com/item?id=25432248>

Apoiar projetos ajuda a assegurar a sustentabilidade dos mesmos e proporciona acesso à informação, podendo assim mesmo ajudar a influenciar e priorizar alguns desenvolvimentos (embora esta não deveria ser a razão principal para apoiar projetos).

Benefícios potenciais desta atividade: assegurar que os relatórios de bugs sejam priorizados e que os desenvolvimentos sejam integrados na versão estável. Custos possíveis associados à atividade: dedicação de tempo aos projetos, dedicação de dinheiro.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta atividade:

- [ ] O projeto beneficiário foi identificado.
- [ ] Opção de apoio foi decidida, tal como contribuição monetária direta ou contribuição em código.
- [ ] O líder de tarefa foi nomeado.
- [ ] Aconteceu alguma contribuição.
- [ ] O resultado da contribuição foi avaliado.

Pontos de verificação emprestados do formulário [auto-certificação da OpenChain](https://certification.openchainproject.org/):

- [ ] Temos uma política de contribuição para projetos de código aberto em nome da organização.
- [ ] Temos um procedimento documentado que rege as contribuições de código aberto.
- [ ] Temos um procedimento documentado para sensibilizar todo o pessoal de software para a política de contribuições de código aberto.

### Ferramentas

Algumas organizações oferecem mecanismos para financiar projetos de código aberto (poderia ser conveniente se o seu projeto alvo estiver nos portfólios delas).

- [Open Collective](https://opencollective.com/).
- [Software Freedom Conservancy](https://sfconservancy.org/).
- [Tidelift](https://tidelift.com/).

### Recomendações

- Concentre-se nos projetos que são críticos para a organização: estes são os projetos que mais deseja ajudar com as suas contribuições.
- Dirija-se a projetos comunitários.
- Esta atividade requer um mínimo de conhecimentos sobre um projeto alvo.

### Recursos

- [How to support open source projects now](https://sourceforge.net/blog/support-open-source-projects-now/): uma breve página com ideias sobre o financiamento de projetos de código aberto.
- [Sustain OSS: a space for conversations about sustaining open source](https://sustainoss.org)

### Novas atividades propostas

- [GGI-A-26 - Contribute to open source projects](https://ospo-alliance.org/ggi/activities/contribute_to_open_source_vendors) The most natural way to engage with an open source initiative is by contributing directly to the project. You will in return collect valuable feedback on your contributions.
- [GGI-A-30 - Support open source communities](https://ospo-alliance.org/ggi/activities/support_open_source_communities) There are many ways to support the open source initiatives that are essential to your organisation. Getting engaged with the communities is a good way to discover and foster them.
