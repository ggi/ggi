## Afirmar publicamente a utilização de código aberto

ID da atividade: [GGI-A-31](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_31.md).

### Descrição

Esta atividade consiste em reconhecer a utilização do OSS num sistema de informação, em aplicações e em produtos novos.

- Fornecer histórias de sucesso.
- Apresentar em eventos.
- Financiamento da participação em eventos.

### Avaliação de oportunidades

É agora geralmente aceite que a maioria dos sistemas de informação funcionam no OSS, e que as novas aplicações são, na maioria, feitas através da reutilização do OSS.

O benefício principal desta atividade é de criar condições equitativas entre o OSS e o software proprietário, para garantir que o OSS é alvo de igual atenção e gerido de forma tão profissional como o software proprietário.

Um benefício secundário é que ajuda muito a elevar o perfil do ecossistema OSS e, uma vez que os utilizadores do OSS são identificados como "inovadores", também aumenta a atratividade da organização.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta atividade:

- [ ] Os vendedores comerciais de código aberto recebem autorização para utilizar o nome da organização como cliente de referência.
- [ ] Contribuidores podem fazê-lo e expressar-se sob o nome da organização.
- [ ] A utilização do OSS é abertamente mencionada no relatório anual do departamento de TI.
- [ ] Não há nenhum obstáculo para a organização explicar o seu uso de OSS na média (entrevistas, OSS e eventos do setor, etc.).

### Recomendações

- O objetivo desta atividade não é que a organização se torne um organismo de ativismo do OSS, mas que se certifique de que não existe nenhum obstáculo para que o público reconheça a utilização dela do OSS.

### Recursos

- Exemplo de [CERN](https://superuser.openstack.org/articles/cern-openstack-update/) que afirma publicamente a utilização do OpenStack deles
