## Código aberto e soberania digital

ID da atividade: [GGI-A-35](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_35.md).

### Descrição

A soberania digital pode ser definida como a

> "Capacidade e oportunidade de indivíduos e instituições executarem o(s) seu(s) papel(s) no mundo digital de forma independente, intencional e segura." &mdash; Centro de Competência de TI Público, Alemanha

Para efectuar os negócios adequadamente, todas as entidades têm de contar com outros parceiros, serviços, produtos e ferramentas. A revisão dos laços e das restrições destas dependências permite à organização avaliar e controlar a sua dependência face a factores externos, melhorando assim a autonomia e a resiliência da mesma.

Como exemplo, a dependência de fornecedores é um forte fator de dependência que pode impedir os processos e o valor acrescentado da organização e, como tal, deve ser evitada. O código aberto é uma das formas de sair desta tranca. O código aberto desempenha um papel significativo na soberania digital, ao permitir uma maior escolha entre soluções, fornecedores e integradores e um maior controlo sobre os roteiros de TI.

É de notar que a soberania digital não é uma questão de confiança: é óbvio que precisamos de confiar nos nossos parceiros e fornecedores, mas a relação melhora ainda mais quando se baseia no consentimento mútuo e no reconhecimento, em vez de contratos e tensões forçados.

Eis algumas vantagens de uma melhor soberania digital:

- Melhorar a capacidade da organização de fazer as próprias escolhas sem constrangimentos.
- Melhorar a resiliência da empresa em relação a protagonistas e fatores externos.
- Melhorar a posição negocial ao lidar com parceiros e provedores de serviços.

### Avaliação de oportunidades

- Quanto difícil/barato é afastar-se de uma solução?
- Poderiam os fornecedores de soluções aumentar unilateralmente preços, simplesmente porque não temos escolha?
- Poderiam os fornecedores de soluções impor condições indesejáveis ao serviço deles (por exemplo, mudança de licença, atualizações de contratos)?

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] Existe uma avaliação das dependências críticas para os fornecedores e parceiros da organização.
- [ ] Existe um plano de reserva para estas dependências identificadas.
- [ ] Existe um requisito declarado de soberania digital quando novas soluções são investigadas.

### Recomendações

- Identifique os riscos principais de dependência dos provedores de serviços e de entidades terceiras.
- Mantenha uma lista de alternativas de código aberto para serviços críticos.
- Adicione um requisito ao selecionar novas ferramentas e serviços utilizados na entidade, declarando a necessidade de soberania digital.

### Recursos

- [A Primer on Digital Sovereignty & Open Source: part I](https://www.opensourcerers.org/2021/08/09/a-promer-on-digital-sovereignty/) e [A Primer on Digital Sovereignty & Open Source: part II](https://www.opensourcerers.org/2021/08/16/a-primer-on-digital-sovereignty-open-source/), do sítio web Open-Sourcerers.
- Um excelente artigo no superuser.openstack.org sobre [The Role of Open Source in Digital Sovereignty](https://superuser.openstack.org/articles/the-role-of-open-source-in-digital-sovereignty-openinfra-live-recap/). Aqui está um pequeno extrato:
   > A soberania digital é uma preocupação fundamental para o século XXI, especialmente para a Europa. O código aberto tem um papel importante a desempenhar na viabilização da soberania digital, permitindo a todos o acesso à tecnologia necessária, mas também a proporcionar a transparência da governação e a interoperabilidade necessárias para que essas soluções sejam bem sucedidas.
- A União Europeia assume a soberania digital, do [Open Source Observatory (OSOR)](https://joinup.ec.europa.eu/collection/open-source-observatory-osor): Open Source, soberania digital e interoperabilidade: A Declaração de Berlim.
- A posição da UNICEF sobre [Open Source for Digital Sovereignty](https://www.unicef.org/innovation/stories/open-source-digital-sovereignty).
