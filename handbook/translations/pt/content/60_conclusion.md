# Conclusão

Como já dissemos antes, a boa governança de código aberto não é um destino; é uma jornada. Precisámo-nos preocupar com os nossos ativos comuns, com as comunidades e o ecossistema que o dificultam, porque o nosso próprio sucesso comum, portanto, individual, depende disso.

**Nós**, como profissionais de software e entusiastas de código aberto, estamos empenhados em continuar a melhorar o manual da Iniciativa de Boa Governança e trabalhar na divulgação e alcance dela. Acreditamos firmemente que organizações, indivíduos e comunidades precisam trabalhar lado a lado para construir um conjunto crescente de bens comuns, disponíveis e benéficos para todos.

**Você** é bem-vindo para se juntar à OSPO Alliance, contribuir para o nosso trabalho, divulgar a ideia e ser o embaixador de uma melhor consciencialização e governança de código aberto dentro do seu próprio ecossistema. Há uma variedade de recursos disponíveis, desde postagens de blog e artigos de pesquisa até a conferências e cursos de treino on-line. Também fornecemos um conjunto de material útil no [nosso site](https://ospo-alliance.org) e estaremos felizes em ajudar o máximo que pudermos.

**Vamos definir e construir juntos o futuro da Good Governance Initiative!**

## Contato

A maneira preferida de entrar em contacto com a OSPO Alliance é postar uma mensagem na nossa lista de discussão pública em <https://accounts.eclipse.org/mailing-list/ospo.zone>. Também pode vir e discutir connosco nos eventos habituais de código aberto, participar dos nossos webinars mensais OSPO OnRamp ou entrar em contacto com qualquer membro - eles gentilmente redirecionarão-no à pessoa certa.

## Apêndice: Modelo personalizado do Quadro de pontuação de atividades personalizados

A versão mais recente do modelo do Quadro de pontuação de atividades personalizado está disponível na secção `recursos` do [Good Governance Initiative GitLab](https://gitlab.ow2.org/ggi/ggi-castalia) no OW2.
