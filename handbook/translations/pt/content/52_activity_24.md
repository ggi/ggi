## Gerir indicadores chave

ID da atividade: [GGI-A-24](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_24.md).

### Descrição

Esta atividade recolhe e monitoriza um conjunto de indicadores que informam as decisões de gestão administrativas e as opções estratégicas relativas ao software de código aberto gerido profissionalmente.

As métricas principais relacionadas com software de código aberto constituem o cenário como os programas de boa governação são implementados. A atividade abrange a seleção de alguns indicadores, a publicação às equipas e à administração e o envio regular de atualizações sobre a iniciativa, por exemplo, por um boletim informativo ou notícias empresariais.

Esta atividade requer:

- que partes interessadas discutem e definem os objetivos do programa,
- a implementação de um instrumento de medição e recolha de dados ligado à infra-estrutura de desenvolvimento,
- a publicação de pelo menos um painel para os interessados e para todas as pessoas envolvidas na iniciativa.

Os indicadores são baseados em dados que devem ser recolhidos de fontes relevantes. Felizmente, há muitas fontes para engenharia de software de código aberto. Exemplos incluem:

- o ambiente de desenvolvimento, a cadeia de produção da IC/DC,
- o departamento de RH,
- as ferramentas de teste e de análise da composição do software,
- os repositórios.

Exemplos de indicadores incluem:

- Quantidade de dependências resolvidas, indicadas por tipo de licença.
- Quantidade de dependências desatualizadas/vulneráveis.
- Quantidade de problemas de licenciamento/PI detetados.
- Contribuições feitas a projetos externos.
- Tempo de abertura de bugs.
- Quantidade de contribuintes num componente, quantidade de commits, etc.

Essa atividade trata-se de definir esses requisitos e necessidades de medição e implementar um painel que mostre os principais indicadores do programa de forma simples e eficiente.

### Avaliação de oportunidades

Os indicadores-chave ajudam a compreender e a gerir melhor os recursos dedicados ao software de código aberto e medem os resultados para comunicar eficazmente e desfrutar de todos os benefícios do investimento. Ao comunicar de forma ampla, mais pessoas podem seguir a iniciativa e sentir-se envolvidas, tornando-a, ultimamente, uma preocupação e um objetivo a nível da organização.

Embora cada atividade tenha critérios de avaliação que ajudam a responder a perguntas sobre os progressos alcançados, ainda há necessidade de monitorização feita com números e indicadores quantitativos.

Quer seja numa pequena startup ou numa grande empresa global, as métricas principais ajudam a manter as equipas concentradas e a monitorizar o desempenho. As métricas são cruciais porque apoiam a tomada de decisões e são a base para monitorizar decisões já tomadas.

Com números e gráficos simples e práticos, todos os membros da organização conseguirão acompanhar e sincronizar esforços em relação ao código aberto, a fazê-lo uma preocupação e ação partilhada. Isto também permite que os vários atores entrem melhor no curso, contribuam para o projeto e obtenham os benefícios globais.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta atividade:

- [ ] Uma lista de métricas e como recolhê-las foi estabelecida.
- [ ] São utilizadas ferramentas para recolher, armazenar, processar e exibir indicadores.
- [ ] Há um painel generalizado disponível para todos os participantes que mostra os progressos feitos na iniciativa.

### Ferramentas

- [GrimoireLab](https://chaoss.github.io/grimoirelab) de Bitergia.
- As ferramentas genéricas de IB (Elasticsearch, Grafana, visualizações de R/Python...) são também uma boa opção, quando os conectores adequados são configurados segundo os objetivos definidos.

### Recomendações

- Anote os objetivos e o roteiro da governança de código aberto.
- Comunique internamente sobre as ações e o estado da iniciativa.
- Envolva pessoas na definição dos KPIs para garantir que
   - são bem compreendidos,
   - fornecem uma visão completa das necessidades e
   - são considerados e seguidos.
- Construa pelo menos um painel que possa ser exibido para todos (por exemplo, num ecrã na sala), com indicadores essenciais para mostrar o progresso e a situação geral.

### Recursos

- A [comunidade CHAOSS](https://chaoss.community/) tem muitas e boas referências e recursos relacionados com indicadores de código aberto.
- Verifique métricas para [atributos de projetos](https://www.ow2.org/view/MRL/Stage2-ProjectAttributes) da [metodologia](https://www.ow2.org/view/MRL/Overview) dos Níveis de Preparação para o Mercado do OW2.
- [A New Way of Measuring Openness: The Open Governance Index](https://timreview.ca/article/512) por Liz Laffan é uma leitura interessante sobre a abertura em projetos de código aberto.
- [Governance Indicators: A Users’ Guide]((https://anfrel.org/wp-content/uploads/2012/02/2007_UNDP_goveranceindicators.pdf) é o guia da ONU sobre indicadores de governação. Embora seja aplicado à democracia, corrupção e transparência das nações, os princípios básicos de medição e indicadores aplicados à governação valem bem a pena ser lidos.

### Novas atividades propostas

- [GGI-A-37 - Open source enabling digital transformation](https://ospo-alliance.org/ggi/activities/open_source_enabling_digital_transformation) Use produced metrics as part of your general open source strategy.
