## Perspetiva do RH

ID da atividade: [GGI-A-28](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_28.md).

### Descrição

A mudança à cultura de código aberto tem impactos profundos nos RH:

- **Novos processos e contratos**: contratos têm de ser adaptados para permitir e promover contribuições externas. Isto inclui questões de PI e licenciamento para o trabalho realizado na empresa, mas também a capacidade do empregado ou contratante ter os seus próprios projetos.
- **Tipos de pessoas diferentes**: pessoas que trabalham com código aberto têm frequentemente incentivos e mentalidades diferentes das pessoas puramente proprietárias e empresariais. Processos e mentalidades precisam de se adaptar a este paradigma orientado para a reputação da comunidade, de modo a atrair novos tipos de talento e a mantê-los ao longo do tempo.
- **Desenvolvimento de carreira**: necessidade de oferecer um percurso profissional que alimente e valorize empregados pelas suas habilidades técnicas e interpessoais, bem como as competências esperadas pela sua organização (colaboração para impulsionar os esforços da comunidade, comunicação para atuar como porta-voz da sua empresa, etc.). Por todos os meios, o RH tem um papel fundamental na viabilização do código aberto como um objetivo cultural.

**Força de trabalho** Para um programador que trabalha há muito tempo na mesma solução proprietária, a mudança para o código aberto pode parecer uma grande mudança e requerer adaptação. Mas para a maioria dos programadores, o software de código aberto traz apenas benefícios.

Os desenvolvedores que saem da escola ou da universidade hoje sempre trabalharam com código aberto. Numa empresa, a maioria dos desenvolvedores usa idiomas de código aberto e importam bibliotecas ou trechos de código aberto diariamente. Na verdade, é muito mais fácil colar linhas de código aberto num programa do que acionar o processo de aprovisionamento interno, que se intensifica por múltiplas validações através da linha de gestão.

O código aberto torna o trabalho do programador mais interessante porque com o código aberto, um programador está sempre atento a descobrir o que os pares fora da empresa inventaram e por isso permanece na vanguarda da tecnologia.

Para uma organização, tem de haver uma estratégia de RH para 1/ qualificar ou requalificar a força de trabalho existente, 2/ refletir e posicionar a empresa na contratação de novos talentos, qual é a atratividade da empresa quando se trata de código aberto.

> Obter pessoas com uma boa mentalidade de FLOSS, que já entendem o código e sabem como trabalhar bem com outros é maravilhoso. A alternativa de evangelizar/treinar/estagiar vale a pena fazer, mas é mais cara e demorada.
>
> &mdash; <cite>CEO do fornecedor de software OSS</cite>

Isto ilustra que contratar pessoas com conhecimento de código aberto é um caminho de aceleração a considerar na estratégia de RH.

#### Processos

- Estabelecer ou rever descrições de funções (competências técnicas, habilidades interpessoais, competências e experiências)
- Programas de treino: auto-treino, treino formal, coaching de gestão, mapeamento de pares, comunidades
- Estabelecer ou rever o percurso profissional: competências, resultados principais/impacto e etapas da carreira

### Avaliação de oportunidades

1. Quadro de práticas de desenvolvimento: o problema provavelmente não é tanto a incentivar os programadores a utilizar mais código aberto, mas para garantir que o utilizam com segurança, conforme os termos de licenciamento de cada tecnologia de código aberto e sem abandonar as verificações de segurança tradicionais (código aberto pode conter códigos maliciosos),
1. Revir as práticas de colaboração: com práticas de desenvolvimento, a oportunidade é alargar a agilidade e colaboração a outras linhas de negócio na sua organização. O fornecimento interno é frequentemente utilizado para fomentar estes comportamentos, embora isto possa ser metade do caminho para a cultura de código aberto,
1. Cultura da organização: no final, trata-se da cultura da sua organização: o código aberto pode ser a estandarte de valores como a abertura, a colaboração, a ética, a sustentabilidade.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] É disponibilizada formação para apresentar tanto os benefícios como as limitações (conformidade com os termos de licenciamento de propriedade intelectual) relacionados ao código aberto.
- [ ] Cada programador, cada arquiteto, cada líder de projeto (ou proprietário do produto/proprietário da empresa), compreende os benefícios e as limitações (conformidade com os termos de licenciamento de propriedade intelectual) relacionados ao código aberto.
- [ ] Programadores são encorajados a contribuir às comunidades de código aberto e a assumir a responsabilidade por eles e poderiam receber formação adequada para o fazer.
- [ ] Habilidades e competências são refletidas em descrições de trabalho de organização e etapas de carreira.
- [ ] A experiência adquirida em código aberto pelos programadores (contribuições às comunidades de código aberto, participação no processo de conformidade interno, pessoas externas a falar em nome da empresa, ...) é tida em conta no processo de avaliação de RH.

### Ferramentas

- Matriz de habilidades.
- Programas públicas de formação (p.ex. open source school).
- Aprovisionamento: GitHub, GitLab, LinkedIn, Meetups, Epitech, Epita, …
- Modelos de contrato (cláusula de lealdade).
- Descrições de trabalho (modelos) & etapas da carreira (modelos).

### Recomendações

Na maioria das vezes, os programadores já conhecem alguns princípios de código aberto e estão dispostos a trabalhar com, e em, software de código aberto. No entanto, ainda existem algumas ações que a administração deve tomar:

- Preferência pela experiência OSS na contratação, mesmo que o trabalho para o qual o programador é contratado esteja relacionado apenas a tecnologia proprietária. É provável que, com a transformação digital, o programador tenha que trabalhar em código aberto eventualmente.
- Programa de treino de OSS: cada programador, cada arquiteto, cada líder de projeto (ou proprietário de produto/proprietário de negócios), deve ter acesso a recursos de treino (treino por vídeos ou presencial) que apresentem os benefícios do código aberto e também as restrições em termos de propriedade intelectual e conformidade com o licenciamento.
- O treino deve ser disponibilizado para programadores que desejam contribuir para comunidades de código aberto e fazer parte dos órgãos de governança dessas comunidades (certificações Linux).
- Reconhecimento nos processos de avaliação pessoal de RH da contribuição do colaborador (programador ou arquiteto) para temas relacionados a código aberto, como contribuições para comunidades de código aberto e cumprimento dos termos de licenciamento de propriedade intelectual. A maioria dos tópicos são compartilhados e encaixam-se em caminhos técnicos de carreira, enquanto alguns podem ou devem ser específicos.
- Segredo melhor guardado e postura da empresa: necessidade de abordar os aspetos de comunicação (como é essencial para a sua organização que possa ser refletido no seu relatório anual), como isso tem impacto na sua postura de comunicação (um colaborador de código aberto pode ser uma pessoa que fala pela sua empresa, incluindo contactos com a imprensa).

### Recursos

- Quanto à capacidade das pessoas de falarem fora da empresa em eventos, consulte a atividade 31: "(objetivo de envolvimento) Afirmar publicamente a utilização de código aberto".
