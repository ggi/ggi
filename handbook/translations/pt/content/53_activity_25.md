## Promover práticas recomendadas de desenvolvimento de código aberto

ID da atividade: [GGI-A-25](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/53_activity_25.md).

### Descrição

Esta atividade consiste em definir, promover ativamente e implementar as práticas recomendadas de código aberto no âmbito do equipas de desenvolvimento.

Como ponto de partida, os tópicos seguintes podem ser considerados à atenção:

- Documentação do utilizador e programador.
- Organização adequada do projeto num repositório acessível ao público.
- Promover e implementar a reutilização controlada.
- Fornecimento de uma documentação completa e actualizada do produto.
- Gestão da configuração: fluxos de trabalho de git, padrões de colaboração.
- Gestão de lançamento: lançamento cedo & lançamento frequente, versões estáveis versus versões de desenvolvimento, etc.

Projetos de OSS têm um modus operandi especial do [tipo bazar](http://www.catb.org/~esr/writings/cathedral-bazaar/). Para permitir e fomentar esta colaboração e mentalidade, práticas que facilitem o desenvolvimento colaborativo e descentralizado e as contribuições de desenvolvedores de terceiros são recomendadas…

#### Documentos comunitários

Assegure que todos os projetos na empresa proponham os documentos seguintes:

- README -- descrição rápida do projeto, como interagir, ligações a recursos.
- Contribuir -- introduções para pessoas dispostas a contribuir.
- Código de Conduta -- o que é aceitável -- ou não -- como se comportar na comunidade.
- LICENSE -- a licença padrão do repositório.

#### Práticas recomendadas do REUSE

[REUSE](https://reuse.software) é uma iniciativa da [Free Software Foundation Europe](https://fsfe.org/index.pt.html) para melhorar a reutilização de software e racionalizar o OSS e o cumprimento de licenças.

### Avaliação de oportunidades

Embora dependa fortemente do conhecimento comum do OSS entre a equipa, a formação de pessoas e a criação de processos que imponham estas práticas é sempre benéfica. É ainda mais importante quando:

- os utilizadores e contribuintes potenciais não são conhecidos,
- os programadores não estão habituados ao desenvolvimento de código aberto.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] O projeto estabelece uma lista de práticas recomendadas de código aberto a cumprir.
- [ ] O projeto monitoriza o seu alinhamento com as práticas recomendadas.
- [ ] A equipa de desenvolvimento criou consciência sobre o cumprimento das práticas recomendadas do OSS.
- [ ] As práticas recomendadas novas são avaliadas regularmente e um esforço é feito para as implementar.

### Ferramentas

- A ferramenta [REUSE helper](https://github.com/fsfe/reuse-tool) auxilia na realização de um repositório conforme as práticas recomendadas do [REUSE](https://reuse.software). Pode ser incluído em muitos processos de desenvolvimento para confirmar o estado atual.
- [ScanCode](https://scancode-toolkit.readthedocs.io) consegue listar todos os documentos comunitários e legais no repositório: veja [descrição da características](https://scancode-toolkit.readthedocs.io/en/latest/cli-reference/scan-options-pre.html#classify).
- GitHub tem uma funcionalidade agradável para [verificar se faltam documentos comunitários](https://docs.github.com/pt/communities/setting-up-your-project-for-healthy-contributions/about-community-profiles-for-public-repositories). Encontra-se na Página do Repositório > "Insights" > "Community".

### Recomendações

- A lista das práticas recomendadas depende do contexto e do domínio do programa e deve ser reavaliada regularmente, para a melhorar continuamente. As práticas devem ser monitorizadas e avaliadas regularmente para rastrear o progresso.
- Forme pessoas sobre a reutilização do OSS (como consumidores) e ecossistemas (como contribuintes).
- Implemente o software REUSE.software como na atividade #14.
- Estabeleça um processo para gerir os riscos legais associados à reutilização e às contribuições.
- Incentive explicitamente as pessoas a contribuírem a projetos externos.
- Forneça um modelo ou orientações oficiais para a estrutura do projeto.
- Instaure verificações automatizadas para garantir que todos os projetos cumprem as diretrizes.

### Recursos

- [OW2's list of open source best practices](https://www.ow2.org/view/MRL/Full_List_of_Best_Practices) da metodologia de avaliação dos Níveis de Aptidão para o Mercado.
- [Site oficial do REUSE](https://reuse.software) com especificação, tutorial e FAQ.
- [Guias de código aberto](https://opensource.guide/pt/).
- Um exemplo de [configuração de práticas recomendadas de gestão utilizando GitHub](https://dev.to/datreeio/top-10-github-best-practices-3kl2).

### Novas atividades propostas

- [GGI-A-42 - Gerir competências e recursos de código aberto](https://ospo-alliance.org/ggi/activities/manage_open_source_skills_and_resources) Adicione a lista de melhores práticas identificadas de desenvolvimento em código aberto aos recursos de formação geral.
- [GGI-A-44 - Pratique revisões de código](https://ospo-alliance.org/ggi/activities/run_code_reviews/) A revisão do código é um elemento considerado essencial nas melhores práticas de desenvolvimento.
