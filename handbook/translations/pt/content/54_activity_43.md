## Política de aprovisionamento de código aberto

ID da atividade: [GGI-A-43](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_43.md).

### Descrição

Esta atividade trata-se de implementar um processo de seleção, aquisição, compra de software e serviços de código aberto. Trata-se também de considerar o custo real do software de código aberto e o aprovisionamento para o mesmo. OSS pode ser "gratuito" à primeira vista, mas não é isento de custos internos e externos, tais como integração, formação, manutenção e apoio.

Tal política exige que tanto as soluções de código aberto como as soluções proprietárias sejam simetricamente consideradas ao avaliar a relação custo-benefício como a combinação óptima do custo total de propriedade e qualidade. Por esse motivo, o departamento de aquisição de TI deve considerar de forma ativa e justa as opções de código aberto, assegurando simultaneamente que as soluções proprietárias sejam consideradas numa base de igualdade nas decisões de compra.

A preferência pelo código aberto pode ser explicitamente declarada com base na flexibilidade inerente à opção de código aberto quando não há diferença significativa de custo global entre soluções proprietárias e de código aberto.

Os departamentos de aprovisionamento devem compreender que empresas que oferecem apoio ao OSS tipicamente não dispõem dos recursos comerciais para participar em concursos de aprovisionamento, e adaptar as políticas e processos de aprovisionamento de código aberto deles em conformidade.

### Avaliação de oportunidades

Várias razões justificam os esforços para estabelecer políticas específicas de aprovisionamento de código aberto:

- O fornecimento de software e serviços comerciais de código aberto está a crescer e não pode ser ignorado, e requer a implementação de políticas e processos de aquisição dedicados.
- Existe uma variedade crescente de soluções comerciais de código aberto altamente competitivas para sistemas de informação empresarial.
- Mesmo após a adoção de uma componente OSS gratuita e integração dela numa aplicação, recursos internos ou externos devem ser fornecidos para manter esse código fonte.
- O Custo Total de Propriedade (TCO) é frequentemente (embora não necessariamente) mais baixo para soluções de FOSS: sem taxas de licença a pagar na compra/atualização, mercado aberto para fornecedores de serviços, opção de fornecer você mesmo uma parte ou a totalidade da solução.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta atividade:

- [ ] Novas aberturas de concurso solicitam proactivamente a apresentação de propostas de código aberto.
- [ ] O departamento de aprovisionamento tem uma forma de avaliar soluções de código aberto versus soluções proprietárias.
- [ ] Um processo simplificado de aquisição de software e serviços de código aberto foi implementado e documentado.
- [ ] Um processo de aprovação baseado na perícia inter-funcional foi definido e documentado.

### Recomendações

- "Assegure-se de que utiliza os conhecimentos especializados das suas TI, DevOps, ciber-segurança, gestão de riscos e equipas de aprovisionamento ao criar o processo". (de [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/)).
- Leis de concorrência podem exigir que o "código aberto" não seja especificamente mencionado.
- Selecione a tecnologia antecipadamente e acesse as aberturas de concurso para serviços de personalização e suporte.

### Recursos

- [Decision factors for open source software procurement](http://oss-watch.ac.uk/resources/procurement-infopack): não novo, mas ainda uma grande leitura pelos nossos colegas da OSS-watch no Reino Unido. Veja as [slides](http://oss-watch.ac.uk/files/procurement.odp).
- [5 Open Source Procurement Best Practices](https://anchore.com/blog/5-open-source-procurement-best-practices/): uma obra recente sobre o aprovisionamento de código aberto com dicas úteis.

### Novas atividades propostas

- [GGI-A-33 - Engage with open source vendors](https://ospo-alliance.org/ggi/activities/engage_with_open_source_vendors) Defining a procument policy helps you identify the OSS providers and communities that you need to care about, and to engage with.
