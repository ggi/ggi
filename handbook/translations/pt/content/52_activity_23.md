## Gerir as dependências de software

ID da atividade: [GGI-A-23](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_23.md).

### Descrição

Um programa *de identificação de dependências* procura as dependências efetivamente utilizadas no código. O resultado é que a organização deve estabelecer e manter uma lista de dependências conhecidas para o seu código e observar a evolução dos fornecedores identificados.

Estabelecer e manter uma lista de dependências conhecidas é um facilitador e um pré-requisito para:

- Verificação de PI e licenças: algumas licenças não podem ser misturadas, mesmo como dependência. É preciso conhecer as dependências para avaliar os riscos legais associados deles.
- Gestão das vulnerabilidades: todo o software é tão fraco como a sua parte mais fraca: veja o exemplo do [bug Heartbleed](https://pt.wikipedia.org/wiki/Heartbleed). É preciso conhecer as suas dependências para avaliar os riscos de segurança que lhe são associados.
- Ciclo de vida e sustentabilidade: uma comunidade ativa no projeto de dependência é um sinal brilhante para correções de bugs, otimizações e novas características.
- Seleção atenciosa de dependências utilizadas, de acordo com critérios de "maturidade" - o objetivo é usar componentes de código aberto seguros, com um código são e bem conservado e uma comunidade viva, ativa e reativa que aceitará contribuições externas, etc.

### Avaliação de oportunidades

A identificação e monitorização de dependências é um passo necessário para mitigar os riscos associados a qualquer reutilização de código. Além disso, a implementação de ferramentas e processos para gerir as dependências de software é um pré-requisito para gerir a qualidade, conformidade e segurança adequadamente.

Considere as questões seguintes:

- Qual é o risco para a empresa (custo, reputação, etc), se o software for corrompido, atacado ou processado?
- O código desenvolvido é considerado crítico para as pessoas, a organização ou o negócio?
- E se um componente de que uma aplicação depende alterar o seu repositório?

O mínimo e primeiro passo é implementar uma ferramenta de análise da composição do software (SCA). O apoio de empresas de consultoria especializadas poderá ser necessário para uma SCA completa ou um mapeamento de dependências.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta atividade:

- [ ] As dependências são identificadas em todo o código desenvolvido internamente.
- [ ] As dependências são identificadas em todo o código externo executado na empresa.
- [ ] Uma análise de composição de software fácil de configurar ou um procedimento de identificação de dependências está disponível para os projetos adicionarem ao seu processo de Integração Contínua.
- [ ] São utilizadas ferramentas de análise de dependências.

### Ferramentas

- [OWASP Dependency check (Verificação de dependências do OWASP)](https://github.com/jeremylong/DependencyCheck): a verificação de dependências é uma ferramenta de Análise de Composição de Software (SCA) que tenta detetar vulnerabilidades divulgadas publicamente contidas nas dependências de um projeto.
- [OSS Review Toolkit (Caixa de ferramentas de revisão de OSS)](https://oss-review-toolkit.org/): um conjunto de ferramentas para ajudar na revisão das dependências de software de código aberto.
- [Fossa](https://github.com/fossas/fossa-cli): análise de dependência rápida, portátil e fiável. Suporta a análise de licenças e vulnerabilidades. Agnóstico de linguagem; integra-se em mais de 20 sistemas de construção.
- [Software 360](https://projects.eclipse.org/projects/technology.sw360).
- [Eclipse Dash license tool](https://github.com/eclipse/dash-licenses): leva uma lista de dependências e solicita [ClearlyDefined](https://clearlydefined.io) a verificar as suas licenças.
- [The FOSSology Project](https://www.fossology.org/): FOSSology é um projeto de código aberto com a missão de promover a conformidade com licenças de código aberto.

### Recomendações

- Realize auditorias regulares sobre as dependências e os requisitos de PI para mitigar riscos legais.
- Idealmente, integre a gestão de dependências no processo de integração contínua de modo que questões (nova dependência, incompatibilidade de licença) sejam identificadas e corrigidas o mais rápido possível.
- Mantenha o controlo das vulnerabilidades relacionadas à dependência, mantenha os utilizadores e desenvolvedores informados.
- Informe as pessoas sobre os riscos associados ao licenciamento errado.
- Proponha uma solução fácil para os projetos criarem a verificação de licenças no seu código.
- Comunique a sua importância e ajude projetos a adicioná-la aos sistemas de IC deles.
- Estabeleça um KPI visível sobre riscos relacionados a dependências.

### Recursos

- Página de grupo sobre [ferramentas existentes com licenças de OSS de conformidade com licenças de OSS](https://oss-compliance-tooling.org/Tooling-Landscape/OSS-Based-licence-Compliance-Tools/).
- [Free and Open Source Software licence Compliance: Tools for Software Composition Analysis](https://www.computer.org/csdl/magazine/co/2020/10/09206429/1npxG2VFQSk), por Philippe Ombredanne, nexB Inc.
- [Software Sustainability Maturity Model](http://oss-watch.ac.uk/resources/ssmm).
- [CAOS](https://chaoss.community/): Software de análise da comunidade de código aberto da saúde.

### Novas atividades propostas

- [GGI-A-21 - Manage legal compliance](https://ospo-alliance.org/ggi/activities/manage_legal_compliance) Before being able to track IP and licences incompatibilities, one needs to identify all dependencies in their open source software.
- [GGI-A-22 - Manage software vulnerabilities](https://ospo-alliance.org/ggi/activities/manage_software_vulnerabilities/) Before being able to track vulnerabilities in their code assets, one needs to identify all dependencies in their open source software.
