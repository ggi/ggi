## Apoiar comunidades de código aberto

ID da atividade: [GGI-A-30](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/54_activity_30.md).

### Descrição

Esta atividade tem a ver com o envolvimento de representantes institucionais do mundo do código aberto.

É conseguida através de:

- Adesão a fundações de OSS (incluindo o custo financeiro da adesão).
- Apoiar e defender as atividades das fundações.

Esta atividade envolve a atribuição de tempo e orçamento às equipas de desenvolvimento e TI para participarem em comunidades de código aberto.

### Avaliação de oportunidades

Comunidades de código aberto estão na vanguarda da evolução do ecossistema de código aberto. O envolvimento nas comunidades de código aberto tem várias vantagens:

- ajuda a estar informado e em dia,
- melhora o perfil da organização,
- a adesão vem com benefícios,
- fornece estruturas adicionais e motivação para a equipa de TI de código aberto.

Os custos incluem:

- taxas de adesão,
- tempo de pessoal e orçamento de viagem atribuído para participar em actividades comunitárias,
- controlo do compromisso de PI.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] A organização é um membro assinado de uma fundação de código aberto.
- [ ] A organização contribui para a governação.
- [ ] O software desenvolvido pela organização é submetido a / foi adicionado à base de códigos de uma fundação.
- [ ] A adesão é reconhecida tanto nos sítios web da organização como na comunidade.
- [ ] Uma avaliação de custo/benefício da adesão foi feita.
- [ ] Um contato para a comunidade foi nomeado.

### Recomendações

- Junte-se a uma comunidade compatível com o seu tamanho e recursos, ou seja, uma comunidade que possa ouvir o que diz e onde possa ser um colaborador reconhecido.

### Recursos

- Veja esta [página útil](https://www.linuxfoundation.org/tools/participating-in-open-source-communities/) da Fundação Linux sobre o porquê e como aderir a uma comunidade de código aberto.

### Novas atividades propostas

- [GGI-A-31 - Publicly assert use of open source](https://ospo-alliance.org/ggi/activities/publicly_assert_use_of_open_source) Now that you officially support some OSS communities, make it known! It is good for your reputation, and it is good for the projects in terms of health and dissemination.
