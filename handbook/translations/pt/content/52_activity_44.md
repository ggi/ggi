## Executar revisões de código

ID da atividade: [GGI-A-44](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/52_activity_44.md).

### Descrição

A revisão de código é uma tarefa de rotina que envolve a revisão manual e/ou automatizada do código-fonte de uma aplicação antes de lançar um produto ou entregar um projeto ao cliente. No caso de software de código aberto, a revisão de código é mais que apanhar oportunisticamente erros; é uma abordagem ao nível de equipa integrada no desenvolvimento colaborativo realizado.

As revisões de código devem se aplicar ao código desenvolvido internamente, bem como ao código reutilizado de fontes externas, uma vez que melhora a confiança geral no código e reforça a propriedade. É também uma excelente forma de melhorar as competências e conhecimentos globais na equipa e fomentar a colaboração da equipa.

### Avaliação de oportunidades

As revisões de código são valiosas sempre que a organização desenvolve software ou reutiliza peças de software externas. Apesar de ser uma etapa padrão no processo de engenharia de software, as revisões de código no contexto de software de código aberto são benéficos específicos, tais como:

- Ao publicar o código-fonte interno, verificar se as diretrizes de qualidade adequadas são respeitadas.
- Ao contribuir para um projeto de código aberto existente, verificar se as diretrizes do projeto visado são respeitadas.
- A documentação disponível publicamente é atualizada em conformidade.

É também uma excelente oportunidade para partilhar e aplicar algumas das regras da política de conformidade legal da sua empresa, como, por exemplo:

- Nunca remova cabeçalhos de licenças existentes ou direitos de autor encontrados em código aberto reutilizado.
- Não copie e cole o código-fonte do Stack Overflow sem permissão prévia da equipa jurídica.
- Inclua a linha de direitos autorais correta quando necessário.

As revisões de código trarão confiança ao código. Se as pessoas não estiverem seguras sobre a qualidade ou os riscos potenciais da utilização de um produto de software, devem realizar revisões por pares e de código.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] A revisão do código aberto é reconhecida como um passo necessário.
- [ ] Revisões de código aberto são planeadas (regularmente ou em momentos críticos).
- [ ] Um processo para a realização de revisões de código aberto foi definido e aceito coletivamente.
- [ ] As revisões de código aberto são uma parte padrão do processo de desenvolvimento.

### Recomendações

- A revisão de código é uma tarefa coletiva que funciona melhor num bom ambiente de colaboração.
- Não hesite em utilizar ferramentas e padrões existentes do mundo de código aberto, no qual as revisões de código são uma norma há anos (décadas).

### Recursos

- [What is Code Review?](https://openpracticelibrary.com/practice/code-review/): uma leitura didática sobre revisão de código encontrada na Open Practice Library do Red Hat.
- [Best Practices for Code Reviews](https://www.perforce.com/blog/qac/9-best-practices-for-code-review): outra perspetiva interessante sobre o que a revisão de códigos é.

### Novas atividades propostas

- [GGI-A-26 - Contribute to open source projects](https://ospo-alliance.org/ggi/activities/contribute_to_open_source_projects) Code review is common practice in open source projects, for it improves code quality and knowledge sharing. Contributors performing code reviews usually feel more comfortable with external contributions and collaboration.
