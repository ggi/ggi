## Código aberto permitindo a transformação digital

ID da atividade: [GGI-A-37](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/55_activity_37.md).

### Descrição

> "Transformação digital é a adoção de tecnologia digital para transformar serviços ou empresas, através da substituição de processos não digitais ou manuais por processos digitais ou da substituição de tecnologia digital mais antiga por tecnologia digital mais recente." (Wikipédia, de um artigo antigo)

Quando as organizações mais avançadas na transformação digital impulsionam em conjunto a mudança através dos seus negócios, TI e finanças para ancorar o digital no caminho, reconsideram:

- Modelo de negócios: cadeia de valor com ecossistemas, "as a Service", SaaS.
- Finanças: despesas operacionais/despesas de capital, pessoas, terceirização.
- TI: inovação, modernização de legados/ativos.

O código aberto está no centro da transformação digital:

- Tecnologias, práticas ágeis, gestão de produtos.
- Pessoas: colaboração, comunicação aberta, ciclo de desenvolvimento/decisão.
- Modelos de negócios: experimentar & comprar, inovação aberta.

Em termos de competitividade, os processos mais visíveis são provavelmente os processos que têm um efeito direto na experiência do cliente. Temos de reconhecer que os intervenientes maiores, bem como as empresas na fase de start-up, ao proporcionarem uma experiência de cliente totalmente sem precedentes, alteraram drasticamente as expectivas dos clientes.

A experiência do cliente, bem como todos os outros processos dentro de uma empresa, dependem inteiramente das TI. Cada empresa tem de transformar as suas TI, é do que se trata na transformação digital. As empresas que ainda não o fizeram, têm agora de conseguir a transformação digital delas o mais rápido possível, caso contrário o risco é que possam ser eliminadas do mercado. A transformação digital é uma condição de sobrevivência. Desde que as apostas são tão altas, uma empresa não pode deixar inteiramente a transformação digital a um fornecedor. Todas as empresas têm de adaptar a informática delas, o que significa que todas as empresas têm de trabalhar com software de código aberto porque não há informática sem software de código aberto.

As vantagens esperadas da transformação digital incluem:

- Simplificar, automatizar processos nucleares, fazê-los agir em tempo real.
- Permitir respostas rápidas às mudanças competitivas.
- Tirar partido da inteligência artificial e dos grandes dados.

### Avaliação de oportunidades

A transformação digital poderia ser dirigida por:

- Segmentos da TI: TI de produção, TI de suporte empresarial (GRC, faturação, aquisição, …), TI de suporte (RH, finanças, contabilidade…), macrodados.
- Tipo de tecnologia ou processo de apoio à TI: infraestrutura (nuvem), Inteligência Artificial, processos (Make-or-Buy, DevSecOps, SaaS).

Injetar código aberto num determinado segmento ou tecnologia das suas TI revela que quer trabalhar neste segmento ou tecnologia, porque avaliou que este segmento ou tecnologia específica das suas TI é importante para a competitividade da sua empresa. É importante avaliar a posição da sua empresa em comparação não só com os seus concorrentes, mas também com outras indústrias e com intervenientes-chave em termos de experiência do cliente e de soluções de mercado.

### Avaliação do progresso


   - [ ] Nível 1: Avaliação da situaçãoIdentifiquei:

   - os segmentos das TI que são importantes para a competitividade da minha empresa, e
   - as tecnologias de código aberto necessárias para desenvolver aplicações nestes segmentos.
E assim decidi:


   - em que segmentos quero gerir internamente o desenvolvimento de projetos, e
   - em que tecnologias de código aberto preciso de criar conhecimentos especializados internos.

   - [ ] Nível 2: EnvolvimentoEm algumas tecnologias de código aberto selecionadas utilizadas na empresa, vários programadores foram treinados e são reconhecidos como contribuintes valiosos pela comunidade de código aberto.Em alguns segmentos selecionados, projetos baseados em tecnologias de código aberto foram lançados.

   - [ ] Nível 3: GeneralizaçãoPara todos os projetos, uma alternativa de código aberto está a ser sistematicamente investigada durante a fase de início do projeto. Para facilitar o estudo dessa alternativa de código aberto por parte da equipa do projeto, um orçamento central e uma equipa central de arquitetos, alojada no departamento de TI, dedica-se a prestar assistência aos projetos.

**KPIs**:

- KPI 1. Taxa para a qual uma alternativa de código aberto foi investigada: (quantidade de projetos / quantidade total de projetos).
- KPI 2: * Taxa para a qual a alternativa de código aberto foi escolhida: (quantidade de projetos / quantidade total de projetos).

### Recomendações

Para além da palavra de ordem, a transformação digital é uma mentalidade que envolve algumas mudanças fundamentais e deve também (ou mesmo principalmente) provir das camadas superiores da organização. A administração deve promover iniciativas, novas ideias, gerir riscos e potencialmente atualizar os procedimentos existentes para os ajustar a novos conceitos.

A paixão é um enorme fator de sucesso. Um dos meios desenvolvidos pelos principais protagonistas no setor é a criação de aberturas para novas ideias, onde as pessoas possam submeter e trabalhar livremente, as ideias deles sobre a transformação digital. A administração deve encorajar tais iniciativas.

### Recursos

- [Eclipse Foundation: Enabling Digital Transformation in Europe Through Global Open Source Collaboration](https://outreach.eclipse.foundation/hubfs/EuropeanOpenSourceWhitePaper-June2021.pdf).
- [Europe: Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#opensourcesoftwarestrategy).
- [Europe: Open source software strategy 2020-2023](https://ec.europa.eu/info/sites/default/files/en_ec_open_source_strategy_2020-2023.pdf).
