## Gerir habilidades e recursos de código aberto

ID da atividade: [GGI-A-42](https://gitlab.ow2.org/ggi/ggi/-/blob/main/handbook/content/51_activity_42.md).

### Descrição

Esta atividade centra-se nas habilidades e nos recursos do **desenvolvimento de software**. Inclui as tecnologias e habilidades específicas de desenvolvimento dos programadores, bem como os processos, métodos e ferramentas globais de desenvolvimento.

Uma vasta quantidade de documentação, fóruns e discussões decorrentes do ecossistema e recursos públicos está disponível para as tecnologias de código aberto. Para beneficiar plenamente da abordagem de código aberto delas, é necessário estabelecer um roteiro dos seus recursos e alvos desejados atuais para estabelecer um programa consistente de habilidades, métodos e ferramentas de desenvolvimento nas equipas.

**Domínios de aplicação**

É necessário estabelecer os domínios onde o programa será aplicado e como irá melhorar a qualidade e eficiência do código e das práticas. Como exemplo, o programa não terá os mesmos benefícios se houver apenas um único programador a trabalhar em componentes de código aberto ou se todo o ciclo de vida do desenvolvimento for otimizado à inclusão das práticas recomendadas de código aberto.

É necessário definir o âmbito a abranger para o desenvolvimento de código aberto: componentes técnicos, aplicações, modernização ou criação de novos desenvolvimentos. São exemplos de práticas de desenvolvimento que podem beneficiar do código aberto:

- Administração de plataformas cloud.
- Aplicações nativas da cloud, como inovar com estas tecnologias.
- DevOps, Integração Contínua / Entrega Contínua.

**Categorias**

- Habilidades e recursos necessários para desenvolver software de código aberto: PI, licenciamento, práticas.
- Habilidades e recursos necessários para desenvolver software a utilizar componentes de código aberto, línguas, tecnologias.
- Habilidades e recursos necessários para utilizar métodos e processos de código aberto.

### Avaliação de oportunidades

Ferramentas de código aberto são cada vez mais populares entre criadores de software. Esta atividade aborda a necessidade de evitar a proliferação de ferramentas heterogéneas numa equipa de desenvolvimento. Ajuda a definir uma política neste domínio. Ajuda a otimizar a formação e a construção de experiência. Um inventário de competências é utilizado para recrutamento, formação e planeamento de sucessão no caso de um funcionário-chave abandonar a empresa.

Precisaríamos de uma metodologia para mapear habilidades de desenvolvimento de software de código aberto.

### Avaliação do progresso

Os seguintes **pontos de verificação** demonstram o progresso nesta Atividade:

- [ ] Existe uma descrição da cadeia de produção de código aberto (a "cadeia logística de software"),
- [ ] Existe um plano (ou uma lista de desejos) para a racionalização dos recursos de desenvolvimento,
- [ ] Existe um inventário de habilidades que resume as competências, educação e experiência dos criadores atuais,
- [ ] Existe uma lista de desejos de formação e um programa que trata das lacunas de habilidades,
- [ ] Há uma lista de práticas recomendadas de desenvolvimento de código aberto em falta e um plano para as aplicar.

### Recomendações

- Comece de forma simples, faça a análise e o roteiro crescer consistentemente.
- Ao recrutar, dê uma forte ênfase às habilidades e experiência de código aberto. É sempre mais fácil quando as pessoas já têm conhecimento de código aberto do que treinar pessoas.
- Verifique programas de formação de vendedores de software e escolas de código aberto.

### Recursos

Mais informações:

- Uma introdução a [o que é um Inventário de Habilidades? (what is a Skills Inventory?)](https://managementisajourney.com/management-toolbox-better-decision-making-with-a-skills-inventory) de Robert Tanner.
- Um artigo sobre habilidades de código aberto: [5 Open Source Skills to Up Your Game and Your Resume](https://sourceforge.net/blog/5-open-source-skills-game-resume/)

Esta atividade pode incluir recursos e habilidades técnicas como, por exemplo:

- **Linguagens populares** (tais como Java, PHP, Perl, Python).
- **Frameworks de código aberto** (Spring, AngularJS, Symfony) e ferramentas de teste.
- **Métodos de desenvolvimento e práticas recomendadas** de Agile, DevOps e código aberto.

### Novas atividades propostas

- [GGI-A-28 - Perspectiva de Recursos Humanos](https://ospo-alliance.org/ggi/activities/hr_perspective) Uma vez identificados internamente os recursos Open Source necessários à sensibilização nestas tecnologias, levar o departamento de Recursos Humanos a perceber o seu valor, para os existentes e novos funcionários.
