#Purpose
Define and describe activities before and after a new release of the GGI handbook. The lists also serve as todo lists to not forget about necessary steps. 


##Pre release
- Switch to the translations branch: `git switch translations`
- Run checkMarkdownContent.sh and correct.
- Run checkUrls.sh and correct.

##Post release
- Switch to the translations branch: `git switch translations`
- Update prevRelTag in diffTranslatedMds.sh.
- New image file requiring translation:
  - Create a new `po2...Img.sh` Bash script.
  - Create a new .pot file.
  - Add translator comments to the new .pot file.
- New .json file requiring translation:
  - Adapt `json2po.sh`
  - Adapt `po2json.sh`
  - Run `json2.po.sh`
  - Add translator comments to the new .pot file.
- New .tex file: 
  - Adapt `latex2pot.sh`
  - Run `latex2pot.sh`
  - Add translator comments to the new .pot file.
- Update .pot files with new or changed strings:
  - Run:
    - `json2po.sh`, if not done before.
    - `latexUpdate2po.sh`
    - `markdown2po.sh -o`
  - Order the strings as in the English original PDF.
  - Add translator comments to the .pot files.
