# Building the BoK

- Setup: `git clone https://gitlab.ow2.org/ggi/ggi.git` OR update later: `git pull`
- `git switch translations`

Please note that when an image is added, and if it contains text that needs to be translated, then some ~~magic~~ processing is required. We have a few scripts that rebuild the images with the proper translation during the build process, and these will need to be updated to take care of the new picture. In that case, it is mandatory to submit an issue and make sure that the translations will work safely.

## Option 1: With Docker

This option encapsulates all the required packages, but also those you might not need for your language, into a disposable Docker container. It is independent from the OS it is run on, e.g. it does not have to be a Debian family (incl. Ubuntu, Mint) OS.

- Setup: install docker
- cd into `handbook/scripts/`
- execute `./convert_translation_docker.sh [lang]`, where [lang] is the language code and exists as a subfolder of `handbook/translations/`

## Option 2: Installed on the host

### Pre-requisites

The following packages are required to build the BoK on the host. The commands provided are for Debian family systems, but should be available on any modern *nix system, possibly through similar package names.

- `xelatex` to build the PDF. On Debian you can install `texlive-xetex`.
- `pandoc` (version >=  2.x): `apt install pandoc`
- `python3`
- `python3 pip` module: `apt install python3-pip`
- `translate-toolkit` module: `apt install translate-toolkit`
- `po4a (po for all)` module: `apt install po4a`

Some specific language-related tooling may be necessary, for example `texlive-lang-german` for German hyphenation.

For translation-specific requirements, please see also the TRANSLATING file in the root directory.

### Building the main version

To build the handbook from the markdown files stored in the content directory, simply go to the `handbook/` directory and execute:

```
$ bash scripts/convert_handbook.sh -w web_out/
```

It will create a virtual environment, install all required dependencies, produce the PDF version of the handbook (with nice pdf features like links and toc) and optionally (`-w|--write-web=path`) produce the HTML export in the specified directory. 

### Building the paperback version

To build the *paperback* version of the handbook from the markdown files stored in the content directory, simply go to the `handbook/` directory and execute:

```
$ bash scripts/paperback_convert_handbook.sh
```

Note that this version is specificly designed for *printed* versions of the handbook. 

### Building the translations

The content files used for the translations of the handbook are stored in `translations/[lang]/`.
To build the handbook for a specific language, execute the build script with the `-l|--language [LANG]` option:

```
$ bash scripts/convert_handbook.sh -l de
```

This will build the handbook pdf in the language-specific directory, adding the language suffix at the end of the pdf name.
Use `--move-pdf YES` of `-m YES` to automatically move the generated file in the `handbook` directory.

### Scripts additional options

If you want to automatically move the generated pdf in the same directory than the main one, you can specify the `-m YES` option, it will work with both paperback and main version.

```
$ bash scripts/convert_handbook.sh -w web_out/ -m YES
```
