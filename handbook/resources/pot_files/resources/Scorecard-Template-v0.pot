#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-07-25 13:32+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 3.7.0\n"

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[0]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "Goal/Activity"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[0]/table:table-cell[0]/text:p[0]/text:span[2]
msgid "Culture 1"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[0]/table:table-cell[1]/text:p[0]
msgid "Promote open source development best practices "
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[0]/table:table-cell[2]/text:p[0]/text:span[0]
msgid "Last update "
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "Customized Description"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[0]/text:p[0]/text:span[1]
msgid "Scope of what has to be done"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[0]/text:p[1]
msgid "Brief essential description..."
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Brief highlights.."
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[1]/text:p[0]/text:span[0]
msgid "Opportunity Assessment"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[1]/text:p[0]/text:span[1]
msgid "Why is this activity relevant"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[1]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Key pain points..."
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[1]/table:table-cell[1]/text:list[0]/text:list-item[1]/text:p[0]
msgid "Key progress opportunities..."
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "Objectives "
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[0]/text:p[0]/text:span[1]
msgid "What we aim to achieve in this iteration"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Objective 1..."
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[0]/text:list[0]/text:list-item[1]/text:p[0]
msgid "Objective 2..."
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[1]/text:p[0]/text:span[0]
msgid "Tools"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[1]/text:p[0]/text:span[2]
msgid "Technologies, "
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[1]/text:p[0]/text:span[3]
msgid "tools and products used in the Activity"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[1]/text:list[0]/text:list-item[0]/text:p[0]/text:a[0]/text:span[0]
msgid "Resources"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[1]/text:list[0]/text:list-item[0]/text:p[0]/text:span[0]
msgid "..."
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[2]/text:p[0]/text:span[0]
msgid "Operational Notes"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[2]/text:p[0]/text:span[3]
msgid "Approach, method to progress in the Activity"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[2]/table:table-cell[2]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Start with..."
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[3]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "Key Result "
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[3]/table:table-cell[0]/text:p[0]/text:span[1]
msgid "How we will measure success in this iteration"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[3]/table:table-cell[1]/text:p[0]
msgid "Progress"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[3]/table:table-cell[2]/text:p[0]
msgid "Score"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[3]/table:table-cell[3]/text:p[0]
msgid "Personal Assessment"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Key result 1 (minimum one key result)"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[1]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[1]/text:p[0]"
"office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[1]/text:p[0]"
"office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[1]/text:p[0]"
msgid "xx%"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[2]/text:p[0]
msgid ".9"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[3]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[3]/text:p[0]"
"office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[3]/text:p[0]"
"office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[4]/table:table-cell[3]/text:p[0]"
msgid "Personal comment"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Key result 2"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[1]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[1]/text:p[0]"
msgid "xx%"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[2]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[2]/text:p[0]"
msgid ".5"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[3]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[5]/table:table-cell[3]/text:p[0]"
msgid "Personal comment"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Key result 3"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[1]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[1]/text:p[0]"
msgid "xx%"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[2]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[2]/text:p[0]"
msgid ".5"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[3]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[6]/table:table-cell[3]/text:p[0]"
msgid "Personal comment"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[7]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Key result 4 (maximum four key results)"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[7]/table:table-cell[1]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[7]/table:table-cell[1]/text:p[0]"
msgid "xx%"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[7]/table:table-cell[2]/text:p[0]
msgid ".0"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[7]/table:table-cell[3]/text:p[0]
msgctxt "office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[7]/table:table-cell[3]/text:p[0]"
msgid "Personal comment"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[8]/table:table-cell[2]/text:p[0]
msgid ".475"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "Timeline"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[0]/text:p[0]/text:span[3]
msgid "Start-End dates, Milestones"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Date indication here"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[1]/text:p[0]/text:span[0]
msgid "Efforts"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[1]/text:p[0]/text:span[2]
msgid "Time and material budget"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[1]/text:list[0]/text:list-item[0]/text:p[0]
msgid "Time allocation over the next three months"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[1]/text:list[0]/text:list-item[1]/text:p[0]
msgid "Budget allowance"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[2]/text:p[0]/text:span[0]
msgid "Assignees"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[2]/text:p[0]/text:span[2]
msgid "Who participates? Leads?"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[9]/table:table-cell[2]/text:list[0]/text:list-item[0]/text:p[0]
msgid "XX to prepare internal presentation"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "Issues"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[0]/text:p[0]/text:span[2]
msgid ""
"Difficulties, uncertainties, roadblocks, points of attention, dependencies"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[0]/text:list[0]/text:list-item[0]/text:p[0]/text:span[0]
msgid "Concern 1..."
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[0]/text:list[0]/text:list-item[1]/text:p[0]
msgid "Concern 2..."
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[1]/text:p[0]/text:span[0]
msgid "Status"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[1]/text:p[0]/text:span[1]
msgid "How the Activity is doing"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[10]/table:table-cell[1]/text:list[0]/text:list-header[0]/text:p[0]
msgid "Personal comment on the health of the Activity"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[11]/table:table-cell[0]/text:p[0]
msgid "Overall Progress Rating"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[11]/table:table-cell[1]/text:p[0]
msgid "XX%"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[0]/table:table-row[12]/table:table-cell[0]/text:p[0]
msgid "Notes"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[1]/table:table-row[0]/table:table-cell[0]/text:p[0]
msgid "Insights from the GitLab Activity Forum"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[1]/table:table-row[1]/table:table-cell[0]/text:p[0]
msgid "https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/25"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[1]/table:table-row[1]/table:table-cell[0]/text:p[2]
msgid ""
"Copy/paste here the content of the Activity description from https://gitlab."
"ow2.org/ggi/ggi-castalia/"
msgstr ""

#: office:document-content[0]/office:body[0]/office:text[0]/table:table[1]/table:table-row[1]/table:table-cell[0]/text:p[4]
msgid ""
"This will serve as a reference to help develop the Customized Activity "
"Scorecard"
msgstr ""

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:header[0]/table:table[0]/table:table-row[0]/table:table-cell[0]/text:p[0]
msgid "OW2 OSS Good Governance initiative"
msgstr ""

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:header[0]/table:table[0]/table:table-row[0]/table:table-cell[1]/text:p[0]
msgid "THE GOOD EXAMPLE COMPANY"
msgstr ""

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:header[0]/table:table[0]/table:table-row[0]/table:table-cell[2]/text:p[0]
msgid "Customized Activity Scorecard"
msgstr ""

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:footer[0]/table:table[0]/table:table-row[0]/table:table-cell[0]/text:p[0]/text:span[0]
msgid "File: "
msgstr ""

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:footer[0]/table:table[0]/table:table-row[0]/table:table-cell[1]/text:p[0]/text:span[0]
msgid "Last revision:"
msgstr ""

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:footer[0]/table:table[0]/table:table-row[0]/table:table-cell[2]/text:p[0]/text:span[0]
msgid "By:"
msgstr ""

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:footer[0]/table:table[0]/table:table-row[0]/table:table-cell[3]/text:p[0]/text:span[0]
msgid "Page: "
msgstr ""

#: office:document-styles[0]/office:master-styles[0]/style:master-page[0]/style:footer[0]/table:table[0]/table:table-row[0]/table:table-cell[3]/text:p[0]/text:span[1]
msgid "/"
msgstr ""
