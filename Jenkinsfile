pipeline {
    agent any

    stages {
        stage('Check links and resources') {
            steps {
              sh '''
cd handbook/ && (sh scripts/checkUrls.sh || echo "There are broken links.")
              '''
            }
        }
        stage('Lint markdown') {
            steps {
              sh '''
cd handbook/ && (mdl --rules "~MD004","~MD007","~MD026","~MD036","~MD013","~MD002","~MD029","~MD033","~MD027" content/* || echo "There are linter violations.")
              '''
            }
        }
        stage('Build PDF') {
            steps {
                echo 'Build PDF..'
                sh '''
cd handbook/ && bash scripts/convert_handbook.sh -w ../ospozone/ggi/
                '''
            }
        }
        stage('Build translations') {
            steps {
                sh '''
set +e
rm -f handbook/translations/*/ggi_handbook*.pdf
cd handbook/scripts/
for i in `ls ../translations/`; do
  echo "Build PDF for ${i%%/}.."
  bash convert_translation_docker.sh ${i%%/}
  EXIT_CODE=$?
  echo "Exit code is $EXIT_CODE."
  if [ $EXIT_CODE -eq 1 ]; then
     echo "#######################################"
     echo "# Incomplete PDF build for translation $i"
     echo "#######################################"
  fi
  mv ../translations/${i%%/}/ggi_handbook_${i%%/}.pdf ../ggi_handbook_${i%%/}.pdf || echo 'Nothing to move.'
done                
                '''
            }
        }
        stage('Deploy') {
            steps {
                echo 'Archiving.'
                archiveArtifacts artifacts: 'handbook/*.pdf', fingerprint: true
                archiveArtifacts artifacts: 'ospozone/**/*.md', fingerprint: true
                echo 'Deploying.'
                sh '''
cp -r handbook/ggi_handbook.pdf /var/www/html/
                '''
            }
        }
    }
}
